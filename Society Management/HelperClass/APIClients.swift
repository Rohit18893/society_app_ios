//  APIClient.swift
//  Created by Jitendra Yadav on 13/08/19.
//  Copyright © 2019 JItendra Yadav. All rights reserved.


import UIKit
import SVProgressHUD
import Alamofire


//MARK:- Public

#if DEBUG
var isLogEnabled: Bool = true
#else
var isLogEnabled: Bool = false
#endif

//MARK:- Private
public func PrintLog <T> (_ object: T) {
    if isLogEnabled {
        NSLog("\(object)")
    }
}


let imageURL = "http://18.189.73.170/admin/"
struct imagesUpload {
    var key:String = ""
    var image:UIImage
    var fileName:String = ""
}

enum API: String {
    
    case signup = "signup"
    case login = "login"
    case mobileLogin = "mobile_login"
    case otpverification = "verify_otp"
    case resendotp = "resend_otp"
    case forgotpassword = "forgotpassword"
    case changepassword = "changepassword"
    case resetPassword = "resetPassword"
    case logout = "logout"
    case city = "city"
    case tower = "tower"
    case unitnumber = "unitnumber"
    case myprofile = "myprofile"
    case profileupdate = "profileupdate"
    case servicecharge = "servicecharge"
    case localservices = "local_services_list"
    case emergencycontacts =  "emergency_contacts"
    case connectresidence =  "connect_residence"
    case floors =  "buildings"
    case residentlist =  "buildings_list"
    case managementcommitee =  "management_commitee"
    case servicepeople =  "service_people_list"
    case peopledetails = "service_people"
    case removepeople = "delete_service_people"
    case helpdesktype = "helpdesk_type_list"
    case helpcategory = "help_category_list"
    case addtohousehold = "add_service_people"
    case helpfilter = "helpdesk_filter"
    case noticetype = "noticeboard_type_list"
    case noticeboardlist = "noticeboard_list"
    case noticeboarddetail = "noticeboard"
    case raisecomplaint = "help_complaint"
    case timeslot = "free_time_slot"
    case complaintlist = "help_complaint_list"
    case helpComment =   "help_complaint_comment_list"
    case addhelpComment = "help_complaint_comment"
    case helpdetails = "help_complaint_details"
    case notificationList = "notification_list"
    
    
    case kidsexit = "kids_exit"
    case frequentlyentrycategory = "frequently_entry_category"
    case futureentry = "frequently_entry"
    case addcompany = "add_frequently_entry_category"
    case msgtoguard = "msg_guard"
    case helpresolved = "help_resolved"
    case verifyMobile = "verify_mobile"
    case requestVisit = "request_visit"
    case recentGuest = "frequently_guest_list"
    case visitingHelp = "visiting_help_list"
    case inviteguests = "frequently_guest"
    case addpeoplesecurity = "security_alert_people"
    case securityContacts = "security_alert_people_list"
    case deletesecuritycontact = "security_alert_people_delete"
    case raiseAlarm = "security_alert"
    case noticeboardcount = "noticeboard_count"
    case addhelpRating = "help_rating"
    case visitcodelist = "visit_code_list"
    
    
    case editrating = "edit_rating"
    case updaterating = "update_rating"
    case rating = "rating"
    case getentry = "get_entry"
    
    case activitiesCategory = "activities_category"
    case activitiesDetails = "activities_details"
    case facilitiesDetails = "facilities_details"
    case addvilla = "addvilla"
    case villalist = "villalist"
    case deletevilla = "deletevilla"
    case editvilla = "editvilla"
    case documentType = "document_type"
    case addreminder = "scan_reminder"
    case myReminder = "scan_reminder_list"
    case deleteReminder = "delete_scan_reminder"
    case editReminder = "edit_scan_reminder"
    case homeNoticeBoard = "home"
    case userBookings = "my_booking"
    case facalityBooking = "facility_booking"
    case activityBooking = "activity_booking"
    case notification_setting = "notification_setting_list"
    case update_notification_setting = "update_notification_setting"
    case transactionHistory = "transaction_history"
    
    
    case surveyList = "survey_list"
    case surveyQuestions = "survey_question_list"
    case surveyaddAnswrs = "survey_user"
    case createPayment = "create_payment"
    case successPayment = "success_payment"
    
    // >>>>>>>>> House Hold API
    
    case householdlist = "household_list"
    case addFamilyMember = "add_family_member"
    case editKid = "edit_family_member"
    case addVehical = "add_vehical"
    case editVehical = "edit_vehical"
    case deleteFamilyMember = "delete_family_member"
    case deleteVehical = "delete_vehical"
    case reviewall = "review_all"
    case deleteguest = "delete_guest"
    case editGuest       = "edit_guest"
    case deleteKid       = "cancel_kids_log"
    
    // >>> Communication API
    
    case communicationtype      = "communication_type"
    case communicationStart     = "communication_start_discussion"
    case discussionList         = "communication_discussion_list"
    case communicationHide      = "communication_hide"
    case communicationShow      = "communication_unhide"
    case communicationHideList  = "communication_hide_list"
    case DiscussionDetails      = "communication_discussion_details"
    case addCommnet_reply       = "communication_add_comment"
    
    
    // >>> Recent Activity API
    case recentActivity = "recent_activity"
    case recentActivityCanceled = "cancel_frequently_entry"
    case acceptDecline       = "accept_decline"
    case visitorinfo = "visitorinfo"
    case frequently_entry_logs = "frequently_entry_logs"
    
}



class APIClient: NSObject {
    
    
     var BaseUrl =  "http://3.17.197.30:4000/"
    
    
    
    enum APPError: Error {
        case networkError(Error)
        case dataNotFound
        case jsonParsingError(Error)
        case invalidStatusCode(Int)
    }
    //Result enum to show success or failure
    enum Result<T> {
        case success(T)
        case failure(APPError)
    }
    /********************************
     Method: GetdataRequest is the method name, Call this method.
     ApiName: Server url/End point
     loader: Bool value for showing activity loader while getting data from server
     block: Will reture data for success and error if request is not satisfield.
     GetdataRequest which sends request to given URL and convert to Decodable Object
     ** This method is strictly written for this class only | Should not be called from outer classes **
     *********************************/
    
    fileprivate func DismissLoader(ISloader:Bool){
        if ISloader {
            SVProgressHUD.dismiss ()
        }
    }
    
    func GetdataRequest<T: Decodable>(WithApiName url: String,isNeedLoader ISloader: Bool, objectType: T.Type, completion: @escaping (Result<T>,_ status:HTTPURLResponse) -> Void) {
        if ISloader {
            
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.custom)
        }
        
        let MCApiName = BaseUrl+url
        PrintLog(MCApiName)
        
        guard let dataURL = URL(string: MCApiName) else { return }
        let session = URLSession.shared
        var request = URLRequest(url: dataURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Double.infinity)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Basic " + AuthorizationID, forHTTPHeaderField:"Authorization")
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        request.setValue(deviceToken, forHTTPHeaderField: "device_token")
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if  let  httpResponseT = response as? HTTPURLResponse {
                PrintLog(httpResponseT.statusCode)
                
                guard error == nil else {
                    completion(Result.failure(APPError.networkError(error!)), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                    return
                }
                guard let data = data else {
                    completion(Result.failure(APPError.dataNotFound), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                    return
                }
                do {
                    let decodedObject = try JSONDecoder().decode(objectType.self, from: data)
                    completion(Result.success(decodedObject), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                    
                } catch let error {
                    completion(Result.failure(APPError.jsonParsingError(error as! DecodingError)), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                }
            }
            self.DismissLoader(ISloader: ISloader)
        })
        task.resume()
    }
    

    
    
    /********************************
     Method: PostdataRequest is the method name, Call this method.
     Params: Dictionary[String:Any], pass dictionary and cannot be nil
     ApiName: Server url/End point
     loader: Bool value for showing activity loader while getting data from server
     block: Will reture data for success and error if request is not satisfield.
     PostdataRequest which sends request to given URL and convert to Decodable Object
     ** This method is strictly written for this class only | Should not be called from outer classes **
     ** Using URLSession
     *********************************/
    
    
    func PostdataRequest<T: Decodable>(WithApiName url: String,Params:[String:Any],isNeedLoader ISloader: Bool, objectType: T.Type, completion: @escaping (Result<T>,_ status:HTTPURLResponse) -> Void) {
        
        if ISloader {
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.custom)
        }
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: Params)
        let MCApiName = BaseUrl+url
        PrintLog(MCApiName)
        let session = URLSession.shared
        guard let dataURL = URL(string: MCApiName) else { return }
        var request = URLRequest(url: dataURL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Double.infinity)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Basic " + AuthorizationID, forHTTPHeaderField:"Authorization")
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        request.setValue(deviceToken, forHTTPHeaderField: "device_token")
        
       
        
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            if  let  httpResponseT = response as? HTTPURLResponse {
                PrintLog(httpResponseT.statusCode)
                guard error == nil else {
                    completion(Result.failure(APPError.networkError(error!)), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                    return
                }
                guard let data = data else {
                    completion(Result.failure(APPError.dataNotFound), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                    return
                }
                do {
                    
                    let decodedObject = try JSONDecoder().decode(objectType.self, from: data)
                    completion(Result.success(decodedObject), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                    
                    
                } catch let error {
                    completion(Result.failure(APPError.jsonParsingError(error as! DecodingError)), httpResponseT)
                    self.DismissLoader(ISloader: ISloader)
                }
            }
            self.DismissLoader(ISloader: ISloader)
        })
        task.resume()
    }
    
    func UploadImageWithParametersRequest<T: Decodable>(WithApiName url: String,Params:[String:Any],Setimages images:[imagesUpload],isNeedLoader ISloader: Bool, objectType: T.Type, completion: @escaping (Result<T>,_ status:HTTPURLResponse) -> Void) {
        
        if ISloader {
            
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.custom)
        }
        
        let MCApiName = BaseUrl+url
        PrintLog(MCApiName)
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
            "Authorization":"Basic " + AuthorizationID,
            "User-Agent":userAgent,
            "device_token":deviceToken
            
        ]
        
        AF.upload(multipartFormData: { multiPart in
            
            for img in images {
                if  let imageData = img.image.jpegData(compressionQuality: 0.5) {
                    print(img.key )
                    print(img.fileName )
                    multiPart.append( imageData , withName: img.key, fileName: img.fileName, mimeType: "image/jpg")
                }
            }
            for p in Params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            print(multiPart)
            
        }, to: MCApiName, method: .post, headers: headers) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
            
        }).responseJSON(completionHandler: { data in
            
            print("upload finished: \(data)")
            
        }).response { (response) in
            switch response.result {
            case .success( _):
                do {
                    if let data = response.data , let sCode = response.response {
                        let decodedObject = try JSONDecoder().decode(objectType.self, from: data)
                        completion(Result.success(decodedObject), sCode )
                        self.DismissLoader(ISloader: ISloader)
                    }
                } catch let error {
                    if let sCode = response.response {
                        completion(Result.failure(APPError.jsonParsingError(error as! DecodingError)), sCode )
                        self.DismissLoader(ISloader: ISloader)
                    }
                }
            case .failure(let err):
                print("upload err: \(err)")
            }
        }
        
    }
    
    
    
    func UploadMultipleImageWithParametersRequest<T: Decodable>(WithApiName url: String,Params:[String:Any],Setimages images:[imagesUpload],isNeedLoader ISloader: Bool, objectType: T.Type, completion: @escaping (Result<T>,_ status:HTTPURLResponse) -> Void) {
        
        if ISloader {
            
            SVProgressHUD.show(withStatus: "Please wait")
            SVProgressHUD.setDefaultMaskType(.custom)
        }
        
        let MCApiName = BaseUrl+url
        PrintLog(MCApiName)
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "Accept": "application/json",
            "Authorization":"Basic " + AuthorizationID,
            "User-Agent":userAgent,
            "device_token":deviceToken
        ]
        
        AF.upload(multipartFormData: { multiPart in
            
            for img in images {
                if  let imageData = img.image.jpegData(compressionQuality: 1.0) {
                    print(img.key )
                    print(img.fileName )
                    multiPart.append( imageData , withName: img.key, fileName: "\(img.fileName).jpg", mimeType: "image/jpg")
                }
            }
            for p in Params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            print(multiPart)
            
        }, to: MCApiName, method: .post, headers: headers) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
            
        }).responseJSON(completionHandler: { data in
            
            print("upload finished: \(data)")
            
        }).response { (response) in
            switch response.result {
            case .success( _):
                do {
                    if let data = response.data , let sCode = response.response {
                        let decodedObject = try JSONDecoder().decode(objectType.self, from: data)
                        completion(Result.success(decodedObject), sCode )
                        self.DismissLoader(ISloader: ISloader)
                    }
                } catch let error {
                    if let sCode = response.response {
                        completion(Result.failure(APPError.jsonParsingError(error as! DecodingError)), sCode )
                        self.DismissLoader(ISloader: ISloader)
                    }
                }
            case .failure(let err):
                print("upload err: \(err)")
            }
        }
        
    }
}
