//  notificationCenter.swift
//  Society Management
//  Created by Jitendra Yadav on 01/06/21.


import Foundation
import UIKit

extension Notification.Name
{
    static let householdReload = Notification.Name("householdReload")
    static let SecurityContactUpdated = Notification.Name("SecurityContactUpdated")
    static let inviteGuest = Notification.Name("inviteGuest")
    static let communicationRefrash = Notification.Name("communicationRefrash")
    static let requestLisRefrash = Notification.Name("requestLisRefrash")
    static let helpComplaintUpdated = Notification.Name("helpComplaintUpdated")
    static let ActivityImageChanged = Notification.Name("ActivityImageChanged")
}
