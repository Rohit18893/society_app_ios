

import Foundation
struct Added_data : Codable {
	let id : String?
	let name : String?
	let email : String?
	let number : String?
	let addrees : String?
	let image : String?
	let gender : String?
	let type : String?
	let lat : String?
	let long : String?
	let permission : String?
    let rfid : String?
    let rfcode : String?
    let tagid : String?
    
    let startDate : String?
    let endDate : String?
    
	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case number = "number"
		case addrees = "addrees"
		case image = "image"
		case gender = "gender"
		case type = "type"
		case lat = "lat"
		case long = "long"
		case permission = "permission"
        case rfid = "rfid"
        case rfcode = "rfcode"
        case tagid = "tagid"
        case startDate = "start_date"
        case endDate = "end_date"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		email = try values.decodeIfPresent(String.self, forKey: .email)
        number = try values.decodeIfPresent(String.self, forKey: .number)
		addrees = try values.decodeIfPresent(String.self, forKey: .addrees)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		type = try values.decodeIfPresent(String.self, forKey: .type)
		lat = try values.decodeIfPresent(String.self, forKey: .lat)
		long = try values.decodeIfPresent(String.self, forKey: .long)
		permission = try values.decodeIfPresent(String.self, forKey: .permission)
        rfid = try values.decodeIfPresent(String.self, forKey: .rfid)
        rfcode = try values.decodeIfPresent(String.self, forKey: .rfcode)
        tagid = try values.decodeIfPresent(String.self, forKey: .tagid)
        startDate = try values.decodeIfPresent(String.self, forKey: .startDate)
        endDate = try values.decodeIfPresent(String.self, forKey: .endDate)
	}
}
