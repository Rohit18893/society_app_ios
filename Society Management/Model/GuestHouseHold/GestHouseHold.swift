//
//  File.swift
//  Society Management
//
//  Created by Jitendra Yadav on 12/06/21.
//

import Foundation

struct guestDetail : Codable {
    let data : guestData?
    let msg : String?

    enum CodingKeys: String, CodingKey {
        case data = "data"
        case msg = "msg"
    }
}

struct guestData:Codable {
    let name : String?
    let validcode : String?
    let createdate : String?
    let enddate : String?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case validcode = "valid_code"
        case createdate = "createdate"
        case enddate = "enddate"
    }
}
