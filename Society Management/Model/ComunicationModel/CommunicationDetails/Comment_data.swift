/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Comment_data : Codable {
	let comment_id : String?
	let comment : String?
	let created_date : Int?
	let username : String?
	let towername : String?
	let unitnumber : String?
	let comment_images : [String]?
	let reply : String?
    let is_user : String?

	enum CodingKeys: String, CodingKey {

		case comment_id = "comment_id"
		case comment = "comment"
		case created_date = "created_date"
		case username = "username"
		case towername = "towername"
		case unitnumber = "unitnumber"
		case comment_images = "comment_images"
		case reply = "reply"
        case is_user = "is_user"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		comment_id = try values.decodeIfPresent(String.self, forKey: .comment_id)
		comment = try values.decodeIfPresent(String.self, forKey: .comment)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		towername = try values.decodeIfPresent(String.self, forKey: .towername)
		unitnumber = try values.decodeIfPresent(String.self, forKey: .unitnumber)
		comment_images = try values.decodeIfPresent([String].self, forKey: .comment_images)
		reply = try values.decodeIfPresent(String.self, forKey: .reply)
        is_user = try values.decodeIfPresent(String.self, forKey: .is_user)
	}

}
