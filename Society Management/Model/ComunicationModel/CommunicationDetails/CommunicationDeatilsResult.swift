/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct CommunicationDeatilsResult : Codable {
	let title : String?
	let description : String?
	let created_date : Int?
	let username : String?
	let towername : String?
	let unitnumber : String?
	let discussion_images : [String]?
	let comment_count : String?
	let group : String?
	let comment_data : [Comment_data]?
    let is_user : String?

	enum CodingKeys: String, CodingKey {

		case title = "title"
		case description = "description"
		case created_date = "created_date"
		case username = "username"
		case towername = "towername"
		case unitnumber = "unitnumber"
		case discussion_images = "discussion_images"
		case comment_count = "comment_count"
		case group = "group"
		case comment_data = "comment_data"
        case is_user = "is_user"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		towername = try values.decodeIfPresent(String.self, forKey: .towername)
		unitnumber = try values.decodeIfPresent(String.self, forKey: .unitnumber)
		discussion_images = try values.decodeIfPresent([String].self, forKey: .discussion_images)
		comment_count = try values.decodeIfPresent(String.self, forKey: .comment_count)
		group = try values.decodeIfPresent(String.self, forKey: .group)
		comment_data = try values.decodeIfPresent([Comment_data].self, forKey: .comment_data)
        is_user = try values.decodeIfPresent(String.self, forKey: .is_user)
	}

}
