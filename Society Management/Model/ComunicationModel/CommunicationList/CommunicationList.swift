/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct CommunicationList : Codable {
	let cid : String?
	let title : String?
	let comment_count : String?
	let description : String?
	let created_date : Int?
	let username : String?
    let approval_status : String?

	enum CodingKeys: String, CodingKey {

		case cid = "communication_id"
		case title = "title"
		case comment_count = "comment_count"
		case description = "description"
		case created_date = "created_date"
		case username = "username"
        case approval_status = "approval_status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        cid = try values.decodeIfPresent(String.self, forKey: .cid)
		title = try values.decodeIfPresent(String.self, forKey: .title)
        comment_count = try values.decodeIfPresent(String.self, forKey: .comment_count)
		description = try values.decodeIfPresent(String.self, forKey: .description)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
		username = try values.decodeIfPresent(String.self, forKey: .username)
        approval_status = try values.decodeIfPresent(String.self, forKey: .approval_status)
	}

}
