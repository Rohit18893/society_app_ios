//
//  SettingHeaderCell.swift
//  Society Gate keeper
//
//  Created by Jitendra Yadav on 15/04/21.
//

import UIKit

class SettingHeaderCell: UITableViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var NumberLbl: UILabel!
    @IBOutlet weak var EmailLbl: UILabel!
    @IBOutlet weak var twoWordLbl: UILabel!
    @IBOutlet weak var btnEditProfile: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
