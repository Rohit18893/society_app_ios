//
//  SettingCell.swift
//  Society Gate keeper
//
//  Created by Jitendra Yadav on 15/04/21.
//

import UIKit

class SettingCell: UITableViewCell {
    
    @IBOutlet weak var titileLbl: UILabel!
    @IBOutlet weak var icon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
