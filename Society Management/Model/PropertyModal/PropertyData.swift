/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct PropertyData : Codable {
	let villa_id : String?
	let towername : String?
	let unitname : String?
	let cityname : String?
    let towerid : String?
    let city_id : String?
    let unitnumber : String?
   

	enum CodingKeys: String, CodingKey
    {
		case villa_id = "villa_id"
		case towername = "towername"
		case unitname = "unitname"
		case cityname = "cityname"
        case towerid = "towerid"
        case city_id = "city_id"
        case unitnumber = "unitnumber"
	}

	init(from decoder: Decoder) throws {
        
		let values = try decoder.container(keyedBy: CodingKeys.self)
		villa_id = try values.decodeIfPresent(String.self, forKey: .villa_id)
		towername = try values.decodeIfPresent(String.self, forKey: .towername)
		unitname = try values.decodeIfPresent(String.self, forKey: .unitname)
		cityname = try values.decodeIfPresent(String.self, forKey: .cityname)
        towerid = try values.decodeIfPresent(String.self, forKey: .towerid)
        city_id = try values.decodeIfPresent(String.self, forKey: .city_id)
        unitnumber = try values.decodeIfPresent(String.self, forKey: .unitnumber)
        
	}

}
