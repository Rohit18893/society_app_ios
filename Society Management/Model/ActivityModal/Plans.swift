/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct Plans : Codable {
    
    
    let plan_id : Int?
	let plan_title : String?
	let plan_price : String?
	let plan_discount : String?
	let plan_description : String?

	enum CodingKeys: String, CodingKey {

		case plan_title = "plan_title"
		case plan_price = "plan_price"
		case plan_discount = "plan_discount"
		case plan_description = "plan_description"
        case plan_id = "plan_id"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		plan_title = try values.decodeIfPresent(String.self, forKey: .plan_title)
		plan_price = try values.decodeIfPresent(String.self, forKey: .plan_price)
		plan_discount = try values.decodeIfPresent(String.self, forKey: .plan_discount)
		plan_description = try values.decodeIfPresent(String.self, forKey: .plan_description)
        plan_id = try values.decodeIfPresent(Int.self, forKey: .plan_id)
	}

}
