/*
 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ActDetailData : Codable {
	let activities_id : String?
	let name : String?
	let titleone : String?
	let priceone : String?
	let discountone : String?
	let descriptionone : String?
	let time_slot : String?
	let details : String?
	let pricing : String?
	let rating : String?
	let service_status : String?
	let activities_images : [String]?
	let created_date : Int?
    let plans : [Plans]?

	enum CodingKeys: String, CodingKey {

		case activities_id = "activities_id"
		case name = "name"
		case titleone = "titleone"
		case priceone = "priceone"
		case discountone = "discountone"
		case descriptionone = "descriptionone"
		case time_slot = "time_slot"
		case details = "details"
		case pricing = "pricing"
		case rating = "rating"
		case service_status = "service_status"
		case activities_images = "activities_images"
		case created_date = "created_date"
        case plans = "plans"
	}

	init(from decoder: Decoder) throws {
        
		let values = try decoder.container(keyedBy: CodingKeys.self)
		activities_id = try values.decodeIfPresent(String.self, forKey: .activities_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		titleone = try values.decodeIfPresent(String.self, forKey: .titleone)
		priceone = try values.decodeIfPresent(String.self, forKey: .priceone)
		discountone = try values.decodeIfPresent(String.self, forKey: .discountone)
		descriptionone = try values.decodeIfPresent(String.self, forKey: .descriptionone)
		time_slot = try values.decodeIfPresent(String.self, forKey: .time_slot)
		details = try values.decodeIfPresent(String.self, forKey: .details)
		pricing = try values.decodeIfPresent(String.self, forKey: .pricing)
		rating = try values.decodeIfPresent(String.self, forKey: .rating)
		service_status = try values.decodeIfPresent(String.self, forKey: .service_status)
		activities_images = try values.decodeIfPresent([String].self, forKey: .activities_images)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
        plans = try values.decodeIfPresent([Plans].self, forKey: .plans)
	}

}
