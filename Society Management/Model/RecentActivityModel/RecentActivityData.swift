


import Foundation
struct RecentActivityData : Codable {
    let uid : String?
    let towerid : String?
    let unitnumber : String?
    let icon : String?
    let title : String?
    let tag : String?
    let valid_code : String?
    let slot_start : String?
    let slot_end : String?
    let service_name : String?
    let added_by : String?
    let accept_flag : String?
    let rating : String?
    let activity_id : String?
    let activity : String?
    let entry_id : String?
    let description : String?
    let company_name : String?
    let company_image : String?
    let lang_id : String?
    let status : String?
    let time_valid : String?
    let vehicle_number : String?
    let mobile : String?
    let entry_type : String?
    let other_id : String?

    enum CodingKeys: String, CodingKey {

        case uid = "uid"
        case towerid = "towerid"
        case unitnumber = "unitnumber"
        case icon = "icon"
        case title = "title"
        case tag = "tag"
        case valid_code = "valid_code"
        case slot_start = "slot_start"
        case slot_end = "slot_end"
        case service_name = "service_name"
        case added_by = "added_by"
        case accept_flag = "accept_flag"
        case rating = "rating"
        case activity_id = "activity_id"
        case activity = "activity"
        case entry_id = "entry_id"
        case description = "description"
        case company_name = "company_name"
        case company_image = "company_image"
        case lang_id = "lang_id"
        case status = "status"
        case time_valid = "time_valid"
        case vehicle_number = "vehicle_number"
        case mobile = "mobile"
        case entry_type = "entry_type"
        case other_id = "other_id"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        uid = try values.decodeIfPresent(String.self, forKey: .uid)
        towerid = try values.decodeIfPresent(String.self, forKey: .towerid)
        unitnumber = try values.decodeIfPresent(String.self, forKey: .unitnumber)
        icon = try values.decodeIfPresent(String.self, forKey: .icon)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        tag = try values.decodeIfPresent(String.self, forKey: .tag)
        valid_code = try values.decodeIfPresent(String.self, forKey: .valid_code)
        slot_start = try values.decodeIfPresent(String.self, forKey: .slot_start)
        slot_end = try values.decodeIfPresent(String.self, forKey: .slot_end)
        service_name = try values.decodeIfPresent(String.self, forKey: .service_name)
        added_by = try values.decodeIfPresent(String.self, forKey: .added_by)
        accept_flag = try values.decodeIfPresent(String.self, forKey: .accept_flag)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        activity_id = try values.decodeIfPresent(String.self, forKey: .activity_id)
        activity = try values.decodeIfPresent(String.self, forKey: .activity)
        entry_id = try values.decodeIfPresent(String.self, forKey: .entry_id)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        company_name = try values.decodeIfPresent(String.self, forKey: .company_name)
        company_image = try values.decodeIfPresent(String.self, forKey: .company_image)
        lang_id = try values.decodeIfPresent(String.self, forKey: .lang_id)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        time_valid = try values.decodeIfPresent(String.self, forKey: .time_valid)
        vehicle_number = try values.decodeIfPresent(String.self, forKey: .vehicle_number)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        entry_type = try values.decodeIfPresent(String.self, forKey: .entry_type)
        other_id = try values.decodeIfPresent(String.self, forKey: .other_id)
    }

}

