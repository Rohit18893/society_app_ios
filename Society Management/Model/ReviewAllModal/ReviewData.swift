/*
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ReviewData : Codable {
    
    let personname : String?
    let servicename : String?
    let image : String?
    let avgrating : String?
    let avgpunctual : String?
    let avgregular : String?
    let avgservice : String?
    let avgattitude : String?
    let reviewcount : String?
    let rating_count : String?
    let totalpunctual : String?
    let totalregular : String?
    let totalservice : String?
    let totalattitude : String?
    let rating_counter : [RatingCounter]?
    let reviewall : [Reviewall]?

    enum CodingKeys: String, CodingKey {

        case personname = "personname"
        case servicename = "servicename"
        case image = "image"
        case avgrating = "avgrating"
        case avgpunctual = "avgpunctual"
        case avgregular = "avgregular"
        case avgservice = "avgservice"
        case avgattitude = "avgattitude"
        case reviewcount = "reviewcount"
        case rating_count = "rating_count"
        case totalpunctual = "totalpunctual"
        case totalregular = "totalregular"
        case totalservice = "totalservice"
        case totalattitude = "totalattitude"
        case rating_counter = "rating_counter"
        case reviewall = "reviewall"
        
        

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        personname = try values.decodeIfPresent(String.self, forKey: .personname)
        servicename = try values.decodeIfPresent(String.self, forKey: .servicename)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        
        avgrating = try values.decodeIfPresent(String.self, forKey: .avgrating)
        avgpunctual = try values.decodeIfPresent(String.self, forKey: .avgpunctual)
        avgregular = try values.decodeIfPresent(String.self, forKey: .avgregular)
        avgservice = try values.decodeIfPresent(String.self, forKey: .avgservice)
        avgattitude = try values.decodeIfPresent(String.self, forKey: .avgattitude)
        reviewcount = try values.decodeIfPresent(String.self, forKey: .reviewcount)
        rating_count = try values.decodeIfPresent(String.self, forKey: .rating_count)
        totalpunctual = try values.decodeIfPresent(String.self, forKey: .totalpunctual)
        totalregular = try values.decodeIfPresent(String.self, forKey: .totalregular)
        totalservice = try values.decodeIfPresent(String.self, forKey: .totalservice)
        totalattitude = try values.decodeIfPresent(String.self, forKey: .totalattitude)
        rating_counter = try values.decodeIfPresent([RatingCounter].self, forKey: .rating_counter)
        reviewall = try values.decodeIfPresent([Reviewall].self, forKey: .reviewall)
    }

}
