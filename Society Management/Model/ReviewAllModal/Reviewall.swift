/*
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Reviewall : Codable {
    
    let username : String?
    let rating : Int?
    let review : String?
    let created_date : Int?
    let punctual : String?
    let service : String?
    let attitude : String?
    let regular : String?
    let is_my_review : String?

    enum CodingKeys: String, CodingKey {

        case username = "username"
        case rating = "rating"
        case review = "review"
        case created_date = "created_date"
        case punctual = "punctual"
        case service = "service"
        case attitude = "attitude"
        case regular = "regular"
        case is_my_review = "is_my_review"
        

        

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        rating = try values.decodeIfPresent(Int.self, forKey: .rating)
        review = try values.decodeIfPresent(String.self, forKey: .review)
        created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
        punctual = try values.decodeIfPresent(String.self, forKey: .punctual)
        service = try values.decodeIfPresent(String.self, forKey: .service)
        attitude = try values.decodeIfPresent(String.self, forKey: .attitude)
        regular = try values.decodeIfPresent(String.self, forKey: .regular)
        is_my_review = try values.decodeIfPresent(String.self, forKey: .is_my_review)
    }

}

