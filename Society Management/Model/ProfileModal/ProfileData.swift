/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation

struct ProfileData : Codable {
	let id : String?
	let fname : String?
	let lname : String?
	let email : String?
	let mobile : String?
	let city : String?
	let towerid : String?
	let unitnumber : String?
	let image : String?
	let gender : String?
	let country : String?
	let nationality : String?
    let cityname :String?
    let towername:String?
    let unitname:String?
           

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case fname = "fname"
		case lname = "lname"
		case email = "email"
		case mobile = "mobile"
		case city = "city"
		case towerid = "towerid"
		case unitnumber = "unitnumber"
		case image = "image"
		case gender = "gender"
		case country = "country"
		case nationality = "nationality"
        case cityname = "cityname"
        case towername = "towername"
        case unitname = "unitname"
        
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		fname = try values.decodeIfPresent(String.self, forKey: .fname)
		lname = try values.decodeIfPresent(String.self, forKey: .lname)
		email = try values.decodeIfPresent(String.self, forKey: .email)
		mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
		city = try values.decodeIfPresent(String.self, forKey: .city)
		towerid = try values.decodeIfPresent(String.self, forKey: .towerid)
		unitnumber = try values.decodeIfPresent(String.self, forKey: .unitnumber)
		image = try values.decodeIfPresent(String.self, forKey: .image)
		gender = try values.decodeIfPresent(String.self, forKey: .gender)
		country = try values.decodeIfPresent(String.self, forKey: .country)
		nationality = try values.decodeIfPresent(String.self, forKey: .nationality)
        cityname = try values.decodeIfPresent(String.self, forKey: .cityname)
        towername = try values.decodeIfPresent(String.self, forKey: .towername)
        unitname = try values.decodeIfPresent(String.self, forKey: .unitname)
	}

}
