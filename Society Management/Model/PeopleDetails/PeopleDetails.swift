/*
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct PeopleDetails : Codable {
    let name : String?
    let mobile : String?
    let image : String?
    let available_time : String?
    let towername : String?
    let rating : String?
    let rating_count : String?
    let review_count : String?
    let is_hired : String?
    let is_rating : String?
    let avgpunctual : String?
    let avgregular : String?
    let avgservice : String?
    let avgattitude : String?
    
    let totalpunctual : String?
    let totalregular : String?
    let totalservice : String?
    let totalattitude : String?
    let date : Int?
    let houses : [Houses]?
    let free_time : String?
    let inside : String?
    

    enum CodingKeys: String, CodingKey {

        case name = "name"
        case mobile = "mobile"
        case image = "image"
        case available_time = "available_time"
        case towername = "towername"
        case rating = "rating"
        case rating_count = "rating_count"
        case review_count = "review_count"
        case is_hired = "is_hired"
        case is_rating = "is_rating"
        case avgpunctual = "avgpunctual"
        case avgregular = "avgregular"
        case avgservice = "avgservice"
        case avgattitude = "avgattitude"
        case totalpunctual = "totalpunctual"
        case totalregular = "totalregular"
        case totalservice = "totalservice"
        case totalattitude = "totalattitude"
        case date = "date"
        case houses = "houses"
        case free_time = "free_time"
        case inside = "inside"

    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        mobile = try values.decodeIfPresent(String.self, forKey: .mobile)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        available_time = try values.decodeIfPresent(String.self, forKey: .available_time)
        towername = try values.decodeIfPresent(String.self, forKey: .towername)
        rating = try values.decodeIfPresent(String.self, forKey: .rating)
        rating_count = try values.decodeIfPresent(String.self, forKey: .rating_count)
        review_count = try values.decodeIfPresent(String.self, forKey: .review_count)
        is_hired = try values.decodeIfPresent(String.self, forKey: .is_hired)
        is_rating = try values.decodeIfPresent(String.self, forKey: .is_rating)
        avgpunctual = try values.decodeIfPresent(String.self, forKey: .avgpunctual)
        avgregular = try values.decodeIfPresent(String.self, forKey: .avgregular)
        avgservice = try values.decodeIfPresent(String.self, forKey: .avgservice)
        avgattitude = try values.decodeIfPresent(String.self, forKey: .avgattitude)
        totalpunctual = try values.decodeIfPresent(String.self, forKey: .totalpunctual)
        totalregular = try values.decodeIfPresent(String.self, forKey: .totalregular)
        totalservice = try values.decodeIfPresent(String.self, forKey: .totalservice)
        totalattitude = try values.decodeIfPresent(String.self, forKey: .totalattitude)
        date = try values.decodeIfPresent(Int.self, forKey: .date)
        houses = try values.decodeIfPresent([Houses].self, forKey: .houses)
        free_time = try values.decodeIfPresent(String.self, forKey: .free_time)
        inside = try values.decodeIfPresent(String.self, forKey: .inside)

    }

}

