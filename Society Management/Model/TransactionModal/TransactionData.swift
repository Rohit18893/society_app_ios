/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct TransactionData : Codable {
	let amount : String?
	let orderid : String?
	let invoiceid : String?
	let transactionid : String?
	let transactiontime : String?
	let card_type : String?
	let card_info : String?
	let paystatus : String?

	enum CodingKeys: String, CodingKey {

		case amount = "amount"
		case orderid = "orderid"
		case invoiceid = "invoiceid"
		case transactionid = "transactionid"
		case transactiontime = "transactiontime"
		case card_type = "card_type"
		case card_info = "card_info"
		case paystatus = "paystatus"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		amount = try values.decodeIfPresent(String.self, forKey: .amount)
		orderid = try values.decodeIfPresent(String.self, forKey: .orderid)
		invoiceid = try values.decodeIfPresent(String.self, forKey: .invoiceid)
		transactionid = try values.decodeIfPresent(String.self, forKey: .transactionid)
		transactiontime = try values.decodeIfPresent(String.self, forKey: .transactiontime)
		card_type = try values.decodeIfPresent(String.self, forKey: .card_type)
		card_info = try values.decodeIfPresent(String.self, forKey: .card_info)
		paystatus = try values.decodeIfPresent(String.self, forKey: .paystatus)
	}

}
