/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct HelpDData : Codable {
	let id : String?
	let title : String?
	let image : [String]?
	let created_date : Int?
	let help_category : String?
	let filter_by_status : String?
	let filter_by_type : String?
	let is_urgent : String?
	let username : String?
	let comment_list : [HelpComment]?
	let resolved_by : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case title = "title"
		case image = "image"
		case created_date = "created_date"
		case help_category = "help_category"
		case filter_by_status = "filter_by_status"
		case filter_by_type = "filter_by_type"
		case is_urgent = "is_urgent"
		case username = "username"
		case comment_list = "comment_list"
		case resolved_by = "resolved_by"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(String.self, forKey: .id)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		image = try values.decodeIfPresent([String].self, forKey: .image)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
		help_category = try values.decodeIfPresent(String.self, forKey: .help_category)
		filter_by_status = try values.decodeIfPresent(String.self, forKey: .filter_by_status)
		filter_by_type = try values.decodeIfPresent(String.self, forKey: .filter_by_type)
		is_urgent = try values.decodeIfPresent(String.self, forKey: .is_urgent)
		username = try values.decodeIfPresent(String.self, forKey: .username)
		comment_list = try values.decodeIfPresent([HelpComment].self, forKey: .comment_list)
		resolved_by = try values.decodeIfPresent(String.self, forKey: .resolved_by)
	}

}
