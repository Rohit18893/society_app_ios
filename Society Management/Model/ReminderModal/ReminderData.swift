/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct ReminderData : Codable {
	let document_id : String?
	let document_end_date : Double?
	let end_date : String?
	let document_number : String?
	let amount : String?
	let images : [String]?
	let created_date : Int?
	let document_name : String?

	enum CodingKeys: String, CodingKey {

		case document_id = "document_id"
		case document_end_date = "document_end_date"
		case end_date = "end_date"
		case document_number = "document_number"
		case amount = "amount"
		case images = "images"
		case created_date = "created_date"
		case document_name = "document_name"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		document_id = try values.decodeIfPresent(String.self, forKey: .document_id)
		document_end_date = try values.decodeIfPresent(Double.self, forKey: .document_end_date)
		end_date = try values.decodeIfPresent(String.self, forKey: .end_date)
		document_number = try values.decodeIfPresent(String.self, forKey: .document_number)
		amount = try values.decodeIfPresent(String.self, forKey: .amount)
		images = try values.decodeIfPresent([String].self, forKey: .images)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
		document_name = try values.decodeIfPresent(String.self, forKey: .document_name)
	}

}
