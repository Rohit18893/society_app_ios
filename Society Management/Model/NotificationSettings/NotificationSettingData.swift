/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct NotificationSettingData : Codable {
	let id : Int?
	let uid : Int?
	let entry_exit : Int?
	let daily_entry : Int?
	let daily_exit : Int?
	let guest_entry : Int?
	let guest_exit : Int?
	let frequent_entry : Int?
	let frequent_exit : Int?
	let help_desk : Int?
	let communication : Int?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case uid = "uid"
		case entry_exit = "entry_exit"
		case daily_entry = "daily_entry"
		case daily_exit = "daily_exit"
		case guest_entry = "guest_entry"
		case guest_exit = "guest_exit"
		case frequent_entry = "frequent_entry"
		case frequent_exit = "frequent_exit"
		case help_desk = "help_desk"
		case communication = "communication"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		id = try values.decodeIfPresent(Int.self, forKey: .id)
		uid = try values.decodeIfPresent(Int.self, forKey: .uid)
		entry_exit = try values.decodeIfPresent(Int.self, forKey: .entry_exit)
		daily_entry = try values.decodeIfPresent(Int.self, forKey: .daily_entry)
		daily_exit = try values.decodeIfPresent(Int.self, forKey: .daily_exit)
		guest_entry = try values.decodeIfPresent(Int.self, forKey: .guest_entry)
		guest_exit = try values.decodeIfPresent(Int.self, forKey: .guest_exit)
		frequent_entry = try values.decodeIfPresent(Int.self, forKey: .frequent_entry)
		frequent_exit = try values.decodeIfPresent(Int.self, forKey: .frequent_exit)
		help_desk = try values.decodeIfPresent(Int.self, forKey: .help_desk)
		communication = try values.decodeIfPresent(Int.self, forKey: .communication)
	}

}
