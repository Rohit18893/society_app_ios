/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct FacalityData : Codable {
	let facility_id : String?
	let name : String?
	let time_slot : String?
	let details : String?
	let catering : String?
	let decoration : String?
	let pricing : String?
	let service_type_name : String?
	let venue_type_name : String?
	let rating : String?
	let seating : [Seating]?
	let facilities_images : [String]?
	let created_date : Int?

	enum CodingKeys: String, CodingKey {

		case facility_id = "facility_id"
		case name = "name"
		case time_slot = "time_slot"
		case details = "details"
		case catering = "catering"
		case decoration = "decoration"
		case pricing = "pricing"
		case service_type_name = "service_type_name"
		case venue_type_name = "venue_type_name"
		case rating = "rating"
		case seating = "seating"
		case facilities_images = "facilities_images"
		case created_date = "created_date"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		facility_id = try values.decodeIfPresent(String.self, forKey: .facility_id)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		time_slot = try values.decodeIfPresent(String.self, forKey: .time_slot)
		details = try values.decodeIfPresent(String.self, forKey: .details)
		catering = try values.decodeIfPresent(String.self, forKey: .catering)
		decoration = try values.decodeIfPresent(String.self, forKey: .decoration)
		pricing = try values.decodeIfPresent(String.self, forKey: .pricing)
		service_type_name = try values.decodeIfPresent(String.self, forKey: .service_type_name)
		venue_type_name = try values.decodeIfPresent(String.self, forKey: .venue_type_name)
		rating = try values.decodeIfPresent(String.self, forKey: .rating)
		seating = try values.decodeIfPresent([Seating].self, forKey: .seating)
		facilities_images = try values.decodeIfPresent([String].self, forKey: .facilities_images)
		created_date = try values.decodeIfPresent(Int.self, forKey: .created_date)
	}

}
