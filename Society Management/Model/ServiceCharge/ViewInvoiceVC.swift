//  ViewInvoiceVC.swift
//  Society Management
//  Created by ROOP KISHOR on 06/09/2021.


import UIKit
import WebKit

class ViewInvoiceVC: MasterVC ,WKNavigationDelegate{
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var webView: WKWebView!

 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title32
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnOption.isHidden = false
        self.viewNAv.btnOption.setImage(UIImage(systemName: "dock.arrow.down.rectangle"), for: .normal)
        let url = URL(string: "https://in.pinterest.com/pin/303007881173228272/")
        let requestObj = URLRequest(url: url! as URL)
        webView.load(requestObj)
      
        
      
        
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
