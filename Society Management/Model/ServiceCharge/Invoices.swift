/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct Invoices : Codable {
	let title : String?
	let tag : String?
	let paid_status : String?
	let issue_date : Int?
	let duedate : Int?
	let amount : String?
    let invoice_number :String?
    let frequency :String?
    let from_date : Int?
    let to_date : Int?
    

	enum CodingKeys: String, CodingKey {

		case title = "title"
		case tag = "tag"
		case paid_status = "paystatus"
		case issue_date = "issue_date"
		case duedate = "duedate"
		case amount = "amount"
        case invoice_number = "invoice_number"
        case frequency = "frequency"
        case from_date = "from_date"
        case to_date = "to_date"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		tag = try values.decodeIfPresent(String.self, forKey: .tag)
		paid_status = try values.decodeIfPresent(String.self, forKey: .paid_status)
		issue_date = try values.decodeIfPresent(Int.self, forKey: .issue_date)
		duedate = try values.decodeIfPresent(Int.self, forKey: .duedate)
		amount = try values.decodeIfPresent(String.self, forKey: .amount)
        invoice_number = try values.decodeIfPresent(String.self, forKey: .invoice_number)
        frequency = try values.decodeIfPresent(String.self, forKey: .frequency)
        from_date = try values.decodeIfPresent(Int.self, forKey: .from_date)
        to_date = try values.decodeIfPresent(Int.self, forKey: .to_date)
	}

}
