/* 
Copyright (c) 2021 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct SericeChargeData : Codable {
	let dueamount : String?
	let deposite : String?
	let advance : String?
	let totalamount : String?
	let invoices : [Invoices]?
    let payment_charge : String?
    let extra_charge : String?
   

	enum CodingKeys: String, CodingKey {

		case dueamount = "dueamount"
		case deposite = "deposite"
		case advance = "advance"
		case totalamount = "totalamount"
		case invoices = "invoices"
        case payment_charge = "payment_charge"
        case extra_charge = "extra_charge"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		dueamount = try values.decodeIfPresent(String.self, forKey: .dueamount)
		deposite = try values.decodeIfPresent(String.self, forKey: .deposite)
		advance = try values.decodeIfPresent(String.self, forKey: .advance)
		totalamount = try values.decodeIfPresent(String.self, forKey: .totalamount)
		invoices = try values.decodeIfPresent([Invoices].self, forKey: .invoices)
        payment_charge = try values.decodeIfPresent(String.self, forKey: .payment_charge)
        extra_charge = try values.decodeIfPresent(String.self, forKey: .extra_charge)
	}

}
