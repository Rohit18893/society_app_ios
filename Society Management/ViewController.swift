//  ViewController.swift
//  Society Management
//  Created by ROOP KISHOR on 05/04/2021.


import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    override func viewDidLoad()
    {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.headerViewHeight.constant = HeaderHeight
        self.navigationController?.navigationBar.isHidden = true
    }

    
    @IBAction func englishButtonClicked(_ sender: UIButton)
    {
        
//        UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
//        UserDefaults.standard.synchronize()
//        UIView.appearance().semanticContentAttribute            = UISemanticContentAttribute.forceLeftToRight
//        Bundle.setLanguage("en")
//        UserDefaults.standard.set("1", forKey: "isFirstTime")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        nextViewController.isSelectLang = true
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle
    {
        return .lightContent
    }
}

