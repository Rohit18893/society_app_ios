//  HouseCell.swift
//  Society Management
//  Created by ROOP KISHOR on 07/06/2021.


import UIKit

class HouseCell: UITableViewCell {
    
    @IBOutlet weak var collectionHouse: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTotalHouse: UILabel!
    
    let sectionInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    let spacingBetweenCells: CGFloat = 0
    var allHouses : [Houses]? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
extension HouseCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return allHouses?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HouseCallCell", for: indexPath) as! HouseCallCell
        cell.backgroundColor = UIColor(named: AssetsColor.BackgroundColor.rawValue)
        cell.lblHouseName.text = self.allHouses?[indexPath.row].houses
       return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(screenWidth - 70)/2, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return spacingBetweenCells
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let phone = allHouses?[indexPath.row].usermobile
        if let url = URL(string: "tel://\(String(describing: phone))"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
      }
    
}
