//
//  ViewRatingProfileCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 13/06/21.
//

import UIKit
import GradientProgressBar

class ViewRatingProfileCell: UITableViewCell {
    
    @IBOutlet weak var fiveStarProgess: GradientProgressBar!
    @IBOutlet weak var fourStarProgess: GradientProgressBar!
    @IBOutlet weak var threeStarProgess: GradientProgressBar!
    @IBOutlet weak var twoStarProgess: GradientProgressBar!
    @IBOutlet weak var oneStarProgess: GradientProgressBar!
    
    @IBOutlet weak var fourStarRating: UILabel!
    @IBOutlet weak var oneStarRating: UILabel!
    @IBOutlet weak var twoStarRating: UILabel!
    @IBOutlet weak var threeStarRating: UILabel!
    @IBOutlet weak var fiveStarRating: UILabel!
    
    @IBOutlet weak var helperTotalReview: UILabel!
    @IBOutlet weak var helpetTotalRating: UILabel!
    @IBOutlet weak var helperRating: UILabel!
    
    @IBOutlet weak var helperType: UILabel!
    @IBOutlet weak var helperName: UILabel!
    @IBOutlet weak var helperProfile: Imageradius!
    
    var data:ReviewData? = nil {
        didSet {
            setData()
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        fiveStarProgess.backgroundColor = .lightGray
        fourStarProgess.backgroundColor = .lightGray
        threeStarProgess.backgroundColor = .lightGray
        twoStarProgess.backgroundColor = .lightGray
        oneStarProgess.backgroundColor = .lightGray
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData() {
        
        self.helperName.text = data?.personname?.capitalized
        self.helperType.text = data?.servicename?.capitalized
        self.helperTotalReview.text = "\(data?.reviewcount ?? "") RIVIEWS"
        self.helpetTotalRating.text = "\(data?.rating_count ?? "") RATINGS"
        self.helperRating.text = data?.avgrating
        
        //MARK: Helper Image
        if let img = data?.image {
            self.helperProfile.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "profile_icon"))
        }
        
        if data?.rating_counter?.count ?? 0 > 0 {
            if let fiveStar = data?.rating_counter?[4], let vl = fiveStar.counter?.toInt() {
                fiveStarRating.text = fiveStar.counter
                if vl > 0 {
                    fiveStarProgess.setProgress(1.0, animated: true)
                }else{
                    fiveStarProgess.setProgress(0.0, animated: true)
                }
                
            }
            
            if let fourStar = data?.rating_counter?[3], let vl = fourStar.counter?.toInt() {
                fourStarRating.text = fourStar.counter
                if vl > 0 {
                    fourStarProgess.setProgress(1.0, animated: true)
                }else{
                    fourStarProgess.setProgress(0.0, animated: true)
                }
                
            }
            
            if let threeStar = data?.rating_counter?[2], let vl = threeStar.counter?.toInt() {
                threeStarRating.text = threeStar.counter
                if vl > 0 {
                    threeStarProgess.setProgress(1.0, animated: true)
                }else{
                    threeStarProgess.setProgress(0.0, animated: true)
                }
            }
            
            if let twoStar = data?.rating_counter?[1], let vl = twoStar.counter?.toInt() {
                twoStarRating.text = twoStar.counter
                if vl > 0 {
                    twoStarProgess.setProgress(1.0, animated: true)
                }else{
                    twoStarProgess.setProgress(0.0, animated: true)
                }
            }
            
            if let oneStar = data?.rating_counter?[0], let vl = oneStar.counter?.toInt() {
                oneStarRating.text = oneStar.counter
                if vl > 0 {
                    oneStarProgess.setProgress(1.0, animated: true)
                }else{
                    oneStarProgess.setProgress(0.0, animated: true)
                }
                
            }
            
            fiveStarProgess.gradientColors =  [#colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),#colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1),#colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)]
            fourStarProgess.gradientColors =  [#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1),#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1),#colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)]
            threeStarProgess.gradientColors = [#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1),#colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)]
            twoStarProgess.gradientColors  =  [#colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1),#colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1),#colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1)]
            oneStarProgess.gradientColors  =  [#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1),#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1),#colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)]
        }else{
            fiveStarProgess.setProgress(0.0, animated: true)
            fourStarProgess.setProgress(0.0, animated: true)
            threeStarProgess.setProgress(0.0, animated: true)
            twoStarProgess.setProgress(0.0, animated: true)
            oneStarProgess.setProgress(0.0, animated: true)
        }
        
    }
    
}


extension String {
        //Converts String to Int
        public func toInt() -> Int? {
            if let num = NumberFormatter().number(from: self) {
                return num.intValue
            } else {
                return nil
            }
        }

        //Converts String to Double
        public func toDoublee() -> Double? {
            if let num = NumberFormatter().number(from: self) {
                return num.doubleValue
            } else {
                return nil
            }
        }

        /// EZSE: Converts String to Float
        public func toFloat() -> Float? {
            if let num = NumberFormatter().number(from: self) {
                return num.floatValue
            } else {
                return nil
            }
        }

        //Converts String to Bool
        public func toBool() -> Bool? {
            return (self as NSString).boolValue
        }
    }
