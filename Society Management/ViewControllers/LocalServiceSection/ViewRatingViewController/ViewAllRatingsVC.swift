//
//  ViewAllRatingsVC.swift
//  Society Management
//
//  Created by ROOP KISHOR on 08/06/2021.
//

import UIKit

class ViewAllRatingsVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    fileprivate let arrCells = [ViewRatingProfileCell.self,HelperProfileCell.self,HelperReviewCell.self]
    
    var id = String()
    var reviewAll:ReviewModel?
    var isSectionCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title31
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.getData(helperID: id, uid: userData?.id ?? "", lang_id: language, isNeedLoader: true)
        
        // Do any additional setup after loading the view.
    }
    
    func getData(helperID:String,uid:String,lang_id:String,isNeedLoader:Bool){
        let param = ["id":helperID,"uid":uid,"lang_id":lang_id]
        PrintLog(param)
        APIClient().PostdataRequest(WithApiName: API.reviewall.rawValue, Params: param, isNeedLoader: isNeedLoader, objectType: ReviewModel.self) { Response, status in
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        self.reviewAll = result
                        self.isSectionCount = 3
                        self.tblView.reloadData()
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
}

extension ViewAllRatingsVC: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return isSectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return reviewAll?.data?.reviewall?.count ?? 0
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tblView.dequeueReusableCell(with: ViewRatingProfileCell.self, for: indexPath)
            cell.data = reviewAll?.data
            return cell
        } else if indexPath.section == 1 {
            let cell = tblView.dequeueReusableCell(with: HelperProfileCell.self, for: indexPath)
            cell.setViewAllRatingView()
            //MARK: Rating View Data set ----------------
            
            if let totalpunctual = reviewAll?.data?.totalpunctual {
                cell.punchualLbl.text = totalpunctual.capitalized
            }
            
            if let avgpunctual = reviewAll?.data?.avgpunctual?.floatValue() {
                let vl = CGFloat(avgpunctual / 100)
                cell.progressView1.setProgress(progress: vl, animated: true)
            }
            
            if let totalregular = reviewAll?.data?.totalregular {
                cell.regular.text = totalregular.capitalized
            }
            
            if let avgregular = reviewAll?.data?.avgregular?.floatValue() {
                let vl = CGFloat(avgregular / 100)
                cell.progressView2.setProgress(progress: vl, animated: true)
            }
            
            if let totalservice = reviewAll?.data?.totalservice {
                cell.serviceLbl.text = totalservice
            }
            
            if let avgservice = reviewAll?.data?.avgservice?.floatValue() {
                let vl = CGFloat(avgservice / 100)
                cell.progressView3.setProgress(progress: vl, animated: true)
            }
            
            if let totalattitude = reviewAll?.data?.totalattitude {
                cell.attitudeLbl.text = totalattitude
            }
            
            if let avgattitude = reviewAll?.data?.avgattitude?.floatValue() {
                let vl = CGFloat(avgattitude / 100)
                cell.progressView4.setProgress(progress: vl, animated: true)
            }
            return cell
        } else{
            let cell = tblView.dequeueReusableCell(with: HelperReviewCell.self, for: indexPath)
            cell.reviewData = reviewAll?.data?.reviewall?[indexPath.row]
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
