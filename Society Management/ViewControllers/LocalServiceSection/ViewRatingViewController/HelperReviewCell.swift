//
//  HelperReviewCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 14/06/21.
//

import UIKit
import Cosmos

class HelperReviewCell: UITableViewCell {

    @IBOutlet weak var ratingStar: CosmosView!
    
    @IBOutlet weak var attitude: UIImageView!
    @IBOutlet weak var service: UIImageView!
    @IBOutlet weak var regular: UIImageView!
    @IBOutlet weak var punchual: UIImageView!
    
    @IBOutlet weak var reviewText: UILabel!
    
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var reviewDate: UILabel!
    @IBOutlet weak var userProfile: UIImageView!
    
    var reviewData:Reviewall? = nil {
        didSet {
            self.setData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        ratingStar.rating = 0
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(){
        if let rating = reviewData?.rating {
            ratingStar.rating = Double(rating)
        }
        
        if let comment = reviewData?.review {
            reviewText.text = comment
        }
        
        if let date = reviewData?.created_date {
            reviewDate.text = TimeStampToDate(timeStamp: date)
        }
        
        if let punchual = reviewData?.punctual , punchual == "1"{
            self.punchual.isHidden = false
        }else {
            self.punchual.isHidden = true
        }
        
        if let regular = reviewData?.regular , regular == "1"{
            self.regular.isHidden = false
        }else {
            self.regular.isHidden = true
        }
        
        if let service = reviewData?.service , service == "1"{
            self.service.isHidden = false
        }else {
            self.service.isHidden = true
        }
        
        if let attitude = reviewData?.attitude , attitude == "1"{
            self.attitude.isHidden = false
        }else {
            self.attitude.isHidden = true
        }
        
        if let isMyReview = reviewData?.is_my_review , isMyReview == "1"{
           // self.editProfileButton.isHidden = false
            self.userProfile.isHidden = false
        }else {
          //  self.editProfileButton.isHidden = true
            self.userProfile.isHidden = true
        }
    }

}

