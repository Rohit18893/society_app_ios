//  LocalServiceCell.swift
//  Society Management
//  Created by Jitendra Yadav on 22/04/21.


import UIKit

class LocalServiceCell: UITableViewCell {
    
    @IBOutlet weak var Lbl1: UILabel!
    @IBOutlet weak var Lbl2: UILabel!
     
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        
        Lbl1.adjustsFontSizeToFitWidth = true
        Lbl2.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
