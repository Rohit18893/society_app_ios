//  LocalServiceTypeCell.swift
//  Society Management
//  Created by Jitendra Yadav on 24/04/21.


import UIKit

class LocalServiceTypeCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblinside: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var img: UIImageView!{
        didSet {
            img.contentMode = .scaleToFill
            img.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            img.borderWidth = 1
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
