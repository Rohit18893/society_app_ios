//  LocalServiceTypeVC.swift
//  Society Management
//  Created by Jitendra Yadav on 24/04/21.


import UIKit

class LocalServiceTypeVC: MasterVC ,timeSlotControllerDelegate,UISearchBarDelegate{
    
    @IBOutlet weak var collectionSlots: UICollectionView!
    @IBOutlet weak var btnFreeSlots: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    fileprivate let arrCells = [LocalServiceTypeCell.self]
    @IBOutlet weak var btnInside: UIButton!
    @IBOutlet weak var noRecordFound: UILabel!
    var serviceType:LocalServiceData?
    var peopleData = [ServicePeopleData]()
    var isInside = String()
    var timeSlotFilter = NSMutableArray()
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
   

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.lblTitle.text = serviceType?.name
        viewHeader.isHidden  =  false
        viewSearch.isHidden  =  true
        searchBar.backgroundImage = UIImage()
        self.searchBar.searchTextField.backgroundColor = .clear
        self.searchBar.layer.cornerRadius = 5
        searchBar.delegate = self
        
        if let flowLayout = collectionSlots?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        isInside = "0"
        self.getPeopleData(arr: timeSlotFilter, strSearch:"")
        collectionSlots.isHidden = true
        btnFreeSlots.isHidden = false
        btnInside.isSelected = false
    }

    @IBAction func btnSearchClicked(_ sender: UIButton)
    {
        viewHeader.isHidden  =  true
        viewSearch.isHidden  =  false
        self.searchBar.becomeFirstResponder()
    }
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCrossClicked(_ sender: UIButton)
    {
        viewHeader.isHidden  =  false
        viewSearch.isHidden  =  true
        self.searchBar.endEditing(true)

    }
    
    @IBAction func timeSlotsClicked(_ sender: UIButton)
    {
        let vc = TimeSlotsVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    func getPeopleData(arr:NSMutableArray,strSearch:String) {
        
        let params = ["local_service_id":serviceType?.id! as Any,"lang_id":language,"free_time":arr,"inside":isInside,"towerid":userData?.towerid! ?? "","uid":userData?.id ?? "","search":strSearch] as [String : Any]
        self.peopleData.removeAll()
        APIClient().PostdataRequest(WithApiName: API.servicepeople.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ServicePeopleModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.peopleData = object.data!
                        if object.data?.count ?? 0 > 0
                        {
                            self.noRecordFound.isHidden = true
                            self.tblView.isHidden = false
                        }
                        else
                        {
                            self.noRecordFound.isHidden = false
                            self.tblView.isHidden = true
                        }
                        self.tblView.reloadData()
                       
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    
                    self.tblView.reloadData()
                    self.noRecordFound.isHidden = false
                    self.tblView.isHidden = true
                 
                }
                
            }
        }

    }
    
    func selecetedTimeSlots(withSlots: NSMutableArray) {
        
        timeSlotFilter = withSlots
        self.collectionSlots.isHidden = false
        btnFreeSlots.isHidden = true
        self.collectionSlots.reloadData()
        self.getPeopleData(arr: timeSlotFilter, strSearch: "")
        
    }
    
    @IBAction func insideButtunClicked(_ sender: UIButton)
    {
        if sender .isSelected
        {
            btnInside.setImage(UIImage(named: "check_box_unactive"), for: UIControl.State.normal)
            btnInside.isSelected = false
            isInside = "0"
            self.getPeopleData(arr: timeSlotFilter, strSearch: "")
            
        } else
        {
            btnInside.setImage(UIImage(named: "check_box_active"), for: UIControl.State.normal)
            btnInside.isSelected = true
            isInside = "1"
            self.getPeopleData(arr: timeSlotFilter, strSearch: "")
            
        }
        
        
    }
    
}

extension LocalServiceTypeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.peopleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocalServiceTypeCell", for: indexPath) as! LocalServiceTypeCell
        cell.lblName.text = peopleData[indexPath.row].name?.capitalized
        if peopleData[indexPath.row].inside == "1"
        {
            cell.lblinside.isHidden = false
            
        } else
        {
            cell.lblinside.isHidden = true
        }
        
        cell.lblRating.text = "\(String(describing: peopleData[indexPath.row].rating!))  \u{2022}  \(String(describing: peopleData[indexPath.row].houses!)) HOUSES"
        cell.img.sd_setImage(with: URL(string: peopleData[indexPath.row].image ?? ""), placeholderImage:#imageLiteral(resourceName: "profile_icon"))
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "ServiceTypeDetailsVC") as! ServiceTypeDetailsVC
        vc.selectedPeople = peopleData[indexPath.row]
        vc.strService = serviceType?.id ?? ""
        vc.strServiceName = serviceType?.name ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension LocalServiceTypeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return timeSlotFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionCell", for: indexPath) as! FilterCollectionCell
        cell.lblTime.text = timeSlotFilter[indexPath.row] as? String
        cell.btnClear.tag  = indexPath.row
        cell.btnClear.addTarget(self, action: #selector(self.btnRemoveFilterClicked), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 180, height: 35)
    }
    
    @objc func btnRemoveFilterClicked(_ sender: UIButton)
    {
        self.timeSlotFilter.removeObject(at: sender.tag)
        self.collectionSlots.reloadData()
        if timeSlotFilter.count > 0
        {
            self.getPeopleData(arr: timeSlotFilter, strSearch: "")
            
        } else
        {
            self.btnFreeSlots.isHidden = false
            self.collectionSlots.isHidden = true
            self.getPeopleData(arr: timeSlotFilter, strSearch: "")
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.count >= 4 {
            
            print("should be search")
            self.getPeopleData(arr: timeSlotFilter, strSearch:searchText)
            
        } else if searchText.count == 0 {
            
            self.getPeopleData(arr: timeSlotFilter, strSearch:"")
            
        }
       

    }

    
}
    
    
