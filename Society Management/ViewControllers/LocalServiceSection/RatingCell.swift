//
//  RatingCell.swift
//  Society Management
//
//  Created by ROOP KISHOR on 08/06/2021.
//

import UIKit

class RatingCell: UITableViewCell {
    
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblPanctual: UILabel!
    @IBOutlet weak var lblRegular: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblAttitude: UILabel!
    @IBOutlet weak var btnViewAll: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
