//  TimeSlotsVC.swift
//  Society Management
//  Created by ROOP KISHOR on 10/05/2021.


import UIKit

protocol timeSlotControllerDelegate
{
    func selecetedTimeSlots(withSlots:NSMutableArray)
}

class TimeSlotsVC: UIViewController ,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    @IBOutlet weak var viewSlots: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnFilter: UIButton!
    
    let sectionInsets = UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
    let numberOfItemsPerRow: CGFloat = 3
    let spacingBetweenCells: CGFloat = 15
    var slots = [String]()
    var selectedIndex = NSMutableArray()
    var delegate:timeSlotControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewSlots.clipsToBounds = true
        self.viewSlots.layer.cornerRadius = 25
        self.viewSlots.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        
        self.collectionView.register(UINib(nibName: "SlotsCell", bundle: nil), forCellWithReuseIdentifier: "SlotsCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.btnFilter.backgroundColor = UIColor.lightGray
        self.btnFilter.isUserInteractionEnabled = false
        
        self.getTimeSlots()
        
    }

    
    @IBAction func crossButtunClicked(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func buttonFilterClicked(_ sender: UIButton)
    {
        self.delegate?.selecetedTimeSlots(withSlots: selectedIndex)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // Configure the view for the selected state
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.slots.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlotsCell", for: indexPath as IndexPath) as? SlotsCell else { return UICollectionViewCell() }
        
        cell.lblName.text = slots[indexPath.row]
        cell.btnRadio.tag  = indexPath.row
        cell.btnRadio.addTarget(self, action: #selector(self.radioButtonClicked), for: .touchUpInside)
        return cell
        
    }
    
    @objc func radioButtonClicked(_ sender: UIButton)
    {
        let slotVal = slots[sender.tag]
        if selectedIndex.contains(slotVal) {
            
            selectedIndex.remove(slotVal)
            sender.setImage(UIImage(named: "radio_unactive"), for: .normal)
            
        } else
        {
            selectedIndex.add(slotVal)
            sender.setImage(UIImage(named: "radio_active"), for: .normal)
            
        }
        
        if selectedIndex.count > 0 {
            
            self.btnFilter.backgroundColor = DEFAULT_COLOR_RED
            self.btnFilter.isUserInteractionEnabled = true
            
        } else {
            
            self.btnFilter.backgroundColor = UIColor.lightGray
            self.btnFilter.isUserInteractionEnabled = false
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let totalSpacing = (2 * sectionInsets.left) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        if let collection = self.collectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: 40)
        } else {
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return spacingBetweenCells
        
    }
   
    
    func getTimeSlots() {
        
        let params = ["":""]
        APIClient().PostdataRequest(WithApiName: API.timeslot.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: TimeSlotModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.slots = (object.data)!
                        self.collectionView.reloadData()
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
               
            }
        }

    }

}
