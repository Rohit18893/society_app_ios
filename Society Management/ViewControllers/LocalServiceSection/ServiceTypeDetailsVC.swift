//  ServiceTypeDetailsVC.swift
//  Society Management
//  Created by Jitendra Yadav on 24/04/21.


import UIKit

class ServiceTypeDetailsVC: MasterVC {
    
   
    @IBOutlet weak var viewNAv: Navigationbar!
    
    fileprivate let arrCells = [HelperProfileCell.self,HelperFooterCell.self,LinkProfileCell.self]
    @IBOutlet weak var tblView: UITableView!
    
    var selectedPeople : ServicePeopleData?
    var allDetails : PeopleDetails?
    var strService = String()
    var strServiceName = String()
    var tableSection = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = "\(strServiceName.capitalized) Profile"
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        self.getPeopleDetails(isNeedLoader: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(houseHoldReload(notification:)), name: .householdReload, object: nil)
    }
    
    //MARK: House Hold Reload
    
    @objc func houseHoldReload(notification: NSNotification) {
        self.getPeopleDetails(isNeedLoader: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .householdReload, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    @IBAction func BackBtn(_ sender: UIButton)  {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addtoHouseHold(_ sender: UIButton) {
        if selectedPeople?.is_hired == "1" {
            self.removeFromHouseHold()
        }
        else {
            self.addToHouseHold()
        }
    }
    
    
    func getPeopleDetails(isNeedLoader:Bool) {
        
        let params = ["id":selectedPeople?.id,"lang_id":language,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.peopledetails.rawValue, Params: params as [String : Any], isNeedLoader: isNeedLoader, objectType: PeopleDetailModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(self)
                        PrintLog(object.data)
                        self.allDetails = object.data
                        self.selectedPeople?.is_hired  = object.data?.is_hired
                        self.tableSection = 5
                        self.tblView.reloadData()
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
            }
        }
        
    }
    
    func addToHouseHold() {
        
        let params = ["uid":userData?.id,"towerid":userData?.towerid,"unitnumber":userData?.unitnumber,"local_service_id":strService,"lang_id":language,"person_id":selectedPeople?.id]
        PrintLog(params)
        APIClient().PostdataRequest(WithApiName: API.addtohousehold.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(_):
                    DispatchQueue.main.async { [self] in
                        PrintLog(self)
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        //                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: ["OK"], actionstyle: [.default], action: [ {_ in  } ])
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{}
        }
        
    }
    
    func removeFromHouseHold() {
        
        let params = ["lang_id":language,"id":selectedPeople?.id,"uid":userData?.id]
        PrintLog(params)
        APIClient().PostdataRequest(WithApiName: API.removepeople.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(_):
                    DispatchQueue.main.async { [self] in
                        PrintLog(self)
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        //                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: ["OK"], actionstyle: [.default], action: [ {_ in } ])
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
            }
        }
        
    }
    
}

extension ServiceTypeDetailsVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableSection
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceTypeDetailsCell", for: indexPath) as! ServiceTypeDetailsCell
            cell.lblName.text = allDetails?.name
            cell.lblNumber.text = allDetails?.mobile
            cell.img.sd_setImage(with: URL(string: allDetails?.image ?? ""), placeholderImage:#imageLiteral(resourceName: "profile_icon"))
            cell.btnCall.addTarget(self, action: #selector(self.callMobileNumber), for: .touchUpInside)
            return cell
            
        }
        
        else if indexPath.section == 1
        {
            let cell = tblView.dequeueReusableCell(with: LinkProfileCell.self, for: indexPath)
            return cell
        }
        else if indexPath.section == 2 {
            
            let cell = tblView.dequeueReusableCell(with: HelperProfileCell.self, for: indexPath)
            cell.setLocalServiceViewAllRating()
            cell.rateNowButton.addTarget(self, action: #selector(rateNowMe), for: .touchUpInside)
            
            if selectedPeople?.is_hired == "1" {
                cell.notifyView.isHidden = false
                guard let isRateing = allDetails?.is_rating , isRateing != "0" else {
                    cell.helperRatingLbl.text = "0.0"
                    cell.rateNowButton.setTitle("RATE NOW", for: .normal)
                    cell.graphView.isHidden = true
                    cell.starRating.rating = 0
                    //MARK: Name
                    if let name = allDetails?.name {
                        cell.helperName.text = name.capitalized
                        cell.rateLbl.text = "Be the first one to rate \(name.capitalized) performance?"
                    }
                    return cell
                }
                cell.rateNowButton.setTitle("EDIT RATING", for: .normal)
                cell.graphView.isHidden = false
                
            }else {
                cell.notifyView.isHidden = true
                cell.rateNowButton.setTitle("VIEW ALL", for: .normal)
            }
            
            //MARK: Name
            if let ratingCount = allDetails?.rating_count,let reviewCount = allDetails?.review_count {
                cell.rateLbl.text = "   \(ratingCount.capitalized) Rating, \(reviewCount.capitalized) Review"
            }
            
            //MARK: Rating
            
            if let rating = allDetails?.rating {
                cell.helperRatingLbl.text = "\(rating.toDouble() ?? 0.0)"
                cell.starRating.rating = rating.toDouble() ?? 0.0
            }
            
            //MARK: Mobile
            if let mobile = allDetails?.mobile {
                cell.helperMobile.text = mobile.capitalized
            }
            
            //MARK: Helper Image
            if let img = allDetails?.image {
                cell.helperProfile.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "profile_icon"))
            }
            
            
            //MARK: Rating View Data set ----------------
            if let totalpunctual = allDetails?.totalpunctual {
                cell.punchualLbl.text = totalpunctual.capitalized
            }
            
            if let avgpunctual = allDetails?.avgpunctual?.floatValue() {
                let vl = CGFloat(avgpunctual / 100)
                cell.progressView1.setProgress(progress: vl, animated: true)
            }
            
            if let totalregular = allDetails?.totalregular {
                cell.regular.text = totalregular.capitalized
            }
            
            if let avgregular = allDetails?.avgregular?.floatValue() {
                let vl = CGFloat(avgregular / 100)
                cell.progressView2.setProgress(progress: vl, animated: true)
            }
            
            if let totalservice = allDetails?.totalservice {
                cell.serviceLbl.text = totalservice
            }
            
            if let avgservice = allDetails?.avgservice?.floatValue() {
                let vl = CGFloat(avgservice / 100)
                cell.progressView3.setProgress(progress: vl, animated: true)
            }
            
            if let totalattitude = allDetails?.totalattitude {
                cell.attitudeLbl.text = totalattitude
            }
            
            if let avgattitude = allDetails?.avgattitude?.floatValue() {
                let vl = CGFloat(avgattitude / 100)
                cell.progressView4.setProgress(progress: vl, animated: true)
            }
            
            cell.clickOnProgressbar = { [weak self] in
                self?.clickProgressbar()
            }
            
            return cell
            
            
        } else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HouseCell", for: indexPath) as! HouseCell
            cell.backgroundColor = UIColor(named: AssetsColor.BackgroundColor.rawValue)
            cell.lblTotalHouse.text = "Work in \(allDetails?.houses?.count ?? 0) houses"
            cell.allHouses = allDetails?.houses
            if allDetails?.houses?.count ?? 0 > 0 {
                
                if (allDetails?.houses!.count)! % 2 == 0  {
                    cell.collectionHeight.constant = 50 * CGFloat((allDetails?.houses!.count)!/2)
                }else  {
                    cell.collectionHeight.constant = 50 * CGFloat(((allDetails?.houses!.count)! + 1)/2 )
                }
                cell.collectionHouse.reloadData()
            }
            else {
                cell.collectionHeight.constant = 0
            }
            return cell
        }else {
            let cell = tblView.dequeueReusableCell(with: HelperFooterCell.self, for: indexPath)
            if selectedPeople?.is_hired == "1" {
                cell.addDateLbl.text = " ADDED BY YOU ON \(TimeStampToDate(timeStamp: allDetails?.date ?? 0))"
                cell.removeButton.isSelected = false
            }else{
                cell.removeButton.isSelected = true
                cell.addDateLbl.text = ""
            }
            cell.removeButton.addTarget(self, action: #selector(removeHelper), for: .touchUpInside)
            return cell
        }
        
    }
    
    @objc func removeHelper(_ sender:UIButton) {
        self.addtoHouseHold(sender)
    }
    
    @objc func callMobileNumber(_ sender: UIButton)
    {
        if let mb = allDetails?.mobile {
            self.callNumber(phoneNumber: mb)
        }
    }
    
    
    @objc func viewAllButtonClicked(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "ViewAllRatingsVC") as! ViewAllRatingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @objc func rateNowMe(_ sender:UIButton){
        if sender.titleLabel?.text?.uppercased() == "VIEW ALL" {
            self.clickProgressbar()
        } else if sender.titleLabel?.text?.uppercased() == "EDIT RATING" {
            self.rateNow(isEdit: true)
        }else{
            self.rateNow(isEdit: false)
        }
        
    }
    
    func rateNow(isEdit:Bool){
        DispatchQueue.main.async { [self] in
            let vc = HelperRatingView()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.isEdit = isEdit
            vc.id = self.selectedPeople?.id
            vc.helperName = self.selectedPeople?.name
            vc.ratingSuccess = {[weak self] value in
                self?.getPeopleDetails(isNeedLoader: false)
            }
            self.present(vc, animated: true, completion: nil)
        }

    }
    
    
    func clickProgressbar(){
        let vc = self.mainStory.instantiateViewController(withIdentifier: "ViewAllRatingsVC") as! ViewAllRatingsVC
        vc.id = selectedPeople?.id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
