//  LocalServiceVC.swift
//  Society Management
//  Created by Jitendra Yadav on 22/04/21.


import UIKit



class LocalServiceVC: MasterVC ,UISearchBarDelegate{
    
    @IBOutlet weak var viewNAv: Navigationbar!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var BtnBack: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var ServiceData = [LocalServiceData]()
    var searchData = [LocalServiceData]()
    var isSearching = false
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tblView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.className(ClassString: self)
        self.lblTitle.text = "Local Services"
        viewHeader.isHidden  =  false
        viewSearch.isHidden  =  true
        searchBar.backgroundImage = UIImage()
        self.searchBar.searchTextField.backgroundColor = .clear
        self.searchBar.layer.cornerRadius = 5
        searchBar.delegate = self
        self.getLocalServices(true)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
    }
    
    @objc func refresh(_ refresh: UIRefreshControl) {
        self.getLocalServices(false)
        refresh.endRefreshing()
    }
    
    @IBAction func btnSearchClicked(_ sender: UIButton) {
        viewHeader.isHidden  =  true
        viewSearch.isHidden  =  false
        self.searchBar.becomeFirstResponder()
    }
    @IBAction func btnBackClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCrossClicked(_ sender: UIButton)
    {
        viewHeader.isHidden  =  false
        viewSearch.isHidden  =  true
        searchData.removeAll()
        isSearching = false
        self.searchBar.endEditing(true)
        self.tblView.reloadData()

    }

}


extension LocalServiceVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching
        {
            return searchData.count
            
        } else
        {
           return ServiceData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocalServiceCell", for: indexPath) as! LocalServiceCell
        if isSearching
        {
            cell.Lbl1.text = searchData[indexPath.row].name
            cell.Lbl2.text = searchData[indexPath.row].countable
            
        } else
        {
            cell.Lbl1.text = ServiceData[indexPath.row].name
            cell.Lbl2.text = ServiceData[indexPath.row].countable
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "LocalServiceTypeVC") as! LocalServiceTypeVC
        if isSearching
        {
            vc.serviceType =  searchData[indexPath.row]
        } else
        {
            vc.serviceType =  ServiceData[indexPath.row]
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getLocalServices(_ isNeedLoader:Bool) {
        
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.localservices.rawValue, Params: params as [String : Any], isNeedLoader: isNeedLoader, objectType: LocalServiceModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.ServiceData = (object.data)!
                        self.tblView.reloadData()
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        if searchText.isEmpty
        {
            searchData.removeAll()
            isSearching = false
        } else
        {
            searchData = ServiceData.filter({ $0.name!.lowercased().prefix(searchText.count) == searchText.lowercased() })
            isSearching = true
        }
        
        tblView.reloadData()
    }
    
}


extension UITableView {

    func reloadWithAnimation() {
        self.reloadData()
     //  let tableViewHeight = self.bounds.size.height / 3
        let cells = self.visibleCells
        var delayCounter = 0
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: 100)
        }
        for cell in cells {
            UIView.animate(withDuration: 1.0, delay: 0.04 * Double(delayCounter),usingSpringWithDamping: 0.6, initialSpringVelocity: 0, options: .curveEaseInOut , animations: {
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            delayCounter += 1
        }
    }
}
