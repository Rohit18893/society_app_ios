//  LoginVC.swift
//  Society Management
//  Created by ROOP KISHOR on 06/04/2021.


import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class LoginVC: MasterVC,UITextFieldDelegate {
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var checkboxTop: NSLayoutConstraint!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!{
        didSet {
            
            txtEmail.keyboardType = .emailAddress
            
        }
    }
    
    
    @IBOutlet weak var txtPass: MDCOutlinedTextField!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var btnPhone: UIButton!
    @IBOutlet var btnRememberMe: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet var btnPassword: UIButton!
    
    var isSelectLang = false
    var isSecurePassword = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        PrintLog(AuthorizationID)
        self.className(ClassString: self)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    
    /*
     This method used to setup text fields designs
     */
    
    
    func setUpView() {
        
        btnRememberMe.isSelected = true
        self.headerViewHeight.constant = HeaderHeight
        txtPass.isHidden = false
        btnEmail.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnPhone.setTitleColor(UIColor.gray, for: .normal)
      
       
        
        self.setTextFieldTheme(txtField:txtEmail, labelText: "Email ID", allowEditing: true)
        self.setTextFieldTheme(txtField:txtPass, labelText: "Password", allowEditing: true)
        
        if isSelectLang {
            backButton.isHidden = false
        }else{
            backButton.isHidden = true
        }
    }
    
    
    @IBAction func passwordHideShow(sender: AnyObject) {
       
            if(isSecurePassword == true) {
                
                txtPass.isSecureTextEntry = false
                btnPassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
                
            } else {
                
                txtPass.isSecureTextEntry = true
                btnPassword.setImage(UIImage(systemName: "eye"), for: .normal)
            }

             isSecurePassword = !isSecurePassword
        }
    
    
    /*
     Actions->
     Method to go back
     Back button action
     */
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
     Sign up Button Clicked
     */
    
    @IBAction func signUpButtonClicked(_ sender: UIButton) {
        DispatchQueue.main.async {
            let nextViewController = self.mainStory.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    /*
     Forgot Password Button Clicked
     */
    
    @IBAction func forgotPasswordButtonClicked(_ sender: UIButton)
    {
        DispatchQueue.main.async {
            let nextViewController = self.mainStory.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    /*
        Login Button Clicked
     */
    @IBAction func loginButtonClicked(_ sender: UIButton) {
        
        if txtPass.isHidden {

            if txtEmail.text!.count < 9 {
                
                ShowAlert(AlertMessage.alMValidMobile, on: "", from: self)
                return
                
            }
            
            let mobile = String(format: "+971%@",txtEmail.text!)
            let param = [
                "mobile":mobile,
                "deviceid":deviceId,
                "devicetoken":deviceToken,
                "lang_id":language
            ]
            
            self.loginWithPhone(param: param)
            
        }else {
            
            guard let email = txtEmail.text , email.isBlank == false, email.isEmail == true  else {
                ShowAlert(AlertMessage.alMValidEmail, on: "", from: self)
                return
            }
            
            guard let password = txtPass.text , password.isBlank == false  else {
                ShowAlert(AlertMessage.alMEmptyPassword, on: "", from: self)
                return
            }
            guard let password1 = txtPass.text , password1.count > 5 else {
                ShowAlert(AlertMessage.alMValidPassword, on: "", from: self)
                return
            }
            
            let param = [
                "email":email,
                "password":password1,
                "deviceid":deviceId,
                "devicetoken":deviceToken,
                "lang_id":language
            ]
            
            self.loginWithEmail(param: param)
        }
    }
    
    
//MARK: ********* >>>>> LOGIN WITH EMAIL ID  <<<<<<**********
    
//    let storyboard = UIStoryboard(name: "Main", bundle: nil)
//    let vc = storyboard.instantiateViewController (withIdentifier: "CustumTabViewController") as! CustumTabViewController
//    let navigation = UINavigationController(rootViewController: vc)
//    let scene = UIApplication.shared.connectedScenes.first
//    navigation.navigationBar.isHidden = true
//    windowT?.rootViewController = navigation
    
    func loginWithEmail(param:[String:Any]) {
        PrintLog(param)
        APIClient().PostdataRequest(WithApiName: API.login.rawValue, Params: param, isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                       UserDefaults.standard.setKey(codable: object, forKey: "userDeatils")
                        let nextViewController = self.storyboard?.instantiateViewController(identifier: "CustumTabViewController") as! CustumTabViewController
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
    }
    
    
    func loginWithPhone(param:[String:Any]) {
        PrintLog(param)
        APIClient().PostdataRequest(WithApiName: API.mobileLogin.rawValue, Params: param, isNeedLoader: true, objectType: MobileDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
             if statuscode.statusCode == 200 {
               DispatchQueue.main.async
                 {
                
                   UserDefaults.standard.setKey(codable: object, forKey: "userDeatils")
                   let nextViewController = self.storyboard?.instantiateViewController(identifier: "OTPVerificationVC") as! OTPVerificationVC
                   nextViewController.userID = (object.data?.id)!
                   nextViewController.strMobile = String(format: "+971%@", self.txtEmail.text!)
                   self.navigationController?.pushViewController(nextViewController, animated: true)
                  
                 }
                    
                } else
               {
                    
                    ShowAlert(object.msg ?? "", on: "Oops!", from: self)
               }
                
                
            case .failure(let error):
                debugPrint(error)
            }

        }
        
    }
    
    /*
     Login with email Clicked
     
     */
    
    @IBAction func loginwithEmail(_ sender: UIButton) {
        
        btnEmail.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnPhone.setTitleColor(UIColor.gray, for: .normal)
        btnEmail.setImage(UIImage(named: "radio_active"), for: .normal)
        btnPhone.setImage(UIImage(named: "radio_unactive"), for: .normal)
        txtEmail.label.text = "Email ID"
        txtPass.isHidden = false
        self.checkboxTop.constant = 35
        self.txtEmail.text = ""
        txtEmail.leftView = nil
        
    }
    
    /*
     Login with phone Clicked
     */
    
    @IBAction func loginwithPhone(_ sender: UIButton) {
        txtEmail.label.text = "Mobile Number"
        txtEmail.placeholder = "Mobile Number"
        btnEmail.setTitleColor(UIColor.gray, for: .normal)
        btnPhone.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnEmail.setImage(UIImage(named: "radio_unactive"), for: .normal)
        btnPhone.setImage(UIImage(named: "radio_active"), for: .normal)
        txtPass.isHidden = true
        self.checkboxTop.constant = -40
        self.txtEmail.text = ""
        let prefix = UILabel()
        prefix.text = "+971"
        prefix.sizeToFit()
        txtEmail.leftView = prefix
        txtEmail.leftViewMode = .always
        
    }
    
    /*
       REMBEMBER ME
     */
    
    @IBAction func rememberMeClicked(_ sender: UIButton)
    {
        if sender .isSelected
        {
            btnRememberMe.setImage(UIImage(named: "check_box_unactive"), for: UIControl.State.normal)
            btnRememberMe.isSelected = false
            //termsAndCondition = "1"
            
        } else
        {
            btnRememberMe.setImage(UIImage(named: "check_box_active"), for: UIControl.State.normal)
            btnRememberMe.isSelected = true
           // termsAndCondition = "2"
            
        }
        
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if txtPass.isHidden {
            guard let textFieldText = textField.text,
                let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                    return false
            }
            let substringToReplace = textFieldText[rangeOfTextToReplace]
            let count = textFieldText.count - substringToReplace.count + string.count
            return count <= 9
        }
        
        return true
    }
    
}
