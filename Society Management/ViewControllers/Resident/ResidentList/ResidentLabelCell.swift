//  ResidentLabelCell.swift
//  Society Management
//  Created by ROOP KISHOR on 02/05/2021.


import UIKit

class ResidentLabelCell: UITableViewCell {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var flatNumber: UILabel!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    var peopleLiving = [Peopledata]()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        
       

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

    
    
}
extension ResidentLabelCell: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.peopleLiving.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "InsideCell", for: indexPath) as! InsideCell
        
        cell.lblName.text = "\(String(describing: peopleLiving[indexPath.row].fname!)) \(String(describing: peopleLiving[indexPath.row].lname!))"
        cell.lblUserRole.text = peopleLiving[indexPath.row].userrole
        cell.btnCall.tag  = indexPath.row
        cell.btnCall.addTarget(self, action: #selector(self.callResident), for: .touchUpInside)
        if peopleLiving[indexPath.row].connect_residence == "0" {
            
            cell.btnCall.setImage(UIImage(named: "green_call"), for: .normal)
            cell.btnCall.isUserInteractionEnabled = true
            
        } else {
            
            cell.btnCall.setImage(UIImage(named: "call"), for: .normal)
            cell.btnCall.isUserInteractionEnabled = false
        }
        
        
       return cell

    }
    
    @objc func callResident(_ sender: UIButton)
    {
        let phone = peopleLiving[sender.tag].mobile
        if let url = URL(string: "tel://\(String(describing: phone))"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
       
    }
    
}
