//  ResidentListVC.swift
//  Society Management
//  Created by ROOP KISHOR on 02/05/2021.


import UIKit

class ResidentListVC: UIViewController {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    var strFloor = String()
    var residentData = [ResidentData]()
    @IBOutlet weak var lblNoData: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = strFloor.capitalized
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnOption.isHidden = false
        lblNoData.isHidden = true
        self.getResidentList()
       
    }
    

}
extension ResidentListVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return residentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResidentLabelCell", for: indexPath) as! ResidentLabelCell
        cell.flatNumber.text = "Flat \(String(describing: residentData[indexPath.row].unitname!))"
        cell.peopleLiving = residentData[indexPath.row].peopledata!
        cell.tableHeight.constant = CGFloat(75 * residentData[indexPath.row].peopledata!.count)
        return cell
        
    }
    
    
    func getResidentList() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language,"flor":strFloor]
        APIClient().PostdataRequest(WithApiName: API.residentlist.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ResidentDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.lblNoData.isHidden = true
                        self.residentData = object.data!
                        self.tblView.reloadData()
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.lblNoData.isHidden = false
                }
                

            }
        }

    }
}
