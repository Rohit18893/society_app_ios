//
//  InsideCell.swift
//  Society Management
//
//  Created by ROOP KISHOR on 02/05/2021.
//

import UIKit

class InsideCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUserRole: UILabel!
    @IBOutlet weak var btnCall: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
