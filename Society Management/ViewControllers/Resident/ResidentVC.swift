//  ResidentVC.swift
//  Society Management
//  Created by Jitendra Yadav on 25/04/21.


import UIKit

class ResidentVC: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var switchBtn: UIButton!
   // var towerData = [String]()
    var towerData = [EmergencyData]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let userflat = userData?.society
        {
            self.viewNAv.lbltitle.text = userflat
        }
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnOption.isHidden = false
        
//        self.viewNAv.completionHandler = {
//            PrintLog("Search")
//            self.logoutUser()
//        }
        
        self.className(ClassString: self)
        self.tblView.delegate = self
        self.tblView.dataSource = self

        
        self.getFloors()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.updateConnectResidence ()
    }
    
    func updateConnectResidence (){
        if isConnectResidence == "0" {
            print("connect_residence - 0")
            switchBtn.isSelected = true
            switchBtn.setImage(UIImage(named: "toggle_on"), for: .normal)
        }
        else {
            print("connect_residence - 1")
            switchBtn.isSelected = false
            switchBtn.setImage(UIImage(named: "toggle_off"), for: .normal)
        }
    }
    
    
    @IBAction func switchButtunClicked(_ sender: UIButton) {
        if sender.isSelected  {
            switchBtn.isSelected = false
            switchBtn.setImage(UIImage(named: "toggle_off"), for: .normal)
            self.connectWithResidence(status: "1")
            UserDefaults.standard.setValue("1", forKey: "isConnectResidence")
            
        } else {
            
            switchBtn.isSelected = true
            switchBtn.setImage(UIImage(named: "toggle_on"), for: .normal)
            self.connectWithResidence(status: "0")
            UserDefaults.standard.setValue("0", forKey: "isConnectResidence")
        }
    }
    
}

extension ResidentVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        }else{
            return towerData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ResidentCell", for: indexPath) as! ResidentCell
        if indexPath.section == 0
        {
            cell.leftImage.isHidden = true
            cell.rightImage.isHidden = false
            cell.titleLbl.font = UIFont(name: "Proxima Nova-Semibold", size: 17)
            cell.titleLbl.text = "Management Committee".uppercased()
            
        } else if indexPath.section == 1
        {
            cell.leftImage.isHidden = true
            cell.rightImage.isHidden = true
            cell.titleLbl.font = UIFont(name: "Proxima Nova-Semibold", size: 15)
            cell.titleLbl.text = "Floors".uppercased()
            
        } else {
            
            cell.leftImage.isHidden = false
            cell.rightImage.isHidden = false
            cell.titleLbl.font = UIFont(name: "Proxima Nova-Semibold", size: 14)
            cell.titleLbl.text = towerData[indexPath.row].name?.capitalized
        }
                
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        
        if indexPath.section == 0
        {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "ManagementCommitteeVC") as! ManagementCommitteeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
       else if indexPath.section == 2 {
            
            let vc = self.mainStory.instantiateViewController(withIdentifier: "ResidentListVC") as! ResidentListVC
            vc.strFloor = self.towerData[indexPath.row].id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func connectWithResidence(status:String)  {
        
        APIClient().PostdataRequest(WithApiName: API.connectresidence.rawValue, Params: ["uid":(userData?.id)!,"connect_residence":status,"lang_id":language], isNeedLoader: false, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(_):
                    DispatchQueue.main.async {
                        self.updateConnectResidence ()
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
    }
    
    func getFloors() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.floors.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: EmergencyDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object.data)
                        if let rs = object.data {
                            self.towerData = rs
                        }
                        self.tblView.reloadData()
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }
    
    
}
