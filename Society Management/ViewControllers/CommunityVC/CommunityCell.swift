//
//  CommunityCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 21/04/21.
//

import UIKit

class CommunityCell: UITableViewCell {
    
    @IBOutlet weak var Lbl1: UILabel!
    @IBOutlet weak var Lbl2: UILabel!
    @IBOutlet weak var LblCount: UILabel!
    
    @IBOutlet weak var DisLbl1: UILabel!
    @IBOutlet weak var DisLbl2: UILabel!
    
    @IBOutlet weak var btn1: UIButton!
    @IBOutlet weak var btn2: UIButton!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    @IBOutlet weak var Image1: UIImageView!
    @IBOutlet weak var Image2: UIImageView!
    
 
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        DisLbl1.adjustsFontSizeToFitWidth = true
        DisLbl2.adjustsFontSizeToFitWidth = true
        Lbl1.adjustsFontSizeToFitWidth = true
        Lbl2.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    func setImageAndBackgroundColor(textName:String)->(UIImage,UIColor,String) {
/*1*/   if textName == "Local Services" {
            return (#imageLiteral(resourceName: "local_icon"),#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1),"Find Daily helps & Service Providers")
        }
/*2*/   else if textName == "Emergency No's" {
            return (#imageLiteral(resourceName: "emergency"),#colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1),"Emergency contacts for your society")
        }
/*3*/   else if textName == "Resident" {
            return (#imageLiteral(resourceName: "resident"),#colorLiteral(red: 0.9882352941, green: 0.7803921569, blue: 0.2745098039, alpha: 1),"View Society Residents & Management Committee")
        }
/*4*/   else if textName == "Helpdesk" {
            return (#imageLiteral(resourceName: "helpdesk"),#colorLiteral(red: 0.9058823529, green: 0.2235294118, blue: 0.5529411765, alpha: 1),"Raise complaints & Service Request")
        }
/*5*/   else if textName == "Notice Board" {
            return (#imageLiteral(resourceName: "notice"),#colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1),"View Society Announcements")
        }
/*6*/   else if textName == "Communications" {
    
            return (#imageLiteral(resourceName: "communications"),#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1),"Host Meeting, Polls and Discussions")
        }
/*7*/   else if textName == "Documents" {
            return (#imageLiteral(resourceName: "documents"),#colorLiteral(red: 0.9960784314, green: 0.3450980392, blue: 0.1960784314, alpha: 1),"Find and store society & personal documents")
        }
/*8*/   else if textName == "Society Dues" {
            return (#imageLiteral(resourceName: "issue_gatepass"),#colorLiteral(red: 0.9725490196, green: 0.4784313725, blue: 0, alpha: 1),"Pay all due easly \n  ")
        }
/*9*/   else{
            return (#imageLiteral(resourceName: "issue_gatepass"),#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1),"")
        }
    }
    
}
