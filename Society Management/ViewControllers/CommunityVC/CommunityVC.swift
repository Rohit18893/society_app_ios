//  CommunityVC.swift
//  Society Management
//  Created by Jitendra Yadav on 21/04/21.


import UIKit

struct CommunityData {
    var name:String = ""
    var image:UIImage?
    var Data = [String]()
}

class CommunityVC: MasterVC, UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    
    var data = [CommunityData]()
    var noticeCount = 0
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        tblView.contentInsetAdjustmentBehavior = .never
        data.append(CommunityData(name: "Discover", image: nil, Data: ["Local Services","Emergency No's","Resident"]))
        data.append(CommunityData(name: "Engage", image: nil, Data: ["Helpdesk", "Notice Board", "Communications"]))
        
      //  data.append(CommunityData(name: "Pay", image: nil, Data: ["Society Dues"]))
        
        
        if let userflat = userData?.society {
            
            self.viewNAv.lbltitle.text = userflat
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getNoticeBoardCount()
        
    }
    
  
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let tt = data[section].Data.count
        return  (tt + 1) / 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].name
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CommunityCell", for: indexPath) as? CommunityCell else{return UITableViewCell()}
        
        cell.btn1.setTitle(data[indexPath.section].Data[indexPath.row * 2] , for: .normal)
        cell.btn1.tag = indexPath.row * 2
        let rr = cell.setImageAndBackgroundColor(textName: data[indexPath.section].Data[indexPath.row * 2])
        cell.Image1.image = rr.0
        cell.view1.backgroundColor = rr.1
        cell.DisLbl1.text = rr.2.capitalized
        cell.Lbl1.text = data[indexPath.section].Data[indexPath.row * 2]
        cell.btn1.addTarget(self, action: #selector(frist1(_:)), for: .touchUpInside)
        if (indexPath.row * 2) + 1 < data[indexPath.section].Data.count {
            
            cell.btn2.setTitle(data[indexPath.section].Data[(indexPath.row * 2) + 1], for: .normal)
            cell.btn2.tag = (indexPath.row * 2) + 1
            cell.btn2.addTarget(self, action: #selector(second2(_:)), for: .touchUpInside)
            cell.view2.isHidden = false
            let rr = cell.setImageAndBackgroundColor(textName: data[indexPath.section].Data[(indexPath.row * 2) + 1])
            cell.Image2.image = rr.0
            cell.view2.backgroundColor = rr.1
            cell.DisLbl2.text = rr.2.capitalized
            cell.Lbl2.text = data[indexPath.section].Data[(indexPath.row * 2) + 1]
            
            
        }else{
            
            cell.view2.isHidden = true
        }
        if indexPath.section == 1 && indexPath.row == 0 {
            
            if noticeCount == 0
            {
                cell.LblCount.isHidden = true
                
            } else
            {
                
                cell.LblCount.layer.cornerRadius = cell.LblCount.frame.size.height/2
                cell.LblCount.isHidden = false
            }
            
            
            
        }
        else
        {
            cell.LblCount.isHidden = true
        }
        
        return cell
        
    }
    
    @objc private func frist1(_ sender: UIButton?) {
        self.didselectRow(txt: sender?.titleLabel?.text ?? "")
    }
    
    @objc private func second2(_ sender: UIButton?) {
        self.didselectRow(txt: sender?.titleLabel?.text ?? "")
    }
    
    
    func didselectRow(txt:String){
        
        if txt == "Local Services" {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "LocalServiceVC")
            self.navigationController?.pushViewController(vc, animated: true)
        } else if txt == "Emergency No's" {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "emergenctNoVC")
            self.navigationController?.pushViewController(vc, animated: true)
        } else if txt == "Resident" {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "ResidentVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if txt == "Helpdesk" {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpDeskVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if txt == "Notice Board" {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "NoticeBoardVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if txt == "Communications" {
            let vc = ComunicationVC()
            vc.modalTransitionStyle = .crossDissolve
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: true, completion: nil)
            
        }else if txt == "Documents" {
            
        }else if txt == "Society Dues" {
            
            let vc = self.mainStory.instantiateViewController(withIdentifier: "ServiceChargeVC")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 35
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    
    func getNoticeBoardCount() {
        
        let params = ["towerid":userData?.towerid,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.noticeboardcount.rawValue, Params: params as [String : Any], isNeedLoader: false, objectType: NoticeboardCountModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        let cou = object.data?.count
                        self.noticeCount = Int(cou! as String) ?? 0
                        self.tblView.reloadData()
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }

    }
    
}
