//  ProfileVC.swift
//  Society Management
//  Created by ROOP KISHOR on 08/04/2021.


import UIKit

import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class ProfileVC: MasterVC, ImagePickerDelegate {
   
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtFname: MDCOutlinedTextField!
    @IBOutlet weak var txtLname: MDCOutlinedTextField!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!
    @IBOutlet weak var txtIsd: MDCOutlinedTextField!
    @IBOutlet weak var txtMobile: MDCOutlinedTextField!
    @IBOutlet weak var txtTower: MDCOutlinedTextField!
    @IBOutlet weak var txtUnit: MDCOutlinedTextField!
    @IBOutlet weak var txtCountry: MDCOutlinedTextField!
    @IBOutlet weak var txtCity: MDCOutlinedTextField!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    var imagePicker: ImagePicker!
    var strGender = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpView()
        self.className(ClassString: self)
    }
    
    func setUpView()
    {
        self.view.backgroundColor = UIColor.appColor(.BackgroundColor)
        self.btnMale.layer.cornerRadius = 4
        self.btnFemale.layer.cornerRadius = 4
        self.btnMale.layer.borderWidth = 1
        self.btnFemale.layer.borderWidth = 1
        self.btnFemale.layer.borderColor = DEFAULT_COLOR_ORANGE.cgColor
        self.btnMale.layer.borderColor = DEFAULT_COLOR_ORANGE.cgColor
        self.getProfileDetails(true)
        profileImage.layer.cornerRadius = 50
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.headerViewHeight.constant = HeaderHeight
        self.setTextFieldTheme(txtField:txtFname, labelText: "First Name", allowEditing: true)
        self.setTextFieldTheme(txtField:txtLname, labelText: "Last Name", allowEditing: true)
        self.setTextFieldTheme(txtField:txtEmail, labelText: "Email", allowEditing: false)
        self.setTextFieldTheme(txtField:txtIsd, labelText: "ISD", allowEditing: false)
        self.txtIsd.setLeftImage(imageName: "flag")
        self.setTextFieldTheme(txtField:txtMobile, labelText: "Mobile Number", allowEditing: false)
        self.setTextFieldTheme(txtField:txtTower, labelText: "Building Name", allowEditing: false)
        self.setTextFieldTheme(txtField:txtUnit, labelText: "Unit Number", allowEditing: false)
        self.setTextFieldTheme(txtField:txtCountry, labelText: "Country", allowEditing: false)
        self.setTextFieldTheme(txtField:txtCity, labelText: "City", allowEditing: false)
        txtIsd.text = "+971"
        
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }

    
   
    
    @IBAction func updateProfileButtonClicked(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let fname = txtFname.text , fname.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyFname, on: "", from: self)
            return
            
        }
        guard let Lname = txtLname.text , Lname.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyLname, on: "", from: self)
            return
            
        }
        
        self.updateProfile()
        
    }
    

    
    func getProfileDetails(_ isNeedLoader:Bool) {
        let params = ["uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.myprofile.rawValue, Params: params as [String : Any], isNeedLoader: isNeedLoader, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.txtFname.text  = object.data?.fname
                        self.txtLname.text  = object.data?.lname
                        self.txtEmail.text  = object.data?.email
                        self.txtMobile.text = object.data?.mobile
                        self.txtTower.text  = object.data?.towername
                        self.txtUnit.text   = object.data?.unitname
                        self.txtCountry.text = "UAE"
                        self.txtCity.text = object.data?.cityname
                        self.strGender = (object.data?.gender)!
                        let url = URL(string: object.data?.image ?? "")
                        self.profileImage.sd_setImage(with: url , placeholderImage:UIImage(named: "profile"))
                                                
                        if self.strGender == "Male" {
                            self.btnMale.setTitleColor(UIColor.white, for: .normal)
                            self.btnMale.setImage(UIImage(named: "male_active"), for: .normal)
                            self.btnMale.backgroundColor = DEFAULT_COLOR_ORANGE
                            self.btnFemale.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
                            self.btnFemale.setImage(UIImage(named: "female_unactive"), for: .normal)
                        } else {
                            
                            self.btnMale.setImage(UIImage(named: "male_unactive"), for: .normal)
                            self.btnFemale.setImage(UIImage(named: "female_active"), for: .normal)
                            self.btnMale.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
                            self.btnMale.backgroundColor = UIColor.white
                            self.btnFemale.backgroundColor = DEFAULT_COLOR_ORANGE
                            self.btnFemale.setTitleColor(UIColor.white, for: .normal)
                        }
                        
                        UserDefaults.standard.setKey(codable: object, forKey: "userDeatils")
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }
    
    
    @IBAction func selectProfilePicClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.imagePicker!.present(from: sender)
    }
    
    
    func didSelect(image: UIImage?, tag: Int) {
        
        self.profileImage.image = image
        
    }
    
    /*
         SELECT NATIONALITY
     */
    
    @IBAction func selectNationalityClicked(_ sender: UIButton)
    {
//        let countryList = Locale.isoRegionCodes.compactMap { Locale.current.localizedString(forRegionCode: $0) }
//        print(countryList)
        
//        var countries: [String] = []
//        for code in NSLocale.isoCountryCodes  {
//            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
//            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
//            countries.append(name)
//        }
//
//        print(countries.count)
//
//        CustomPickerView.show(items: countries , itemIds: [] ,selectedValue:self.txtNationality.text, doneBottonCompletion: { (item, index) in
//
//             self.txtNationality.text = item
//
//            }, didSelectCompletion: { (item, index) in
//
//                self.txtNationality.text = item
//
//            }) { (item, index) in
//            }
        
        
     
    }
    
    @IBAction func btnMaleClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        btnMale.setImage(UIImage(named: "male_active"), for: .normal)
        btnFemale.setImage(UIImage(named: "female_unactive"), for: .normal)
        btnFemale.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnFemale.backgroundColor = UIColor.white
        btnMale.backgroundColor = DEFAULT_COLOR_ORANGE
        btnMale.setTitleColor(UIColor.white, for: .normal)
        strGender = "Male"

    }
    
   
    @IBAction func btnFemaleClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        btnMale.setImage(UIImage(named: "male_unactive"), for: .normal)
        btnFemale.setImage(UIImage(named: "female_active"), for: .normal)
        btnMale.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnMale.backgroundColor = UIColor.white
        btnFemale.backgroundColor = DEFAULT_COLOR_ORANGE
        btnFemale.setTitleColor(UIColor.white, for: .normal)
        strGender = "Female"
    }
    
    
    /*
        Get All Countries
     */
    
    func getAllCountries()
    {
        var countries: [String] = []
        for code in NSLocale.isoCountryCodes  {
            let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            countries.append(name)
        }
        print(countries.count)
        
    }
    
    func updateProfile() {

        USER_ID = userData?.id ?? ""
        
        let param = [
            "fname":txtFname.text ?? "",
            "lname":txtLname.text ?? "",
            "lang_id":language,
            "uid":userData?.id ?? "" ,
            "gender":self.strGender
        ]

        var imageArr = [imagesUpload]()
        imageArr.append(imagesUpload(key: "document", image: self.profileImage.image!, fileName: "profile.png"))
        print(imageArr)
        print(param)
        APIClient().UploadImageWithParametersRequest(WithApiName: API.profileupdate.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
//                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                        let vc = storyboard.instantiateViewController (withIdentifier: "CustumTabViewController") as! CustumTabViewController
//                        let navigation = UINavigationController(rootViewController: vc)
//                        let scene = UIApplication.shared.connectedScenes.first
//                        navigation.navigationBar.isHidden = true
//                        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate) {
//                            sd.window?.rootViewController = navigation
//
//                        }
                        
                        self.getProfileDetails(false)
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                case .failure(let error):
                    debugPrint(error) 
                }
            }else{
                
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() { [weak self] in
                self?.profileImage.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}

