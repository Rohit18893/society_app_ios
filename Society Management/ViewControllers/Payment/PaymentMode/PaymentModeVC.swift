//  PaymentModeVC.swift
//  Society Management
//  Created by ROOP KISHOR on 27/04/2021.


import UIKit

class PaymentModeVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title29
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false

        // Do any additional setup after loading the view.
    }
    
    @IBAction func makePaymentClicked(_ sender: UIButton)
    {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "AddCardVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
   
}
extension PaymentModeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentModeCell", for: indexPath) as! PaymentModeCell

        if indexPath.row == 0 {
            
            cell.lblName.text = "Pay With Debit Card"
        }
        else
        {
            cell.lblName.text = "Pay With Credit Card"
        }
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
