//
//  PaymentModeCell.swift
//  Society Management
//
//  Created by ROOP KISHOR on 27/04/2021.
//

import UIKit

class PaymentModeCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
