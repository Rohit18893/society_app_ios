//  AddCardVC.swift
//  Society Management
//  Created by ROOP KISHOR on 27/04/2021.


import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import PaymentSDK
import PassKit


struct PaymentModel:Codable {
    var data:paymentDataModel?
    var msg:String?
}
struct paymentDataModel:Codable {
    var orderid:String?
    var profileid:String?
    var serviceid:String?
    var clientid:String?
}

class PaymentDetailsVC: MasterVC, UITextFieldDelegate {

    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var txtAmount: MDCOutlinedTextField!
    @IBOutlet weak var btntotalPayment: UIButton!
    @IBOutlet weak var btnManualPayment: UIButton!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var txtFieldTopCons: NSLayoutConstraint!
    @IBOutlet weak var lblInvoiceAmt: UILabel!
    @IBOutlet weak var lblPaymentCharge: UILabel!
    @IBOutlet weak var lblextraCharge: UILabel!
    @IBOutlet weak var lblPayableAmt: UILabel!
    
    var invoiceDetails : Invoices?
    var isTotalPayment = true
    
    var percentageCharge : Double = 0.0
    var extraCharge : Double = 0.0
    var amountToPay : Double = 0.0
    var netAmount = String()
    
    var profileID = String()
    var serverKey = String()
    var clientKey = String()
    var orderId = String()
    var totalAmount = String()
    
    
    var userName:String! {
        return "\(userData?.fname ?? "") \(userData?.lname ?? "")"
    }
    
    
    var billingDetails: PaymentSDKBillingDetails! {
        return PaymentSDKBillingDetails(name: userName,
                                        email: userData?.email,
                                        phone: userData?.mobile,
                                        addressLine: userData?.society,
                                        city: userData?.cityname,
                                        state: userData?.state,
                                        countryCode: userData?.countrycode, // ISO alpha 2
                                        zip: "12345")
    }
    
    var shippingDetails: PaymentSDKShippingDetails! {
        return PaymentSDKShippingDetails(name: userName,
                                         email: userData?.email,
                                         phone: userData?.mobile,
                                         addressLine: userData?.society,
                                         city: userData?.cityname,
                                         state: userData?.state,
                                         countryCode: userData?.countrycode, // ISO alpha 2
                                         zip: "12345")
    }
    
    var configuration: PaymentSDKConfiguration! {
        let theme = PaymentSDKTheme.default
        theme.logoImage = UIImage(named: "headerimg")
        
        return PaymentSDKConfiguration(profileID: profileID,
                                       serverKey: serverKey,
                                       clientKey: clientKey,
                                       cartID: orderId,
                                       currency: "AED",
                                       amount: totalAmount.toDouble() ?? 0.0,
                                       cartDescription: "Service Charges",
                                       merchantCountryCode: "AE",
                                       forceShippingInfo: true,
                                       showBillingInfo: true,
                                       showShippingInfo: true,
                                       screenTitle: "Pay Service Charges",
                                       billingDetails: billingDetails,
                                       shippingDetails: shippingDetails,
                                       theme: theme)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewNAv.lbltitle.text = AlertMessage.title28
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.setUpView()
        self.className(ClassString: self)
    }
    
    func setUpView()
    {
        if let invoiceAmt = invoiceDetails?.amount
        {
            lblPaymentCharge.text = "AED \(percentageCharge) %"
            lblextraCharge.text = "AED \(extraCharge)"
            amountToPay = self.calculatePayableAmount(invoiceAmt, percentageCharge: percentageCharge, extraCharge: extraCharge)
            lblPayableAmt.text = "AED \(amountToPay)"
            netAmount = invoiceAmt
            
        }
        else
        {
            lblPaymentCharge.text = "--"
            lblextraCharge.text = "--"
        }
        
        txtAmount.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        lblAmount.isHidden = true
        txtAmount.isHidden = true
        txtFieldTopCons.constant = 0
        txtAmount.delegate = self
        self.setTextField(txtField:txtAmount, labelText: "", allowEditing: true)
        lblInvoiceAmt.text = "AED \(invoiceDetails?.amount ?? "")"
       
    }
    
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        if textField.text?.count ?? 0 > 0 {
            amountToPay = self.calculatePayableAmount(textField.text ?? "", percentageCharge: percentageCharge, extraCharge: extraCharge)
            lblPayableAmt.text = "AED \(amountToPay)"
            netAmount = textField.text ?? ""
            
        } else {
            
            lblPayableAmt.text = ""
            
        }
        
        
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textstring.count
        if length > 6 {
            return false
        }
        return true
    }
    
    
    @IBAction func totalPaymentClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        isTotalPayment = true
        lblAmount.isHidden = true
        txtAmount.isHidden = true
        txtFieldTopCons.constant = 0
        btntotalPayment.setImage(UIImage(named: "radio_active"), for: .normal)
        btnManualPayment.setImage(UIImage(named: "radio_unactive"), for: .normal)
        USER_TYPE = TYPE_OWNER
        btntotalPayment.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnManualPayment.setTitleColor(UIColor.black, for: .normal)
        amountToPay = self.calculatePayableAmount(invoiceDetails?.amount ?? "", percentageCharge: percentageCharge, extraCharge: extraCharge)
        lblPayableAmt.text = "AED \(amountToPay)"
        netAmount = invoiceDetails?.amount ?? ""
    }
    
    
    @IBAction func manualPaymentClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        isTotalPayment = false
        lblAmount.isHidden = false
        txtAmount.isHidden = false
        txtAmount.text = ""
        txtFieldTopCons.constant = 60
        btntotalPayment.setImage(UIImage(named: "radio_unactive"), for: .normal)
        btnManualPayment.setImage(UIImage(named: "radio_active"), for: .normal)
        USER_TYPE = TYPE_TENANT
        btnManualPayment.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btntotalPayment.setTitleColor(UIColor.black, for: .normal)
        lblPayableAmt.text = ""
        
    }
    
    func setTextField(txtField:MDCOutlinedTextField,labelText:String,allowEditing:Bool)
    {
        txtField.isUserInteractionEnabled = allowEditing
        txtField.label.text = labelText
        txtField.setOutlineColor(UIColor.lightGray, for: .normal)
        txtField.setOutlineColor(UIColor.red, for: .editing)
        txtField.setOutlineColor(UIColor.lightGray, for: .disabled)
        txtField.tintColor = UIColor.black
        txtField.textColor = UIColor.lightGray
        txtField.setFloatingLabelColor(.red, for: .editing)
        txtField.setFloatingLabelColor(.lightGray, for: .normal)
        txtField.sizeToFit()
        
    }

    @IBAction func payNowClicked(_ sender: UIButton)
    {
        if isTotalPayment {
            
            self.CreatePayment(netAmount)
            
        } else {
            
            guard let amt = txtAmount.text , amt.isBlank == false  else {
                ShowAlert(AlertMessage.enterAmount, on: "", from: self)
                return
            }
            self.CreatePayment(netAmount)
        }
        
    }
    
    
    func CreatePayment(_ amount:String) {
        let params = ["uid":userData?.id,"amount":amount,"lang_id":language,"invoiceid":invoiceDetails?.invoice_number]
        APIClient().PostdataRequest(WithApiName: API.createPayment.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: PaymentModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.profileID = object.data?.profileid ?? ""
                        self.orderId = object.data?.orderid ?? ""
                        self.serverKey = object.data?.serviceid ?? ""
                        self.clientKey = object.data?.clientid ?? ""
                        //self.totalAmount = amount
                        self.totalAmount = String(self.amountToPay)
                        PaymentManager.startCardPayment(on: self, configuration:self.configuration,
                                                        delegate: self)
                    }
                case .failure(let error):
                    debugPrint(error)
                    DispatchQueue.main.async {
                        if let WT = windowT?.rootViewController {
                            ShowAlert("\(error)", on: "", from: WT)
                        }
                    }
                }
            }else{
                DispatchQueue.main.async {
                    if let WT = windowT?.rootViewController {
                        ShowAlert(AlertMessage.somethingWrong, on: "Opps!", from: WT)
                    }
                }
            }
        }
    }
    
    
    func SuccessPayment(_ params:[String:String]) {
        
        APIClient().PostdataRequest(WithApiName: API.successPayment.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: StatusModel.self) {(response, statuscode) in
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        if let WT = windowT?.rootViewController {
                            
                            //ShowAlert(object.msg ?? "", on: "Success!", from: WT)
                            self.showCustumAlert(message: object.msg ?? "Success!", isBack: true)
                            
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                DispatchQueue.main.async {
                    if let WT = windowT?.rootViewController {
                        ShowAlert(AlertMessage.somethingWrong, on: "", from: WT)
                    }
                }
            }
        }
    }
    
}
extension PaymentDetailsVC: PaymentManagerDelegate {
    
    func paymentManager(didFinishTransaction transactionDetails: PaymentSDKTransactionDetails?, error: Error?) {
        if let transactionDetails = transactionDetails {
            debugPrint("Response Code: " + (transactionDetails.paymentResult?.responseCode ?? ""))
            debugPrint("Result: " + (transactionDetails.paymentResult?.responseMessage ?? ""))
            debugPrint("Token: " + (transactionDetails.token ?? ""))
            debugPrint("Transaction Reference: " + (transactionDetails.transactionReference ?? ""))
            debugPrint("Transaction Time: " + (transactionDetails.paymentResult?.transactionTime ?? "" ))
            debugPrint("card Type: " + (transactionDetails.paymentInfo?.cardType ?? "" ))
            debugPrint("card Scheme: " + (transactionDetails.paymentInfo?.cardScheme ?? "" ))
            debugPrint("card info: " + (transactionDetails.paymentInfo?.paymentDescription ?? "" ))
            debugPrint("Response Status: " + (transactionDetails.paymentResult?.responseStatus ?? ""))
            
            let params:[String:String] = ["response_code":transactionDetails.paymentResult?.responseCode ?? "",
                                          "result":transactionDetails.paymentResult?.responseMessage ?? "",
                                          "transactionid":transactionDetails.transactionReference ?? "",
                                          "transactiontime":transactionDetails.paymentResult?.transactionTime ?? "",
                                          "lang_id":language,
                                          "orderid":orderId,
                                          "response_status":transactionDetails.paymentResult?.responseStatus ?? "",
                                          "card_type":transactionDetails.paymentInfo?.cardType ?? "",
                                          "card_scheme":transactionDetails.paymentInfo?.cardScheme ?? "",
                                          "card_info":transactionDetails.paymentInfo?.paymentDescription ?? ""]
            DispatchQueue.main.async {
                self.SuccessPayment(params)
            }
            
        } else if let error = error {
            showError(message: error.localizedDescription)
        }
    }
    
    func paymentManager(didCancelPayment error: Error?) {
        
    }
    
    func showError(message: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController.init(title: self.title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction.init(title: "Ok", style: .cancel, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
