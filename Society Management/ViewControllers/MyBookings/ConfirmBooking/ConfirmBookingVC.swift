//  ConfirmBookingVC.swift
//  Society Management
//  Created by ROOP KISHOR on 10/07/2021.


import UIKit

class ConfirmBookingVC: MasterVC,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var noRecordFound: UILabel!
    var refreshControl = UIRefreshControl()
    var myBookings = [BookingData]()
    
    override func viewDidLoad()
    {
        self.className(ClassString: self)
        super.viewDidLoad()
        tblView.delegate = self
        tblView.dataSource = self
        noRecordFound.isHidden = false
//        if #available(iOS 10.0, *) {
//
//            tblView.refreshControl = refreshControl
//
//        } else {
//
//            tblView.addSubview(refreshControl)
//        }
        self.getBookings()
        
    }
    
    @objc func refresh(_ refresh: UIRefreshControl)
    {
       // refresh.endRefreshing()
    }

    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return myBookings.count
        
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmBookingCell", for: indexPath) as! ConfirmBookingCell
        cell.lblTitle.text = self.myBookings[indexPath.row].title
        cell.lblDate.text = self.myBookings[indexPath.row].booking_date
        cell.lblTime.text = self.myBookings[indexPath.row].time_slot
        let url = URL.init(string:self.myBookings[indexPath.row].image ?? "")
        cell.imgView.sd_setImage(with: url , placeholderImage:UIImage(named: "No-image"))
        if self.myBookings[indexPath.row].status == 1
        {
            cell.lblStatus.text = "Confirmed"
            cell.lblStatus.backgroundColor = UIColor.systemGreen
            
        } else
        {
            cell.lblStatus.text = "Ongoing"
            cell.lblStatus.backgroundColor = UIColor.systemOrange
        }
        
        return cell
        
    }
    
    func getBookings() {
      
        let params = ["lang_id":language,"towerid":userData?.towerid,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.userBookings.rawValue, Params: params as [String : Any], isNeedLoader: false, objectType: BookingDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.myBookings.removeAll()
                        if let data = object.result
                        {
                            self.myBookings = data
                        }
                        self.noRecordFound.isHidden = true
                        self.tblView.reloadData()
                        
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.myBookings.removeAll()
                    self.noRecordFound.isHidden = false
                    self.tblView.reloadData()
                }
                
            }
        }
        
    }

}
