//  SideMenuVC.swift
//  ALMEVENT
//  Created by ROOP KISHOR on 08/11/2020.


import UIKit
import ViewAnimator

class SideMenuVC: MasterVC ,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblEmail: UILabel!
    
    var rowData = [[String:Any]]()
    private let animations = [AnimationType.vector(CGVector(dx: -150, dy: 0))]
  

    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        tblMenu.tableFooterView = UIView()
        self.view.clipsToBounds = true
        self.view.layer.cornerRadius = 25
        self.view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        userImage.layer.cornerRadius = userImage.frame.height/2
        userImage.layer.borderWidth = 0.5
        userImage.layer.borderColor = UIColor.lightGray.cgColor
        
        lblName.text = "\(String(describing:( userData?.fname ?? "").capitalized)) \(String(describing:(userData?.lname ?? "").capitalized))"
        
        lblEmail.text = "\(String(describing:( userData?.email ?? "")))"
        
        let url = URL.init(string:userData?.image ?? "")
        userImage.sd_setImage(with: url , placeholderImage:UIImage(named: "profile"))
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.profileUpdate(_:)), name: NSNotification.Name(rawValue: "ProfileUpdate"), object: nil)
        
       
        
         
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1)
        {
            if userData?.usertype == "1"
            {
                self.rowData = [["key":"Service Charge","icon":"icon_service"],
                           ["key":"Facilities","icon":"icon_facilities"],
                           ["key":"Activities","icon":"activities"],
                           ["key":"My Bookings","icon":"my_booking"],
                           ["key":"Scan & Reminder","icon":"scan_reminder"],
                           ["key":"Survey","icon":"survey"],
                           ["key":"Meetings","icon":"my_booking"],
                           ["key":"Logout","icon":"side_menu_logout"]]
            }
            else
            {
                self.rowData = [["key":"Facilities","icon":"icon_facilities"],
                           ["key":"Activities","icon":"activities"],
                           ["key":"My Bookings","icon":"my_booking"],
                           ["key":"Scan & Reminder","icon":"scan_reminder"],
                           ["key":"Survey","icon":"survey"],
                           ["key":"Meetings","icon":"my_booking"],
                           ["key":"Logout","icon":"side_menu_logout"]]
            }
            
               
            self.tblMenu.reloadData()
            UIView.animate(views: self.tblMenu.visibleCells, animations: self.animations, completion: {
               
            })
            
        }
        
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    @objc func profileUpdate(_ notification: NSNotification)  {
        
        let ud = UserDefaults.standard
        if let val = ud.value(forKey: "UserData") as? Data,
        let obj = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(val) as? NSDictionary {
             print(obj)
            let userPDetail = obj.value(forKey: "user_details") as! NSDictionary
            let fname = userPDetail.value(forKey: "first_name") as! String
            let Lname = userPDetail.value(forKey: "last_name") as! String
            lblName.text = "\(String(describing: fname)) \(String(describing: Lname))"
            

            
        }
       
    }
    
    
    
    @IBAction func profileButtonClicked(_ sender: UIButton) {

        let nextViewController = mainStory.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    @IBAction func crossButtunClicked(_ sender: UIButton)
    {
        rowData.removeAll()
        UIView.animate(views: tblMenu.visibleCells, animations: animations, reversed: true,
                       initialAlpha: 1.0, finalAlpha: 0.0, completion: {
                        
            self.tblMenu.reloadData()
                       
        })
        self.dismiss(animated: true, completion: nil)
        
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return rowData.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.rowname.text = NSLocalizedString((rowData[indexPath.row] as [String:Any])["key"] as? String ?? "", comment: "")
        cell.rowImage.image = UIImage(named:(rowData[indexPath.row] as [String:Any])["icon"] as? String ?? "")
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if userData?.usertype == "1" {
            if indexPath.row == 0
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "ServiceChargeVC")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "FacalitiesDetailVC") as! FacalitiesDetailVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "ActivitiesVC") as! ActivitiesVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "MyBookingVC") as! MyBookingVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 4
            {
                let nextViewController = mainStory.instantiateViewController(withIdentifier: "Scan_Reminder") as! Scan_Reminder
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else if indexPath.row == 5
            {
                let nextViewController = mainStory.instantiateViewController(withIdentifier: "AllSurveyListVC") as! AllSurveyListVC
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else if indexPath.row == 6
            {
                let nextViewController = mainStory.instantiateViewController(withIdentifier: "MeetingVC") as! MeetingVC
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
                self.AlertController()
            }
            
            
        } else {
             if indexPath.row == 0
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "FacalitiesDetailVC") as! FacalitiesDetailVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 1
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "ActivitiesVC") as! ActivitiesVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 2
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "MyBookingVC") as! MyBookingVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.row == 3
            {
                let nextViewController = mainStory.instantiateViewController(withIdentifier: "Scan_Reminder") as! Scan_Reminder
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else if indexPath.row == 4
            {
                let nextViewController = mainStory.instantiateViewController(withIdentifier: "AllSurveyListVC") as! AllSurveyListVC
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else if indexPath.row == 5
            {
                let nextViewController = mainStory.instantiateViewController(withIdentifier: "MeetingVC") as! MeetingVC
                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
                self.AlertController()
            }
            
        }
        
        
    }
    
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message:NSLocalizedString("Are you sure you want to logout?", comment: "") , preferredStyle: .actionSheet)

        let OK = UIAlertAction(title:NSLocalizedString("Ok", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

              self.callLogoutApi()

            
        })
        let Cancel = UIAlertAction(title:NSLocalizedString("Cancel", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })

        optionMenu.addAction(OK)
        optionMenu.addAction(Cancel)
        
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    
    
    
    
    func changeLang()
    {
        /*
        let optionMenu = UIAlertController(title: "", message:NSLocalizedString("Change Language", comment: ""), preferredStyle: .actionSheet)
        let englishAct = UIAlertAction(title: "English", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            UIView.appearance().semanticContentAttribute            = UISemanticContentAttribute.forceLeftToRight
            Bundle.setLanguage("en")
            self.rootView()
            
        })
        
        let arabicAct = UIAlertAction(title:NSLocalizedString("Arabic", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            UIView.appearance().semanticContentAttribute            = UISemanticContentAttribute.forceRightToLeft
            Bundle.setLanguage("ar")
            self.rootView()
            
        })
        
        let Cancel = UIAlertAction(title:NSLocalizedString("Cancel", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        optionMenu.addAction(englishAct)
        optionMenu.addAction(arabicAct)
        optionMenu.addAction(Cancel)
        
        self.present(optionMenu, animated: true, completion: nil)
        */
    }
    
    
    func rootView()  {
       /*
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let scene = UIApplication.shared.connectedScenes.first
        if let sd : SceneDelegate = (scene?.delegate as? SceneDelegate)
        {
            let vc = storyboard.instantiateViewController (withIdentifier: "TabbarVC") as! TabbarVC
            let navigation = UINavigationController(rootViewController: vc)
            sd.window?.rootViewController = navigation
            sd.window?.makeKeyAndVisible()
        }
        */
        
    }
    
}
