//  SideMenuCell.swift
//  ALMEVENT
//  Created by ROOP KISHOR on 08/11/2020.


import UIKit

class SideMenuCell: UITableViewCell {
    
    @IBOutlet weak var rowname: UILabel!
    @IBOutlet weak var rowImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
