//  OTPVerificationVC.swift
//  Society Management
//  Created by ROOP KISHOR on 08/04/2021.


import UIKit

class OTPVerificationVC: UIViewController ,UITextFieldDelegate{
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var txt3: UITextField!
    @IBOutlet weak var txt4: UITextField!
    var userID = Int()
    var strMobile = String()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpView()
        self.className(ClassString: self)
    }
    
    
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func resendOtpClicked(_ sender: UIButton)
    {
        
        APIClient().PostdataRequest(WithApiName: API.resendotp.rawValue, Params: ["lang_id":language,"mobile":strMobile], isNeedLoader: true, objectType: MobileDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                    
                } else {
                    
                    ShowAlert(object.msg ?? "", on: "", from: self)
                }
                
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
    }
    
    
    
    @IBAction func verifyButtonClicked(_ sender: UIButton)
    {
        if txt1.text == "" || txt2.text == "" || txt3.text == "" || txt4.text == "" {
            
            ShowAlert(AlertMessage.validOtp, on: "", from: self)
            
        } else {
            
            self.otpVerfication()
        }
        
    }
    
    
    func setUpView()
    {
        
        self.headerViewHeight.constant = HeaderHeight
        self.txt1.delegate = self
        self.txt2.delegate = self
        self.txt3.delegate = self
        self.txt4.delegate = self
        
        self.txt1.layer.borderWidth = 1.0
        self.txt2.layer.borderWidth = 1.0
        self.txt3.layer.borderWidth = 1.0
        self.txt4.layer.borderWidth = 1.0
        
        self.txt1.layer.borderColor = UIColor.lightGray.cgColor
        self.txt2.layer.borderColor = UIColor.lightGray.cgColor
        self.txt3.layer.borderColor = UIColor.lightGray.cgColor
        self.txt4.layer.borderColor = UIColor.lightGray.cgColor
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            if !(string == "") {
                textField.text = string
                if textField == txt1
                {
                    txt2.becomeFirstResponder()
                }
                else if textField == txt2
                {
                    txt3.becomeFirstResponder()
                }
                else if textField == txt3 {
                    
                    txt4.becomeFirstResponder()
                }
                else {
                    
                    textField.resignFirstResponder()
                }
                
                return false
            }
            return true
        }

        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
            
            if (textField.text?.count ?? 0) > 0 {

            }
            
            return true
        }
    
    
    func otpVerfication()
    {
        self.view.endEditing(true)
       // let otp = String(format: "%@%@%@%@", txt1.text!,txt2.text!,txt3.text!,txt4.text!)
        let otp = "352026"
        APIClient().PostdataRequest(WithApiName: API.otpverification.rawValue, Params: ["id":userID,"lang_id":language,"otp":otp], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                    
                } else {
                    
                    ShowAlert(object.msg ?? "", on: "", from: self)
                }
                
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
        
    }
    
}
