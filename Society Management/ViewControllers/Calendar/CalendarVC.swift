/*
 * ViewController.swift
 * Created by Michael Michailidis on 01/04/2015.
 * http://blog.karmadust.com/
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */


import UIKit
import KDCalendar

class CalendarVC: MasterVC {
    
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var lblSelectedDate: UILabel!
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let spacingBetweenCells: CGFloat = 5
    var slots = Int()
    var timeSlotArr = [String]()
    var strSeatingID = String()
    var strDate = String()
    var strTimeSlot = String()
    var isTimeSelect :Bool = false
    var isFacality = String()
    
    var activity_id = String()
    var planID = String()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        PrintLog(strSeatingID)
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title12
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        
        self.collectionView.register(UINib(nibName: "SlotitemsCell", bundle: nil), forCellWithReuseIdentifier: "SlotitemsCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let style = CalendarView.Style()
        
        style.cellShape                = .round
        style.cellColorDefault         = UIColor.clear
        
        style.cellColorToday           = UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        style.cellSelectedBorderColor  = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        style.cellEventColor           = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        style.headerTextColor          = UIColor.black
        style.cellTextColorDefault     = UIColor.black
        style.cellTextColorToday       = UIColor.orange
        style.cellTextColorWeekend     = UIColor(red: 237/255, green: 103/255, blue: 73/255, alpha: 1.0)
        style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)
        style.headerBackgroundColor    = UIColor.white
        style.weekdaysBackgroundColor  = UIColor.white
        style.firstWeekday             = .sunday
        style.locale                   = Locale(identifier: "en_US")
        style.cellFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.headerFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        
        calendarView.style = style
        
        calendarView.dataSource = self
        calendarView.delegate = self
        
        calendarView.direction = .horizontal
        calendarView.multipleSelectionEnable = false
        calendarView.marksWeekends = false
        calendarView.backgroundColor = UIColor.clear
        
        slots = timeSlotArr.count
        if  slots  > 0 {
            
            if (slots) % 3 == 0  {
                collectionHeight.constant = 45 * CGFloat((slots)/3)
            }else if (slots) % 3 == 1
            
            {
                collectionHeight.constant = 45 * CGFloat(((slots) + 2)/3 )
            }
            else
            {
                collectionHeight.constant = 45 * CGFloat(((slots) + 1)/3 )
            }
            
        }
        else {
            
            collectionHeight.constant = 0
        }
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        let today = Date()
        
        let event = CalendarEvent(title: "Today", startDate: today, endDate: today)

        self.calendar(self.calendarView, didSelectDate: today, withEvents:[event])
        self.calendarView.setDisplayDate(today, animated: false)
        

        #if KDCALENDAR_EVENT_MANAGER_ENABLED
        self.calendarView.loadEvents() { error in
            if error != nil {
                let message = "The karmadust calender could not load system events. It is possibly a problem with permissions"
                let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        #endif
        
        
        
      
    }
    
    
    // MARK : Events
    
//    @IBAction func onValueChange(_ picker : UIDatePicker) {
//        self.calendarView.setDisplayDate(picker.date, animated: true)
//    }
    
    @IBAction func goToPreviousMonth(_ sender: Any) {
        self.calendarView.goToPreviousMonth()
    }
    @IBAction func goToNextMonth(_ sender: Any) {
        self.calendarView.goToNextMonth()
        
    }
    
    @IBAction func confirmBooking(_ sender: Any) {
        
        if isTimeSelect {
            
            if isFacality == "1" {
                self.bookFacality()
                
            } else {
              
                self.bookActivity()
                
            }
            
            
        } else {
            ShowAlert(AlertMessage.selectTimeSlot, on: "", from: self)
        }
        
        
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
}


extension CalendarVC: CalendarViewDataSource {
    
    func headerString(_ date: Date) -> String? {
        return nil
    }
    
    
      func startDate() -> Date {
          
          var dateComponents = DateComponents()
          dateComponents.month = 0
          let today = Date()
          let threeMonthsAgo = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
          return threeMonthsAgo
      }
      
      func endDate() -> Date {
          
          var dateComponents = DateComponents()
          dateComponents.month = 12
          let today = Date()
          let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
          return twoYearsFromNow
    
      }
    
}

extension CalendarVC: CalendarViewDelegate {
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date: Date) {
        
    }
    
    func calendar(_ calendar: CalendarView, canSelectDate date: Date) -> Bool {
        
        return true
        
    }
    
    
    func calendar(_ calendar: CalendarView, didDeselectDate date: Date)
    {
        
    }
    
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent])
    {
        print("Did Select: \(date) with \(events.count) events")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dateStr = dateFormatter.string(from: date)
        strDate = dateStr
        dateFormatter.dateFormat = "MMM dd, yyyy"
        lblSelectedDate.text = self.convertDateFormaterTo(dateStr)
           
    }
   
       
       func calendar(_ calendar: CalendarView, didLongPressDate date : Date, withEvents events: [CalendarEvent]?) {
           /*
           if let events = events {
               for event in events {
                   print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
               }
           }
           
           let alert = UIAlertController(title: "Create New Event", message: "Message", preferredStyle: .alert)
           
           alert.addTextField { (textField: UITextField) in
               textField.placeholder = "Event Title"
           }
           
           let addEventAction = UIAlertAction(title: "Create", style: .default, handler: { (action) -> Void in
               let title = alert.textFields?.first?.text
               self.calendarView.addEvent(title!, date: date)
           })
           
           let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
           
           alert.addAction(addEventAction)
           alert.addAction(cancelAction)
           
           self.present(alert, animated: true, completion: nil)
           */
       }
    
}
extension CalendarVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return timeSlotArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SlotitemsCell", for: indexPath) as! SlotitemsCell
        cell.lblSlotName.text = timeSlotArr[indexPath.row]
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CGFloat(screenWidth - 45)/3, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? SlotitemsCell {
            
            cell.backgroundColor = UIColor(named: "HeaderColor")
            cell.lblSlotName.textColor = UIColor.white
            strTimeSlot = timeSlotArr[indexPath.row]
            isTimeSelect = true
            
        }
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? SlotitemsCell {
            cell.backgroundColor = UIColor.clear
            cell.lblSlotName.textColor = UIColor.gray
        }
    }
    
    
    func bookFacality(){
       
        let params = ["uid":userData?.id,"towerid":userData?.towerid,"unitnumber":userData?.unitnumber,"seating_id":strSeatingID,"booking_date":strDate,"time_slot":strTimeSlot,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.facalityBooking.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.showCustumAlert(message: object.msg ?? "", isBack: true)
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                   
                }
                
            }
        }
        
    }
    
    
    func bookActivity() {
      
        let params = ["uid":userData?.id,"towerid":userData?.towerid,"unitnumber":userData?.unitnumber,"seating_id":strSeatingID,"booking_date":strDate,"time_slot":strTimeSlot,"lang_id":language,"activity_id":activity_id,"plan_id":planID]
        PrintLog(params)
        APIClient().PostdataRequest(WithApiName: API.activityBooking.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.showCustumAlert(message: object.msg ?? "", isBack: true)
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                   
                }
                
            }
        }
        
    }
    
}





