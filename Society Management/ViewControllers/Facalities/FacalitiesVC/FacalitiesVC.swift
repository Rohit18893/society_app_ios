//  FacalitiesVC.swift
//  Society Management
//  Created by ROOP KISHOR on 19/06/2021.


import UIKit

class FacalitiesVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    fileprivate let arrCells = [FacTableCell.self]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title14
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        // Do any additional setup after loading the view.
    }
    

   

}
extension FacalitiesVC: UITableViewDataSource,UITableViewDelegate {
    
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FacTableCell", for: indexPath) as! FacTableCell
        if indexPath.row == 0 {
            
            cell.headerLbl.text = "Wedding Venue"
        } else {
            cell.headerLbl.text = "Birthday Venue"
        }
        
        cell.viewAllButton.isHidden = false
        cell.vc = self
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
}
