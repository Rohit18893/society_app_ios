//  FacTableCell.swift
//  Society Management
//  Created by ROOP KISHOR on 19/06/2021.


import UIKit

class FacTableCell: UITableViewCell ,UICollectionViewDataSource, UICollectionViewDelegate{
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var vc: UIViewController?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        
        // Initialization code
        self.collectionView.register(UINib(nibName: "FaciItemCell", bundle: nil), forCellWithReuseIdentifier: "FaciItemCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // Configure the view for the selected state
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FaciItemCell", for: indexPath as IndexPath) as? FaciItemCell else { return UICollectionViewCell() }
        
        return cell
        
    }
}
extension FacTableCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 150 , height: 240)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsets(top: 5, left: 5, bottom:5, right: 5)
        return sectionInset
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FacalitiesDetailVC") as! FacalitiesDetailVC
        vc?.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
}
