//  FacalitiesDetailVC.swift
//  Society Managementme
//  Created by ROOP KISHOR on 20/06/2021.


import UIKit

class FacalitiesDetailVC: MasterVC {
    
    @IBOutlet weak var viewNAv: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblAfterDis: UILabel!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var imgFacility: UIImageView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewPrice: UIView!
    var facalityData = [FacalityData]()
    var isFirstTime = true
    
    var strTabData = String()
    var selectedIndex = Int()
    
    fileprivate let arrCells = [PhotoCell.self,OptionTblCell.self,DetailTblCell.self]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.viewNAv.clipsToBounds = true
        self.viewNAv.layer.cornerRadius = 25
        self.viewNAv.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate   = self
        self.tblView.dataSource = self
        self.collectionView.register(UINib(nibName: "SeatingCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SeatingCollectionCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        selectedIndex = 0
        viewPrice.isHidden = false
        self.className(ClassString: self)
        tblView.isHidden = true
        imgFacility.isHidden = true
        lblNoData.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(activityImageChange(notification:)), name: .ActivityImageChanged, object: nil)
    }
    
    
    @objc func activityImageChange(notification: NSNotification)
    {
        if let img = notification.object as? String {
            
            imgFacility.sd_setImage(with: URL(string:img), placeholderImage:UIImage(named: "No-image"))
            
        }
      
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: .ActivityImageChanged, object: nil)
    }
    
    
    @IBAction func backClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getFacalityDetails()
    }
    
    
    @IBAction func bookNowClicked(_ sender: UIButton)
    {
        if let timeslots = facalityData[0].time_slot {
            
            let array = timeslots.components(separatedBy: ",")
            let vc = self.mainStory.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            vc.timeSlotArr = array
            vc.strSeatingID = facalityData[0].seating?[selectedIndex].seating_id ?? ""
            vc.isFacality = "1"
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
       
    }
    
    func getFacalityDetails() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.facilitiesDetails.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: FacalityDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        if let  data = object.result
                        {
                            self.facalityData = data
                            self.strTabData = self.facalityData[0].details ?? ""
                            if let seatingData = self.facalityData[0].seating
                            {
                              // self.lblTotal.text = String(format: "AED %@", seatingData[0].price!)
                              // self.lblDiscount.text = "\(seatingData[0].discount!) %"
//                               let value = NSString(string: seatingData[0].price ?? "0").doubleValue
//                               let percentage = NSString(string: seatingData[0].discount ?? "0").doubleValue
//                               let perPay = (value * percentage)/100
//                               let netAmount = value - perPay
//                               self.lblAfterDis.text = String(format: "AED %0.1f", netAmount)
                            }
                            
                            if let imgArr = self.facalityData[0].facilities_images
                            {
                                let img = imgArr[0]
                                self.imgFacility.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "No-image"))
                            }
                            
                            
                        }
                        
                        self.lblNoData.isHidden = true
                        self.tblView.isHidden = false
                        self.imgFacility.isHidden = false
                        self.tblView.reloadData()
                        self.collectionView.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async
                {
                    self.lblNoData.isHidden = false
                    self.imgFacility.isHidden = true
                    self.tblView.isHidden = true
                    
                }
                 
            }
        }

    }

}

extension FacalitiesDetailVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
            if facalityData.count > 0 {
                if let images = facalityData[0].facilities_images {
                    cell.allImages = images
                    cell.collectionView.reloadData()
                }
            }
            
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OptionTblCell", for: indexPath) as! OptionTblCell
            cell.btnDetail.addTarget(self, action: #selector(self.tabButtonClicked), for: .touchUpInside)
            cell.btnCateoring.addTarget(self, action: #selector(self.tabButtonClicked), for: .touchUpInside)
            cell.btnDecoration.addTarget(self, action: #selector(self.tabButtonClicked), for: .touchUpInside)
            cell.btnPricing.addTarget(self, action: #selector(self.tabButtonClicked), for: .touchUpInside)
            if isFirstTime {
                
                cell.btnDetail.setTitleColor(UIColor.black, for: .normal)
                cell.btnCateoring.setTitleColor(UIColor.gray, for: .normal)
                cell.btnDecoration.setTitleColor(UIColor.gray, for: .normal)
                cell.btnPricing.setTitleColor(UIColor.gray, for: .normal)
            }
            
            
            return cell
            
        }
        else
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTblCell", for: indexPath) as! DetailTblCell
            if facalityData.count > 0
            {
                cell.lblTitle.text = facalityData[0].name
                let rating = facalityData[0].rating
                cell.starRating.rating  = NSString(string: rating ?? "0").doubleValue
                cell.starRating.isUserInteractionEnabled = false
                let str = strTabData.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
                cell.lblDesc.text = str
            }
            
            return cell
            
        }
        
       
    }
    
    @objc func tabButtonClicked(_ sender: UIButton)
    {
        isFirstTime = false
        let indexPath = NSIndexPath(row:1, section: 0)
        let cell = self.tblView.cellForRow(at:indexPath as IndexPath) as! OptionTblCell
        
        let indexPath2 = NSIndexPath(row:2, section: 0)
        let cell2 = self.tblView.cellForRow(at:indexPath2 as IndexPath) as! DetailTblCell
        
        if sender.tag == 10
        {
            cell2.lblSeatingArragment.isHidden = false
            viewPrice.isHidden = false
            PrintLog("btnDetail")
            cell.btnDetail.setTitleColor(UIColor.black, for: .normal)
            cell.btnCateoring.setTitleColor(UIColor.gray, for: .normal)
            cell.btnDecoration.setTitleColor(UIColor.gray, for: .normal)
            cell.btnPricing.setTitleColor(UIColor.gray, for: .normal)
            strTabData = facalityData[0].details ?? ""
            
        }
        else if sender.tag == 20
        {
            cell2.lblSeatingArragment.isHidden = true
            viewPrice.isHidden = true
            PrintLog("btnCateoring")
            cell.btnDetail.setTitleColor(UIColor.gray, for: .normal)
            cell.btnCateoring.setTitleColor(UIColor.black, for: .normal)
            cell.btnDecoration.setTitleColor(UIColor.gray, for: .normal)
            cell.btnPricing.setTitleColor(UIColor.gray, for: .normal)
            strTabData = facalityData[0].catering ?? ""
            
        }
        else if sender.tag == 30
        {
            cell2.lblSeatingArragment.isHidden = true
            viewPrice.isHidden = true
            PrintLog("btnDecoration")
            cell.btnDetail.setTitleColor(UIColor.gray, for: .normal)
            cell.btnCateoring.setTitleColor(UIColor.gray, for: .normal)
            cell.btnDecoration.setTitleColor(UIColor.black, for: .normal)
            cell.btnPricing.setTitleColor(UIColor.gray, for: .normal)
            strTabData = facalityData[0].decoration ?? ""
        }
        else
        {
            cell2.lblSeatingArragment.isHidden = true
            viewPrice.isHidden = true
            PrintLog("btnPricing")
            cell.btnDetail.setTitleColor(UIColor.gray, for: .normal)
            cell.btnCateoring.setTitleColor(UIColor.gray, for: .normal)
            cell.btnDecoration.setTitleColor(UIColor.gray, for: .normal)
            cell.btnPricing.setTitleColor(UIColor.black, for: .normal)
            strTabData = facalityData[0].pricing ?? ""
        }
        
        tblView.reloadData()
        
    }
    
}

extension FacalitiesDetailVC: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if facalityData.count > 0 {
            
            return facalityData[0].seating?.count ?? 0
        }
        else
        {
            return 0
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeatingCollectionCell", for: indexPath as IndexPath) as? SeatingCollectionCell else { return UICollectionViewCell() }
        
        if let seatData = facalityData[0].seating
        {
            cell.lblSeatTitle.text = seatData[indexPath.row].title
            if indexPath.row == selectedIndex {
                cell.btnRadio.setImage(UIImage(named: "radio_active"), for: .normal)
            }
            else
            {
                cell.btnRadio.setImage(UIImage(named: "radio_unactive"), for: .normal)
            }
            
            cell.btnRadio.tag = indexPath.row
            cell.btnRadio.addTarget(self, action: #selector(self.radioButtunClicked), for: .touchUpInside)
            
        }
        
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:(screenWidth - 30)/2 , height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
        return sectionInset
        
    }
    
    
    @objc func radioButtunClicked(_ sender: UIButton)
    {
        selectedIndex  = sender.tag
        self.collectionView.reloadData()
       // let indexPath = NSIndexPath(row:sender.tag, section: 0)
      //  if let seatingData = facalityData[0].seating {
            
//           self.lblTotal.text = String(format: "AED %@", seatingData[indexPath.row].price!)
//           self.lblDiscount.text = "\(seatingData[indexPath.row].discount!) %"
//           let value = NSString(string: seatingData[indexPath.row].price ?? "0").doubleValue
//           let percentage = NSString(string: seatingData[indexPath.row].discount ?? "0").doubleValue
//           let perPay = (value * percentage)/100
//           let netAmount = value - perPay
//           self.lblAfterDis.text = String(format: "AED %0.1f", netAmount)
           
     //   }
        
        
        
        
    }
    
   
}
