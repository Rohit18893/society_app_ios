//  SeatingCollectionCell.swift
//  Society Management
//  Created by ROOP KISHOR on 20/06/2021.


import UIKit

class SeatingCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblSeatTitle: UILabel!
    @IBOutlet weak var lblSeat: UILabel!
    @IBOutlet weak var btnRadio: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

}
