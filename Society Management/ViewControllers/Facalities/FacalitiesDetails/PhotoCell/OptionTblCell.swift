//  OptionTblCell.swift
//  Society Management
//  Created by ROOP KISHOR on 20/06/2021.


import UIKit

class OptionTblCell: UITableViewCell {
    
    @IBOutlet weak var btnDetail: UIButton!
    @IBOutlet weak var btnCateoring: UIButton!
    @IBOutlet weak var btnDecoration: UIButton!
    @IBOutlet weak var btnPricing: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
