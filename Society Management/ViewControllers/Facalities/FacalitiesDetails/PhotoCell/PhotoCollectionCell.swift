//  PhotoCollectionCell.swift
//  Society Management
//  Created by ROOP KISHOR on 20/06/2021.


import UIKit

class PhotoCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

}
