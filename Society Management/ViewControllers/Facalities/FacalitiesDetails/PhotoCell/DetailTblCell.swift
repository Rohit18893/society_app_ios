//  DetailTblCell.swift
//  Society Management
//  Created by ROOP KISHOR on 20/06/2021.


import UIKit
import Cosmos

class DetailTblCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var starRating: CosmosView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblSeatingArragment: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        
       
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
   
}

