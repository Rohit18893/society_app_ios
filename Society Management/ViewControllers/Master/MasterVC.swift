//  MasterVC.swift
//  Society Gate keeper
//  Created by Jitendra Yadav on 07/04/21.


import UIKit


import MaterialComponents

var appcolor = #colorLiteral(red: 0.4447140694, green: 0.8065553904, blue: 0.7541809678, alpha: 1)



class MasterVC : UIViewController {
        
    let mainStory = UIStoryboard.init(name: "Main", bundle: nil)
    let imageport = "https://ubcared.com/"
    var imageView = UIImageView()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.className(ClassString: self)
        NotificationCenter.default.addObserver(self,selector: #selector(statusManager),name: .flagsChanged,object: nil)
    }
    deinit
    {
        NotificationCenter.default.removeObserver(self, name: .flagsChanged, object: nil)
    }
    
    
    func updateUserInterface() {
        switch Network.reachability.status {
        case .unreachable:
            print("No Connection")
        //            let vc = NoInterNet()
        //            vc.modalPresentationStyle = .overFullScreen
        //            vc.modalTransitionStyle = .crossDissolve
        //            vc.done = { [weak self] in
        //                DispatchQueue.main.async {
        //
        //                    self?.updateUserInterface()
        //                }
        //            }
        //            self.present(vc, animated: true, completion: nil)
        
        case .wwan:
            print("Reachability Summary")
            print("Reachable:", Network.reachability.isReachable)
        case .wifi:
            print("Reachable:", Network.reachability.isReachable)
            print("Wifi:", Network.reachability.isReachableViaWiFi)
        //  view.backgroundColor = .green
        }
        print("Reachability Summary")
        print("Status:", Network.reachability.status)
        print("HostName:", Network.reachability.hostname ?? "nil")
        print("Reachable:", Network.reachability.isReachable)
        print("Wifi:", Network.reachability.isReachableViaWiFi)
    }
    
    @objc func statusManager(_ notification: Notification) {
        updateUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.updateUserInterface()
    }
    
    var myProfileImage:UIImage
    {
        return getImage(key: "myprofile")
    }
    
    func setImage(image : UIImage , key:String) {
        UserDefaults.standard.set(image.jpegData(compressionQuality: 100), forKey: key)
    }
    
    func getImage(key:String) -> UIImage {
        
        if let imageData = UserDefaults.standard.value(forKey: key) as? Data {
            if let imageFromData = UIImage(data: imageData){
                return imageFromData
            }
        }
        return #imageLiteral(resourceName: "profile_icon")
    }
    
    func callNumber(phoneNumber: String) {
        
        guard let url = URL(string: "telprompt://\(phoneNumber)"),
              UIApplication.shared.canOpenURL(url) else {
            return
        }
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func setTextFieldTheme(txtField:MDCOutlinedTextField,labelText:String,allowEditing:Bool) {
        
        txtField.isUserInteractionEnabled = allowEditing
        txtField.label.text = labelText
        
        txtField.setOutlineColor(UIColor.lightGray, for: .normal)
        txtField.setOutlineColor(UIColor.red, for: .editing)
        txtField.setOutlineColor(UIColor.lightGray, for: .disabled)
        txtField.setNormalLabelColor(UIColor.lightGray, for: .normal)
        
        txtField.setTextColor(.black, for: .normal)
        txtField.setTextColor(.red, for: .editing)
        txtField.setTextColor(.black, for: .disabled)
        
        txtField.setFloatingLabelColor(.red, for: .editing)
        txtField.setFloatingLabelColor(.lightGray, for: .normal)
        txtField.sizeToFit()
        
    }
    
    func makeBlureView()
    {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubviewToBack(blurEffectView)
    }
    
    func accessContactList() {
        requestAccess { (responce) in
            if responce {
                print("Contacts Acess Granted")
            } else {
                print("Contacts Acess Denied")
            }
        }
        
        authorizationStatus { (status) in
            switch status {
            case .authorized:
                print("authorized")
                break
            case .denied:
                print("denied")
                break
            default:
                break
            }
        }
    }
    
    
    
    func showCustumAlert(message:String, isBack:Bool)
    {
        let vc = AlertVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.alertmessage = message
        vc.isBack = isBack
        vc.onClickOk = { value in
            print("ROOP\(value)")
            if isBack {
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    func calculatePayableAmount(_ amount:String, percentageCharge:Double, extraCharge:Double) -> Double {
        
        let actualAmt = Double(amount) ?? 0.0
        let percentage =  (actualAmt * percentageCharge) / 100
        let   amountToPay = percentage + actualAmt + extraCharge
        return amountToPay.rounded(digits: 2)
    }
    
    
    func convertFormater(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    
    func convertDateFormater(_ date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date!)
        
    }
    
    func convertDateFormaterTo(_ date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM dd, yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    
    func convert12HoursTo24HoursFormat(time:String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from:time)
        dateFormatter.dateFormat = "HH:mm"
        let Date24 = dateFormatter.string(from: date!)
        return Date24
    }
    
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<length).map{ _ in letters.randomElement()! })
    }
    
    func isValidPhone(testStr:String) -> Bool {
        let phoneRegEx = "^[3-9]\\d{9}$"
        let phoneNumber = NSPredicate(format:"SELF MATCHES %@", phoneRegEx)
        return phoneNumber.evaluate(with: testStr)
    }
    
    @objc func goToNotification(){
        self.tabBarController?.selectedIndex = 4
    }
    
    //    @IBAction func back(_ sender: UIButton) {
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setBorder(view : Any , width : CGFloat,color : UIColor) {
        
        //        if #available(iOS 13.0, *) {
        (view as AnyObject).layer?.masksToBounds = false
        (view as AnyObject).layer?.borderWidth = width
        (view as AnyObject).layer?.borderColor = color.cgColor
        //
        //        } else {
        //            // Fallback on earlier versions
        //        }
        //
    }
    
    
    func timeStampToDate(timeStamp:Double)->String
    {
        let date = Date(timeIntervalSince1970:timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        let localDate = dateFormatter.string(from: date)
        return localDate
        
    }
    
    func dateFromTimestamp(timeStamp:Double)->Date
    {
        let date = Date(timeIntervalSince1970:timeStamp)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = .current
        return date
        
    }
    
    
    func getCurrentDateWithTime()->String
    {
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    
    func viewShadow(view: Any){
        
        
        (view as AnyObject).layer?.masksToBounds = false
        (view as AnyObject).layer?.shadowColor = UIColor.gray.cgColor
        (view as AnyObject).layer?.shadowOpacity = 0.8
        (view as AnyObject).layer?.shadowOffset = CGSize(width: -1, height: 1)
        (view as AnyObject).layer?.shadowRadius = 10
        
        
    }
    
    
    func shareWithFriends(Name:String,address:String){
        DispatchQueue.main.async {
            let textToShare = "\(Name) has invited you to download the ALM for \(address). Get The ALM APP From:"
            if let myWebsite = URL(string: "http://onelink.to/jp9g2g") {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                activityVC.popoverPresentationController?.sourceView = self.view
                activityVC.isModalInPresentation = true
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    
    
    func shareWithFriends(shareAddress:String){
        DispatchQueue.main.async {
            let textToShare = shareAddress
            let objectsToShare = [textToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            activityVC.popoverPresentationController?.sourceView = self.view
            activityVC.isModalInPresentation = true
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func shareWithFriends(){
        DispatchQueue.main.async {
            let textToShare = "Hi there! We've been using the ALM Society App in our society and find it really useful. It's great for managing guest entries, finding the best maids/cooks, making utility payments and more. Strongly recommend for your society."
            if let myWebsite = URL(string: "http://onelink.to/jp9g2g") {
                let objectsToShare = [textToShare, myWebsite] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
                activityVC.popoverPresentationController?.sourceView = self.view
                activityVC.isModalInPresentation = true
                self.present(activityVC, animated: true, completion: nil)
            }
        }
    }
    
//    func logoutUser(){
//        if let vc = self.mainStory.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC {
//            if let navigator = self.navigationController {
//                navigator.pushViewController(vc, animated: false)
//            }
//        }
//    }
    
    func callLogoutApi() {
        let params = ["lang_id":language,"uid":userData?.id,"devicetoken":deviceToken,"deviceid":deviceId]
        APIClient().PostdataRequest(WithApiName: API.logout.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ActivityData.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object)
                        UserDefaults.standard.removeObject(forKey: "userDeatils")
                        UserDefaults.standard.removeObject(forKey: "isConnectResidence")
                        UserDefaults.standard.removeObject(forKey: "myprofile")
                        let nextViewController = self.mainStory.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.navigationController?.pushViewController(nextViewController, animated: true)
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                DispatchQueue.main.async {
                    
                   
                }
                
            }
        }
        
    }
    
    func gradientBackgroundColors(view: UIView){
        //rgb(13,174,183)
        //rgb(26,225,203)
        let firstColor =  UIColor(red:13/255, green: 174/255, blue:183/255, alpha:1.0).cgColor
        let secondColor = UIColor(red:26/255, green:225/255, blue:203/255, alpha:1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [ secondColor, firstColor]
        gradientLayer.locations = [ 0.0, 1.0]
        
        //            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.0)
        //            gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.frame = view.bounds
        
        view.layer.addSublayer(gradientLayer)
        
    }
    
    func setUserDetails(key: String ,value: Any){
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func getUserDetails(key : String)-> Any{
        let detail = UserDefaults.standard.object(forKey: key) as Any
        print(detail)
        return detail
    }
    
    func getToken() -> NSDictionary {
        let tok = UserDefaults.standard.string(forKey: "api_token")
        let Auth_header   = [ "Authorization" : tok ]
        return Auth_header as NSDictionary
    }
    
    func getDeviceSize() ->CGFloat{
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                return 70
            case 1334:
                print("iPhone 6/6S/7/8")
                return 75
            case 1920, 2208:
                print("iPhone 6+/6S+/7+/8+")
                return 80
            case 2436:
                print("iPhone X, Xs")
                return 85
            case 2688:
                print("iPhone Xs Max")
                return 90
            case 1792:
                print("iPhone Xr")
                return 85
            default:
                print("Default Case")
                return 64
            }
        }else if UIDevice().userInterfaceIdiom == .pad{
            // for tab
            switch UIScreen.main.nativeBounds.height {
            case 1366:
                print("iPadPro")
                return 90
            case 2732:
                print("iPadPro 12")
                return 90
            default:
                print("Default Case")
                return 90
            }
            
        }else{
           
            return 64
        }
    }
    
    func popUpAlert(title: String,message: String,actiontitle: [String],actionstyle: [UIAlertAction.Style],action: [(UIAlertAction) -> Void ] ) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for(index,title) in actiontitle.enumerated() {
            let action = UIAlertAction(title: title, style: actionstyle[index], handler: action[index])
            
            alert.addAction(action)
        }
        present(alert, animated: true, completion: nil)
        
    }
    
    func daysDifferences(from: String, to: String) -> Int? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        guard
            let date1 = formatter.date(from: from),
            let date2 = formatter.date(from: to)
        else {
            return nil
        }
        
        return Calendar.current.dateComponents([.day], from: date1, to: date2).day
    }
    
}



extension UIView {
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}



extension String {
    public func getAcronyms(_ separator: String = "") -> String {
        let components = self.components(separatedBy: .whitespacesAndNewlines)
        let words = components.filter { !$0.isEmpty }
        if words.count == 1 {
            if self.count > 3 {
                let result = String(self.prefix(3))
                return result.uppercased()
            }else{
                let result = self
                return result.uppercased()
            }
        }else {
            let acronyms = self.components(separatedBy: " ").map({ String($0.first ?? " ") }).joined(separator: separator);
            return acronyms.uppercased()
        }
    }
}
