//  MessageGuardVC.swift
//  Society Management
//  Created by ROOP KISHOR on 03/05/2021.


import UIKit

class MessageGuardVC: MasterVC ,UITextViewDelegate{
    
    @IBOutlet weak var txtMessage: UITextView!
    @IBOutlet weak var messageLimit: UILabel!
    
    var charCount1 = 0
    let maxLength1 = 250

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.txtMessage.layer.cornerRadius = 6
        self.txtMessage.borderWidth = 0.5
        self.txtMessage.borderColor = UIColor.lightGray
        txtMessage.text = AlertMessage.messagePlaceHolder
        txtMessage.textColor = UIColor.lightGray
        txtMessage.delegate = self
        self.className(ClassString: self)
        self.makeBlureView()
        
    }

    @IBAction func messageClicked(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneButtonClick(_ sender: UIButton)
    {
        guard let msgBlank = txtMessage.text , msgBlank.isBlank == false  else {
            ShowAlert(AlertMessage.enterComment, on: "", from: self)
            return
        }
        
       
        if txtMessage.text == AlertMessage.messagePlaceHolder {
            
            ShowAlert(AlertMessage.enterComment, on: "", from: self)
            return
        }
        
        self.view.endEditing(true)
        let params = ["uid":userData?.id ?? "","towerid":userData?.towerid ?? "","lang_id":language,"unitnumber":userData?.unitnumber ?? "","content":txtMessage.text ?? ""] as [String :Any]
        APIClient().PostdataRequest(WithApiName: API.msgtoguard.rawValue, Params: params, isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                       
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                       ])
                        
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
        
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if textView.text == "" {

            textView.text = AlertMessage.messagePlaceHolder
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "" {
            if textView.text.count == 0 {
                charCount1 = 0
                messageLimit.text = String(format: "%i", maxLength1 - charCount1)
                return false
            }
            charCount1 = (textView.text.count - 1)
            messageLimit.text = String(format: "%i", maxLength1 - charCount1)
            return true
        } else {
            charCount1 = (textView.text.count + 1)
            messageLimit.text = String(format: "%i", maxLength1 - charCount1)
            if charCount1 >= maxLength1 + 1  {
                charCount1 = maxLength1
                messageLimit.text = String(format: "%i", maxLength1 - charCount1)
                return false;
            }
        }
        return true
      }
   
}
