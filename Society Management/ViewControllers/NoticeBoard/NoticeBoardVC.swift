//  NoticeBoardVC.swift
//  Society Management
//  Created by Jitendra Yadav on 03/05/21.


import UIKit

class NoticeBoardVC: MasterVC {
    
    @IBOutlet weak var addFliterTextLbl: UILabel!
    @IBOutlet weak var addFilterTextView: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var lblNoData: UILabel!
    
    fileprivate let arrCells = [NoticeBoardCell.self,noticBoardHeaderCell.self]
    
    var filterData = [EmergencyData]()
    var noticeBoardData = [NoticeData]()
    var readStatus = String()
    var filterTypeID = String()
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        filterTypeID = ""
        readStatus = "1"
        self.viewNAv.lbltitle.text = AlertMessage.title20
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        
//        self.viewNAv.btnOption.isHidden = false
//        self.viewNAv.completionHandler = {
//            PrintLog("Search Click")
//        }
        
        lblNoData.isHidden = true
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        self.filterByType()
        self.getNoticeBoardList(withType: filterTypeID, isRead: readStatus)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        
        if #available(iOS 10.0, *) {
            
            tblView.refreshControl = refreshControl
            
        } else {
            
            tblView.addSubview(refreshControl)
        }
        
    }
    
    
    @objc func refresh(_ refresh: UIRefreshControl)
    {
        self.getNoticeBoardList(withType:filterTypeID, isRead: readStatus)
        refresh.endRefreshing()
    }
    
    @IBAction func unreadButton(_ sender: UIButton) {
        if sender.isSelected
        {
            readStatus = "1"
            self.getNoticeBoardList(withType:filterTypeID, isRead: readStatus)
            sender.isSelected = false
        } else
        {
            readStatus = "0"
            sender.isSelected = true
            self.getNoticeBoardList(withType:filterTypeID, isRead: readStatus)
        }
    }
    
    @IBAction func filterRemoveButton(_ sender: Any) {
        self.addFilterTextView.isHidden = true
        self.addFliterTextLbl.text = ""
        filterTypeID = ""
        self.getNoticeBoardList(withType: filterTypeID, isRead: readStatus)
    }
    
    @IBAction func filterButton(_ sender: Any) {
        if  let noticetype = filterData.map({$0.name }) as? [String] ,
            let noticeid = filterData.map({$0.id}) as? [String] {
            CustomPickerView.show(items: noticetype , itemIds: noticeid ,selectedValue:nil, doneBottonCompletion: { (item, index) in
                
                self.addFliterTextLbl.text = item
                self.addFilterTextView.isHidden = false
                if let index = index {
                    
                    self.filterTypeID = index
                    self.getNoticeBoardList(withType: self.filterTypeID, isRead: self.readStatus)
                }
            }, didSelectCompletion: { (item, index) in
                
            }) { (item, index) in
            }
        }
    }
   
}

extension NoticeBoardVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return noticeBoardData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticeBoardCell", for: indexPath) as! NoticeBoardCell
        cell.lblName.text = noticeBoardData[indexPath.row].name
        cell.lblType.text = noticeBoardData[indexPath.row].notice_type
        cell.lblDesc.text = noticeBoardData[indexPath.row].description?.html2String
        let stamp = noticeBoardData[indexPath.row].created_date
        cell.lblDate.text = convertTimeStampToDate(timeStamp: stamp!)
        if noticeBoardData[indexPath.row].image?.count ?? 0 == 0
        {
            cell.attachment.isHidden = true
        }
        else
        {
            cell.attachment.isHidden = false
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "NoticeDetailsVC") as! NoticeDetailsVC
        vc.strNoticeID = noticeBoardData[indexPath.row].id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func filterByType() {
        
        let params = ["lang_id":language,"towerid":userData?.towerid]
        APIClient().PostdataRequest(WithApiName: API.noticetype.rawValue, Params: params as [String : Any], isNeedLoader: false, objectType: EmergencyDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        
                        self.filterData = (object.data)!
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                
            }
        }
        
    }
    
    
    func getNoticeBoardList(withType:String,isRead:String) {
        
       
        let params = ["lang_id":language,"towerid":userData?.towerid,"notice_type":withType,"uid":userData?.id,"is_read":isRead]
        APIClient().PostdataRequest(WithApiName: API.noticeboardlist.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: NoticeModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.noticeBoardData.removeAll()
                        self.noticeBoardData = (object.data)!
                        self.tblView.reloadData()
                        self.lblNoData.isHidden = true
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.noticeBoardData.removeAll()
                    self.lblNoData.isHidden = false
                    self.tblView.reloadData()
                }
                
            }
        }
        
    }
    
}


