//  NoticeDetailsVC.swift
//  Society Management
//  Created by Jitendra Yadav on 03/05/21.


import UIKit

class NoticeDetailsVC: MasterVC {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    var detailsData : NoticeData?
    var strNoticeID = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title20
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false

        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.getNoticeBoardDetail()
        
    }

   
  
    func getNoticeBoardDetail() {
        
   
        let params = ["lang_id":language,"id":strNoticeID,"uid":userData?.id,"towerid":userData?.towerid,"unitnumber":userData?.unitnumber]
        APIClient().PostdataRequest(WithApiName: API.noticeboarddetail.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: DetailsModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        
                        self.detailsData = object.data
                        self.tblView.reloadData()
                      
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }

    }
    
    
}

extension NoticeDetailsVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticeDetailsCell", for: indexPath) as! NoticeDetailsCell
        
        cell.lblName.text = detailsData?.name
        cell.lblType.text = detailsData?.notice_type
        cell.lblDesc.text = detailsData?.description?.html2String
        let stamp = detailsData?.created_date
        cell.lblDate.text = convertTimeStampToDate(timeStamp: stamp ?? 00)
        if let img = detailsData?.image {
            cell.noticeImage.isHidden = false
            cell.noticeImage.sd_setImage(with: URL(string: img))
        }else{
            cell.noticeImage.isHidden = true
            
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    
}


//MARK: Notice Details Cell Here

class NoticeDetailsCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var noticeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
