//
//  noticBoardHeaderCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 03/05/21.
//

import UIKit

class noticBoardHeaderCell: UITableViewCell {
    
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnSwitch: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
