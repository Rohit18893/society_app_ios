//  ServiceChargeVC.swift
//  Society Management
//  Created by ROOP KISHOR on 26/04/2021.


import UIKit

class ServiceChargeVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var lblDue: UILabel!
    @IBOutlet weak var lblDeposite: UILabel!
    @IBOutlet weak var lblAdvance: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var btnTransaction: UIButton!
    var serviceCharge : SericeChargeData?


    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title29
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        lblNoData.isHidden = false
        self.getServiceCharge()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getServiceCharge()
    }
    
    
    @IBAction func transactions(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "TransactionHistoryVC") as! TransactionHistoryVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getServiceCharge() {
        
        let params = ["uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.servicecharge.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: SericeChargeModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        if let data = object.data  {
                            self.serviceCharge = data
                            self.lblDue.text = self.serviceCharge?.dueamount
                            self.lblDeposite.text = self.serviceCharge?.deposite
                            self.lblAdvance.text = self.serviceCharge?.totalamount
                            if let invoices = self.serviceCharge?.invoices {
                                if invoices.count > 0 {
                                    
                                    self.lblNoData.isHidden = true
                                }
                                else  {
                                    self.lblNoData.isHidden = false
                                }
                            }
                            self.tblView.reloadData()
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }
        }
    }
}

extension ServiceChargeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return serviceCharge?.invoices?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceChargeCell", for: indexPath) as! ServiceChargeCell
        cell.viewBack.layer.cornerRadius = 5
        cell.lblInvoiceName.text = self.serviceCharge?.invoices?[indexPath.row].title
        cell.lblInvoiceNum.text =  self.serviceCharge?.invoices?[indexPath.row].invoice_number
        cell.lblFrequency.text = self.serviceCharge?.invoices?[indexPath.row].frequency?.capitalized
        
        if let fromDate = self.serviceCharge?.invoices?[indexPath.row].from_date {
            
            let strFrom = TimeStampToDate(timeStamp: fromDate)
            let strTo = TimeStampToDate(timeStamp: self.serviceCharge?.invoices?[indexPath.row].to_date ?? 0)
            cell.lblPeriod.text = "\(strFrom) To \(strTo)"
        }
        
        if let amt = self.serviceCharge?.invoices?[indexPath.row].amount {
            cell.lblAmount.text = "AED \(amt)"
        }
        
        
        cell.btnTag.setTitle(self.serviceCharge?.invoices?[indexPath.row].tag, for: .normal)

        if let dt = self.serviceCharge?.invoices?[indexPath.row].issue_date {
            cell.lblIssueDate.text = TimeStampToDate(timeStamp: dt)
        }
        
        if let dt = self.serviceCharge?.invoices?[indexPath.row].duedate
        {
            cell.lblDueDate.text = TimeStampToDate(timeStamp: dt)
        }
        
        cell.btnPayNow.tag = indexPath.row
        cell.btnPayNow.addTarget(self, action: #selector(self.paynowClicked), for: .touchUpInside)
        if self.serviceCharge?.invoices?[indexPath.row].paid_status == "1"
        {
            cell.btnPayNow.setTitle("PAID", for: .normal)
            cell.btnPayNow.isUserInteractionEnabled = false
        }
        else
        {
            cell.btnPayNow.setTitle("PAY NOW", for: .normal)
            cell.btnPayNow.isUserInteractionEnabled = true
        }
        
        cell.btnViewInvoice.tag = indexPath.row
        cell.btnViewInvoice.addTarget(self, action: #selector(self.viewInvoice), for: .touchUpInside)
        return cell
        
    }
    
    
    @objc func paynowClicked(_ sender: UIButton) {
        
        if let data = self.serviceCharge?.invoices?[sender.tag] {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "PaymentDetailsVC") as! PaymentDetailsVC
            vc.invoiceDetails = data
            vc.percentageCharge = (self.serviceCharge?.payment_charge)?.toDouble() ?? 0.0
            vc.extraCharge = (self.serviceCharge?.extra_charge)?.toDouble() ?? 0.0
            self.navigationController?.pushViewController(vc, animated: true)
           
        }
    }
    
    
    @objc func viewInvoice(_ sender: UIButton) {
        
        if let data = self.serviceCharge?.invoices?[sender.tag] {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "ViewInvoiceVC") as! ViewInvoiceVC
            self.navigationController?.pushViewController(vc, animated: true)
           
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    
}


