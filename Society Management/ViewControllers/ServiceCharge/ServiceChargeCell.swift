//
//  ServiceChargeCell.swift
//  Society Management
//
//  Created by ROOP KISHOR on 26/04/2021.
//

import UIKit

class ServiceChargeCell: UITableViewCell {
    
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblInvoiceName: UILabel!
    @IBOutlet weak var lblIssueDate: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var btnTag: UIButton!
    @IBOutlet weak var btnPayNow: UIButton!
    @IBOutlet weak var btnViewInvoice: UIButton!
    @IBOutlet weak var lblInvoiceNum: UILabel!
    @IBOutlet weak var lblPeriod: UILabel!
    @IBOutlet weak var lblFrequency: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
