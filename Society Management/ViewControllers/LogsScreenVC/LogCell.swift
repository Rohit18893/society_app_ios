//  LogCell.swift
//  Society Management
//  Created by ROOP KISHOR on 05/08/2021.


import UIKit

class LogCell: UITableViewCell {

    @IBOutlet weak var outTimeLbl: UILabel!
    @IBOutlet weak var intimeLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
