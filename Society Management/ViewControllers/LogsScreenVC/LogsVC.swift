//  LogsVC.swift
//  Society Management
//  Created by Jitendra Yadav on 07/06/21.


import UIKit

struct LogsModel:Codable {
    var data: [LogsData]?
    var msg: String?
}
struct LogsData:Codable {
    var intime: String?
    var outtime: String?
}

class LogsVC: MasterVC {

    @IBOutlet weak var activityName: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var uniqeCodeLbl: UILabel!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var icons: UIImageView!
    @IBOutlet weak var messageIcon: UIImageView!
    
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var noEntryLogsView: UIView!
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    fileprivate let arrCells = [LogCell.self]
    
    var logsTitle = "Entry/Exit Logs"
    var logsData:[LogsData]?
    var data:RecentActivityData?
    var result:Added_data?
    
    var icon = UIImage()
    var isRecentActivit = true
    
    var vehicelNUmber:String {
        return data?.vehicle_number?.count ?? 0 > 0 ? "#\(data?.vehicle_number ?? "") • " : ""
    }
    var serviceName:String {
        return data?.service_name?.count ?? 0 > 0 ? " • \(data?.service_name ?? "")" : ""
    }
    var companyName:String {
        return data?.company_name?.count ?? 0 > 0 ? " • \(data?.company_name ?? "")" : ""
    }
    
    override func viewDidLoad() {
        
        //  print(result!)
        
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title15
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
        self.tblView.tableFooterView = UIView()
        self.className(ClassString: self)
        self.noEntryLogsView.isHidden = true
        
        self.getLogs(data?.valid_code ?? result?.tagid ?? "")
        
        if isRecentActivit {
            uniqeCodeLbl.text = ""
            if let title =  data?.title?.capitalized  {
                let fullNameArr = title.components(separatedBy: " •")
                self.activityName.text = title.count > 0 ? "\(fullNameArr.first ?? "")\(serviceName)".uppercased() : ""
            }
            if let timeValid = data?.time_valid {
                timeLbl.text = timeValid.count > 0 ? timeValid : " "
            }
            if let description = data?.description {
                let rs = description.count > 0 ? "\(description)\(companyName.uppercased())" : ""
                messageLbl.text = rs
                messageView.isHidden = rs.count > 0 ? false : true
            }
            if let added_by = data?.added_by {
                let rs = added_by.count > 0 ? added_by:""
                statusLbl.text = rs
                statusView.isHidden = rs.count > 0 ? false : true
            }
            icons.image = icon
        }else{
            messageView.isHidden = true
            statusView.isHidden = true
            self.viewNAv.lbltitle.text = AlertMessage.title15
            
            if let title =  result?.name?.capitalized  {
                self.activityName.text = title
            }
            if let number =  result?.number?.uppercased() , number.count > 0  {
                self.timeLbl.text = number
            }else{
                self.timeLbl.isHidden = true
                uniqeCodeLbl.text = "#\(result?.tagid ?? "")"
            }
            if let img = result?.image , img.count > 0 {
                icons.sd_setImage(with: URL(string: img), placeholderImage: icon)
            }else{
                icons.image = icon
            }
        }
    }
    
    
    func getLogs(_ passcode:String){
        let params = ["passcode":passcode,
                      "lang_id":language]
        debugPrint(params)
        APIClient().PostdataRequest(WithApiName: API.frequently_entry_logs.rawValue, Params: params, isNeedLoader: true, objectType: LogsModel.self) { response, statusCode in
            switch response {
            case .success(let rs):
                DispatchQueue.main.async {
                    if rs.data?.count ?? 0 > 0 {
                        self.logsData = rs.data
                        self.noEntryLogsView.isHidden = true
                        self.tblView.reloadData()
                    }else{
                        self.noEntryLogsView.isHidden = false
                    }
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    
}

extension LogsVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logsData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogCell", for: indexPath) as! LogCell
        let rs = logsData?[indexPath.row]
        cell.dateLbl.text = rs?.intime?.count ?? 0 > 0 ? "\(TimeStampToDate(timeStamp: rs?.intime?.toInt() ?? 0))":""
        cell.intimeLbl.text = rs?.intime?.count ?? 0 > 0 ? "\(getTimeOnly(rs?.intime?.toInt() ?? 0)) • Entry":""
        cell.outTimeLbl.text = rs?.outtime?.count ?? 0 > 0 ? "\(getTimeOnly(rs?.outtime?.toInt() ?? 0)) • Exit":""
        return cell
    }
    
}
