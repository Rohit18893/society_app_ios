//  ShareProfileVC.swift
//  Society Management
//  Created by Jitendra Yadav on 24/05/21.
//

import UIKit
import SDWebImage

class ShareProfileVC: MasterVC, ImagePickerDelegate {
    
    var selectionSelectImage:((UIImage)->())? = nil
    var data:Added_data?
    var imagePicker: ImagePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.className(ClassString: self)
        
        
        DispatchQueue.main.async {
            self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        }
        
        if let editButton = self.view.viewWithTag(301) as? UIButton {
            editButton.addTarget(self, action: #selector(editImage), for: .touchUpInside)
        }
        
        if let shareButton = self.view.viewWithTag(308) as? UIButton {
            shareButton.addTarget(self, action: #selector(shareAddress), for: .touchUpInside)
        }
        
        if let profileImage = self.view.viewWithTag(300) as? UIImageView {
            profileImage.image = myProfileImage
        }
        
        self.makeBlureView()
        self.setDate()
    }
    
    
    func setDate(){
        if  let name = self.view.viewWithTag(302) as? UILabel {
            name.text = data?.name?.capitalized
            name.adjustsFontSizeToFitWidth = true
        }
        if let tagId = self.view.viewWithTag(303) as? UILabel {
            tagId.text = "#\(data?.tagid ?? "")"
            tagId.adjustsFontSizeToFitWidth = true
        }
        if let mobileNumber = self.view.viewWithTag(304) as? UILabel {
            mobileNumber.text = data?.number
            mobileNumber.adjustsFontSizeToFitWidth = true
        }
        if let emailID = self.view.viewWithTag(305) as? UILabel {
            emailID.text = data?.email
            emailID.adjustsFontSizeToFitWidth = true
        }
        if let userType = self.view.viewWithTag(306) as? UILabel {
            userType.text = data?.type?.uppercased()
            userType.adjustsFontSizeToFitWidth = true
        }
        if let address = self.view.viewWithTag(307) as? UILabel {
            address.text = data?.addrees
            address.adjustsFontSizeToFitWidth = true
        }
        
        DispatchQueue.main.async {
        if let profileImage = self.view.viewWithTag(300) as? UIImageView {
            if let img = self.data?.image {
                profileImage.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "profile_icon"))
            }
        }}
    }
    
    
    @objc func editImage(_ sender:UIButton) {
        DispatchQueue.main.async {
            self.imagePicker.present(from: sender)
        }
    }
    
    @objc func shareAddress(_ sender:UIButton) {
        
        print("Share My Address")
       if let add = data?.addrees , let lat = data?.lat , let long = data?.long {
        self.shareWithFriends(shareAddress: "My address is: \(add) , Google Coordinates: http://www.google.com/maps/place/\(lat),\(long)")
        }
        
    }
    
    func didSelect(image: UIImage?, tag: Int) {
        DispatchQueue.main.async {
            if let profileImage = self.view.viewWithTag(300) as? UIImageView {
                if let image = image {
                    profileImage.image = image
                    if let ind = self.selectionSelectImage {
                        ind(image)
                        self.setImage(image: image ,key:"myprofile")
                    }
                }
            }
        }
        
    }
    
}
