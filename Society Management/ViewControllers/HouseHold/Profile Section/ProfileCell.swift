//
//  ProfileCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 23/05/21.
//

import UIKit

class ProfileCell: UITableViewCell {

    var data:Added_data? {
        didSet{
            self.setData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
    func setData(){
        let name = self.viewWithTag(100) as? UILabel
        name?.text = data?.name?.capitalized
        let tagId = self.viewWithTag(101) as? UILabel
        tagId?.text = "#\(data?.tagid ?? "")".uppercased()
        if let profileImage = self.viewWithTag(102) as? UIImageView {
            if let img = data?.image {
                profileImage.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "profile_icon"))
            }
        }
    }
}
