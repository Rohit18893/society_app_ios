//  MyVehiclesCell.swift
//  Society Management
//  Created by Jitendra Yadav on 23/05/21.


import UIKit

class MyVehiclesCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var emptyText = "+ Add Vehicle"
    var emptyTextDetail = "Add your vehicles for easy identification in your community. If your community has opted for it, get notified of your vehicles entry exit."
    var emptyCellImage = #imageLiteral(resourceName: "visiting_popup")
    
    var addIVehicles:((Int)->())? = nil
    var vehicleDetails:((Added_data)->())? = nil
    var vehicleLogs:((Added_data)->())? = nil
    var vehicleNotify:((Added_data)->())? = nil
    
    let sectionInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    let numberOfItemsPerRow: CGFloat = 1
    let spacingBetweenCells: CGFloat = 15
    
    var data:[Added_data]? = nil {
        didSet{
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        self.collectionView.register(UINib(nibName: "FamilyCVC", bundle: nil), forCellWithReuseIdentifier: "FamilyCVC")
        self.collectionView.register(UINib(nibName: "AddCVC", bundle: nil), forCellWithReuseIdentifier: "AddCVC")
        self.collectionView.register(UINib(nibName: "AddsitemCell", bundle: nil), forCellWithReuseIdentifier: "AddsitemCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}

extension MyVehiclesCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let rs = data?.count ?? 0
        return rs + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        if (indexPath.item == lastRowIndex - 1) {
            if data?.count ?? 0 == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddsitemCell", for: indexPath) as! AddsitemCell
                self.collectionView.shadow = false
                self.collectionView.layer.shadowOpacity = 0
                cell.cellType.text = emptyText
                cell.cellTypetext.text = emptyTextDetail
                cell.icon.image = emptyCellImage
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCVC", for: indexPath) as! AddCVC
                return cell
            }
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FamilyCVC", for: indexPath) as! FamilyCVC
            
            let rs = data?[indexPath.item]
            cell.nameLbl.text = rs?.name?.capitalized
            cell.ticketID.text = rs?.type?.uppercased()
            cell.ticketID.textColor = UIColor(named: AssetsColor.titleColor.rawValue)
            if let img = rs?.image {
                cell.profileIcon.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "police"))
            }
                        
            cell.leftButton.setImage(#imageLiteral(resourceName: "important_notification"), for: .normal)
            cell.rightButton.setImage(#imageLiteral(resourceName: "menu_icon"), for: .normal)
//            cell.profileIcon.image = #imageLiteral(resourceName: "dummyBike.png")
            cell.rightButton.tag = indexPath.row
            cell.leftButton.tag = indexPath.row
            cell.leftButton.addTarget(self, action: #selector(notifyBTN), for: .touchUpInside)
            cell.rightButton.addTarget(self, action: #selector(VehicleLog), for: .touchUpInside)
            self.collectionView.shadow = true
            self.collectionView.layer.shadowOpacity = 0.3
            return cell
        }
    }
    
    @objc func notifyBTN(_ sender:UIButton) {
        DispatchQueue.main.async {
            if let ind = self.vehicleNotify, let rs = self.data?[sender.tag]{
                ind(rs)
            }
        }
    }
    
    @objc func VehicleLog(_ sender:UIButton) {
        DispatchQueue.main.async {
            if let ind = self.vehicleLogs , let rs = self.data?[sender.tag]{
                ind(rs)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        if (indexPath.item == lastRowIndex - 1) {
            DispatchQueue.main.async {
                if let ind = self.addIVehicles {
                    ind(indexPath.item)
                }
            }
        }else{
            DispatchQueue.main.async {
                if let ind = self.vehicleDetails  , let rs = self.data?[indexPath.item]{
                    ind(rs)
                }
            }
        }
    }
    
    @IBAction func addFamilyButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            if let ind = self.addIVehicles {
                ind(sender.tag)
            }
        }
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.data?.count ?? 0 > 0 {
            return CGSize(width: 140, height: 180)
        }else{
            return CGSize(width: self.collectionView.bounds.width, height: 180)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
}
