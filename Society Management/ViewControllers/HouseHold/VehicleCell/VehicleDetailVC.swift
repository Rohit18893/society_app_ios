//
//  VehicleDetailVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/05/21.
//

import UIKit
import SDWebImage

class VehicleDetailVC: MasterVC {

    @IBOutlet weak var switchNotify: UISwitch!
    
    @IBOutlet weak var vehicleName: UILabel!
    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var vehicleType: UILabel!
    
    var editVehicles:((Added_data)->())? = nil
    var ViewLogs:((Added_data)->())? = nil
    var notify:((Added_data)->())? = nil
    
    var VehicleData:Added_data? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        self.className(ClassString: self)
        
        vehicleName.text = VehicleData?.name?.capitalized
        vehicleType.text = "\(VehicleData?.number ?? "") ● \(VehicleData?.type ?? "")".uppercased()
        vehicleImage.sd_setImage(with: URL(string: VehicleData?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "police"))
        
        

        // Do any additional setup after loading the view. ●
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func editButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            if let ind = self.editVehicles, let dt = self.VehicleData {
                ind(dt)
            }
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func deleteButton(_ sender: UIButton) {
        let param = ["id":VehicleData?.id ?? "","lang_id":language]
        print("Delete Data: \(param)")
        APIClient().PostdataRequest(WithApiName: API.deleteVehical.rawValue, Params: param, isNeedLoader: true, objectType: StatusModel.self) { Response, status in
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200 :
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(result.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
    @IBAction func viewLogsButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: { [weak self] in
                if let ind = self?.ViewLogs, let dt = self?.VehicleData {
                    ind(dt)
                }
            })
        }
    }
    @IBAction func notifyAlertActive(_ sender: UISwitch) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: { [weak self] in
                if let ind = self?.notify, let dt = self?.VehicleData {
                    ind(dt)
                }
            })
        }
    }
    
    @IBAction func LinkRfidBTN(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: { [weak self] in
                if let ind = self?.editVehicles, let dt = self?.VehicleData {
                    ind(dt)
                }
            })
        }
    }
}
