//
//  AddVehicleVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/05/21.
//

import UIKit
import MaterialComponents

class AddVehicleVC: MasterVC, ImagePickerDelegate {

    @IBOutlet weak var vehicleImage: Imageradius!
    @IBOutlet weak var fourWheeler: UIButton!
    @IBOutlet weak var twoWheeler: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var validTextLbl: UILabel!
    @IBOutlet weak var vehicletextLbl: UILabel!
    @IBOutlet weak var wheelerView: UIView!
    
    @IBOutlet weak var VehicleName: MDCOutlinedTextField!
    @IBOutlet weak var vehicleNumber: MDCOutlinedTextField!
    @IBOutlet weak var codeNumber: MDCOutlinedTextField!
    @IBOutlet weak var RFidNumber: MDCOutlinedTextField!
    var imagePicker: ImagePicker!
    var isEdit = false
    
    var VehicleData:Added_data? = nil
    
   fileprivate var buttons:[UIButton] {
      return [twoWheeler,fourWheeler]
    }
    
    var vehicleType:String{
        if twoWheeler.isSelected {
            return "1"
        }else{
            return "2"
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        if isEdit && VehicleData != nil {
            
            validTextLbl.isHidden = true
            vehicletextLbl.isHidden = true
            wheelerView.isHidden = true
          //  editButton.isHidden = true
            
            self.VehicleName.text = VehicleData?.name
            self.vehicleNumber.text = VehicleData?.number
            self.RFidNumber.text = VehicleData?.rfid
            self.codeNumber.text = VehicleData?.rfcode
            if let updBtn = self.view.viewWithTag(444) as? UIButton {
                updBtn.setTitle("UPDATE", for: .normal)
            }
            if VehicleData?.type == "2 Wheeler" {
                twoWheeler.isSelected = true
            }else{
                fourWheeler.isSelected = true
            }
            vehicleImage.sd_setImage(with: URL(string: VehicleData?.image ?? ""), placeholderImage: #imageLiteral(resourceName: "police"))
        }
        
        
        
        self.setTextFieldTheme(txtField: VehicleName, labelText: VehicleName.placeholder ?? "", allowEditing: true)
        self.setTextFieldTheme(txtField: vehicleNumber, labelText: vehicleNumber.placeholder ?? "", allowEditing: true)
        self.setTextFieldTheme(txtField: codeNumber, labelText: codeNumber.placeholder ?? "", allowEditing: true)
        self.setTextFieldTheme(txtField: RFidNumber, labelText: RFidNumber.placeholder ?? "", allowEditing: true)
        
        self.makeBlureView()
        
        DispatchQueue.main.async {
            self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        }
    }
    

    @IBAction func AddVehicleButton(_ sender: UIButton) {
        
        guard let name = VehicleName.text , !name.isBlank   else {
            ShowAlert(AlertMessage.vehicleName, on: "", from: self)
            return
        }
        guard let vehicleNumber = vehicleNumber.text , !vehicleNumber.isBlank   else {
            ShowAlert(AlertMessage.vehicleNumber, on: "", from: self)
            return
        }
        if isEdit && VehicleData != nil {
            self.EditVehicleAPI(name: name, vehicalNumber: vehicleNumber, img: vehicleImage.image ?? #imageLiteral(resourceName: "delivery_icon"), id: VehicleData?.id ?? "")
        }else{
            self.AddVehicleAPI(name: name, vehicalNumber: vehicleNumber, img: vehicleImage.image ?? #imageLiteral(resourceName: "delivery_icon"))
        }
    }
    
    
    func AddVehicleAPI(name:String,vehicalNumber:String,img:UIImage){
        let param = [
            "name":name,
            "vehical_number":vehicalNumber,
            "vehical_type":vehicleType,
            "lang_id":language,
            "uid":userData?.id ?? "",
            "unitnumber":userData?.unitnumber ?? "",
            "towerid":userData?.towerid ?? ""
        ]
        
        var imageArr = [imagesUpload]()
        imageArr.append(imagesUpload(key: "document", image:img , fileName: "\(name)_Image.png"))

        
        APIClient().UploadImageWithParametersRequest(WithApiName: API.addVehical.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {[weak self](response, status) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    func EditVehicleAPI(name:String,vehicalNumber:String,img:UIImage,id:String){
        let param = [
            "name":name,
            "vehical_number":vehicalNumber,
            "vehical_type":vehicleType,
            "lang_id":language,
            "id":id
        ]
        
        var imageArr = [imagesUpload]()
        imageArr.append(imagesUpload(key: "document", image:img , fileName: "\(name)_Image.png"))

        
        APIClient().UploadImageWithParametersRequest(WithApiName: API.editVehical.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {[weak self](response, status) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    @IBAction func twoWheelerButton(_ sender: UIButton) {
        self.buttons.forEach({$0.isSelected = ($0 == sender) ? true : false })
        
    }
    
    @IBAction func fourWheelerButton(_ sender: UIButton) {
        self.buttons.forEach({$0.isSelected = ($0 == sender) ? true : false })
        
    }
    
    @IBAction func editProfileButton(_ sender: UIButton) {
        self.imagePicker?.present(from: sender)
        
    }
    
    func didSelect(image: UIImage?, tag: Int) {
        vehicleImage.image = image
    }
}
