//
//  AddsitemCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 23/06/21.
//

import UIKit

class AddsitemCell: UICollectionViewCell {
    
    @IBOutlet weak var cellType: UILabel!
    @IBOutlet weak var cellTypetext: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

}
