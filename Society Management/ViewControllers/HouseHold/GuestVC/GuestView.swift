//
//  GuestView.swift
//  Society Management
//
//  Created by Jitendra Yadav on 11/06/21.
//

import UIKit



class GuestView: MasterVC {
    
    @IBOutlet weak var validTime: UILabel!
    @IBOutlet weak var inviteBy: UILabel!
    @IBOutlet weak var addTime: UILabel!
    @IBOutlet weak var shareCode: UILabel!
    @IBOutlet weak var guestName: UILabel!
    @IBOutlet weak var profilePicture: UIImageView!
    var sharecode:((Added_data)->())? = nil
    @IBOutlet weak var popupView: UIView!
    
    var data:Added_data?
    var porpletype :String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        self.popupView.isHidden = true
        self.getGuestDetail(type: porpletype ?? "")
        self.className(ClassString: self)
    }
    
    @objc func viewClicked(_ sender: Any) {
        self.back(sender)
    }
    
    func getGuestDetail(type:String){
        let params = ["id":data?.id ?? "","lang_id":language,"type":type]
        print(params)
        APIClient().PostdataRequest(WithApiName: API.getentry.rawValue, Params: params, isNeedLoader: true, objectType: guestDetail.self) { [weak self] Response, status in
            guard let self = self else {return}
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        self.popupView.isHidden = false
                        self.guestName.text = result.data?.name
                        self.shareCode.text = "#\(result.data?.validcode ?? "")"
                        self.addTime.text = result.data?.createdate
                        self.validTime.text = result.data?.enddate
                        self.inviteBy.text = "Invited by you"
                    }
                default:
                    DispatchQueue.main.async {
                        self.popupView.isHidden = false
                    }
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    //http://3.18.213.80:4000/get_entry
    
    @IBAction func deleteButton(_ sender: Any) {
        let param = ["id":data?.id ?? "","lang_id":language]
        print("Delete Data: \(param)")
        APIClient().PostdataRequest(WithApiName: API.deleteguest.rawValue, Params: param, isNeedLoader: true, objectType: StatusModel.self) { Response, status in
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200 :
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(result.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
    @IBAction func shareButton(_ sender: Any) {
        DispatchQueue.main.async {
            if let ind = self.sharecode,let data = self.data {
                ind(data)
                self.back(self)
            }
        }
    }
    
    @IBAction func callButton(_ sender: Any) {
        if let number  = data?.number {
            self.callNumber(phoneNumber: number)
        }
    }
}
