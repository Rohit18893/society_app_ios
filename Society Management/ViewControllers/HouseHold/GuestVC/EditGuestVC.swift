//
//  EditGuestVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 11/07/21.
//

import UIKit
import MaterialComponents

class EditGuestVC: MasterVC {

    var startdate = String ()
    var enddate = String ()
    
    var isStatDate = true
    var theme: SambagTheme = .light
    var data:Added_data?
    @IBOutlet weak var startDate: MDCOutlinedTextField!
    @IBOutlet weak var endDate: MDCOutlinedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTextFieldTheme(txtField: startDate, labelText: startDate.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: endDate, labelText: endDate.placeholder ?? "", allowEditing: false)
        startDate.text = self.convertDateFormaterTo(data?.startDate ?? "")
        endDate.text =  self.convertDateFormaterTo(data?.endDate ?? "")
        startdate = data?.startDate ?? ""
        enddate = data?.endDate ?? ""
        self.makeBlureView()
        self.className(ClassString: self)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func updateButton(_ sender: UIButton) {
        let param = ["guest_id": data?.id ?? "","start_date":startdate,"end_date": enddate]
        APIClient().PostdataRequest(WithApiName: API.editGuest.rawValue, Params: param, isNeedLoader: true, objectType: StatusModel.self) { response, status in
            switch response {
            case .success(let result):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .householdReload, object: nil)
                    self.popUpAlert(title: "", message: result.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [{_ in
                        self.dismiss(animated: true, completion: nil)
                        }
                   ])
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    

    
    @IBAction func selectStartDate(_ sender: UIButton) {
        isStatDate = true
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func SelectEndDate(_ sender: UIButton) {
        isStatDate = false
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
}

extension EditGuestVC: SambagDatePickerViewControllerDelegate {

    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        let date = self.convertDateFormater("\(result)")
        if isStatDate {
            startdate = date
            startDate.text = "\(result)"
        }else{
            endDate.text = "\(result)"
            enddate = date
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}
