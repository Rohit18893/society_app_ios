//
//  ShareCodeVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 04/07/21.
//

import UIKit

class ShareCodeVC: MasterVC {
    
    @IBOutlet weak var codeType: UILabel!
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var weblink: UILabel!
    @IBOutlet weak var inviteCode: PaddingLabel!
    @IBOutlet weak var guestAddressLbl: UILabel!
    @IBOutlet weak var guestNameWithCodeLbl: UILabel!
    
    var data:Added_data?
    var id: String? = nil
    var guestType = "1"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        popupView.isHidden = true
        if let id = id {
            self.getRatingDataFromServer(type: self.guestType, id: id)
        }else{
            self.getRatingDataFromServer(type: self.guestType, id: data?.id ?? "")
        }
        // Do any additional setup after loading the view.
        self.className(ClassString: self)
    }
    
    func getRatingDataFromServer(type:String,id:String){
        let params = ["id":id,"lang_id":language,"type":type]
        print(params)
        APIClient().PostdataRequest(WithApiName: API.getentry.rawValue, Params: params, isNeedLoader: true, objectType: guestDetail.self) { [weak self] Response, status in
            guard let self = self else {return}
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        self.popupView.isHidden = false
                        
                        if self.guestType == "1"{
                            self.guestAddressLbl.text = "\(userData?.fname ?? "") \(userData?.lname ?? "") has added you as a frequent guest to visit \(userData?.society ?? "")\n From \(result.data?.enddate ?? "")"
                            if  let name = result.data?.name , let code = result.data?.validcode {
                                let txt = "Frequent entry code for \(name) : \(code)"
                                self.guestNameWithCodeLbl.colorString(text: txt, coloredText: code,color: .red)
                                self.inviteCode.text = code
                                self.codeType.text = "Frequent entry code"
                            }
                        }else{
                            self.guestAddressLbl.text = "\(userData?.fname ?? "") \(userData?.lname ?? "") has invited you to \(userData?.society ?? "")\n on \(result.data?.enddate ?? "")"
                            if  let name = result.data?.name , let code = result.data?.validcode {
                                let txt = "Entry code create for  \(name) : \(code)"
                                self.guestNameWithCodeLbl.colorString(text: txt, coloredText: code,color: .red)
                                self.inviteCode.text = code
                                self.codeType.text = "Entry code"
                            }
                        }
                    }
                default:
                    DispatchQueue.main.async {
                        self.popupView.isHidden = false
                    }
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
    @IBAction func shareButton(_ sender: Any) {
        
        let image = self.shareView.makeScreenshot()
        
        // set up activity view controller
        let imageToShare = [image]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}
