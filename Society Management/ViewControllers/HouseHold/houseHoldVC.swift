//  houseHoldVC.swift
//  Society Management
//  Created by Jitendra Yadav on 23/05/21.


import UIKit




class houseHoldVC: MasterVC {
    
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    
    //MARK: Register Cells
    
    fileprivate let arrCells = [
        ProfileCell.self,
        MyFamilyCell.self,
        MyVehiclesCell.self,
        IntercomCell.self,
        DailyHelpCell.self
    ]
    
    let householdViewModel = HouseholdViewModel()
    var addRaw = 0
        
    //MARK: viewDidLoad
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.className(ClassString: self)
     //   self.viewNAv.lbltitle.text = "Tower-B14 502"
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        self.getData(isNeedLoader: true)
        NotificationCenter.default.addObserver(self, selector: #selector(houseHoldReload(notification:)), name: .householdReload, object: nil)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
    }
    
    //MARK: House Hold Reload
    
    @objc func houseHoldReload(notification: NSNotification) {
        self.getData(isNeedLoader: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .householdReload, object: nil)
    }
    
    @objc func refresh(_ refresh: UIRefreshControl) {
        self.getData(isNeedLoader: false)
        refresh.endRefreshing()
    }
    
    //MARK: Get Date From Server
    
    func getData(isNeedLoader:Bool){
        householdViewModel.vc = self
        householdViewModel.getData(uid: userData?.id ?? "", lang_id: language,isNeedLoader: isNeedLoader)
    }
    
}



extension houseHoldVC: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        let dt = householdViewModel.householdModel?.data?.count ?? 0
        return  dt
            //+ addRaw
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return householdViewModel.householdModel?.data?[section].added_data?.count ?? 0
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
//        if indexPath.section == tblView.numberOfSections - 1 {
//            //MARK: Intercom  Cell
//            let cell = tableView.dequeueReusableCell(with: IntercomCell.self, for: indexPath)
//            return cell
//
//        } else
        if indexPath.section == 0 {
            //MARK: Profile  Cell
            let cell = tableView.dequeueReusableCell(with: ProfileCell.self, for: indexPath)
            let rs = householdViewModel.householdModel?.data?[indexPath.section].added_data?[indexPath.row]
            cell.data = rs
            if let shareAddress = cell.viewWithTag(309) as? UIButton {
                shareAddress.addTarget(self, action: #selector(shareMyAddress), for: .touchUpInside)
            }
            return cell
            
        } else if indexPath.section == 1 {
            //MARK: Family  Cell
            let cell = tableView.dequeueReusableCell(with: MyFamilyCell.self, for: indexPath)
            cell.data = householdViewModel.householdModel?.data?[indexPath.section].added_data
            cell.titleLbl.text = householdViewModel.householdModel?.data?[indexPath.section].title?.uppercased()
            cell.addItems = {[weak self] value in
                self?.addItmes()
            }
            cell.MemberDetails = {[weak self] value in
                self?.openItmes(value: value)
            }
            cell.openLogs = {[weak self] value in
                self?.openLogs(value: value)
            }
            cell.openpermissions = {[weak self] value in
                self?.openPermissions(value: value)
            }
            return cell
        } else if indexPath.section == 2 {
            //MARK: Vehicle  Cell
            let cell = tableView.dequeueReusableCell(with: MyVehiclesCell.self, for: indexPath)
            cell.data = householdViewModel.householdModel?.data?[indexPath.section].added_data
            cell.titleLbl.text = householdViewModel.householdModel?.data?[indexPath.section].title?.uppercased()
            cell.addIVehicles = {[weak self] index in
                self?.addVehicles()
            }
            cell.vehicleDetails = {[weak self] value in
                self?.vehiclesDetails(value: value)
            }
            cell.vehicleLogs = {[weak self] value in
                self?.vehicleLogs(value: value)
            }
            cell.vehicleNotify = {[weak self] value in
                self?.vehiclesNotify(value: value)
            }
            return cell
        }else if indexPath.section == 3 {
            //MARK: Daily Help Cell
            let cell = tableView.dequeueReusableCell(with: DailyHelpCell.self, for: indexPath)
            cell.data = householdViewModel.householdModel?.data?[indexPath.section].added_data
            cell.titleLbl.text = householdViewModel.householdModel?.data?[indexPath.section].title?.uppercased()
            cell.collectionView.tag = indexPath.section
            cell.addDailyHelp = {[weak self] index in
                self?.addDailyHelp()
            }
            cell.DailyHelpDetails = {[weak self] value in
                self?.OpenDetailDailyHelper(value: value)
            }
            cell.CallBtn = {[weak self] value in
                self?.dailyHelpercall(value: value)
            }
            cell.DailyHelpNotify = {[weak self] value in
                self?.dailyHelperNotify(value: value)
            }
            cell.emptyText = "+ Add Daily Help"
            cell.emptyTextDetail = "Add the domestic staff which comes daily to help you. Get notified of their entry exit and easily track their Attendance."
            cell.emptyCellImage = #imageLiteral(resourceName: "my_daily")
            return cell
        }else {
            //MARK: Guest Cell
            let cell = tableView.dequeueReusableCell(with: DailyHelpCell.self, for: indexPath)
            cell.collectionView.tag = indexPath.section
            cell.data = householdViewModel.householdModel?.data?[indexPath.section].added_data
            cell.titleLbl.text = householdViewModel.householdModel?.data?[indexPath.section].title?.uppercased()
            cell.DailyHelpDetails = {[weak self] value in
                self?.OpenGuestDetail(value: value)
            }
            cell.CallBtn = {[weak self] value in
                self?.dailyHelpercall(value: value)
            }
            
            cell.editGuest = {[weak self] value in
                self?.editGuest(value: value)
            }
            
            cell.addDailyHelp = {[weak self] index in
                self?.addGuest()
            }
            
            cell.ShareCodeWithFriends = {[weak self] value in
                self?.CodeShareWithFriends(value: value)
            }
            cell.emptyText = "+ Add Guest"
            cell.emptyTextDetail = "Add guests who are going to stay for a longer time period to avoid any entry hassle."
            cell.emptyCellImage = #imageLiteral(resourceName: "guest_popup")
            
            return cell
        }
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            DispatchQueue.main.async {
                if let rs = self.householdViewModel.householdModel?.data?[indexPath.section].added_data?[indexPath.row] {
                    let vc = ShareProfileVC()
                    vc.modalPresentationStyle = .overFullScreen
                    vc.selectionSelectImage = { image in
                        let cell = tableView.cellForRow(at: indexPath) as! ProfileCell
                        if let profileImage = cell.viewWithTag(102) as? UIImageView {
                            profileImage.image = image
                        }
                    }
                    vc.data = rs
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }else{
                    PrintLog("No Data")
                }
            }
        }
    }
    
    //MARK: Share My Address
    
    @objc func shareMyAddress(_ sender:UIButton) {
        print("Share My Address")
        if let rs = householdViewModel.householdModel?.data?[0].added_data?.first , let add = rs.addrees , let lat = rs.lat , let long = rs.long {
            self.shareWithFriends(shareAddress: "My address is: \(add) , Google Coordinates: http://www.google.com/maps/place/\(lat),\(long)")
        }
    }
    
    
}

//MARK: This is use for Add family........

extension houseHoldVC {
    
    //MARK: Add family Member
    
    func addItmes(){
        DispatchQueue.main.async {
            print("addItmes")
            let vc = AddFamilyVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    //MARK: Open Details
    
    func openItmes(value:Added_data){
        DispatchQueue.main.async {
            print("openItmes")
            let vc = familyDetailPopupVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.familyData = value
            vc.editKid = {[weak self] data in
                self?.editKid(value:data)
            }
            vc.viewLogs = {[weak self] data in
                self?.openLogs(value:data)
            }
            
            vc.allowExit = {[weak self] data in
                self?.openPermissions(value: data)
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Logs
    
    func openLogs(value:Added_data){
        DispatchQueue.main.async {
            print("openLogs")
            if value.type == "kid"{
                let vc = self.mainStory.instantiateViewController(withIdentifier: "LogsVC") as! LogsVC
                vc.result = value
                vc.isRecentActivit = false
                vc.icon = #imageLiteral(resourceName: "kids_big")
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else{
                self.shareWithFriends(Name: value.name ?? "", address: value.addrees ?? "")
            }
        }
    }
    
    //MARK: Open Allow Kid Permissions
    func openPermissions(value:Added_data){
        DispatchQueue.main.async {
            print("openPermissions")
            if value.type == "kid"{
                let vc = KidsVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }else{
                if let cl = value.number {
                    self.callNumber(phoneNumber: cl)
                }
            }
            
        }
    }
    
    //MARK: Edit Kid
    func editKid(value:Added_data){
        DispatchQueue.main.async {
            let vc = EdirKidVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.familyData = value
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

//MARK: This is use for Add Vehicles, Details, Logs and Notify........

extension houseHoldVC {
    
    //MARK: Add Vehicle
    func addVehicles(){
        DispatchQueue.main.async {
            print("addVehicles")
            let vc = AddVehicleVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Vehicle Logs
    func vehicleLogs(value:Added_data){
        DispatchQueue.main.async {
            print("vehicleLogs")
            let vc = self.mainStory.instantiateViewController(withIdentifier: "LogsVC") as! LogsVC
            vc.result = value
            vc.isRecentActivit = false
            if value.number?.lowercased() == "4 wheeler" {
                vc.icon = #imageLiteral(resourceName: "car")
            }else{
                vc.icon = #imageLiteral(resourceName: "bike")
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK: Vehicle Details
    func vehiclesDetails(value:Added_data){
        DispatchQueue.main.async {
            print("vehiclesDetails")
            let vc = VehicleDetailVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.VehicleData = value
            vc.editVehicles = {[weak self] data in
                self?.editvehicles(value: data)
            }
            vc.ViewLogs = {[weak self] data in
                self?.vehicleLogs(value: data)
            }
//            vc.notify = {[weak self] data in
//                self?.vehiclesNotify(value: data)
//            }
            self.present(vc, animated: true, completion: nil)
            
        }
    }
    
    //MARK: Vehicle Notify
    
    func vehiclesNotify(value:Added_data){
        DispatchQueue.main.async {
            print("vehiclesNotify")
            
        }
    }
    
    //MARK: Edit vehicle
    
    func editvehicles(value:Added_data){
        DispatchQueue.main.async {
            DispatchQueue.main.async {
                print("addVehicles")
                let vc = AddVehicleVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.isEdit = true
                vc.VehicleData = value
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}

//MARK: This is use for Add Daily Helper, Details, Call and Notify........

extension houseHoldVC {
    
    //MARK: Add Daily Helper
    
    func addDailyHelp(){
        DispatchQueue.main.async {
            print("addDailyHelp")
            let vc = self.mainStory.instantiateViewController(withIdentifier: "LocalServiceVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: Daily Helper Call Function
    
    func dailyHelpercall(value:Added_data){
        DispatchQueue.main.async {
            print("dailyHelpercall")
            if let cl = value.number {
                self.callNumber(phoneNumber: cl)
            }
        }
    }
    
    //MARK: Open Detail Daily Helper
    
    func OpenDetailDailyHelper(value:Added_data){
        DispatchQueue.main.async {
            let mainVC = DailyHelperDetailView(nibName:"DailyHelperDetailView", bundle:nil)
            mainVC.data = value
            self.navigationController?.pushViewController(mainVC, animated: true)
        }
    }
    
    //MARK: Daily Helper Notify
    
    func dailyHelperNotify(value:Added_data){
        DispatchQueue.main.async {
            print("dailyHelperNotify")
        }
    }
    

    //MARK: ADD Guest
    func addGuest(){
        DispatchQueue.main.async {
            print("addGuest")
            let vc = GuestVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.isHouseHold  = true
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Open Guest Details
    func OpenGuestDetail(value:Added_data){
        DispatchQueue.main.async {
            print("OpenGuestDetail")
            let vc = GuestView()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.data = value
            vc.porpletype = "1"
            vc.sharecode = {[weak self] value in
                self?.CodeShareWithFriends(value: value)
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Guest Code Share With Friends
    func CodeShareWithFriends(value:Added_data){
        DispatchQueue.main.async {
            print("ShareCodeVC")
            let vc = ShareCodeVC()
            vc.data = value
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //MARK: Edit Guest schedule
    func editGuest(value:Added_data){
        DispatchQueue.main.async {
            print("ShareCodeVC")
            let vc = EditGuestVC()
            vc.data = value
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
}
