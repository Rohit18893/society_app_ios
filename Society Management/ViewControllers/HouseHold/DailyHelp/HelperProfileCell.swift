//
//  HelperProfileCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 08/06/21.
//

import UIKit
import MKMagneticProgress
import Cosmos

class HelperProfileCell: UITableViewCell {
    
    @IBOutlet weak var stacktop: NSLayoutConstraint!
    @IBOutlet weak var stackTrailing: NSLayoutConstraint!
    @IBOutlet weak var stackLeading: NSLayoutConstraint!
    
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var notifyView: UIView!
    @IBOutlet weak var profileView: UIView!
    
    @IBOutlet weak var helperRatingLbl: UILabel!
    @IBOutlet weak var rateLbl: UILabel!
    @IBOutlet weak var insideTag: UILabel!
    @IBOutlet weak var helperProfile: Imageradius!
    @IBOutlet weak var helperName: UILabel!
    @IBOutlet weak var helperMobile: UILabel!
    @IBOutlet weak var rateNowButton: UIButton!
    
    @IBOutlet weak var progressView1: MKMagneticProgress!
    @IBOutlet weak var progressView2: MKMagneticProgress!
    @IBOutlet weak var progressView3: MKMagneticProgress!
    @IBOutlet weak var progressView4: MKMagneticProgress!
    
    @IBOutlet weak var graphView: UIView!
    @IBOutlet weak var punchualLbl: PaddingLabel!
    @IBOutlet weak var regular: PaddingLabel!
    @IBOutlet weak var serviceLbl: PaddingLabel!
    @IBOutlet weak var attitudeLbl: PaddingLabel!
    
    @IBOutlet weak var starRating: CosmosView!
    
    
    
    var calltoHelper:(()->())? = nil
    var shareHelper:(()->())? = nil
    var gatePassHelper:(()->())? = nil
    var clickOnProgressbar:(()->())? = nil
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func CallButton(_ sender: UIButton) {
        if let ind = self.calltoHelper {
            ind()
        }
    }
    @IBAction func shareButton(_ sender: UIButton) {
        if let ind = self.shareHelper {
            ind()
        }
    }
    @IBAction func gatePass(_ sender: UIButton) {
        if let ind = self.gatePassHelper {
            ind()
        }
    }
    
    @IBAction func notifySwitch(_ sender: UISwitch) {
        
    }
    
    @IBAction func clickOnProgressBar(_ sender: UIButton) {
        if let ind = self.clickOnProgressbar {
            ind()
        }
    }
    
    func setViewAllRatingView(){
        self.profileView.isHidden = true
        self.notifyView.isHidden = true
        self.rateLbl.isHidden = true
        self.ratingView.isHidden = true
        self.graphView.isHidden = false
        self.stackTrailing.constant = 0
        self.stackLeading.constant = 0
        self.stacktop.constant = 0
        DispatchQueue.main.async {
            self.shadow = false
            self.layer.shadowOpacity = 0

        }
    }
    
    func setLocalServiceViewAllRating(){

        self.profileView.isHidden = true
        
        self.rateLbl.isHidden = false
        self.ratingView.isHidden = false
        self.graphView.isHidden = false
        self.stacktop.constant = 20

    }
}
