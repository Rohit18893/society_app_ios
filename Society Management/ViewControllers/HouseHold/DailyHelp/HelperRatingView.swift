//
//  HelperRatingView.swift
//  Society Management
//
//  Created by Jitendra Yadav on 10/06/21.
//

import UIKit
import Cosmos

class HelperRatingView: MasterVC {
    
    @IBOutlet weak var starRatingView: CosmosView!
    @IBOutlet weak var helperratingLbl: UILabel!
    
    @IBOutlet weak var punctualBtn: UIButton!
    @IBOutlet weak var regularBtn: UIButton!
    @IBOutlet weak var serviceBTN: UIButton!
    @IBOutlet weak var attitudeBtn: UIButton!
    
    @IBOutlet weak var submiltBtn: UIButton!
    
    @IBOutlet weak var paunctualImg: UIImageView!
    @IBOutlet weak var regularImg: UIImageView!
    @IBOutlet weak var serviceImg: UIImageView!
    @IBOutlet weak var attitudeImg: UIImageView!
    
    @IBOutlet weak var reviewTV: UITextView!
    
    @IBOutlet weak var ratingView: UIView!
    
    var helperName:String?
    var id:String?
    var rating:String?
    var isEdit = false
    
    var ratingSuccess:((String)->())? = nil
    
    
    var isPunctual:String {
        if punctualBtn.isEnabled {
            return "1"
        }else{
            return "0"
        }
    }
    var isRegular:String {
        if regularBtn.isEnabled {
            return "1"
        }else{
            return "0"
        }
    }
    var isService:String {
        if serviceBTN.isEnabled {
            return "1"
        }else{
            return "0"
        }
    }
    var isAttitude:String {
        if attitudeBtn.isEnabled {
            return "1"
        }else{
            return "0"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        self.className(ClassString: self)
        self.helperratingLbl.text = "Would uoy recommend \(helperName?.capitalized ?? "")\nfor below qualities"
        
        if isEdit {
            //ratingView.isHidden = true
            starRatingView.rating = 0
            self.getRatingDataFromServer()
        }else{
           // ratingView.isHidden = false
            starRatingView.rating = 5
            self.rating = "5"
        }
        
        starRatingView.didTouchCosmos = { [weak self] value in
            if value > 0 {
                self?.rating = "\(value)"
                self?.submiltBtn.isEnabled = true
                UIView.animate(withDuration: 0.5) {
                    self?.submiltBtn.alpha = 1.0
                }
            }else{
                self?.submiltBtn.isEnabled = false
                UIView.animate(withDuration: 0.5) {
                    self?.submiltBtn.alpha = 0.3
                }
            }
        }
    }
    
    //MARK: get Rating Data From Server
    
    func getRatingDataFromServer(){
        let params = ["people_id":id ?? "","uid":userData?.id ?? "","lang_id":language
        ]
        print(params)
        APIClient().PostdataRequest(WithApiName: API.editrating.rawValue, Params: params, isNeedLoader: true, objectType: GetRating.self) { [weak self] Response, status in
            guard let self = self else {return}
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                       // self.ratingView.isHidden = false
                        if let rate = result.data?.rating?.toDouble() {
                            self.starRatingView.rating = rate
                            self.rating = "\(rate)"
                        }
                        
                        if let review = result.data?.review {
                            self.reviewTV.text = review
                        }
                        
                        if result.data?.punctual != "0" {
                            self.punctualBtn.isEnabled = true
                            self.paunctualImg.image = #imageLiteral(resourceName: "ui")
                            if let selectBtn = self.view.viewWithTag(450) as? UIButton {
                                selectBtn.isSelected = true
                            }
                        }
                        if result.data?.regular != "0" {
                            self.regularBtn.isEnabled = true
                            self.regularImg.image = #imageLiteral(resourceName: "ui")
                            if let selectBtn = self.view.viewWithTag(451) as? UIButton {
                                selectBtn.isSelected = true
                            }
                        }
                        if result.data?.service != "0" {
                            self.serviceBTN.isEnabled = true
                            self.serviceImg.image = #imageLiteral(resourceName: "ui")
                            if let selectBtn = self.view.viewWithTag(452) as? UIButton {
                                selectBtn.isSelected = true
                            }
                        }
                        if result.data?.attitude != "0" {
                            self.attitudeBtn.isEnabled = true
                            self.attitudeImg.image = #imageLiteral(resourceName: "ui")
                            if let selectBtn = self.view.viewWithTag(453) as? UIButton {
                                selectBtn.isSelected = true
                            }
                        }
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
    @IBAction func SelectButton(_ sender: UIButton) {
        switch sender.tag {
        case 450:
            if sender.isSelected{
                punctualBtn.isEnabled = false
                sender.isSelected = false
                paunctualImg.image = #imageLiteral(resourceName: "radio_unactive")
            }else{
                punctualBtn.isEnabled = true
                sender.isSelected = true
                paunctualImg.image = #imageLiteral(resourceName: "ui")
            }
        case 451:
            if sender.isSelected{
                regularBtn.isEnabled = false
                sender.isSelected = false
                regularImg.image = #imageLiteral(resourceName: "radio_unactive")
            }else{
                regularBtn.isEnabled = true
                sender.isSelected = true
                regularImg.image = #imageLiteral(resourceName: "ui")
            }
        case 452:
            if sender.isSelected{
                serviceBTN.isEnabled = false
                sender.isSelected = false
                serviceImg.image = #imageLiteral(resourceName: "radio_unactive")
            }else{
                serviceBTN.isEnabled = true
                sender.isSelected = true
                serviceImg.image = #imageLiteral(resourceName: "ui")
            }
        case 453:
            if sender.isSelected{
                attitudeBtn.isEnabled = false
                sender.isSelected = false
                attitudeImg.image = #imageLiteral(resourceName: "radio_unactive")
            }else{
                attitudeBtn.isEnabled = true
                sender.isSelected = true
                attitudeImg.image = #imageLiteral(resourceName: "ui")
            }
        default:
            break
        }
        
    }
    
    @IBAction func submitButton(_ sender: Any) {
        if isEdit {
            self.editRating()
        }else{
            self.giveRating()
        }
    }
    
    //MARK: Post Rating Data to Server
    
    func giveRating(){
        let params = ["uid":userData?.id ?? "",
                      "towerid":userData?.towerid ?? "",
                      "unitnumber":userData?.unitnumber ?? "",
                      "people_id":id ?? "",
                      "rating":rating ?? "",
                      "punctual":isPunctual,
                      "regular":isRegular,
                      "service":isService,
                      "attitude":isAttitude,
                      "review":reviewTV.text ?? "",
                      "lang_id":language
        ]
        
        APIClient().PostdataRequest(WithApiName: API.rating.rawValue, Params: params, isNeedLoader: true, objectType: StatusModel.self) { [weak self] Response, status in
            guard let self = self else {return}
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        if let success = self.ratingSuccess {
                            success("")
                            self.back(self)
                        }
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
    //MARK: Edit Rating Data to Server
    func editRating(){
        let params = ["uid": userData?.id ?? "",
                      "people_id":id ?? "",
                      "rating":rating ?? "",
                      "punctual":isPunctual,
                      "regular":isRegular,
                      "service":isService,
                      "attitude":isAttitude,
                      "review":reviewTV.text ?? "",
                      "lang_id":language
        ]
        
        APIClient().PostdataRequest(WithApiName: API.updaterating.rawValue, Params: params, isNeedLoader: true, objectType: StatusModel.self) {[weak self] Response, status in
            guard let self = self else {return}
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        if let success = self.ratingSuccess {
                            success("")
                            self.back(self)
                        }
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
    
}


extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}
