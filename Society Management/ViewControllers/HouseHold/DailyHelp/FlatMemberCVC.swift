//
//  FlatMemberCVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 09/06/21.
//

import UIKit

class FlatMemberCVC: UICollectionViewCell {

    @IBOutlet weak var callButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        callButton.titleLabel?.adjustsFontSizeToFitWidth = true
    }

}
