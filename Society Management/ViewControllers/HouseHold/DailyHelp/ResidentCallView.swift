//  ResidentCallView.swift
//  Society Management
//  Created by Jitendra Yadav on 11/06/21.


import UIKit

class ResidentCallView: MasterVC {

    @IBOutlet weak var type: PaddingLabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profile: Imageradius!
    @IBOutlet weak var towerName: UILabel!
    
    var residentData:HelperHouses?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        self.type.text = residentData?.userrole?.uppercased()
        self.name.text = residentData?.username?.uppercased()
        self.towerName.text = residentData?.houses?.uppercased()
        self.className(ClassString: self)
    }

    @IBAction func CallButton(_ sender: Any) {
        self.callNumber(phoneNumber: residentData?.usermobile ?? "")
    }
    
}
