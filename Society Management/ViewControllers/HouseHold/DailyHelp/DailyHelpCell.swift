//
//  DailyHelpCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 08/06/21.
//

import UIKit

class DailyHelpCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var emptyText = ""
    var emptyTextDetail = ""
    var emptyCellImage = UIImage()
    
    var addDailyHelp:((Int)->())? = nil
    var DailyHelpDetails:((Added_data)->())? = nil
    var CallBtn:((Added_data)->())? = nil
    var DailyHelpNotify:((Added_data)->())? = nil
    var ShareCodeWithFriends:((Added_data)->())? = nil
    var editGuest:((Added_data)->())? = nil
    
    let sectionInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    let numberOfItemsPerRow: CGFloat = 1
    let spacingBetweenCells: CGFloat = 15
    
    var data:[Added_data]? = nil {
        didSet{
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        
        self.collectionView.register(UINib(nibName: "FamilyCVC", bundle: nil), forCellWithReuseIdentifier: "FamilyCVC")
        self.collectionView.register(UINib(nibName: "AddCVC", bundle: nil), forCellWithReuseIdentifier: "AddCVC")
        self.collectionView.register(UINib(nibName: "AddsitemCell", bundle: nil), forCellWithReuseIdentifier: "AddsitemCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addFamilyButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            if let ind = self.addDailyHelp {
                ind(sender.tag)
            }
        }
    }
    
}

extension DailyHelpCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let rs = data?.count ?? 0
        return rs + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        if (indexPath.item == lastRowIndex - 1) {
            if data?.count ?? 0 == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddsitemCell", for: indexPath) as! AddsitemCell
                self.collectionView.shadow = false
                self.collectionView.layer.shadowOpacity = 0
                cell.cellType.text = emptyText
                cell.cellTypetext.text = emptyTextDetail
                cell.icon.image = emptyCellImage
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCVC", for: indexPath) as! AddCVC
                return cell
            }
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FamilyCVC", for: indexPath) as! FamilyCVC
            
            let rs = data?[indexPath.item]
            cell.nameLbl.text = rs?.name?.capitalized
            
            cell.ticketID.textColor = UIColor(named: AssetsColor.titleColor.rawValue)
            
            if let img = rs?.image {
                cell.profileIcon.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "profile_icon"))
            }
            
            if collectionView.tag == 3 {
                cell.leftButton.setImage(#imageLiteral(resourceName: "green_call"), for: .normal)
                cell.rightButton.setImage(#imageLiteral(resourceName: "logout_grey"), for: .normal)
                cell.ticketID.text = rs?.type?.uppercased()
            }else{
                cell.leftButton.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
                cell.rightButton.setImage(#imageLiteral(resourceName: "share_icon"), for: .normal)
                cell.rightButton.tintColor = .gray
                cell.ticketID.textColor = .red
                cell.ticketID.text = "#\(rs?.tagid ?? "")"
            }
            
            cell.rightButton.tag = indexPath.row
            cell.leftButton.tag = indexPath.row
            cell.leftButton.addTarget(self, action: #selector(callBtn), for: .touchUpInside)
            cell.rightButton.addTarget(self, action: #selector(notifyBTN), for: .touchUpInside)
            self.collectionView.shadow = true
            self.collectionView.layer.shadowOpacity = 0.3
            return cell
        }
    }
    
    @objc func notifyBTN(_ sender:UIButton) {
        
        DispatchQueue.main.async {
            
            if self.collectionView.tag == 3 {
                if let ind = self.DailyHelpNotify, let rs = self.data?[sender.tag]{
                    ind(rs)
                }
            }else{
                if let ind = self.ShareCodeWithFriends , let rs = self.data?[sender.tag]{
                    ind(rs)
                }
            }
        }
    }
    
    @objc func callBtn(_ sender:UIButton) {
        DispatchQueue.main.async {
            if self.collectionView.tag == 3 {
                if let ind = self.CallBtn , let rs = self.data?[sender.tag]{
                    ind(rs)
                }
            }else{
                if let ind = self.editGuest , let rs = self.data?[sender.tag]{
                    ind(rs)
                }
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let lastRowIndex = collectionView.numberOfItems(inSection: collectionView.numberOfSections-1)
        if (indexPath.item == lastRowIndex - 1) {
            DispatchQueue.main.async {
                if let ind = self.addDailyHelp {
                    ind(indexPath.item)
                }
            }
        }else{
            DispatchQueue.main.async {
                if let ind = self.DailyHelpDetails  , let rs = self.data?[indexPath.item]{
                    ind(rs)
                }
            }
        }
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.data?.count ?? 0 > 0 {
            return CGSize(width: 140, height: 180)
        }else{
            return CGSize(width: self.collectionView.bounds.width, height: 180)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
}
