//  DailyHelperDetailView.swift
//  Society Management
//  Created by Jitendra Yadav on 08/06/21.


import UIKit

class DailyHelperDetailView: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    
    var isOpen = true
    
    var helperType = ""
    var helperId = ""
    
    fileprivate let arrCells = [HelperProfileCell.self,HelperHousesCell.self,HelperFooterCell.self]
    
    var recentData:RecentActivityData?
    var data:Added_data?
    var sectionData = 0
    
    let helperDetailViewModal = HelperDetailViewModal()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.lbltitle.text = "\(data?.type ?? recentData?.service_name ?? "") Profile"
        self.tblView.register(cellTypes: arrCells)
        self.tblView.dataSource = self
        self.tblView.delegate = self
        self.getData(isNeedLoader: true)
        self.className(ClassString: self)
    }
    
    func getData(isNeedLoader:Bool){
        helperDetailViewModal.vc = self
        helperDetailViewModal.getData(helperID: data?.id ?? recentData?.entry_id ?? "", uid: userData?.id ?? "", lang_id: language,isNeedLoader: isNeedLoader)
    }
}

extension DailyHelperDetailView: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sectionData
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tblView.dequeueReusableCell(with: HelperProfileCell.self, for: indexPath)
                        
            if let insideTag = helperDetailViewModal.helperModel?.data?.inside {
                if insideTag == "1" {
                    cell.insideTag.text = "INSIDE"
                    cell.insideTag.backgroundColor = UIColor.init(named: AssetsColor.HeaderColor.rawValue)
                }else{
                    cell.insideTag.text = "LEFT"
                    cell.insideTag.backgroundColor = UIColor.lightGray
                }
            }
            
            cell.helperName.text = helperDetailViewModal.helperModel?.data?.name?.capitalized
            
            
            cell.rateNowButton.addTarget(self, action: #selector(rateNowMe), for: .touchUpInside)
            //MARK:  ----------------  XXXXXXXXXXX ----------------
            
            //MARK: Call Action
            cell.calltoHelper = { [weak self] in
                if let mobile = self?.helperDetailViewModal.helperModel?.data?.mobile {
                    self?.callNumber(phoneNumber: mobile)
                }
            }
            
            //MARK: Share Action
            cell.shareHelper = { [weak self] in
                self?.shareWithFriends()
            }
            
            
            guard let isRateing = helperDetailViewModal.helperModel?.data?.is_rating , isRateing != "0" else {
                cell.helperRatingLbl.text = "0.0"
                cell.rateNowButton.setTitle("RATE NOW", for: .normal)
                cell.graphView.isHidden = true
                cell.starRating.rating = 0
                //MARK: Name
                if let name = helperDetailViewModal.helperModel?.data?.name {
                    cell.helperName.text = name.capitalized
                    cell.rateLbl.text = "Be the first one to rate \(name.capitalized) performance?"
                }
                
                return cell
            }
            
            //MARK: Name
            if let ratingCount = helperDetailViewModal.helperModel?.data?.rating_count,let reviewCount = helperDetailViewModal.helperModel?.data?.review_count {
                cell.rateLbl.text = "   \(ratingCount.capitalized) Rating, \(reviewCount.capitalized) Review"
            }
            
            cell.rateNowButton.setTitle("EDIT RATING", for: .normal)
            cell.graphView.isHidden = false
            //MARK: Rating
            
            if let rating = helperDetailViewModal.helperModel?.data?.rating {
                cell.helperRatingLbl.text = "\(rating.toDouble() ?? 0.0)"
                cell.starRating.rating = rating.toDouble() ?? 0.0
            }
            
            //MARK: Mobile
            if let mobile = helperDetailViewModal.helperModel?.data?.mobile {
                cell.helperMobile.text = mobile.capitalized
            }
            
            //MARK: Helper Image
            if let img = helperDetailViewModal.helperModel?.data?.image {
                cell.helperProfile.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "profile_icon"))
            }
            
            
            //MARK: Rating View Data set ----------------
            if let totalpunctual = helperDetailViewModal.helperModel?.data?.totalpunctual {
                cell.punchualLbl.text = totalpunctual.capitalized
            }
            
            if let avgpunctual = helperDetailViewModal.helperModel?.data?.avgpunctual?.floatValue() {
                let vl = CGFloat(avgpunctual / 100)
                cell.progressView1.setProgress(progress: vl, animated: true)
            }
            
            if let totalregular = helperDetailViewModal.helperModel?.data?.totalregular {
                cell.regular.text = totalregular.capitalized
            }
            
            if let avgregular = helperDetailViewModal.helperModel?.data?.avgregular?.floatValue() {
                let vl = CGFloat(avgregular / 100)
                cell.progressView2.setProgress(progress: vl, animated: true)
            }
            
            if let totalservice = helperDetailViewModal.helperModel?.data?.totalservice {
                cell.serviceLbl.text = totalservice
            }
            
            if let avgservice = helperDetailViewModal.helperModel?.data?.avgservice?.floatValue() {
                let vl = CGFloat(avgservice / 100)
                cell.progressView3.setProgress(progress: vl, animated: true)
            }
            
            if let totalattitude = helperDetailViewModal.helperModel?.data?.totalattitude {
                cell.attitudeLbl.text = totalattitude
            }
            
            if let avgattitude = helperDetailViewModal.helperModel?.data?.avgattitude?.floatValue() {
                let vl = CGFloat(avgattitude / 100)
                cell.progressView4.setProgress(progress: vl, animated: true)
            }
            
            //MARK: Share Action
            cell.clickOnProgressbar = { [weak self] in
                self?.clickProgressbar()
            }
            
            
            return cell
            
        } else if indexPath.section == 1 {
            let cell = tblView.dequeueReusableCell(with: HelperHousesCell.self, for: indexPath)
            cell.dropDownBtn.isUserInteractionEnabled = false
            cell.housesData = self.helperDetailViewModal.helperModel?.data?.houses ?? []
            //MARK: Houses Count
            if let housesCount = self.helperDetailViewModal.helperModel?.data?.houses?.count {
                cell.housesLbl.text = "Works in \(housesCount) Houses".uppercased()
            }
            if self.isOpen {
                cell.collectionView.isHidden = false
            } else {
                cell.collectionView.isHidden  = true
            }
            cell.residentDetailView = { [weak self] value  in
                self?.openResidentDetail(value: value)
            }
            return cell
        } else {
            let cell = tblView.dequeueReusableCell(with: HelperFooterCell.self, for: indexPath)
            cell.addDateLbl.text = " ADDED BY YOU ON \(TimeStampToDate(timeStamp: self.helperDetailViewModal.helperModel?.data?.date ?? 0))"
         //   cell.removeButton.isSelected = false
            cell.removeButton.addTarget(self, action: #selector(removeHelper), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if isOpen {
                isOpen = false
            }else {
                isOpen = true
            }
            self.tblView.reloadData()
        }
    }
    
    @objc func rateNowMe(_ sender:UIButton){
        if sender.titleLabel?.text?.uppercased() == "EDIT RATING" {
            rateNow(isEdit: true)
        }else{
            rateNow(isEdit: false)
        }

    }
    
    @objc func removeHelper(_ sender:UIButton){
        let params = ["id":data?.id ?? recentData?.entry_id ?? "","uid":userData?.id ?? "","lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.removepeople.rawValue, Params: params, isNeedLoader: true, objectType: StatusModel.self) {[weak self] Response, status in
            guard let self = self else {return}
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.showCustumAlert(message:result.msg ?? "", isBack: true)
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }

    }
    
    
    func rateNow(isEdit:Bool){
        let vc = HelperRatingView()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.isEdit = isEdit
        vc.id = data?.id
        vc.helperName = data?.name
        vc.ratingSuccess = { [weak self] value in
           self?.getData(isNeedLoader: false)
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func openResidentDetail(value:HelperHouses){
        DispatchQueue.main.async {
            let vc = ResidentCallView()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.residentData = value
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func clickProgressbar(){
        let vc = self.mainStory.instantiateViewController(withIdentifier: "ViewAllRatingsVC") as! ViewAllRatingsVC
        vc.id = data?.id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}


extension String {
    func floatValue() -> Float? {
        return Float(self)
    }
}

