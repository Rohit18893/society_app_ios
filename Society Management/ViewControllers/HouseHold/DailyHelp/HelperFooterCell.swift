//
//  HelperFooterCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 08/06/21.
//

import UIKit

class HelperFooterCell: UITableViewCell {

    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var addDateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
