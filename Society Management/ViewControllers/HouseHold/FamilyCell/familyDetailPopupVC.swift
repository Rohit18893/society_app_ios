//
//  familyDetailPopupVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/05/21.
//

import UIKit

class familyDetailPopupVC: MasterVC {
    @IBOutlet weak var requirePermission: UIButton!
    @IBOutlet weak var sendNotification: UIButton!
    @IBOutlet weak var doNothing: UIButton!
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    @IBOutlet weak var NameView: UIView!
    @IBOutlet weak var allowExitView: UIView!
    @IBOutlet weak var shareCallView: UIView!
    
    @IBOutlet weak var profileIcon: Imageradius!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var mobileLbl: UILabel!
    @IBOutlet weak var tagLbl: UILabel!
    @IBOutlet weak var familyType: UILabel!
    
    var familyData:Added_data? = nil
    var editKid:((Added_data)->())? = nil
    var viewLogs:((Added_data)->())? = nil
    var allowExit:((Added_data)->())? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.makeBlureView()
        
        // Do any additional setup after loading the view.
        nameLbl.text = familyData?.name
        tagLbl.text = "#\(familyData?.tagid ?? "")"
        
        if familyData?.type == "kid" {
            showKid()
        }else{
            showMember()
        }
    }
    
    
    func showKid(){
        shareCallView.isHidden = true
        editButton.isHidden = false
        allowExitView.isHidden = false
        profileIcon.image = UIImage(named: "kids")
        mobileLbl.text = ""
        familyType.text = ""
        if familyData?.permission == "1" {
            requirePermission.isSelected = true
        } else if familyData?.permission == "2" {
            sendNotification.isSelected = true
        }else{
            doNothing.isSelected = true
        }
    }
    
    func showMember(){
        editButton.isHidden = true
        shareCallView.isHidden = false
        allowExitView.isHidden = true
        mobileLbl.text = familyData?.number
        familyType.text = familyData?.type
        familyType.text = "Family".uppercased()
        if let img = familyData?.image {
            self.profileIcon.sd_setImage(with: URL(string: img), placeholderImage: #imageLiteral(resourceName: "police"))
        }
    }

    @IBAction func CallButton(_ sender: Any) {
        if let mb = familyData?.number {
            self.callNumber(phoneNumber: mb)
        }
    }
    
    @IBAction func inviiteButton(_ sender: Any) {
        self.shareWithFriends(Name: familyData?.name ?? "", address: familyData?.addrees ?? "")
    }
    
    @IBAction func allowButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: { [weak self] in
                if let ind = self?.allowExit, let dt = self?.familyData {
                    ind(dt)
                }
            })
        }
    }
    
    @IBAction func ViewLogButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: { [weak self] in
                if let ind = self?.viewLogs, let dt = self?.familyData {
                    ind(dt)
                }
            })
        }
    }
    
    @IBAction func editButton(_ sender: Any) {
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: { [weak self] in
                if let ind = self?.editKid, let dt = self?.familyData {
                    ind(dt)
                }
            })
        }
    
    }
    
    @IBAction func deleteButton(_ sender: Any) {
        let param = ["id":familyData?.id ?? "","lang_id":language]
        print("Delete Data: \(param)")
        APIClient().PostdataRequest(WithApiName: API.deleteFamilyMember.rawValue, Params: param, isNeedLoader: true, objectType: StatusModel.self) { Response, status in
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200 :
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(result.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }

    
    @IBAction func requirePermissionButton(_ sender: UIButton) {
        self.SelectAction(isPermission: true, isNotification: false, isNothing: false)
    }
    
    @IBAction func sendNotificationTFButton(_ sender: UIButton) {
        self.SelectAction(isPermission: false, isNotification: true, isNothing: false)
    }
    
    @IBAction func doNothingButton(_ sender: UIButton) {
        self.SelectAction(isPermission: false, isNotification: false, isNothing: true)
    }
    
    
    func SelectAction(isPermission:Bool, isNotification:Bool, isNothing:Bool){
        requirePermission.isSelected = isPermission
        sendNotification.isSelected = isNotification
        doNothing.isSelected = isNothing
        
    }

}
