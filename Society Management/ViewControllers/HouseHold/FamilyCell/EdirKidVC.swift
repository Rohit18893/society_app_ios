//
//  EdirKidVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 02/06/21.
//

import UIKit
import MaterialComponents

class EdirKidVC: MasterVC {
    
    @IBOutlet weak var doNotBTn: UIButton!
    @IBOutlet weak var sndNotify: UIButton!
    @IBOutlet weak var reqBtn: UIButton!
    @IBOutlet weak var nameTF: MDCOutlinedTextField!
    
    var familyData:Added_data? = nil
    
    fileprivate var buttons:[UIButton] {
        return [reqBtn,sndNotify,doNotBTn]
    }
    
    var permisions:String{
        if reqBtn.isSelected {
            return "1"
        } else if sndNotify.isSelected {
            return "2"
        }else{
            return "3"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTF.text = familyData?.name
        self.className(ClassString: self)
        self.setTextFieldTheme(txtField: nameTF, labelText: nameTF.placeholder ?? "", allowEditing: true)
        if familyData?.permission == "1" {
            reqBtn.isSelected = true
        } else if familyData?.permission == "2" {
            sndNotify.isSelected = true
        }else{
            doNotBTn.isSelected = true
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectButton(_ sender: UIButton) {
        if sender.tag == 3331 {
            self.buttons.forEach({$0.isSelected = ($0 == sender) ? true : false })
        } else if sender.tag == 3332 {
            self.buttons.forEach({$0.isSelected = ($0 == sender) ? true : false })
        } else if sender.tag == 3333 {
            self.buttons.forEach({$0.isSelected = ($0 == sender) ? true : false })
        }else{
            
        }
    }
    
    @IBAction func updateButton(_ sender: Any) {
        guard let name = nameTF.text , !name.isBlank   else {
            ShowAlert(AlertMessage.childName, on: "", from: self)
            return
        }
        EditMemberAPI(name: name, id: familyData?.id ?? "",  img: #imageLiteral(resourceName: "kids"))
    }
   
    
    
//    id,
//    name,
//    mobile,
//    age_type
//    ( 1 = adult or 0= kids),
//    lang_id,
//    document,
//    permission ( 0 = when choose adult or 1,2,3 options for kids)
    
    func EditMemberAPI(name:String,id:String,img:UIImage){
        let param = [
            "id":id,
            "name":name,
            "mobile":"",
            "age_type":"0",
            "permission":permisions,
            "lang_id":language
        ]
        PrintLog(param)
        var imageArr = [imagesUpload]()
        imageArr.append(imagesUpload(key: "document", image:img , fileName: "\(name)_Image.png"))

        APIClient().UploadImageWithParametersRequest(WithApiName: API.editKid.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {[weak self](response, status) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
}

