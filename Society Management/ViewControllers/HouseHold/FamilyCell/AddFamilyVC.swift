//
//  AddFamilyVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 24/05/21.
//

import UIKit
import MaterialComponents

class AddFamilyVC: MasterVC, ImagePickerDelegate {
    
    var adultButtn = UIButton()
    var kidButtn = UIButton()
    
    var imagePicker: ImagePicker!
    var requireButton = UIButton()
    var sendNotify = UIButton()
    var doNot = UIButton()
    
    
    @IBOutlet weak var adultIcon: Imageradius!
    @IBOutlet weak var nameTF: MDCOutlinedTextField!
    @IBOutlet weak var iSDTF: MDCOutlinedTextField!
    @IBOutlet weak var MobileTF: MDCOutlinedTextField!
    
    @IBOutlet weak var childNameTF: MDCOutlinedTextField!
    
    @IBOutlet weak var ImageView: UIView!
    @IBOutlet weak var adultView: UIView!
    @IBOutlet weak var kidView: UIView!
    
    @IBOutlet weak var requirePermission: UIButton!
    @IBOutlet weak var sendNotificationTF: UIButton!
    @IBOutlet weak var doNothing: UIButton!
    
    
    
    var type:String {
        if adultButtn.isSelected {
            return "1"
        }else{
            return "0"
        }
    }
    
    var permisions:String {
        if requireButton.isSelected{
            return "1"
        } else if sendNotify.isSelected {
            return "2"
        }else{
            return "3"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        kidView.isHidden = true
        // Do any additional setup after loading the view.
        MobileTF.keyboardType = .phonePad
        adultButtn = self.view.viewWithTag(2019) as! UIButton
        kidButtn = self.view.viewWithTag(2020) as! UIButton
        
        requireButton = self.view.viewWithTag(2041) as! UIButton
        sendNotify = self.view.viewWithTag(2042) as! UIButton
        doNot = self.view.viewWithTag(2043) as! UIButton
        
        adultButtn.addTarget(self, action: #selector(addAdult), for: .touchUpInside)
        kidButtn.addTarget(self, action: #selector(kid), for: .touchUpInside)
        
        
        iSDTF.text = "+971"
        self.setTextFieldTheme(txtField: nameTF, labelText: nameTF.placeholder ?? "", allowEditing: true)
        self.setTextFieldTheme(txtField: iSDTF, labelText: iSDTF.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: MobileTF, labelText: MobileTF.placeholder ?? "", allowEditing: true)
        self.setTextFieldTheme(txtField: childNameTF, labelText: childNameTF.placeholder ?? "", allowEditing: true)
        
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "resident"), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: -5, left: -5, bottom: -5, right: -5)
        button.frame = CGRect(x: CGFloat(nameTF.frame.size.width - 25), y: CGFloat(5), width: CGFloat(25), height: CGFloat(25))
        button.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        nameTF.rightView = button
        nameTF.rightViewMode = .always
        
        
        self.makeBlureView()
        
        DispatchQueue.main.async {
            self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        }
    }
    
    @IBAction func refresh(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = ContactListVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.isAddfamily = true
            vc.getContactList = { (name,phone) in
                self.nameTF.text = name
                self.MobileTF.text = phone
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @objc func addAdult(_ sender:UIButton) {
        sender.isSelected = true
        sender.bottomLineColor = UIColor(named: "HeaderColor") ?? #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        sender.bottomLineWidth = 3
        kidButtn.isSelected = false
        kidButtn.bottomLineColor = .lightGray
        kidButtn.bottomLineWidth = 3
        ImageView.isHidden = false
        adultView.isHidden = false
        kidView.isHidden = true
    }
    
    @objc func kid(_ sender:UIButton) {
        sender.isSelected = true
        sender.bottomLineColor = UIColor(named: "HeaderColor") ?? #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        sender.bottomLineWidth = 3
        adultButtn.isSelected = false
        adultButtn.bottomLineColor = .lightGray
        adultButtn.bottomLineWidth = 3
        ImageView.isHidden = true
        adultView.isHidden = true
        kidView.isHidden = false
    }
    
    
    
    @IBAction func addAdultButton(_ sender: UIButton) {
        guard let name = nameTF.text , !name.isBlank   else {
            ShowAlert(AlertMessage.alMEmptyFname, on: "", from: self)
            return
        }
        guard let mobile = MobileTF.text , !mobile.isBlank else {
            ShowAlert(AlertMessage.alEmptyMobile, on: "", from: self)
            return
        }
        guard let mobileNumber = MobileTF.text , isValidPhone(testStr: mobileNumber) else {
            ShowAlert(AlertMessage.alMValidMobile, on: "", from: self)
            return
        }
        self.AddMemberAPI(name: name, mobile: "\(iSDTF.text ?? "")\(mobile)", img: adultIcon.image ?? #imageLiteral(resourceName: "profile_icon"))
    }
    

    
    @IBAction func addChildButton(_ sender: UIButton) {
        guard let name = childNameTF.text , !name.isBlank   else {
            ShowAlert(AlertMessage.childName, on: "", from: self)
            return
        }
        self.AddMemberAPI(name: childNameTF.text ?? "", mobile: "", img: #imageLiteral(resourceName: "kids"))
    }
    
    func AddMemberAPI(name:String,mobile:String,img:UIImage){
        let param = [
            "name":name,
            "mobile":mobile,
            "age_type":type,
            "permission":permisions,
            "lang_id":language,
            "uid":userData?.id ?? "",
            "unitnumber":userData?.unitnumber ?? "",
            "towerid":userData?.towerid ?? ""
        ]
        
        var imageArr = [imagesUpload]()
        imageArr.append(imagesUpload(key: "document", image:img , fileName: "\(name)_Image.png"))

        
        APIClient().UploadImageWithParametersRequest(WithApiName: API.addFamilyMember.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {[weak self](response, status) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                default:
                    DispatchQueue.main.async {
                        ShowAlert(object.msg ?? "", on: "", from: self)
                    }
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    @IBAction func requirePermissionButton(_ sender: UIButton) {
        self.SelectAction(isPermission: true, isNotification: false, isNothing: false)
    }
    
    @IBAction func sendNotificationTFButton(_ sender: UIButton) {
        self.SelectAction(isPermission: false, isNotification: true, isNothing: false)
    }
    
    @IBAction func doNothingButton(_ sender: UIButton) {
        self.SelectAction(isPermission: false, isNotification: false, isNothing: true)
    }
    
    @IBAction func AddImageButton(_ sender: UIButton) {
        self.imagePicker?.present(from: sender)
        
    }
    
    func didSelect(image: UIImage?, tag: Int) {
        adultIcon.image = image
    }
    
    func SelectAction(isPermission:Bool, isNotification:Bool, isNothing:Bool){
        requirePermission.isSelected = isPermission
        sendNotificationTF.isSelected = isNotification
        doNothing.isSelected = isNothing
        PrintLog(permisions)
    }
    
}



