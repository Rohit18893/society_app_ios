//
//  FamilyCVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 23/05/21.
//

import UIKit

class FamilyCVC: UICollectionViewCell {

    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var ticketID: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var profileIcon: Imageradius!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        nameLbl.adjustsFontSizeToFitWidth = true
        nameLbl.numberOfLines = 2
    }

}
