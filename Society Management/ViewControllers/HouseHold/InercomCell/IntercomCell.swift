//
//  IntercomCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 07/06/21.
//

import UIKit

class IntercomCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
