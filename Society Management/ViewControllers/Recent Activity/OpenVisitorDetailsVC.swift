//
//  OpenVisitorDetailsVC.swift
//  Society Management
//
//  Created by Cricket Mazza on 30/08/21.
//

import UIKit

class OpenVisitorDetailsVC: MasterVC {

    @IBOutlet weak var inTime: UILabel!
    @IBOutlet weak var outTime: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var companyImage: UIImageView!
    
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var statusView: UIView!
    
    @IBOutlet weak var outView: UIView!
    @IBOutlet weak var inView: UIView!
    
    @IBOutlet weak var visitorMobile: UILabel!
    @IBOutlet weak var tagLbl: PaddingLabel!
    @IBOutlet weak var visitorName: UILabel!
    @IBOutlet weak var visitorImage: Imageradius!
    
    @IBOutlet weak var titleLbl: UILabel!
    var visitordata:RecentActivityData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        self.setData()

    }
    
    func setData(){
                
        var vehicelNUmber:String {
            return visitordata?.vehicle_number?.count ?? 0 > 0 ? "  •  #\(visitordata?.vehicle_number ?? "")" : ""
        }
        
        if let name = visitordata?.title {
            visitorName.text = name
        }
        
        if let tag = visitordata?.tag {
            tagLbl.text = tag.uppercased()
            self.tagLbl.backgroundColor = statusColor(self.tagLbl.text?.lowercased() ?? "")
        }
        
        if let mobile = visitordata?.mobile {
            visitorMobile.text = mobile
        }
        
        if let service_name = visitordata?.service_name {
            titleLbl.text = service_name
        }
        if let added_by = visitordata?.added_by {
            if visitordata?.accept_flag != "1" {
                if let setTime = visitordata?.slot_start {
                    self.statusLbl.text = added_by + " • \(getTimedate(Int(setTime) ?? 0))"
                }
            }else{
                self.statusLbl.text = added_by
            }
        }
        
        if let icon = visitordata?.icon {
            visitorImage.sd_setImage(with: URL(string: icon), placeholderImage:setImages(visitordata?.entry_type ?? "") )
            visitorImage.contentMode = .scaleAspectFill
        }
        
        if let setTime = visitordata?.slot_start , setTime.count > 0 {
            self.inTime.text = "\(getTimedate(Int(setTime) ?? 0))"
        }
        
        if let outTime = visitordata?.slot_end , outTime.count > 0 {
            self.outTime.text = "\(getTimedate(Int(outTime) ?? 0))"
        }
        
        if let company_image = visitordata?.company_image,
           let company_name = visitordata?.company_name ,
           company_name.count > 0 {
            companyView.isHidden = false
            companyName.text = company_name.capitalized + vehicelNUmber
            companyImage.sd_setImage(with: URL(string: company_image), placeholderImage:UIImage(systemName: "person.circle.fill") )
        } else{
            companyView.isHidden = false
            companyName.text = ""
        }
        
        if visitordata?.accept_flag == "1" {
            outView.isHidden = false
            inView.isHidden = false
        }else{
            outView.isHidden = true
            inView.isHidden = true
        }
    }

    @IBAction func callButton(_ sender: Any) {
        if let mobile = visitordata?.mobile {
            self.callNumber(phoneNumber: mobile)
        }
    }
}
