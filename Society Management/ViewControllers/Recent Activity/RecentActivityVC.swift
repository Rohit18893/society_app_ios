//
//  RecentActivityVC.swift
//  Society Management
//
//  Created by Cricket Mazza on 11/08/21.
//

import UIKit
import SVProgressHUD
class RecentActivityVC: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var noDataView: UIView!
    
    var refreshControl = UIRefreshControl()
    let recentActivityViewModel = RecentActivityViewModel()
    
    fileprivate let Cells = [
        CellRecentActivity.self,
        RecentHelperCell.self
    ]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.noDataView.isHidden = true
        self.className(ClassString: self)
        tblView.delegate = self
        tblView.dataSource = self
        self.tblView.register(cellTypes: Cells)
        self.className(ClassString: self)
        self.tblView.backgroundColor = .clear
        self.getData(true)
        // self.tblView.isHidden = true
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(houseHoldReload(notification:)), name: .householdReload, object: nil)
        
    }
    
    @objc func houseHoldReload(notification: NSNotification) {
        self.getData(false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .householdReload, object: nil)
    }
    
    @objc func refresh(_ refresh: UIRefreshControl) {
        self.getData(false)
        refresh.endRefreshing()
    }
    
    func getData(_ isNeedLoader:Bool){
        let params = ["uid":userData?.id ?? "","towerid":userData?.towerid ?? "","unitnumber":userData?.unitnumber ?? "","lang_id":language]
        debugPrint(params)
        recentActivityViewModel.featchRecentActivity(isNeedLoader, params) { [weak self] isSuccess in
            guard let self = self else {return}
            DispatchQueue.main.async {
                if isSuccess {
                    self.tblView.reloadData()
                    self.noDataView.isHidden = true
                }else{
                    self.noDataView.isHidden = false
                }
            }
        }
    }
    
    //    @objc func reloadData() {
    //    self.perform(#selector(self.reloadData), with: nil, afterDelay: 1.0)
    //        self.tblView.isHidden = false
    //        self.tblView.reloadData()
    //        SVProgressHUD.dismiss()
    //    }
}

extension RecentActivityVC: UITableViewDelegate,UITableViewDataSource  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return recentActivityViewModel.recentActivityModel?.recent_activity?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentActivityViewModel.recentActivityModel?.recent_activity?[section].data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.center = headerView.center
        let dateText = recentActivityViewModel.recentActivityModel?.recent_activity?[section].date ?? ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM d, yyyy"
        
        if let date: Date = dateFormatterGet.date(from: dateText) {
            label.text = dateFormatterPrint.string(from: date)
        }
        label.font = .boldSystemFont(ofSize: 20)
        label.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        label.textAlignment = .center
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let result = recentActivityViewModel.recentActivityModel?.recent_activity?[indexPath.section].data?[indexPath.row]
        if result?.activity_id == "4" || result?.other_id == "4" {
            let cell = tableView.dequeueReusableCell(with: RecentHelperCell.self, for: indexPath)
            cell.recentActivityData = result
            cell.firstButton.tag = indexPath.row
            cell.secondButton.tag = indexPath.row
            cell.callButton.tag = indexPath.row
            
            cell.onClickButton = { text,index in
                if let cellResult = self.recentActivityViewModel.recentActivityModel?.recent_activity?[indexPath.section].data?[index] {
                    self.AllButtonAction(text.lowercased(), cellResult)
                }
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(with: CellRecentActivity.self, for: indexPath)
            cell.recentActivityData = result
            cell.firstButton.tag = indexPath.row
            cell.secondButton.tag = indexPath.row
            cell.thiredButton.tag = indexPath.row
            cell.onClickButton = { text,index in
                if let cellResult = self.recentActivityViewModel.recentActivityModel?.recent_activity?[indexPath.section].data?[index] {
                    self.AllButtonAction(text.lowercased(), cellResult)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let recentActivityData = recentActivityViewModel.recentActivityModel?.recent_activity?[indexPath.section].data?[indexPath.row] {
            print(recentActivityData)
            //MARK: Cab
            if recentActivityData.activity_id == "1" && recentActivityData.entry_type == "2" {
                self.ViewLogs(#imageLiteral(resourceName: "cab"), recentActivityData, "Frequent Entry Log")
                
                //MARK: Delivery
            } else if recentActivityData.activity_id == "2" && recentActivityData.entry_type == "2" {
                self.ViewLogs(#imageLiteral(resourceName: "delivery_icon"), recentActivityData, "Frequent Entry Log")
                
                //MARK: Guest
            } else if recentActivityData.activity_id == "3" && recentActivityData.entry_type == "2" {
                self.ViewLogs(#imageLiteral(resourceName: "guest"), recentActivityData,"Frequent Entry Log")
                
                //MARK: Vehcle
            } else if recentActivityData.activity_id == "4" || recentActivityData.other_id == "4" {
                DispatchQueue.main.async {
                    let mainVC = DailyHelperDetailView(nibName:"DailyHelperDetailView", bundle:nil)
                    mainVC.recentData = recentActivityData
                    self.navigationController?.pushViewController(mainVC, animated: true)
                }
                
                //MARK: Vehcle
            } else if recentActivityData.activity_id == "5" {
                //                if recentActivityData.entry_type == "1" {
                //                    self.ViewLogs(#imageLiteral(resourceName: "car"), recentActivityData, "Vehical Entry Log")
                //                }else{
                //                    self.ViewLogs(#imageLiteral(resourceName: "bike"), recentActivityData, "Vehical Entry Log")
                //                }
                
                
                //MARK: Kid
                
                //                else if recentActivityData.activity_id == "6" || (recentActivityData.activity_id == "11" && recentActivityData.entry_type == "2") {
                //                   self.ViewLogs(#imageLiteral(resourceName: "kids"), recentActivityData, "Kid Entry Log")
                
            } else if recentActivityData.activity_id == "6" {
                self.ViewLogs(#imageLiteral(resourceName: "kids"), recentActivityData, "Kid Entry Log")
                
                //MARK: Visiting Help
            } else if recentActivityData.activity_id == "7"  && recentActivityData.entry_type == "2" {
                self.ViewLogs(#imageLiteral(resourceName: "visiting_help"), recentActivityData, "Frequent Entry Log")
                
                
            } else if recentActivityData.activity_id == "8" {
                
            } else if recentActivityData.activity_id == "9" {
                
            } else if recentActivityData.activity_id == "10" {
                
            } else if recentActivityData.activity_id == "12" {
                let vc = OpenVisitorDetailsVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.visitordata = recentActivityData
                self.present(vc, animated: true, completion: nil)
            } else if recentActivityData.activity_id == "13" {
                
                if recentActivityData.other_id == "1" && recentActivityData.entry_type == "2" {
                    self.ViewLogs(#imageLiteral(resourceName: "cab"), recentActivityData, "Frequent Entry Log")
                }else if recentActivityData.other_id == "2" && recentActivityData.entry_type == "2" {
                    self.ViewLogs(#imageLiteral(resourceName: "delivery_icon"), recentActivityData, "Frequent Entry Log")
                } else if recentActivityData.other_id == "3" && recentActivityData.entry_type == "2" {
                    self.ViewLogs(#imageLiteral(resourceName: "guest"), recentActivityData,"Frequent Entry Log")
                }else if recentActivityData.other_id == "7" && recentActivityData.entry_type == "2" {
                    self.ViewLogs(#imageLiteral(resourceName: "visiting_help"), recentActivityData, "Frequent Entry Log")
                }else{}
            } else {
                
            }
        }
    }
    
    
    func AllButtonAction(_ Text:String,_ rowData:RecentActivityData){
        if Text.contains("edit") {
            print(Text)
            print(rowData)
        } else if Text.contains("invite to") {
            self.shareWithFriends()
        } else if Text.contains("decline") {
            print("decline")
            self.AcceptDecline(rowData.entry_id ?? "", "2")
        } else if Text.contains("accept") {
            print("Accept")
            self.AcceptDecline(rowData.entry_id ?? "", "1")
        } else if Text.contains("wrong") {
            print("Wrong entry")
            self.AcceptDecline(rowData.entry_id ?? "", "2")
        } else if Text.contains("share") {
            let vc = ShareCodeVC()
            vc.id = rowData.entry_id
            vc.guestType = rowData.entry_type == "1" ? "2":"1"
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        } else if Text.contains("call") {
            print(Text)
            print(rowData)
            if let mobile = rowData.mobile, mobile.count > 0 {
                self.callNumber(phoneNumber: mobile)
            }else{
                ShowAlert("For the privacy of your guest, we are not taking their mobile number", on: "ALM Alert!", from: self)
            }
        } else if Text.contains("cancel") || Text.contains("remove") {
            canceledAPI(rowData)
        }else{
            print(Text)
            print(rowData)
        }
    }
    
    func canceledAPI(_ rowData:RecentActivityData) {
        if rowData.activity_id == "3" {
            self.cancelguest(rowData.entry_id ?? "")
        } else if rowData.activity_id == "6" {
            self.cancelkid(rowData.entry_id ?? "")
        } else{
            self.cancelCab_visitingHelp_delivery(rowData.entry_id ?? "")
        }
    }
    
    func cancelkid(_ entryId:String) {
        APIClient().PostdataRequest(WithApiName: API.deleteKid.rawValue, Params: ["lang_id":language,"kids_log_id":entryId], isNeedLoader: true, objectType: StatusModel.self) { response, statusCode in
            switch response {
            case .success(_):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .householdReload, object: nil)
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    func cancelguest(_ entryId:String){
        APIClient().PostdataRequest(WithApiName: API.deleteguest.rawValue, Params: ["lang_id":language,"id":entryId], isNeedLoader: true, objectType: StatusModel.self) { response, statusCode in
            switch response {
            case .success(_):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .householdReload, object: nil)
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    func cancelCab_visitingHelp_delivery(_ entryId:String){
        APIClient().PostdataRequest(WithApiName: API.recentActivityCanceled.rawValue, Params: ["lang_id":language,"frequently_id":entryId], isNeedLoader: true, objectType: StatusModel.self) { response, statusCode in
            switch response {
            case .success(_):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .householdReload, object: nil)
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
    
    func ViewLogs(_ img:UIImage,_ rowData:RecentActivityData,_ title:String){
        let vc = self.mainStory.instantiateViewController(withIdentifier: "LogsVC") as! LogsVC
        vc.logsTitle = title
        vc.data = rowData
        vc.icon = img
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func AcceptDecline(_ entryId:String,_ flag:String){
        let params = ["entry_id":entryId,
                      "lang_id":language,
                      "accept_decline_time":Date.getCurrentDate(),
                      "accept_decline_flag":flag]
        debugPrint(params)
        APIClient().PostdataRequest(WithApiName: API.acceptDecline.rawValue, Params: params, isNeedLoader: true, objectType: StatusModel.self) { response, statusCode in
            switch response {
            case .success(_):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .householdReload, object: nil)
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
}



