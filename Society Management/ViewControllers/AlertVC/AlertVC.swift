//  AlertVC.swift
//  Society Management
//  Created by ROOP KISHOR on 02/09/2021.


import UIKit

class AlertVC: MasterVC {
    
    @IBOutlet weak var lblMessage: UILabel!
    var alertmessage = String()
    var isBack: Bool = false
    var onClickOk:((Bool)->())? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblMessage.text = alertmessage
       
    }

    @IBAction func okButtunClicked(_ sender: Any) {
        if let r = self.onClickOk{
            r(isBack)
        }
        self.back(self)
    }
   
}
