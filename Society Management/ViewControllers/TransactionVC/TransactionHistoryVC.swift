//  TransactionHistoryVC.swift
//  Society Management
//  Created by ROOP KISHOR on 07/09/2021.


import UIKit

class TransactionHistoryVC: MasterVC {

    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var noRecordFound: UILabel!
    var refreshControl = UIRefreshControl()
    var transactionData = [TransactionData]()
    fileprivate let arrCells = [AdminTransactionsCell.self]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title33
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
       
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        self.tblView.tableFooterView = UIView()
        self.getTransactionHistory(isNeedLoader: true)
        noRecordFound.isHidden = true
        if #available(iOS 10.0, *) {
            
           tblView.refreshControl = refreshControl
            
        } else {
            
           tblView.addSubview(refreshControl)
        }
        
    }
    
    @objc func refresh(_ refresh: UIRefreshControl)
    {
        refresh.endRefreshing()
        self.getTransactionHistory(isNeedLoader: false)
    }
  

}
extension TransactionHistoryVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return transactionData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if self.transactionData[indexPath.row].orderid == "" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AdminTransactionsCell", for: indexPath) as! AdminTransactionsCell
//            cell.backgroundColor = UIColor.clear
//            cell.lblAmount.text = " AED \(self.transactionData[indexPath.row].amount ?? "")"
//            cell.lblOrderID.text = self.transactionData[indexPath.row].orderid
//            cell.lblOrderID.text = self.transactionData[indexPath.row].orderid
//            cell.lblTransactionID.text = self.transactionData[indexPath.row].transactionid
//            cell.lblPaymentMode.text = self.transactionData[indexPath.row].card_type
//            cell.lblCardNum.text = self.transactionData[indexPath.row].card_info
//            cell.lblDate.text = self.transactionData[indexPath.row].transactiontime
//
//            if self.transactionData[indexPath.row].paystatus == "1" {
//
//                cell.lblStatus.text = "Successfull"
//                cell.lblStatus.backgroundColor = UIColor.systemGreen
            
//            } else {
//
//                cell.lblStatus.text = "Failed"
//                cell.lblStatus.backgroundColor = UIColor.red
//            }
            
            return cell
            
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionCell", for: indexPath) as! TransactionCell
            cell.backgroundColor = UIColor.clear
            cell.lblAmount.text = " AED \(self.transactionData[indexPath.row].amount ?? "")"
            cell.lblOrderID.text = self.transactionData[indexPath.row].orderid
            cell.lblOrderID.text = self.transactionData[indexPath.row].orderid
            cell.lblTransactionID.text = self.transactionData[indexPath.row].transactionid
            cell.lblPaymentMode.text = self.transactionData[indexPath.row].card_type
            cell.lblCardNum.text = self.transactionData[indexPath.row].card_info
            cell.lblDate.text = self.transactionData[indexPath.row].transactiontime
            
            if self.transactionData[indexPath.row].paystatus == "1" {
                
                cell.lblStatus.text = "Successfull"
                cell.lblStatus.backgroundColor = UIColor.systemGreen
            } else {
                
                cell.lblStatus.text = "Failed"
                cell.lblStatus.backgroundColor = UIColor.red
            }
            
            return cell
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
   
    func getTransactionHistory(isNeedLoader:Bool) {
      
        let params = ["uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.transactionHistory.rawValue, Params: params as [String : Any], isNeedLoader: isNeedLoader, objectType: TransactionDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.transactionData.removeAll()
                        if let data = object.data
                        {
                            self.transactionData = data
                        }
                        self.noRecordFound.isHidden = true
                        self.tblView.reloadData()
                        
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.transactionData.removeAll()
                    self.noRecordFound.isHidden = false
                    self.tblView.reloadData()
                }
                
            }
        }
        
    }
    
    
}
