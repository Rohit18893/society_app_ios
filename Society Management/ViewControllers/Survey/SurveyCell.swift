//  SurveyCell.swift
//  Society Management
//  Created by ROOP KISHOR on 29/06/2021.


import UIKit

class SurveyCell: UITableViewCell {
    
   
    @IBOutlet weak var viewBack: UIView!
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var btnCheckmark: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
