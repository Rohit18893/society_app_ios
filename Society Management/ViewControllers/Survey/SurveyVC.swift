//  SurveyVC.swift
//  Society Management
//  Created by ROOP KISHOR on 29/06/2021.


import UIKit

class SurveyVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    var questionData = [QuestionData]()
    var strSurveyID = String()
    var answerArray = NSMutableArray()
    var selectedIndex = [IndexPath]()
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblNoData: UILabel!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title8
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        tblView.allowsMultipleSelection = true
        self.btnSubmit.isHidden = true
        lblNoData.isHidden = true
        self.getAllQuestions()
    }
   
    
    @IBAction func submitButtunClicked(_ sender: UIButton)
    {
        for i in 0..<selectedIndex.count
        {
            let indexpath = selectedIndex[i]
            let idsArr  = NSMutableArray()
            idsArr.add(self.questionData[indexpath.section].answer_result?[indexpath.row].survey_answer_id ?? "")
            let dict = ["qid":self.questionData[indexpath.section].question_id as Any,"answer":idsArr] as [String : Any]
            self.answerArray.add(dict)
        }
        self.submitSurvey()
        
    }
    

}

extension SurveyVC: UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return self.questionData.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let rows = self.questionData[section].answer_result
        return rows?.count ?? 0
    }
    


    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let label = TableViewHeaderLabel()
        let quest =  self.questionData[section].question
        label.text = String(format: "%ld: %@", section + 1,quest!)
        return label
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyCell", for: indexPath) as! SurveyCell
        if let answers = questionData[indexPath.section].answer_result
        {
            cell.lblOption.text = answers[indexPath.row].answer
        }
        
        if selectedIndex.contains(indexPath) {
            
            cell.btnCheckmark.setImage(UIImage(named: "radio_active"), for: .normal)
        }
        else
        {
            cell.btnCheckmark.setImage(UIImage(named: "radio_unactive"), for: .normal)
        }
        
        return cell
        
    }
    
    
    
    //On Selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

            let selectedIndexPathAtCurrentSection = selectedIndex.filter({ $0.section == indexPath.section})

            for indexPath in selectedIndexPathAtCurrentSection {
                tableView.deselectRow(at: indexPath, animated: true)
                if let indexOf = selectedIndex.firstIndex(of: indexPath) {
                    selectedIndex.remove(at: indexOf)
                }
            }
        selectedIndex.append(indexPath)
        self.tblView.reloadData()


    }
    // On DeSelection
     func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

        if let index = selectedIndex.firstIndex(of: indexPath) {
            selectedIndex.remove(at: index)
        }
    }
   
    func getAllQuestions() {
        
        let params = ["survey_id":strSurveyID,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.surveyQuestions.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: QuestionsDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object.result)
                        if let rs = object.result
                        {
                            self.questionData = rs
                        }
                        self.tblView.isHidden = false
                        self.btnSubmit.isHidden = false
                        print(self.answerArray)
                        self.lblNoData.isHidden = true
                        self.tblView.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                    self.tblView.isHidden = true
                    self.btnSubmit.isHidden = true
                    self.lblNoData.isHidden = false
                }
                
            }
            
        }

    }
    
    func submitSurvey() {
        
     
        let params = ["uid":userData?.id ?? "","towerid":userData?.towerid ?? "","unitnumber":userData?.unitnumber ?? "","survey_id":strSurveyID,"lang_id":language,"survey_answer":answerArray] as [String : Any]
        APIClient().PostdataRequest(WithApiName: API.surveyaddAnswrs.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object.msg)
                        self.showCustumAlert(message:object.msg ?? "", isBack: true)
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
                
            }else {
                
            }
            
        }

    }
    
}
