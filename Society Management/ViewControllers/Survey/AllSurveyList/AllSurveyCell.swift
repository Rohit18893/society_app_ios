//  AllSurveyCell.swift
//  Society Management
//  Created by ROOP KISHOR on 12/07/2021.

import UIKit

class AllSurveyCell: UITableViewCell {
    
    @IBOutlet weak var lblsurveyName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgCreatedBy: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
