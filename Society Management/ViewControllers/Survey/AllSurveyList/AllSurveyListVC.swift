//  AllSurveyListVC.swift
//  Society Management
//  Created by ROOP KISHOR on 12/07/2021.


import UIKit

class AllSurveyListVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var noRecordFound: UILabel!
    var surveyData = [SurveyData]()
    var refreshControl = UIRefreshControl()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title8
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        
        if #available(iOS 10.0, *) {
            
            tblView.refreshControl = refreshControl
            
        } else {
            
            tblView.addSubview(refreshControl)
        }
       
    }
    
    
    @objc func refresh(_ refresh: UIRefreshControl)
    {
        self.getSurveyData()
        refresh.endRefreshing()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        self.getSurveyData()
        
    }
    
}
extension AllSurveyListVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.surveyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllSurveyCell", for: indexPath) as! AllSurveyCell
        cell.lblsurveyName.text = surveyData[indexPath.row].surveyname
        
        if let imgArr = surveyData[indexPath.row].images {
            if imgArr.count > 0 {
                
                let img = imgArr[0]
                cell.imgCreatedBy.sd_setImage(with: URL(string:img), placeholderImage:UIImage(named: "setting_logo"))
                
            } else {
                cell.imgCreatedBy.image = UIImage(named: "No-image")
            }
            
        }
        
        let type = surveyData[indexPath.row].membername
        let stamp = surveyData[indexPath.row].created_date
        let date = convertTimeStampTo(timeStamp: stamp!)
        cell.lblType.text = String(format: "%@ %@",type!,date)
        cell.lblStatus.text = surveyData[indexPath.row].survey_flag
        if surveyData[indexPath.row].survey_flag == "Open"
        {
            cell.lblStatus.backgroundColor  = UIColor(named: AssetsColor.otherCatColor.rawValue)
        }
        else
        {
            cell.lblStatus.backgroundColor  = UIColor(named: AssetsColor.Headersecond.rawValue)
        }
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        if surveyData[indexPath.row].survey_flag == "Open" {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "SurveyVC") as! SurveyVC
            vc.strSurveyID = surveyData[indexPath.row].servey_id ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        } 
        
    }
   
    func getSurveyData() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.surveyList.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: SurveyDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object.result)
                        if let rs = object.result
                        {
                            self.surveyData = rs
                        }
                        
                        if self.surveyData.count > 0
                        {
                            self.noRecordFound.isHidden = true
                            self.tblView.isHidden = false
                        }
                        else
                        {
                            self.noRecordFound.isHidden = false
                            self.tblView.isHidden = true
                        }
                        self.tblView.reloadData()
                        
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                    self.noRecordFound.isHidden = false
                    self.tblView.isHidden = true
                 
                }
            }
            
        }

    }
    
}
