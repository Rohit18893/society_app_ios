//  ForgotPasswordVC.swift
//  Society Management
//  Created by ROOP KISHOR on 07/04/2021.


import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields


class ForgotPasswordVC: MasterVC {
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTextFieldTheme(txtField: txtEmail, labelText: "Mobile Number/Email ID", allowEditing: true)
        self.className(ClassString: self)
    }
    
   
    @IBAction func backButtonClicked(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func forgotPasswordClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
        guard let email = txtEmail.text , email.isBlank == false, email.isEmail == true  else {
           // ShowAlert(AlertMessage.alMValidEmail, on: "Error!", from: self)
           self.showCustumAlert(message: AlertMessage.alMValidEmail, isBack: false)

            
            return
        }
        
        APIClient().PostdataRequest(WithApiName: API.forgotpassword.rawValue, Params: ["email":email,"lang_id":language], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async
                    {
                        self.showCustumAlert(message:object.msg ?? "", isBack: true)
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
               //
                DispatchQueue.main.async
                {
                    ShowAlert("Invalid Credentials", on: "Opps?", from: self)
                   // self.showCustumAlert(message:"Invalid Credentials", isBack: false)
                }
                
            }
        }
    }
}
