//  AddFlatCell.swift
//  Society Management
//  Created by ROOP KISHOR on 27/06/2021.


import UIKit

class AddFlatCell: UITableViewCell {
    
    @IBOutlet weak var lblHoueseName: UILabel!
    @IBOutlet weak var btnAddFlat: UIButton!
    @IBOutlet weak var tblVilla: UITableView!
    fileprivate let arrCells = [VillaNameCell.self]
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var tableViewHeightValue: CGFloat = 0
    var villaInfo = [PropertyData]()
    var viewVC = UIViewController()
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        self.tblVilla.register(cellTypes: arrCells)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AddFlatCell: UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return villaInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "VillaNameCell", for: indexPath) as! VillaNameCell
        cell.lblHoueseName.text = String(format: "%@ , %@", villaInfo[indexPath.row].towername!,villaInfo[indexPath.row].unitname!)
        return cell
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ActionVillaVC") as! ActionVillaVC
        nextViewController.villaInfo = villaInfo[indexPath.row]
        viewVC.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
}
