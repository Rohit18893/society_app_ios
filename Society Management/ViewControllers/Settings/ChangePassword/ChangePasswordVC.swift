//  ChangePasswordVC.swift
//  Society Management
//  Created by ROOP KISHOR on 27/06/2021.


import UIKit

import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class ChangePasswordVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var currentPass: MDCOutlinedTextField!
    @IBOutlet weak var newPass: MDCOutlinedTextField!
    @IBOutlet weak var ConfirmNewPass: MDCOutlinedTextField!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title10
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.setTextFieldTheme(txtField:currentPass, labelText: "Current Password", allowEditing: true)
        self.setTextFieldTheme(txtField:newPass, labelText: "New Password", allowEditing: true)
        self.setTextFieldTheme(txtField:ConfirmNewPass, labelText: "New Confirm Password", allowEditing: true)
        
    }
    
    
    @IBAction func saveButtunClicked(_ sender: UIButton)
    {
        guard let currentpassword = currentPass.text , currentpassword.isBlank == false else {
            ShowAlert(AlertMessage.currentPassBlank, on: "", from: self)
            return
            
        }
        guard let passwordValid = currentPass.text , passwordValid.isValidPass == false else {
            ShowAlert(AlertMessage.alMValidPassword, on: "", from: self)
            return
        }
        guard let newpassword = newPass.text , newpassword.isBlank == false else {
            ShowAlert(AlertMessage.newPassBlank, on: "", from: self)
            return
            
        }
        guard let newpasswordValid = newPass.text , newpasswordValid.isValidPass == false else {
            ShowAlert(AlertMessage.alMValidPassword, on: "", from: self)
            return
            
        }
        guard let cPassword = ConfirmNewPass.text , cPassword.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyConfirmPassword, on: "", from: self)
            return
            
        }
        guard let cPasswordValid = ConfirmNewPass.text , cPasswordValid.isValidPass == false else {
            ShowAlert(AlertMessage.alMInvalidConfirmPass, on: "", from: self)
            return
            
        }
        if newPass.text != ConfirmNewPass.text {
            
            ShowAlert(AlertMessage.alMPasswordNotMatch, on: "", from: self)
            return
        }
       
        APIClient().PostdataRequest(WithApiName: API.changepassword.rawValue, Params: ["uid":userData?.id ?? "" ,"lang_id":language,"password":newPass.text!], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.showCustumAlert(message:object.msg ?? "", isBack: true)
                        
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
        
    
        
        
    }

   
}
