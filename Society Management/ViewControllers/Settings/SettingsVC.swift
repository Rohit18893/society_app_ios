//  SettingsVC.swift
//  Society Gate keeper
//  Created by Jitendra Yadav on 15/04/21.


import UIKit

class SettingsVC: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var supportView: UIView!
    @IBOutlet weak var lblWriteAlmudeer: UILabel!
    
    
    var villaData = [PropertyData]()
    
    fileprivate let arrCells = [AddFlatCell.self]
    var settingData = [Setting]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.className(ClassString: self)
        settingData.append(Setting(title: "Support & Feedback", icon: "feedback"))
        settingData.append(Setting(title: "Tell a friend about ALM", icon: "about"))
        settingData.append(Setting(title: "Change Password", icon: "change_pass"))
       // settingData.append(Setting(title: "App Language", icon: "app_language"))
        settingData.append(Setting(title: "Logout", icon: "logout_setting"))
        
        self.viewNAv.vc = self
        self.viewNAv.lbltitle.text = AlertMessage.title9
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(writeToAlmudeer(tapGestureRecognizer:)))
        lblWriteAlmudeer.addGestureRecognizer(tapGesture)
        
//        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(goToSupport(tapGestureRecognizer:)))
//        lblSupport.addGestureRecognizer(tapGesture2)
        
        
    }
    
    @objc func writeToAlmudeer(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let email = "IT@almudeer.ae"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    
    @objc func goToSupport(tapGestureRecognizer: UITapGestureRecognizer) {
      
        guard let url = URL(string: "https://almudeerit.com/contact") else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool)
    {
        self.getAllProperty()
    }
    
    
    @IBAction func SupportViewRemove(_ sender: UIButton)
    {
        supportView.isHidden = true
    }
    
    
}

extension SettingsVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 4
        {
            return settingData.count
            
        }else
        {
             return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingHeaderCell", for: indexPath) as! SettingHeaderCell
            cell.nameLbl.text = "\(String(describing:( userData?.fname)!.capitalized)) \(String(describing:(userData?.lname)!.capitalized))"
            cell.EmailLbl.text = "\(String(describing:( userData?.email)!))"
            cell.NumberLbl.text = "\(String(describing:( userData?.mobile)!))"

            if let name = cell.nameLbl.text {
                cell.twoWordLbl.text = name.getAcronyms()
            }
            cell.btnEditProfile.addTarget(self, action: #selector(self.editProfileClicked), for: .touchUpInside)
            
            return cell
            
        } else if indexPath.section == 1 {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
            cell.titileLbl.text = "App Notification Settings"
            cell.icon.image = UIImage(named: "app_notification")
            return cell
        }
        
        else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddFlatCell", for: indexPath) as! AddFlatCell
            cell.btnAddFlat.addTarget(self, action: #selector(self.addFlatClicked), for: .touchUpInside)
            cell.tableViewHeight.constant = CGFloat(65 * (villaData.count))
            cell.villaInfo = villaData
            cell.viewVC = self
            cell.tblVilla.reloadData()
            
            return cell
        }
        
        else if indexPath.section == 3
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
            cell.titileLbl.text = "Security Alert List"
            cell.icon.image = UIImage(named: "setting_alert")
            return cell
           
        }
        else if indexPath.section == 4
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
            cell.titileLbl.text = settingData[indexPath.row].title
            cell.icon.image = UIImage(named: settingData[indexPath.row].icon)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "footerCell", for: indexPath) as! footerCell
            return cell
            
        }
    }
    
    
    @objc func editProfileClicked(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "ProfileVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func addFlatClicked(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 4 {
            
            if indexPath.row == 0
            {
                self.supportView.isHidden = false
            }
            else if indexPath.row == 1
            {
                self.shareWithFriends()
            }
            else if indexPath.row == 2
            {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
//            else if indexPath.row == 3
//            {
//                self.changeLang()
//            }
            else
            {
                self.AlertController()
            }
        }
        else if indexPath.section == 1
        {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "NotificationSetting") as! NotificationSetting
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if indexPath.section == 3
        {
            
            let vc = SecurityAlertList()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        }
 
    }
    
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message:NSLocalizedString("Are you sure you want to logout?", comment: "") , preferredStyle: .actionSheet)

        let OK = UIAlertAction(title:NSLocalizedString("Ok", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            self.callLogoutApi()

            
        })
        let Cancel = UIAlertAction(title:NSLocalizedString("Cancel", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })

        optionMenu.addAction(OK)
        optionMenu.addAction(Cancel)
        
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func changeLang()
    {
        
        let optionMenu = UIAlertController(title: "", message:NSLocalizedString("Change Language", comment: ""), preferredStyle: .actionSheet)
        let englishAct = UIAlertAction(title: "English", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            UserDefaults.standard.set(["en"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            UIView.appearance().semanticContentAttribute            = UISemanticContentAttribute.forceLeftToRight
           // Bundle.setLanguage("en")
           // self.rootView()
            
        })
        
        let arabicAct = UIAlertAction(title:NSLocalizedString("Arabic", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            UserDefaults.standard.set(["ar"], forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            UIView.appearance().semanticContentAttribute            = UISemanticContentAttribute.forceRightToLeft
           // Bundle.setLanguage("ar")
           // self.rootView()
            
        })
        
        let Cancel = UIAlertAction(title:NSLocalizedString("Cancel", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        optionMenu.addAction(englishAct)
        optionMenu.addAction(arabicAct)
        optionMenu.addAction(Cancel)
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
    
    func getAllProperty() {
        APIClient().PostdataRequest(WithApiName: API.villalist.rawValue, Params: ["uid":userData?.id ?? "" ,"lang_id":language], isNeedLoader: false, objectType: PropertyDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                
                case .success(let object):
                    DispatchQueue.main.async {
                        print(object)
                        self.villaData = object.data!
                        self.tblView.reloadData()
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
            }
        }
        
    }
    
}
