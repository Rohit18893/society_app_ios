//  EntryNotificationCell.swift
//  Society Management
//  Created by ROOP KISHOR on 28/06/2021.


import UIKit

class EntryNotificationCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var entrySwitch: UISwitch!
    @IBOutlet weak var exitSwitch: UISwitch!
   

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
