//  NotificationSetting.swift
//  Society Management
//  Created by ROOP KISHOR on 28/06/2021.


import UIKit

class NotificationSetting: MasterVC {
   
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    var cellData = [Setting]()
    var NotificationSetting : NotificationSettingData?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title11
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
       
        cellData.append(Setting(title: "Entry/Exit Notifications", icon: "about"))
        cellData.append(Setting(title: "Daily Helps", icon: "about"))
        cellData.append(Setting(title: "Your Guests", icon: "setting_alert"))
        cellData.append(Setting(title: "Delivery,Cabs and Others", icon: "about"))
        self.getNotificationSettings()
        
    }
    

   
}
extension NotificationSetting: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 1
        {
            return 2
        }
        else
        {
            return cellData.count
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
         if indexPath.section == 0
        {
            if indexPath.row == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "AppNotifiCell", for: indexPath) as! AppNotifiCell
                cell.titileLbl.text = "Entry/Exit Notifications"
                cell.icon.image = UIImage(named: "validated")
                cell.btnSwitch.isHidden = true
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EntryNotificationCell", for: indexPath) as! EntryNotificationCell
                cell.lblTitle.text = cellData[indexPath.row].title
                cell.img.image =  UIImage(named:cellData[indexPath.row].icon)
                
                cell.entrySwitch.tag = indexPath.row
                cell.exitSwitch.tag = indexPath.row
                
                cell.entrySwitch.addTarget(self, action: #selector(self.switchEntry(_:)), for: .valueChanged)
                cell.exitSwitch.addTarget(self, action: #selector(self.switchExit(_:)), for: .valueChanged)

                if indexPath.row == 1 {
                    if NotificationSetting?.daily_entry == 1 {
                        cell.entrySwitch.isOn = true
                    } else {
                        cell.entrySwitch.isOn = false
                    }
                    
                    if NotificationSetting?.daily_exit == 1 {
                        cell.exitSwitch.isOn = true
                    } else {
                        cell.exitSwitch.isOn = false
                    }
                    
                    
                }
                else if indexPath.row == 2 {
                    
                    if NotificationSetting?.guest_entry == 1 {
                        cell.entrySwitch.isOn = true
                    } else {
                        cell.entrySwitch.isOn = false
                    }
                    
                    if NotificationSetting?.guest_exit == 1 {
                        cell.exitSwitch.isOn = true
                    } else {
                        cell.exitSwitch.isOn = false
                    }
                }
                else
                {
                    if NotificationSetting?.frequent_entry == 1 {
                        cell.entrySwitch.isOn = true
                    } else {
                        cell.entrySwitch.isOn = false
                    }
                    
                    if NotificationSetting?.frequent_exit == 1 {
                        cell.exitSwitch.isOn = true
                    } else {
                        cell.exitSwitch.isOn = false
                    }
                }
                
                return cell
            }
            
           
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AppNotifiCell", for: indexPath) as! AppNotifiCell
            cell.btnSwitch.isHidden = false
            cell.btnSwitch.addTarget(self, action: #selector(self.helpAndCommunication(_:)), for: .valueChanged)
            cell.btnSwitch.tag = indexPath.row
            if indexPath.row == 0 {
                
                cell.titileLbl.text = "Help Desk"
                cell.icon.image = UIImage(named: "helpdesk")
                if NotificationSetting?.help_desk == 1 {
                    cell.btnSwitch.isOn = true
                } else {
                    cell.btnSwitch.isOn = false
                }
                
            }
            else
            {
                cell.titileLbl.text = "Communications"
                cell.icon.image = UIImage(named: "communications")
                if NotificationSetting?.communication == 1 {
                    cell.btnSwitch.isOn = true
                } else {
                    cell.btnSwitch.isOn = false
                }
            }
            
            return cell
        }
        
    }
   
    
    @objc func switchEntry(_ sender : UISwitch!)
    {
        if sender.tag == 1
        {
           print("Daily Helps")
            if sender.isOn {
               
                self.updateNotificationSettings(daily_entry: 1, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
            }
            
        }
        else if sender.tag == 2
        {
            print("guest ")
            if sender.isOn {
               
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: 1, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
            }
        }
        else
        {
            print("frequent")
            if sender.isOn {
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry:1, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry:NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry:0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
            }
        }
        
        
    }
    
    @objc func switchExit(_ sender : UISwitch!)
    {
         
        if sender.tag == 1
        {
           print("Daily Helps")
            if sender.isOn {
               
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit:1, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit:0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
            }
            
        }
        else if sender.tag == 2
        {
            print("guest ")
            if sender.isOn {
               
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: 1, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit:0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
            }
        }
        else
        {
            print("frequent")
            if sender.isOn {
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry:NotificationSetting?.frequent_entry ?? 0, frequent_exit: 1, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry:NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry:NotificationSetting?.frequent_entry ?? 0, frequent_exit:0, help_desk: NotificationSetting?.help_desk ?? 0, communication: NotificationSetting?.communication ?? 0)
            }
        }
        
        
    }
    
    
    @objc func helpAndCommunication(_ sender : UISwitch!)
    {
        
        if sender.tag == 0 {
            
            print("Help Desk")
            if sender.isOn {
               
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: 1, communication: NotificationSetting?.communication ?? 0)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit:NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk:0, communication: NotificationSetting?.communication ?? 0)
            }
            
        } else {
            
            print("Comminication")
            if sender.isOn {
               
                self.updateNotificationSettings(daily_entry: NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit: NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk: NotificationSetting?.help_desk ?? 0, communication: 1)
                
            }
            else
            {
                self.updateNotificationSettings(daily_entry:NotificationSetting?.daily_entry ?? 0, daily_exit: NotificationSetting?.daily_exit ?? 0, guest_entry: NotificationSetting?.guest_entry ?? 0, guest_exit:NotificationSetting?.guest_exit ?? 0, frequent_entry: NotificationSetting?.frequent_entry ?? 0, frequent_exit: NotificationSetting?.frequent_exit ?? 0, help_desk:NotificationSetting?.help_desk ?? 0, communication:0)
            }
        }
        
    }
    
    
    func getNotificationSettings() {
        let params = ["uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.notification_setting.rawValue, Params: params as [String : Any], isNeedLoader: false, objectType: NotificationSettingModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                       
                        if let setting = object.data?.first
                        {
                            self.NotificationSetting = setting
                        }
                        self.tblView.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }
        }
        
    }
   
        
    func updateNotificationSettings(daily_entry:Int,daily_exit:Int,guest_entry:Int,guest_exit:Int,frequent_entry: Int,frequent_exit: Int,help_desk: Int,communication: Int) {
        let params = ["uid":userData?.id ?? "","entry_exit":0,"daily_entry":daily_entry,"daily_exit":daily_exit,"guest_entry":guest_entry,"guest_exit":guest_exit,"frequent_entry":frequent_entry,"frequent_exit":frequent_exit,"help_desk":help_desk,"communication":communication] as [String : Any]
        
        print(params)
        
        APIClient().PostdataRequest(WithApiName: API.update_notification_setting.rawValue, Params: params as [String : Any], isNeedLoader: false, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                       
                        PrintLog(object)
                      //  self.getNotificationSettings()
                       // ShowAlert(object.msg ?? "", on: "", from: self)
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }
        }
        
    }
    
   
    
}
