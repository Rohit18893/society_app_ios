//  AppNotifiCell.swift
//  Society Management
//  Created by ROOP KISHOR on 28/06/2021.


import UIKit

class AppNotifiCell: UITableViewCell {
    
    @IBOutlet weak var titileLbl: UILabel!
    @IBOutlet weak var btnSwitch: UISwitch!
    @IBOutlet weak var icon: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
