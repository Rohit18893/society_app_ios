//  NotificationListVC.swift
//  Society Management
//  Created by ROOP KISHOR on 01/09/2021.


import UIKit

class NotificationListVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var noRecordFound: UILabel!
    var refreshControl = UIRefreshControl()
    var notifictionList = [NotificationListData]()
    fileprivate let arrCells = [NotificationListCell.self]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title1
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        self.tblView.register(cellTypes: arrCells)
        self.tblView.tableFooterView = UIView()
        self.noRecordFound.isHidden = false
        noRecordFound.isHidden = true
        if #available(iOS 10.0, *) {
            
            tblView.refreshControl = refreshControl
            
        } else {
            
            tblView.addSubview(refreshControl)
        }

        
    }
    
    @objc func refresh(_ refresh: UIRefreshControl)
    {
        refresh.endRefreshing()
        self.getNotificationList(loaderNeed: false)
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getNotificationList(loaderNeed: true)
    }

}
extension NotificationListVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notifictionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationListCell", for: indexPath) as! NotificationListCell
        
        cell.lblTitle.text = notifictionList[indexPath.row].title
        cell.lblDesc.text = notifictionList[indexPath.row].msg
        
        
        if let dt = notifictionList[indexPath.row].created_at {
            cell.lblTIme.text = timeElapsed(date: Date(timeIntervalSince1970:TimeInterval(dt)))
        }
        
        
        switch indexPath.row % 4 {
        case 0:
               cell.colorView.backgroundColor = .systemOrange
               cell.icon.tintColor = .systemOrange
        case 1:
            cell.colorView.backgroundColor = .systemRed
            cell.icon.tintColor = .systemRed
        case 2:
            cell.colorView.backgroundColor = .systemGreen
            cell.icon.tintColor = .systemGreen
        case 3:
            cell.colorView.backgroundColor = .systemPink
            cell.icon.tintColor = .systemPink
        default:
            cell.colorView.backgroundColor = .black
            cell.icon.tintColor = .black
        }
        
        return cell
        
    }
    
    func getNotificationList(loaderNeed:Bool)
    {
        let params = ["lang_id":language,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.notificationList.rawValue, Params: params as [String : Any], isNeedLoader: loaderNeed, objectType: NotificationListModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.notifictionList.removeAll()
                        if let data = object.data
                        {
                            self.notifictionList = data
                        }
                        self.noRecordFound.isHidden = true
                        self.tblView.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                 DispatchQueue.main.async {
                    self.notifictionList.removeAll()
                    self.noRecordFound.isHidden = false
                    self.tblView.reloadData()
                }
                
            }
        }
        
    }
    
}
