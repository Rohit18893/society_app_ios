//  AddProperty.swift
//  Society Management
//  Created by ROOP KISHOR on 03/07/2021.


import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class AddProperty: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var txtCity: MDCOutlinedTextField!
    @IBOutlet weak var txtTower: MDCOutlinedTextField!
    @IBOutlet weak var txtUnitNumber: MDCOutlinedTextField!
    @IBOutlet weak var btnSubmit: UIButton!
    var strIsEdit = String()
    var villaDetail : PropertyData?
    
    
    let cityModelView = CityModelView ()
    var towerModelView = TowerModalView ()
    var unitModelView = UnitModalView ()
    
    var cityID = String()
    var towerID = String()
    var unitID = String()
   

    override func viewDidLoad() {
       
        super.viewDidLoad()
        self.className(ClassString: self)
       
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.setTextFieldTheme(txtField:txtCity, labelText: "City", allowEditing: false)
        self.setTextFieldTheme(txtField:txtTower, labelText: "Tower Name", allowEditing: false)
        self.setTextFieldTheme(txtField:txtUnitNumber, labelText: "Unit Number", allowEditing: true)
        cityModelView.getAllCities()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.strIsEdit == "1" {
            
            self.viewNAv.lbltitle.text =  "\(villaDetail?.towername ?? "") , \(villaDetail?.unitname ?? "")"
            self.txtCity.text = villaDetail?.cityname
            self.txtTower.text = villaDetail?.towername
            self.txtUnitNumber.text = villaDetail?.unitname
            btnSubmit.setTitle("EDIT", for: .normal)
            cityID = villaDetail?.city_id ?? ""
            towerID = villaDetail?.towerid ?? ""
            unitID = villaDetail?.unitnumber ?? ""
        }
        else
        {
            self.viewNAv.lbltitle.text = AlertMessage.title3
            btnSubmit.setTitle("ADD", for: .normal)
        }
        
    }
    
    @IBAction func addFlat(_ sender: UIButton)
    {
        guard let city = txtCity.text , city.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyCity, on: "", from: self)
            return
            
        }
        guard let tower = txtTower.text , tower.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyTower, on: "", from: self)
            return
            
        }
        guard let unit = txtUnitNumber.text , unit.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyUnit, on: "", from: self)
            return
            
        }
        
        if self.strIsEdit == "1"
        {
            self.editProperty()
        }
        else
        {
            self.addProperty()
        }
        
        
    }
    
    @IBAction func selectCityClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
      if  let cityname = cityModelView.cityData?.data?.map({$0.name }) as? [String] ,
          let cityID = cityModelView.cityData?.data?.map({$0.id}) as? [String] {
        CustomPickerView.show(items: cityname , itemIds: cityID ,selectedValue:self.txtCity.text, doneBottonCompletion: { (item, index) in
            
               self.txtCity.text = item
               PrintLog(index)
               self.txtTower.text = ""
               self.txtUnitNumber.text = ""
               self.cityID = index!
               self.towerModelView = TowerModalView()
               self.unitModelView = UnitModalView()
               self.towerModelView.getAllTowers(params: ["city" : index!])
            
            }, didSelectCompletion: { (item, index) in
                self.txtCity.text = item
                
            }) { (item, index) in
            }
       }
    }
    
    /*
          SELCT TOWER
     */
    
    @IBAction func selectTowerClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if  let towerName = towerModelView.towerData?.data?.map({$0.name }) as? [String] ,
            let towerID = towerModelView.towerData?.data?.map({$0.id}) as? [String] {
          CustomPickerView.show(items: towerName , itemIds: towerID ,selectedValue:self.txtTower.text, doneBottonCompletion: { (item, index) in
              
                  self.txtTower.text = item
                  self.towerID = index!
                  self.txtUnitNumber.text = ""
                  self.unitModelView = UnitModalView()
                  self.unitModelView.getAllUnits(params: ["city" : self.cityID,"towerid":self.towerID])
              
              }, didSelectCompletion: { (item, index) in
                  self.txtTower.text = item
                  
                  
              }) { (item, index) in
              }
         }
    }
    
    
    /*
          SELCT TOWER
     */
    
    @IBAction func selectUnitClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if  let unitName = unitModelView.unitData?.data?.map({$0.name }) as? [String] ,
            let unitID = unitModelView.unitData?.data?.map({$0.id}) as? [String] {
          CustomPickerView.show(items: unitName , itemIds: unitID ,selectedValue:self.txtUnitNumber.text, doneBottonCompletion: { (item, index) in
              
                  self.txtUnitNumber.text = item
                  self.unitID = index!
              
              }, didSelectCompletion: { (item, index) in
                  self.txtUnitNumber.text = item
                  
                  
              }) { (item, index) in
              }
         }
    }
    
    func addProperty() {
   
        let params = ["lang_id":language,"uid":userData?.id,"towerid":towerID,"unitnumber":unitID,"city_id":cityID]
        APIClient().PostdataRequest(WithApiName: API.addvilla.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: DetailsModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
           // if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    if statuscode.statusCode == 200 {
                        DispatchQueue.main.async { [self] in
                            self.showCustumAlert(message:object.msg ?? "", isBack: true)
                            
                        }
                    } else {
                        
                        DispatchQueue.main.async { [self] in
                            self.showCustumAlert(message:object.msg ?? "", isBack: true)
                           
                        }
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
          //  }
        }

    }
    
    func editProperty() {
        
        let params = ["villa_id":villaDetail?.villa_id,"lang_id":language,"uid":userData?.id,"towerid":towerID,"unitnumber":unitID,"city_id":cityID]
        APIClient().PostdataRequest(WithApiName: API.editvilla.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: DetailsModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
                switch response {
                case .success(let object):
                    if statuscode.statusCode == 200 {
                        DispatchQueue.main.async { [self] in
                            self.showCustumAlert(message:object.msg ?? "", isBack: true)
                            
                        }
                    } else {
                        
                        DispatchQueue.main.async { [self] in
                            self.showCustumAlert(message:object.msg ?? "", isBack: true)
                           
                            
                        }
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }

        }

    }
}
