//  ActionVillaVC.swift
//  Society Management
//  Created by ROOP KISHOR on 04/07/2021.


import UIKit

class ActionVillaVC: MasterVC ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    var strVillaID = String()
    var villaInfo : PropertyData?
   

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        
        if let villaName = villaInfo?.towername {
            self.viewNAv.lbltitle.text! = "\(villaName) , \(villaInfo?.unitname ?? "")"
        }
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        tblView.tableFooterView = UIView()
      
    }
    
   
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        if indexPath.row == 0 {
            
            cell.titileLbl.text = "Edit"
            cell.icon.image = UIImage(named:"data")
        }
        else
        {
            cell.titileLbl.text = "Delete"
            cell.icon.image = UIImage(named:"delete")
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "AddProperty") as! AddProperty
            vc.strIsEdit = "1"
            vc.villaDetail = villaInfo
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else
        {
            self.AlertController()
        }
        
    }
    
    func AlertController()
    {
        let optionMenu = UIAlertController(title: "", message:NSLocalizedString(AlertMessage.deleteConfirm, comment: "") , preferredStyle: .actionSheet)
        let OK = UIAlertAction(title:NSLocalizedString(AlertMessage.OK, comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            
            self.deleteVilla()

            
        })
        let Cancel = UIAlertAction(title:NSLocalizedString("Cancel", comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })

        optionMenu.addAction(OK)
        optionMenu.addAction(Cancel)
        
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func deleteVilla() {
        
        APIClient().PostdataRequest(WithApiName: API.deletevilla.rawValue, Params: ["villa_id":villaInfo?.villa_id ?? "" ,"lang_id":language], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.showCustumAlert(message:object.msg ?? "", isBack: true)
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
        
    }
    
    
    
}
