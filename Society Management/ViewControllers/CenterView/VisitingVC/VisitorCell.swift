//  VisitorCell.swift
//  Society Management
//  Created by Jitendra Yadav on 11/05/21.


import UIKit

class VisitorCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
