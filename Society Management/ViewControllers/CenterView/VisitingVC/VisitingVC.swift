//  VisitingVC.swift
//  Society Management
//  Created by Jitendra Yadav on 11/05/21.


import UIKit

class VisitingVC: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var tv_Height: NSLayoutConstraint!
    fileprivate let arrCells = [VisitorCell.self]
    
    var selectionBtnCallBAck:((VisitingHelpData,String,String)->())? = nil
    var categoryID = String()
    var data = [VisitingHelpData]()
    
    
    override func viewDidLoad()
    {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = 50
        
        self.makeBlureView()
        self.visitingHelpCat()
        
    }
    
    
   
    
}

extension VisitingVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "VisitorCell", for: indexPath) as! VisitorCell
       // cell.textLabel?.text = visitorHelpName[indexPath.row]
        cell.textLabel?.text = data[indexPath.row].name
        tv_Height.constant = CGFloat(50 * data.count)
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if data[indexPath.row].name == "Others" {
            
        }else{
            self.dismiss(animated: false, completion: {
                DispatchQueue.main.async {
                    if let ind = self.selectionBtnCallBAck {
                        ind(self.data[indexPath.row],"visiting_help",self.categoryID)
                    }
                }
            })
        }
    }
    
    
    func visitingHelpCat() {
        
        let params = ["lang_id":language,"tower_id":userData?.towerid ?? ""] as [String : Any]
        APIClient().PostdataRequest(WithApiName: API.visitingHelp.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: VisitingHelpDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                       
                        self.data = (object.data)!
                        self.tblView.reloadData()
                       
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
    }
}
