//  SecurityAlertList.swift
//  Society Management
//  Created by ROOP KISHOR on 03/06/2021.


import UIKit

class SecurityAlertList: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var lblText: UILabel!
    @IBOutlet weak var lblSocity: UILabel!
    
    var data = [SecurityContact]()
    fileprivate let arrCells = [SecurityAlertCell.self]

    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title23
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnBack.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.tblView.register(cellTypes: arrCells)
        self.tblView.tableFooterView = UIView()
        
        lblNoRecord.isHidden = true
        lblText.isHidden = true
        lblSocity.isHidden = true
        
        self.getSecurityContacts()
        
    }
    

}
extension SecurityAlertList: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SecurityAlertCell", for: indexPath) as! SecurityAlertCell
        let contact = data[indexPath.row]
        cell.lblName?.text = contact.name
        cell.lblMobile?.text = contact.mobile
        cell.lblName?.textColor  = UIColor.gray
        cell.lblStatus.textColor = UIColor.blue
        
        if contact.status == "1"
        {
            cell.lblStatus.text = "Pending"
            
        } else
        {
            cell.lblStatus.text = "Approved"
        }

        cell.btnDelete.tag  = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(self.deleteContactFromSecurity), for: .touchUpInside)
        
        return cell
        
    }
    
    
    @objc func deleteContactFromSecurity(_ sender: UIButton)
    {
        let contact = data[sender.tag]
        let conID = contact.id
        APIClient().PostdataRequest(WithApiName: API.deletesecuritycontact.rawValue, Params: ["id":conID ?? "","lang_id":language], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success( _):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        
                        NotificationCenter.default.post(name: .SecurityContactUpdated, object: nil)
                        self.data.remove(at: sender.tag)
                        
                        if self.data.count > 0 {
                            
                            self.lblNoRecord.isHidden = true
                            self.lblText.isHidden = false
                            self.lblSocity.isHidden = false
                            
                        } else {
                            
                            self.lblNoRecord.isHidden = false
                            self.lblText.isHidden = true
                            self.lblSocity.isHidden = true
                        }
                        
                        self.tblView.reloadData()
                        
                    }
                    
                } else {
                    
                   
                }
                
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
       
    }
    
    
    func getSecurityContacts()
    {
        APIClient().PostdataRequest(WithApiName: API.securityContacts.rawValue, Params: ["uid":userData?.id! ?? "","lang_id":language,"towerid":userData?.towerid! ?? ""], isNeedLoader: true, objectType: SecurityContactModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        
                        if let data = object.data
                        {
                            self.data = data
                            if self.data.count > 0 {
                                
                                self.lblNoRecord.isHidden = true
                                self.lblText.isHidden = false
                                self.lblSocity.isHidden = false
                                
                            } else {
                                
                                self.lblNoRecord.isHidden = false
                                self.lblText.isHidden = true
                                self.lblSocity.isHidden = true
                            }
                        }
                        
                      
                        self.tblView.reloadData()
                        
                    }
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        self.lblNoRecord.isHidden = false
                        self.lblText.isHidden = true
                        self.lblSocity.isHidden = true
                    }
                    
                }
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
        
    }
    
    
}
