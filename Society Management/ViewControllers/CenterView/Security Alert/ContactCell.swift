//
//  ContactCell.swift
//  Society Management
//
//  Created by ROOP KISHOR on 03/06/2021.
//

import UIKit

class ContactCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
