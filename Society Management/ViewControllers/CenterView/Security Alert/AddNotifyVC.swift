//  AddNotifyVC.swift
//  Society Management
//  Created by Jitendra Yadav on 17/05/21.


import UIKit

class AddNotifyVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    fileprivate let arrCells = [addnotifycell.self]
    
    @IBOutlet weak var addName: UILabel!
    @IBOutlet weak var limitLbl: UILabel!
    
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [String]()
    
    var addnamStr = "Society Security"
    var selectedContacts = [contactData]()
    var existContacts = NSMutableArray()
    
    struct contactData
    {
        let name: String
        let mobile: String
    }
    
    
    var contacts:[CNContact]? = nil {
        didSet {
            setUpCollation()
        }
    }
    
    var contactsWithSections = [[CNContact]]()
    let collation = UILocalizedIndexedCollation.current() // create a locale collation object, by which we can get section index titles of current locale. (locale = local contry/language)
    var sectionTitles = [String]()
    
    @objc func setUpCollation(){
        DispatchQueue.main.async {
            if let cn = self.contacts {
                let (arrayContacts, arrayTitles) = self.collation.partitionObjects(array: cn, collationStringSelector: #selector(getter: self.contacts?.first?.givenName))
                self.contactsWithSections = arrayContacts as! [[CNContact]]
                self.sectionTitles = arrayTitles
                self.tblView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title22
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnBack.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = 60
        self.tblView.register(cellTypes: arrCells)
        self.tblView.backgroundColor = .white
        self.addName.text = addnamStr
        self.addName.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view.
        self.className(ClassString: self)
        for i in 0..<self.existContacts.count
        {
            let dict = self.existContacts[i] as! NSDictionary
            let nm = dict.value(forKey: "name") as? String ?? ""
            let mobile = dict.value(forKey: "mobile") as? String ?? ""
            let single  = contactData(name: nm, mobile: mobile)
            arrSelectedData.append(nm)
            self.selectedContacts.append(single)
        }
        
        
        self.accessContactList()
        fetchContacts(order: .givenName) { result in
            switch result {
            case .success(let contacts):
                // Do your thing here with [CNContacts] array
                self.contacts = contacts
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {
            
            self.tblView.reloadData()
            
        }
    }
    
    
    @IBAction func addContacts(_ sender: UIButton)
    {
        if self.selectedContacts.count < 1
        {
            ShowAlert(AlertMessage.selectContact, on:"", from: self)
            return
        }
        
        var params = ["uid":(userData?.id)!,"lang_id":language,"towerid":(userData?.towerid)!,"unitnumber":(userData?.unitnumber)!] as [String:Any]
        let arrCont = NSMutableArray()
        for cont in self.selectedContacts
        {
            let Contact = ["name":cont.name,"mobile":cont.mobile]
            arrCont.add(Contact)
        }
       
        params["alert_people"] = arrCont
        self.addPeopleToSecurityList(params:params)
        
    }
    
    
    func addPeopleToSecurityList(params:[String:Any])
    {
      
        APIClient().PostdataRequest(WithApiName: API.addpeoplesecurity.rawValue, Params: params, isNeedLoader: true, objectType: GuestModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        NotificationCenter.default.post(name: .SecurityContactUpdated, object: nil)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                       ])
                        
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
        
    }
    
    
}

extension AddNotifyVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactsWithSections[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addnotifycell", for: indexPath) as! addnotifycell
        
        let contact = contactsWithSections[indexPath.section][indexPath.row]
        cell.titleLbl?.text = ("\(contact.givenName)" + " \(contact.middleName)" + " \(contact.familyName)").capitalized
        
        let localizedLabel = CNLabeledValue<NSString>.localizedString(forLabel: contact.phoneNumbers.first?.label ?? "")
        cell.deviceLabel.text = localizedLabel.capitalized
        
        if arrSelectedIndex.contains(indexPath) {
            cell.checkBox.image = UIImage(named: "check_box_active")
        }else {
            cell.checkBox.image = UIImage(named: "check_box_unactive")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    //Changing color for the Letters in the section titles
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.red
            view.tintColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sectionTitles
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let contact = contactsWithSections[indexPath.section][indexPath.row]
        let nm = ("\(contact.givenName)" + " \(contact.middleName)" + " \(contact.familyName)").capitalized
        var mobile = ""
        if let number = (contact.phoneNumbers.first)?.value , let cn = number.value(forKey: "digits") as? String
        {
            mobile = cn
        }
       
        
        if arrSelectedIndex.contains(indexPath) {
            
            arrSelectedIndex = arrSelectedIndex.filter { $0 != indexPath}
            arrSelectedData = arrSelectedData.filter { $0 != nm}
            selectedContacts = selectedContacts.filter { $0.name != nm}
            self.limitLbl.isHidden = true
        }
        else {
            
            if arrSelectedData.count < 3 {
                arrSelectedIndex.append(indexPath)
                arrSelectedData.append(nm)
                self.limitLbl.isHidden = true
                let singleC = contactData(name: nm, mobile: mobile)
                selectedContacts.append(singleC)
                
            }else{
                
                self.limitLbl.isHidden = false
            }
            
        }
        
        
        
        self.tblView.reloadData()
        if arrSelectedData.count > 0 {
            let name =  arrSelectedData.joined(separator:", ")
            self.addName.text = "\(name), \(addnamStr)"
        }else{
            self.addName.text = addnamStr
        }
    }
    
    
    
}

