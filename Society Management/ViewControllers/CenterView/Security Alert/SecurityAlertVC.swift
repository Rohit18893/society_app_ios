//  SecurityAlertVC.swift
//  Society Management
//  Created by Jitendra Yadav on 17/05/21.


import UIKit

class SecurityAlertVC: MasterVC {

    @IBOutlet weak var visitorLbl: UILabel!
    @IBOutlet weak var animalLBl: UILabel!
    @IBOutlet weak var stuckLbl: UILabel!
    @IBOutlet weak var fireLbl: UILabel!
    @IBOutlet weak var visitorBTN: UIButton!
    @IBOutlet weak var animalBTN: UIButton!
    @IBOutlet weak var stuckBTN: UIButton!
    @IBOutlet weak var fireBTN: UIButton!
    @IBOutlet weak var tblContact: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    fileprivate let arrCells = [ContactCell.self]
    
    var isIssueSelect = false
    var strIssueName = ""
    var isSocitey = ""
    
    @IBOutlet weak var btnsocietySecurity: UIButton!
    @IBOutlet weak var otherIssueTF: UITextField!
    
    var data = [SecurityContact]()
    var selectedContacts = [contactData]()
   
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [String]()

    struct contactData
    {
        let name: String
        let mobile: String
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.makeBlureView()
        self.tblContact.register(cellTypes: arrCells)
        self.tblContact.rowHeight = 50
        self.strIssueName = ""
        btnsocietySecurity.isSelected = true
        isSocitey = "1"
        NotificationCenter.default.addObserver(self, selector: #selector(SecurityContactUpdated(notification:)), name: .SecurityContactUpdated, object: nil)
        self.className(ClassString: self)
    }

   
    override func viewWillAppear(_ animated: Bool)
    {
        self.getSecurityContacts()
    }
    
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: .SecurityContactUpdated, object: nil)
    }
    
    
    @objc func SecurityContactUpdated(notification: Notification)
    {
        self.getSecurityContacts()
    }

    @IBAction func societySecurity(_ sender: UIButton)
    {
        if sender .isSelected
        {
            btnsocietySecurity.setImage(UIImage(named: "check_box_unactive"), for: UIControl.State.normal)
            btnsocietySecurity.isSelected = false
            isSocitey = "0"
            
        } else
        {
            btnsocietySecurity.setImage(UIImage(named: "check_box_active"), for: UIControl.State.normal)
            btnsocietySecurity.isSelected = true
            isSocitey = "1"
            
        }
        
    }
    
    
    @IBAction func addAnyIssueButton(_ sender: UIButton)
    {
        sender.isHidden = true
        otherIssueTF.isHidden = false
    }
    
    @IBAction func fireButton(_ sender: UIButton)
    {
        self.selectButton(fire: true, animal: false, stuck: false, visitor: false,f: #colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1),s: .gray,a: .gray,v: .gray)
        self.strIssueName = "Fire"
        self.isIssueSelect = true
    }
    
    @IBAction func StuckButton(_ sender: UIButton)
    {
        self.selectButton(fire: false, animal: false, stuck: true, visitor: false,f: .gray,s: #colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1),a: .gray,v: .gray)
        self.strIssueName = "Stuck in lift"
        self.isIssueSelect = true
    }
    
    @IBAction func animalButton(_ sender: UIButton)
    {
        self.selectButton(fire: false, animal: true, stuck: false, visitor: false,f: .gray,s: .gray,a: #colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1),v: .gray)
        self.strIssueName = "Animal threat"
        self.isIssueSelect = true
    }
    
    @IBAction func visitorButton(_ sender: UIButton)
    {
        self.selectButton(fire: false, animal: false, stuck: false, visitor: true,f: .gray,s: .gray,a: .gray,v: #colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1))
        self.strIssueName = "Visitor threat"
        self.isIssueSelect = true
    }
    
    @IBAction func notifyAddButton(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            if self.data.count < 3
            {
                
                let vc = AddNotifyVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                let exist = NSMutableArray()
                for cont in self.data
                {
                    let Contact = ["name":cont.name,"mobile":cont.mobile]
                    exist.add(Contact)
                    
                }
                vc.existContacts = exist
                self.present(vc, animated: true, completion: nil)
            }
            else
            {
                let vc = SecurityAlertList()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            }
            
        }
        
    }
    
    
    @IBAction func raiseButton(_ sender: UIButton)
    {
       
        if (isIssueSelect == false) && (otherIssueTF.text!.isEmpty)
        {
            ShowAlert(AlertMessage.selectIssue, on: "", from: self)
            
        } else
        {
            
//            if self.selectedContacts.count < 1
//            {
//                ShowAlert(AlertMessage.selectContact, on:"", from: self)
//                return
//            }
            
            let arrCont = NSMutableArray()
            for cont in self.selectedContacts {

                let Contact = ["name":cont.name,"mobile":cont.mobile]
                arrCont.add(Contact)

            }
           
            var strOtherIssue = ""
            if let txtOther = otherIssueTF.text
            {
                strOtherIssue = txtOther
            }
            let param = ["towerid":(userData?.towerid)!,"uid":(userData?.id)!,"unitnumber":(userData?.unitnumber)!,"lang_id":language,"security_type":strIssueName,"other_issue":strOtherIssue,"is_society_security":isSocitey,"alert_people":arrCont] as [String : Any]
           
            self.raiseAlarm(params:param)
            
        }
        
        
    }
    
    
    func selectButton(fire:Bool, animal:Bool, stuck:Bool, visitor:Bool, f:UIColor, s:UIColor, a:UIColor, v:UIColor)
    {
        
        fireBTN.isSelected = fire
        stuckBTN.isSelected = stuck
        animalBTN.isSelected = animal
        visitorBTN.isSelected = visitor
        fireLbl.textColor = f
        stuckLbl.textColor = s
        animalLBl.textColor = a
        visitorLbl.textColor = v
        
    }
    
    
    func getSecurityContacts()
    {
        APIClient().PostdataRequest(WithApiName: API.securityContacts.rawValue, Params: ["uid":userData?.id! ?? "","lang_id":language,"towerid":userData?.towerid! ?? ""], isNeedLoader: true, objectType: SecurityContactModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        
                        self.data = (object.data)!
                        self.tblHeight.constant = CGFloat(self.data.count * 50)
                        self.tblContact.reloadData()
                        
                    }
                    
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        self.data.removeAll()
                        self.tblHeight.constant = CGFloat(self.data.count * 50)
                        self.tblContact.reloadData()
                    }
                }
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
        
    }
    
    
    func raiseAlarm(params:[String:Any])
    {
        APIClient().PostdataRequest(WithApiName: API.raiseAlarm.rawValue, Params:params, isNeedLoader: true, objectType: RaiseAlarmDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        
                        weak var pvc = self.presentingViewController
                        self.dismiss(animated: true, completion: {
                            let vc = AlarmRaiseAlertVC()
                            vc.modalPresentationStyle = .overFullScreen
                            vc.modalTransitionStyle = .crossDissolve
                            vc.alarmData = object.data!
                            vc.strAlarmType = self.strIssueName
                            pvc?.present(vc, animated: true, completion: nil)
                            
                        })
                        
                    }
                    
                }
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
        
    }

}
extension SecurityAlertVC: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        let contact = data[indexPath.row]
        cell.lblName?.text = contact.name
        cell.lblName?.textColor  = UIColor.gray
        cell.lblStatus.textColor = UIColor.blue
        if contact.status == "1"
        {
            cell.lblStatus.text = "Pending"
            
        } else
        {
            cell.lblStatus.text = "Approved"
        }
       
        if arrSelectedIndex.contains(indexPath) {
            
            cell.checkBox.image = UIImage(named: "check_box_active")
            
        }else {
            
            cell.checkBox.image = UIImage(named: "check_box_unactive")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nm =  data[indexPath.row].name!
        let mobile =  data[indexPath.row].mobile!
        
        if data[indexPath.row].status == "1" {
            
            ShowAlert(AlertMessage.contactApprove, on: "", from: self)
            
        } else {
            
            if arrSelectedIndex.contains(indexPath) {
                
                arrSelectedIndex = arrSelectedIndex.filter { $0 != indexPath}
                arrSelectedData = arrSelectedData.filter { $0 != nm}
                selectedContacts = selectedContacts.filter { $0.name != nm}
                
            }
            else {
                
                arrSelectedIndex.append(indexPath)
                arrSelectedData.append(nm)
                let singleC = contactData(name: nm, mobile: mobile)
                selectedContacts.append(singleC)
            }
            self.tblContact.reloadData()
        }
        
        
    }
    
    
}
