//  AlarmRaiseAlertVC.swift
//  Society Management
//  Created by ROOP KISHOR on 05/06/2021.


import UIKit

class AlarmRaiseAlertVC: MasterVC {
    
    @IBOutlet weak var tblAlarm: UITableView!
    @IBOutlet weak var tblHeight: NSLayoutConstraint!
    fileprivate let arrCells = [RaiseCell.self]
    var alarmData = [RaiseAlarmData]()
    @IBOutlet weak var lblAlarmType: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    var strAlarmType = String()

    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.makeBlureView()
        self.tblAlarm.register(cellTypes: arrCells)
        tblHeight.constant = CGFloat(self.alarmData.count * 80)
        lblAlarmType.text = strAlarmType
        lblStatus.layer.cornerRadius = 10;
        
        
    }


}
extension AlarmRaiseAlertVC: UITableViewDataSource,UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.alarmData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RaiseCell", for: indexPath) as! RaiseCell
        
        cell.lblName.text = self.alarmData[indexPath.row].name
        cell.lblDesc.text = "A guard will accepting it soon"
        cell.lblTime.text = "10:11 PM"
        
//        let contact = data[indexPath.row]
//        cell.lblName?.text = contact.name
//        cell.lblName?.textColor  = UIColor.gray
//        cell.lblStatus.textColor = UIColor.blue
//        if contact.status == "1" {
//
//            cell.lblStatus.text = "Pending"
//
//        } else {
//
//            cell.lblStatus.text = "Approved"
//        }
//
//        if arrSelectedIndex.contains(indexPath) {
//
//            cell.checkBox.image = UIImage(named: "check_box_active")
//
//        }else {
//
//            cell.checkBox.image = UIImage(named: "check_box_unactive")
//        }
        
        return cell
    }
    
    
    
}
