//  addnotifycell.swift
//  Society Management
//  Created by Jitendra Yadav on 18/05/21.


import UIKit

class addnotifycell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    @IBOutlet weak var deviceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
