//  CenterCollectionCell.swift
//  Society Management
//  Created by ROOP KISHOR on 29/04/2021.


import UIKit

class CenterCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var iconImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

}
