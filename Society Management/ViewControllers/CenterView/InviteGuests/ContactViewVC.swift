//  ContactListVC.swift
//  Society Management
//  Created by ROOP KISHOR on 23/05/2021.


import UIKit

class ContactViewVC: MasterVC,UISearchBarDelegate {
    @IBOutlet weak var searchText: UISearchBar!
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    
    fileprivate let arrCells = [addnotifycell.self]
    
    @IBOutlet weak var addName: UILabel!
    @IBOutlet weak var limitLbl: UILabel!
    
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [String]()
    // var selectedContacts = [contactData]()
    
    
    var searchData = [[CNContact]]()
    var isSearch = false
    
    struct contactData  {
        let name: String
        let mobile: String
    }
    
    var selectionBtnCallBack:(([contactData])->())? = nil
    
    var contacts:[CNContact]? = nil {
        didSet {
            setUpCollation()
        }
        
    }
    
    var contactsWithSections = [[CNContact]]()
    
    let collation = UILocalizedIndexedCollation.current()
    var sectionTitles = [String]()
    
    @objc func setUpCollation(){
        
        DispatchQueue.main.async {
            if let cn = self.contacts {
                let (arrayContacts, arrayTitles) = self.collation.partitionObjects(array: cn, collationStringSelector: #selector(getter: self.contacts?.first?.givenName))
                self.contactsWithSections = arrayContacts as! [[CNContact]]
                self.sectionTitles = arrayTitles
                self.tblView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = 50
        self.tblView.register(cellTypes: arrCells)
        self.tblView.backgroundColor = .clear
        self.tblView.tableFooterView = UIView()
        
        self.searchText.delegate = self
        self.searchText.returnKeyType = .done
        self.searchText.enablesReturnKeyAutomatically = false
        
        
        self.accessContactList()
        fetchContacts(order: .givenName) { result in
            switch result {
            case .success(let contacts):
                self.contacts = contacts
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    
    @IBAction func inviteClicked(_ sender: UIButton)  {
        
        //        if self.selectedContacts.count < 1 {
        //
        //            ShowAlert(AlertMessage.selectGuest, on:"", from: self)
        //            return
        //
        //        }
        //
        //        if let parentVC = self.parent {
        //
        //            if let parentVC = parentVC as? InviteGuestVC {
        //
        //                let arrCont = NSMutableArray()
        //                for cont in self.selectedContacts {
        //                    let Contact = ["name":cont.name,"mobile":cont.mobile]
        //                    arrCont.add(Contact)
        //                }
        //
        //                parentVC.params["entry"] = arrCont
        //                print(parentVC.params)
        //                self.inviteGuests(params:parentVC.params)
        //
        //            }
        //        }
        
    }
    
    
    func inviteGuests(params:[String:Any])  {
        APIClient().PostdataRequest(WithApiName: API.inviteguests.rawValue, Params: params, isNeedLoader: true, objectType: GuestModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                        ])
                        
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                
            }
        }
        
    }
    
}



extension ContactViewVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearch {
            return 1
        }else{
            return sectionTitles.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearch {
            return searchData[section].count
        }else{
            return contactsWithSections[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "addnotifycell", for: indexPath) as! addnotifycell
        
        var rS:[[CNContact]] {
            if isSearch {
                return searchData
            }else{
                return contactsWithSections
            }
        }
        
        let contact = rS[indexPath.section][indexPath.row]
        
        
        cell.titleLbl?.text = removeSpace(("\(contact.givenName)" + " \(contact.middleName)" + " \(contact.familyName)").capitalized)
        
        let localizedLabel = CNLabeledValue<NSString>.localizedString(forLabel: contact.phoneNumbers.first?.label ?? "")
        cell.deviceLabel.text = localizedLabel.capitalized
        
        if let txt = cell.titleLbl?.text {
            if arrSelectedData.contains(txt) {
                cell.checkBox.image = UIImage(named: "check_box_active")
            }else {
                cell.checkBox.image = UIImage(named: "check_box_unactive")
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if isSearch {
            return ""
        }else {
            return sectionTitles[section]
        }
        
    }
    
    //Changing color for the Letters in the section titles
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.red
            view.tintColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if isSearch {
            return []
        }else{
            return sectionTitles
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var rS:[[CNContact]] {
            if isSearch {
                return searchData
                
            }else{
                return contactsWithSections
            }
        }
        
        let  contact = rS[indexPath.section][indexPath.row]
        let nm = removeSpace(("\(contact.givenName) " + "\(contact.middleName) " + "\(contact.familyName)").capitalized)
        
        if arrSelectedData.contains(nm) {
            
            arrSelectedData = arrSelectedData.filter { $0 != nm}
            
        } else {
            
            arrSelectedData.append(nm)
        }
        
        self.tblView.reloadData()
        contactListArr = arrSelectedData
        NotificationCenter.default.post(name: .inviteGuest, object: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearch = false
            self.view.endEditing(true)
            tblView.reloadData()
        } else {
            
            DispatchQueue.main.async {
                searchContact(SearchString: searchBar.text ?? "a") {[weak self] (result) in
                    switch result{
                    case .success(let contacts):
                        self?.searchData = [contacts]
                        break
                    case .failure(let error):
                        print(error)
                        break
                    }
                }
                self.isSearch = true
                self.tblView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func removeSpace(_ string: String) -> String{
        var str: String = String(string[string.startIndex])
        for (index,value) in string.enumerated(){
            if index > 0{
                let indexBefore = string.index(before: String.Index.init(encodedOffset: index))
                if value == " " && string[indexBefore] == " "{
                }else{
                    str.append(value)
                }
            }
        }
        return str
    }
}

