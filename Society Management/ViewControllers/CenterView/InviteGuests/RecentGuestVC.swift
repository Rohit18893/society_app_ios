//  RecentGuestVC.swift
//  Society Management
//  Created by ROOP KISHOR on 23/05/2021.


import UIKit

class RecentGuestVC: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    var data = [RecentContactData]()
    var selectedContacts = [contactData]()
    
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [String]()

    struct contactData
    {
        let name: String
        let mobile: String
    }
    
    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.rowHeight = 60
        self.tblView.backgroundColor = .clear
        self.tblView.tableFooterView = UIView()
        self.getRecentGuestList()
        
    }
    
    
    @IBAction func inviteClicked(_ sender: UIButton)
    {
        if self.selectedContacts.count < 1 {
            
            ShowAlert(AlertMessage.selectGuest, on:"", from: self)
            return
            
        }
        
        if let parentVC = self.parent {
            
            if let parentVC = parentVC as? InviteGuestVC {
                
                let arrCont = NSMutableArray()
                for cont in self.selectedContacts {

                    let Contact = ["name":cont.name,"mobile":cont.mobile]
                    arrCont.add(Contact)

                }
               
                parentVC.params["entry"] = arrCont
                print(parentVC.params)
                self.inviteGuests(params:parentVC.params)
                
            }
        }
        
    }
    
    func getRecentGuestList()
    {
       
        APIClient().PostdataRequest(WithApiName: API.recentGuest.rawValue, Params: ["uid":userData?.id! ?? "","lang_id":language], isNeedLoader: true, objectType: RecentContactModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            switch response {
            case .success(let object):
                if statuscode.statusCode == 200 {
                    
                    DispatchQueue.main.async {
                        
                        self.data = (object.data)!
                        self.tblView.reloadData()
                    }
                    
                } else {
                    
                   // ShowAlert(object.msg ?? "", on: "Success!", from: self)
                }
                
                
            case .failure(let error):
                debugPrint(error)
            }
          
        }
        
    }
    
    
    func inviteGuests(params:[String:Any])
    {
        APIClient().PostdataRequest(WithApiName: API.inviteguests.rawValue, Params: params, isNeedLoader: true, objectType: GuestModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                       ])
                        
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
        
    }

}
extension RecentGuestVC: UITableViewDataSource,UITableViewDelegate {
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return sectionTitles.count
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentGuestCell", for: indexPath) as! RecentGuestCell
        cell.titleLbl.text = data[indexPath.row].name
        
        if arrSelectedIndex.contains(indexPath) {
            
            cell.checkBox.image = UIImage(named: "check_box_active")
        }else {
            cell.checkBox.image = UIImage(named: "check_box_unactive")
        }
     
        
        return cell
    }
    
   
    //Changing color for the Letters in the section titles
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.red
            view.tintColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        }
    }
    
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nm =  data[indexPath.row].name!
        let mobile =  data[indexPath.row].mobile!
        
        
        if arrSelectedIndex.contains(indexPath) {
            
            arrSelectedIndex = arrSelectedIndex.filter { $0 != indexPath}
            arrSelectedData = arrSelectedData.filter { $0 != nm}
            selectedContacts = selectedContacts.filter { $0.name != nm}
            
        }
        else {
            
            arrSelectedIndex.append(indexPath)
            arrSelectedData.append(nm)
            let singleC = contactData(name: nm, mobile: mobile)
            selectedContacts.append(singleC)
        }
        self.tblView.reloadData()
        
    }
   
}
