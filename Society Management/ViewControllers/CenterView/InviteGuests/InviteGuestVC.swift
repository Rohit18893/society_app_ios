//  InviteGuestVC.swift
//  Society Management
//  Created by ROOP KISHOR on 23/05/2021.


import UIKit

var contactListArr = [String]()
var manulaListArr = [String]()

class InviteGuestVC: MasterVC {
    
    @IBOutlet weak var validLbl: UILabel!
    
    @IBOutlet weak var guestNameLbl: UILabel!
    @IBOutlet weak var isInvireView: UIView!
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var contactsView: UIView!
    @IBOutlet weak var recentView: UIView!
    @IBOutlet weak var manualView: UIView!
    
    @IBOutlet weak var btnContact: UIButton!
    @IBOutlet weak var btnRecent: UIButton!
    @IBOutlet weak var btnManual: UIButton!
    
    @IBOutlet weak var inviteButton: UIButton!
    
    var params = [String:Any]()
    var inviteGuestArr = [String]()
    
    var isOnes = true
    var stateID = 1
        
    var validfrom:String {
        let totalHrs = params["total_hrs"] as? String
        if totalHrs == "1" {
            return "\(totalHrs ?? "") Hour"
        }else{
            return "\(totalHrs ?? "") Hours"
        }
    }
    
    var validFor: String {
        if isOnes {
            if let startTime = params["start_time"] as? String , startTime.count > 0 {
                return "\(params["start_date"] as? String ?? "") at \(startTime) valid for \(validfrom)"
            }else{
                return "Today"
            }
        }else{
            return "\(params["total_days"] as? String ?? "") days"
 
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactListArr = [String]()
        manulaListArr = [String]()
        self.className(ClassString: self)
        // Do any additional setup after loading the view.
        self.viewNAv.lbltitle.text = AlertMessage.title21
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnBack.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.setUpView(state:stateID)
        guestNameLbl.numberOfLines = 5
        validLbl.adjustsFontSizeToFitWidth = true
        NotificationCenter.default.addObserver(self, selector: #selector(inviteGuestList(notification:)), name: .inviteGuest, object: nil)
        
    }
    
    @objc func inviteGuestList(notification: NSNotification) {
        self.inviteGuestArr = contactListArr + manulaListArr
        if self.inviteGuestArr.count > 0 {
            let str = inviteGuestArr.map {"\($0)"}.joined(separator: ", ")
            guestNameLbl.text = "> \(str)"
            isInvireView.isHidden = false
            inviteButton.isHidden = false
        }else{
            isInvireView.isHidden = true
            inviteButton.isHidden = true
            guestNameLbl.text = ""
        }
        validLbl.text = validFor

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .inviteGuest, object: nil)
    }
    
    @IBAction func changeMenuClcked(_ sender: UIButton)  {
        self.setUpView(state:sender.tag)
    }
    
    
    func setUpView(state:Int) {
        
        if state == 1 {
            contactsView.isHidden = false
            manualView.isHidden = true
            btnContact.backgroundColor = UIColor(named: "HeaderColor")
            btnManual.backgroundColor =  UIColor.white
            btnContact.setTitleColor(UIColor.white, for: .normal)
            btnManual.setTitleColor(UIColor.black, for: .normal)
        }
        else  {
            contactsView.isHidden = true
            manualView.isHidden = false
            btnManual.backgroundColor = UIColor(named: "HeaderColor")
            btnContact.backgroundColor = UIColor.white
            btnContact.setTitleColor(UIColor.black, for: .normal)
            btnManual.setTitleColor(UIColor.white, for: .normal)
        }
    }
    
    @IBAction func onClickInviteGuest(_ sender: Any) {
        let addParm:[String:Any] =  ["entry":self.inviteGuestArr]
        let combinedDict = params.merging(addParm) { $1 }
        print(combinedDict)
        self.inviteGuests(params: combinedDict)
    }
    
    func inviteGuests(params:[String:Any])  {
        APIClient().PostdataRequest(WithApiName: API.inviteguests.rawValue, Params: params, isNeedLoader: true, objectType: GuestModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        if object.id?.count ?? 0 > 0 {
                            self.dismiss(animated: true, completion: {
                                var type:String {
                                    if self.params["guest_type"] as? String ?? "" == "2" { return "1" }
                                    else{return ""}
                                }
                                let vc = ShareCodeVC()
                                vc.id = object.id ?? ""
                                vc.guestType = type
                                vc.modalPresentationStyle = .overFullScreen
                                vc.modalTransitionStyle = .crossDissolve
                                windowT?.rootViewController?.present(vc, animated: true, completion: nil)
                            })
                        }else{
                            self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [ {_ in
                                    self.dismiss(animated: true, completion: nil)
                                }
                            ])
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
            }
        }
        
    }
}
