//  ManualGuestVC.swift
//  Society Management
//  Created by ROOP KISHOR on 23/05/2021.


import UIKit
import MaterialComponents

class ManualGuestVC: MasterVC {

    
    @IBOutlet weak var textfield: MDCOutlinedTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.setTextFieldTheme(txtField: textfield, labelText: textfield.placeholder ?? "", allowEditing: true)
    }
    
   
    @IBAction func addMoreGuest(_ sender: UIButton) {
        
        guard let textfield = self.textfield.text , textfield.isBlank == false  else {
            ShowAlert(AlertMessage.guestName , on:"", from: self)
            return
        }
        manulaListArr.append(textfield)
        self.textfield.text = nil
        NotificationCenter.default.post(name: .inviteGuest, object: nil)
    }
    
}
