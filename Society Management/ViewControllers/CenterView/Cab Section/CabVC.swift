//  CabVC.swift
//  Society Management
//  Created by Jitendra Yadav on 04/05/21.

import UIKit
import MaterialComponents


class CabVC: MasterVC, UITextFieldDelegate {

    
    @IBOutlet weak var onceView: UIView!
    @IBOutlet weak var frequentlyView: UIView!
    @IBOutlet weak var allowTxt: UILabel!
    
    //MARK: Freq
    
    @IBOutlet weak var selectWeekTF: MDCOutlinedTextField!
    @IBOutlet weak var selectVilidity: MDCOutlinedTextField!
    @IBOutlet weak var selectSatrtTimeTF: MDCOutlinedTextField!
    @IBOutlet weak var selectEndTimeTF: MDCOutlinedTextField!
    @IBOutlet weak var freqCompanyName: MDCOutlinedTextField!
    
    //MARK: ONCE VIEW OUTLET
    @IBOutlet weak var advanceBTN: UIButton!
    
    @IBOutlet weak var selectDateTF: MDCOutlinedTextField!
    @IBOutlet weak var CompanyNameTF: MDCOutlinedTextField!
    @IBOutlet weak var vecicleNoTF: MDCOutlinedTextField!
    @IBOutlet weak var validforNextTF: MDCOutlinedTextField!
    @IBOutlet weak var startDate: MDCOutlinedTextField!
    @IBOutlet weak var timeTF: MDCOutlinedTextField!
    
    @IBOutlet weak var onetimeLbl: UILabel!
    @IBOutlet weak var selectDateView: UIView!
    @IBOutlet weak var CompanyNameView: UIView!
   
    @IBOutlet weak var validforNextView: UIView!
    @IBOutlet weak var startDateView: UIView!
    @IBOutlet weak var timeTFView: UIView!
    @IBOutlet weak var logo: UIImageView!
    var logiName = String()
    @IBOutlet weak var frequentlyBtn: UIButton!
    @IBOutlet weak var onceBtn: UIButton!
    
    var isViewOnce = true
    var sTdate = true
    var SetName = String()
    var theme: SambagTheme = .light
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [String]()
    var allowText = String()
   
    var categoryID = String()
    var selectedCompany = String()
    var isAdvanceOption = false
    var frequentSDate = String()
    var frequentEDate = String()
    var visitor_cat_name = ""
       
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.timeTF.text = "1 Hour"
        self.logo.image = UIImage(named: logiName)
        vecicleNoTF.delegate = self
        allowTxt.text = allowText
        //PrintLog(companies)
        self.className(ClassString: self)
        setChangeColorButton(color1: UIColor.appColor(.HeaderColor) ?? .red, color2: .lightGray,isOnceView: false,isFreqView: true)
        isViewOnce = true
        
    
        
        self.setTextFieldTheme(txtField: selectWeekTF, labelText: selectWeekTF.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: selectVilidity, labelText: selectVilidity.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: selectSatrtTimeTF, labelText: selectSatrtTimeTF.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: selectEndTimeTF, labelText: selectEndTimeTF.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: freqCompanyName, labelText: freqCompanyName.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: selectDateTF, labelText: selectDateTF.placeholder ?? "", allowEditing: false)
        
        self.setTextFieldTheme(txtField: CompanyNameTF, labelText: CompanyNameTF.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: vecicleNoTF, labelText: vecicleNoTF.placeholder ?? "", allowEditing: true)
        self.setTextFieldTheme(txtField: validforNextTF, labelText: validforNextTF.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: startDate, labelText: startDate.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: timeTF, labelText: timeTF.placeholder ?? "", allowEditing: false)
        
        self.makeBlureView()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        PrintLog(categoryID)
    }
    
    @IBAction func frequantlyClicked(_ sender: Any) {
        setChangeColorButton(color1:.lightGray , color2: UIColor.appColor(.HeaderColor) ?? .red,isOnceView: true,isFreqView: false)
        isViewOnce = false
    }
    
    @IBAction func onceClickButton(_ sender: Any) {
        
        setChangeColorButton(color1: UIColor.appColor(.HeaderColor) ?? .red, color2: .lightGray,isOnceView: false,isFreqView: true)
        isViewOnce = true
    }
    
    
    func setChangeColorButton(color1:UIColor,color2:UIColor,isOnceView: Bool,isFreqView: Bool) {
        onceBtn.bottomLineColor = color1
        frequentlyBtn.bottomLineColor = color2
        
        onceBtn.bottomLineWidth = 2
        frequentlyBtn.bottomLineWidth = 2
        
        onceBtn.setTitleColor(color1, for: .normal)
        frequentlyBtn.setTitleColor(color2, for: .normal)
        onceView.isHidden = isOnceView
        frequentlyView.isHidden = isFreqView
        
    }
   
    
    @IBAction func DoneButtonAction(_ sender: Any) {
        
        if isViewOnce
        {
            if isAdvanceOption {
                guard let txtDate = selectDateTF.text , txtDate.isBlank == false  else {
                    ShowAlert(AlertMessage.alDateBlank, on:"", from: self)
                    return
                }
                
                guard let txtTime = startDate.text , txtTime.isBlank == false  else {
                    ShowAlert(AlertMessage.alTimeBlank, on:"", from: self)
                    return
                }
                
//                guard let txtVeh = vecicleNoTF.text , txtVeh.isBlank == false  else {
//                    ShowAlert(AlertMessage.alVehicleBlank, on:"", from: self)
//                    return
//                }
                
                guard let company = CompanyNameTF.text , company.isBlank == false  else {
                    ShowAlert(AlertMessage.alCompanyBlank, on:"", from: self)
                    return
                }
                
                let txtVeh = vecicleNoTF.text ?? ""
                
                let hours: String = self.validforNextTF.text ?? ""
                let hoursStr = hours.components(separatedBy: [" "])
                let arr = [""]
                
                let date = self.convertDateFormater(selectDateTF.text!)
                let sTime = self.convert12HoursTo24HoursFormat(time: startDate.text!)
                
                self.allowFutureEntery(entryCat:categoryID, entryType:"1", companyID:selectedCompany, totalHours:hoursStr[0], vehicalNumber:txtVeh, totalDays:arr, startDate:date, endDate:"", startTime: sTime, endTime: "")
                
            }
            else
            {
//                guard let txtVeh = vecicleNoTF.text , txtVeh.isBlank == false  else {
//                    ShowAlert(AlertMessage.alVehicleBlank, on:"", from: self)
//                    return
//                }
                
                let txtVeh = vecicleNoTF.text ?? ""
                
                let date = getCurrentDateWithTime()
                
                let hours: String = self.timeTF.text ?? ""
                let hoursStr = hours.components(separatedBy: [" "])
                let arr = [""]
                self.allowFutureEntery(entryCat:categoryID, entryType:"1", companyID:"0", totalHours:hoursStr[0], vehicalNumber: txtVeh, totalDays:arr, startDate:date, endDate:"", startTime:"", endTime:"")
            }
            
            
        }else
        {
            
            guard let txtWeek = selectWeekTF.text , txtWeek.isBlank == false  else {
                ShowAlert(AlertMessage.alDays, on:"", from: self)
                return
            }
            
            guard let valadity = selectVilidity.text , valadity.isBlank == false  else {
                ShowAlert(AlertMessage.alValidity, on:"", from: self)
                return
            }
            
            guard let sTime = selectSatrtTimeTF.text , sTime.isBlank == false  else {
                ShowAlert(AlertMessage.alStime, on:"", from: self)
                return
            }
            
            guard let eTime = selectEndTimeTF.text , eTime.isBlank == false  else {
                ShowAlert(AlertMessage.alEtime, on:"", from: self)
                return
            }
            
            guard let comName = freqCompanyName.text , comName.isBlank == false  else {
                ShowAlert(AlertMessage.alCompanyBlank, on:"", from: self)
                return
            }
           
            let time1 = self.convert12HoursTo24HoursFormat(time: selectSatrtTimeTF.text!)
            let time2 = self.convert12HoursTo24HoursFormat(time: selectEndTimeTF.text!)
            
            self.allowFutureEntery(entryCat:categoryID, entryType:"2", companyID:selectedCompany, totalHours:"0", vehicalNumber:"", totalDays:arrSelectedData, startDate:frequentSDate, endDate:frequentEDate, startTime: time1, endTime: time2)
           
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let textstring = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        let length = textstring.count
        if length > 5 {
            return false
        }
        return true
    }
}


//MARK: For Once Selection Functionality.

extension CabVC {
    
    @IBAction func AdvanceButtonAction(_ sender: Any) {
        advanceBTN.isHidden = true
        timeTFView.isHidden = true
        onetimeLbl.isHidden = true
        self.validforNextTF.text = "1 Hour"
        selectDateView.isHidden = false
        validforNextView.isHidden = false
        startDateView.isHidden = false
        CompanyNameView.isHidden = false
        isAdvanceOption = true
    }
    
    @IBAction func selectTimeButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.fieldText = self.timeTF.text ?? ""
            vc.selectionBtnCallBAck = { [weak self] value in
                self?.timeTF.text = value
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectDateButtonAction(_ sender: Any) {
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func startDateButtonAction(_ sender: Any) {
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func validNextButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.fieldText = self.validforNextTF.text ?? ""
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.selectionBtnCallBAck = { [weak self] value in
                self?.validforNextTF.text = value
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func companyNameButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.selectType = 1
            vc.categoryID = self.categoryID
            vc.titleStr = "Do a Strict match"
            vc.fieldText = self.CompanyNameTF.text ?? ""
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.selectionCompany = {[weak self] company,value in
               
                guard let self = self else {return}
                self.CompanyNameTF.text = company
                self.freqCompanyName.text = company
                self.selectedCompany = value
                
            }
            self.present(vc, animated: true, completion: nil)
            
        }
    }
}


extension CabVC: SambagTimePickerViewControllerDelegate {
    
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        if isViewOnce {
            
            startDate.text = "\(result)"
        }else{
            
            if sTdate{
                
                selectSatrtTimeTF.text = "\(result)"
                
            }else{
                
                selectEndTimeTF.text = "\(result)"
            }
        }
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}


extension CabVC: SambagDatePickerViewControllerDelegate {

    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        selectDateTF.text = "\(result)"
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}


extension CabVC {
    
    func allowFutureEntery(entryCat:String ,entryType:String ,companyID:String ,totalHours:String ,vehicalNumber:String ,totalDays:[String] ,startDate:String,endDate:String, startTime:String, endTime:String) {
        
        let params:[String : Any] =
                     ["towerid":userData?.towerid ?? "",
                      "uid":userData?.id ?? "",
                      "unitnumber":userData?.unitnumber ?? "" ,
                      "lang_id":language,
                      "entry_category":entryCat,
                      "entry_type":entryType,
                      "total_hrs":totalHours,
                      "vehical_number":vehicalNumber,
                      "sub_category":companyID,
                      "total_days":totalDays,
                      "start_date":startDate,
                      "end_date":endDate,
                      "start_time": startTime,
                      "end_time": endTime,
                      "visitor_cat_name": visitor_cat_name]
        
        print("params: \(params)")
        
        APIClient().PostdataRequest(WithApiName: API.futureentry.rawValue, Params: params, isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [ {_ in
                                self.dismiss(animated: true, completion: nil)
                            }
                       ])
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
        
        
    }
    
    
    
}
