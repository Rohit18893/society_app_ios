//  SelectPopupVC.swift
//  Society Management
//  Created by Jitendra Yadav on 04/05/21.


import UIKit

class SelectPopupVC: MasterVC ,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout ,UIImagePickerControllerDelegate ,UINavigationControllerDelegate{
    
    var selectionBtnCallSetArrayBAck:(([IndexPath],[String])->())? = nil
    var selectionBtnCallBAck:((String)->())? = nil
    
    var selectionCompany:((String,String)->())? = nil
    
    @IBOutlet weak var cv_Height: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLbl: PaddingLabel!
    @IBOutlet weak var btnAddNew: UIButton!
    var companies = [Datavalue]()
    
    let sectionInsets = UIEdgeInsets(top: 15.0, left: 15.0, bottom: 15.0, right: 15.0)
    let numberOfItemsPerRow: CGFloat = 3
    let spacingBetweenCells: CGFloat = 15
    var fieldText = String()
    var selectType = 0
    var titleStr = "Select Validity"
    //var companyName = [String]()
    var companyID = [String]()
    var categoryID = String()
    var frequentData = [FrequentCat]()
    var selectData = [String]()
    @IBOutlet weak var buttonHeight: NSLayoutConstraint!
    var arrSelectedIndex = [IndexPath]()
    var arrSelectedData = [String]()
    
    
    /*
     selectData = {
        if selectType == 1 || selectType == 4 {
            return companyName
        } else if selectType == 2 {
            
            return ["7 Days","15 days","30 Days"]
        } else if selectType == 3 {
            
            return ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        }
        else{
            
            return ["1 Hour","2 Hours","4 Hours","8 Hours","12 Hours","24 Hours"]
        }
 
    }
    
     */
    
    @IBAction func updateButton(_ sender: Any) {
        if let ind = selectionBtnCallSetArrayBAck {
            ind(arrSelectedIndex,arrSelectedData)
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    
    @IBAction func addNewCompany(_ sender: Any)
    {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true, completion: {
            let vc = AddNewCompany()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.catID = self.categoryID
            pvc?.present(vc, animated: true, completion: nil)
        })

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.text = titleStr
        self.collectionView.register(UINib(nibName: "selectPopupCell", bundle: nil), forCellWithReuseIdentifier: "selectPopupCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        if selectType == 3 {
            
            buttonHeight.constant = -60
            
        }else{
            
            buttonHeight.constant = 0
        }
        
        if selectType == 1 || selectType == 4 {
            
            self.getCategories()
            btnAddNew.isHidden = false
        }else{
            
            btnAddNew.isHidden = true
        }
        self.staticCategory()
        self.className(ClassString: self)
        
    }
    
    
    func staticCategory()  {
        
        if selectType == 1 || selectType == 4 {
            
        }
        else if selectType == 2
        {
            selectData = ["7 Days","15 days","30 Days"]
        }
        else if selectType == 3
        {
            selectData = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"]
        }
        else
        {
            selectData = ["1 Hour","2 Hours","4 Hours","8 Hours","12 Hours","24 Hours"]
        }
        
    }
    
    
    func getCategories()
    {
        let params = ["towerid":userData?.towerid,"lang_id":language,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.frequentlyentrycategory.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: FrequentCatModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.frequentData = object.data!
                        if self.categoryID == "1" {
                            self.companies  = self.frequentData[0].datavalue!
                            let allComp     = self.companies.map({$0.category_value }) as? [String]
                            self.selectData = allComp!
                            self.companyID  = (self.companies.map({$0.id }) as? [String])!
                        } else if self.categoryID == "2" {
                            self.companies   =  self.frequentData[1].datavalue!
                            let allComp      =  self.companies.map({$0.category_value }) as? [String]
                            self.selectData  = allComp!
                            self.companyID   = (self.companies.map({$0.id }) as? [String])!
                        } else {
                            self.companies   = self.frequentData[3].datavalue!
                            let allComp      = self.companies.map({$0.category_value }) as? [String]
                            self.selectData  = allComp!
                            self.companyID   = (self.companies.map({$0.id }) as? [String])!
                        }
                        self.collectionView.reloadData()
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{}
        }
        
    }
   
    // Configure the view for the selected state
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return selectData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "selectPopupCell", for: indexPath as IndexPath) as? selectPopupCell else { return UICollectionViewCell() }
        if selectType == 3 {
            if arrSelectedIndex.contains(indexPath) {
                cell.backgroundColor =  UIColor(named: "HeaderColor") ?? .red
                cell.titlelbl.textColor = .white
            }
            else {
                cell.backgroundColor = .clear
                cell.titlelbl.textColor = .gray
            }
        }else{
            if selectData[indexPath.row] == fieldText {
                cell.backgroundColor =  UIColor(named: "HeaderColor") ?? .red
                cell.titlelbl.textColor = .white
            }else{
                cell.backgroundColor = .clear
                cell.titlelbl.textColor = .gray
            }
        }
        
        cell.titlelbl.text = selectData[indexPath.row].capitalized
        cv_Height.constant = collectionView.contentSize.height
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if selectType == 3 {
            let strData = selectData[indexPath.item]
            if arrSelectedIndex.contains(indexPath) {
                
                arrSelectedIndex = arrSelectedIndex.filter { $0 != indexPath}
                arrSelectedData = arrSelectedData.filter { $0 != strData}
            }
            else {
                
                arrSelectedIndex.append(indexPath)
                arrSelectedData.append(strData)
            }
            collectionView.reloadData()
        }
        else if selectType == 1
        {
            if let compData = selectionCompany {
                
                compData(selectData[indexPath.row], companyID[indexPath.row])
                self.dismiss(animated: false, completion: nil)
            }
        }
        else{
            
            if let ind = selectionBtnCallBAck {
                ind(selectData[indexPath.row])
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalSpacing = (2 * sectionInsets.left) + ((numberOfItemsPerRow - 1) * spacingBetweenCells) //Amount of total spacing in a row
        if let collection = self.collectionView{
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: 40)
        } else {
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
    
    
    
}
