//  AddNewCompany.swift
//  Society Management
//  Created by ROOP KISHOR on 20/05/2021.


import UIKit

import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class AddNewCompany: MasterVC  {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var txtServiceName: MDCOutlinedTextField!
    var catID = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title27
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnBack.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.setUpView()
        PrintLog(catID)
        
    }

    func setUpView()
    {
        self.setTextFieldTheme(txtField:txtServiceName, labelText: "Service Name", allowEditing: true)
       
    }

   
    
    @IBAction func addButtonClicked(_ sender: Any)
    {
        
        guard let company = txtServiceName.text , company.isBlank == false  else {
            ShowAlert(AlertMessage.alCompanyBlank, on:"", from: self)
            return
        }
        
        self.addServiceName()
    }
    
    
    func addServiceName() {
        
        let params = ["entry_category":catID,"value":txtServiceName.text!,"lang_id":language,"uid":userData?.id ?? ""] as [String : Any]
        APIClient().PostdataRequest(WithApiName: API.addcompany.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                       
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                       ])
                        
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
    }

}
