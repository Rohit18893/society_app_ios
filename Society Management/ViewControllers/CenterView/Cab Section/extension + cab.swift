//
//  extension + cab.swift
//  Society Management
//
//  Created by Jitendra Yadav on 07/05/21.
//

import UIKit

extension CabVC {
    
    @IBAction func SelectWeekButton(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.selectType = 3
            vc.titleStr = "select days of week".capitalized
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.arrSelectedData = self.arrSelectedData
            vc.arrSelectedIndex = self.arrSelectedIndex
            vc.selectionBtnCallSetArrayBAck = {[weak self] indix,data in
                self?.arrSelectedData = data
                self?.arrSelectedIndex = indix
                if data.count > 0 {
                    if data.count == 1 {
                        self?.selectWeekTF.text = data.first
                    } else if data.count > 6 {
                        self?.selectWeekTF.text = "All days of week"
                    }else{
                        self?.selectWeekTF.text = "\(data.count) days of week"
                    }
                }else{
                    self?.selectWeekTF.text = nil
                }
            }
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectValidityButton(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.selectType = 2
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.fieldText = self.SetName
            vc.selectionBtnCallBAck = { [weak self] value in
                //  PrintLog(value)
                let string = value
                let stringArray = string.components(separatedBy: CharacterSet.decimalDigits.inverted)
                for item in stringArray {
                    if let number = Int(item) {
                        print("number: \(number)")
                      //  self?.selectVilidity.text = selectValidDate(days: number - 1)
                        let result = selectValidDate(days: number - 1)
                        self?.selectVilidity.text = result.0
                        self?.frequentSDate = result.1
                        self?.frequentEDate = result.2
                        
                        
                        self?.SetName = value
                    }
                }
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectStartTimeButton(_ sender: Any) {
        sTdate = true
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    @IBAction func selectendTimeButton(_ sender: Any) {
        sTdate = false
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
}
