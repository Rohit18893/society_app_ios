//
//  selectPopupCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 04/05/21.
//

import UIKit

class selectPopupCell: UICollectionViewCell {

    @IBOutlet weak var titlelbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        titlelbl.adjustsFontSizeToFitWidth = true
    }

}
