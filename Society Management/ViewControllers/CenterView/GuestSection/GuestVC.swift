//  CabVC.swift
//  Society Management
//  Created by Jitendra Yadav on 04/05/21.


import UIKit
import MaterialComponents

class GuestVC: MasterVC, UITextFieldDelegate {

    
    fileprivate let arrCells = [GuestFromCell.self]
    /// ONCE VIEW
    @IBOutlet weak var onceView: UIView!
    @IBOutlet weak var onceBtn: UIButton!
    @IBOutlet weak var OncelblAllowGuest: UILabel!
    @IBOutlet weak var btnAdvanceOption: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    var isViewOnce = true
    var isAdvance = false
    var sTdate = true
    
    @IBOutlet weak var txtStartDate: MDCOutlinedTextField!
    @IBOutlet weak var txtSTime: MDCOutlinedTextField!
    @IBOutlet weak var txtValid: MDCOutlinedTextField!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewTime: UIView!
    
    /// FRQUENT VIEW
    
    @IBOutlet weak var viewFrequent: UIView!
    @IBOutlet weak var frequentlyBtn: UIButton!
    @IBOutlet weak var frequentlblAllowGuest: UILabel!
    @IBOutlet weak var tblFrequent: UITableView!
    @IBOutlet weak var txtSDate: MDCOutlinedTextField!
    @IBOutlet weak var txtEndDate: MDCOutlinedTextField!
    var theme: SambagTheme = .light
    var guestType = ""
    var isHouseHold = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.tblView.register(cellTypes: arrCells)
        self.tblFrequent.register(cellTypes: arrCells)
        onceView.isHidden = false
        viewFrequent.isHidden = true
        viewDate.isHidden = true
        viewTime.isHidden = true
        guestType = "1"
        guestType = "1"
        tblView.rowHeight = 40
        tblFrequent.rowHeight = 40
        self.makeBlureView()
        
        self.setTextFieldTheme(txtField: txtSDate, labelText: txtSDate.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: txtEndDate, labelText: txtEndDate.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: txtStartDate, labelText: txtStartDate.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: txtSTime, labelText: txtSTime.placeholder ?? "", allowEditing: false)
        self.setTextFieldTheme(txtField: txtValid, labelText: txtValid.placeholder ?? "", allowEditing: false)
        
        if isHouseHold {
            frequantlyClicked(self)
            self.onceBtn.isUserInteractionEnabled = false
        }
        
    }
    
    
    @IBAction func frequantlyClicked(_ sender: Any) {
        
        setChangeColorButton(color1:.lightGray , color2: UIColor(named: "HeaderColor") ?? .red,isOnceView: true,isFreqView: false)
        viewFrequent.isHidden = false
        onceView.isHidden = true
        isViewOnce = false
        guestType = "2"
        
    }
    
    @IBAction func onceClickButton(_ sender: Any)  {
        setChangeColorButton(color1: UIColor(named: "HeaderColor") ?? .red, color2: .lightGray,isOnceView: false,isFreqView: true)
        viewFrequent.isHidden = true
        onceView.isHidden = false
        isViewOnce = true
        guestType = "1"
    }
    
    
    @IBAction func advanceButtonClicked(_ sender: Any)  {
        btnAdvanceOption.isHidden = true
        OncelblAllowGuest.isHidden = true
        viewDate.isHidden = false
        viewTime.isHidden = false
        isAdvance = true
        
    }
    
    
    
    func setChangeColorButton(color1:UIColor,color2:UIColor,isOnceView: Bool,isFreqView: Bool) {
        onceBtn.bottomLineColor = color1
        frequentlyBtn.bottomLineColor = color2
        onceBtn.bottomLineWidth = 2
        frequentlyBtn.bottomLineWidth = 2
        onceBtn.setTitleColor(color1, for: .normal)
        frequentlyBtn.setTitleColor(color2, for: .normal)
        onceView.isHidden = false
        
    }
    
    @IBAction func selectDateButtonAction(_ sender: Any) {
        
        sTdate = true
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func selectEndDateButtonAction(_ sender: Any) {
        
        sTdate = false
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func startTimeButtonAction(_ sender: Any) {
        let vc = SambagTimePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
    }
   
 
}

extension GuestVC: UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GuestFromCell", for: indexPath) as! GuestFromCell
        if indexPath.row == 0 {
            cell.lblName.text = "Phone Contacts"
        }
        else {
            cell.lblName.text = "Enter Manually"
        }
        
        return cell
       
     }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var param = [String:Any]()
        weak var pvc = self.presentingViewController
        if self.isViewOnce {
            
            if self.isAdvance {
                
                guard let txtDate = self.txtStartDate.text , txtDate.isBlank == false  else {
                    ShowAlert(AlertMessage.alDateBlank, on:"", from: self)
                    return
                }
                
                guard let txtSTime = self.txtSTime.text , txtSTime.isBlank == false  else {
                    ShowAlert(AlertMessage.alStime, on:"", from: self)
                    return
                }
                
                
                guard let txtValid = self.txtValid.text , txtValid.isBlank == false  else {
                    ShowAlert(AlertMessage.alValidity, on:"", from: self)
                    return
                }
                
                let hours = Int.parse(from: txtValid)
                let date = self.convertDateFormater(txtDate)
                let sTime = self.convert12HoursTo24HoursFormat(time:txtSTime)
                
                param = [
                         "towerid":userData?.towerid ?? "",
                         "uid":userData?.id ?? "",
                         "unitnumber":userData?.unitnumber ?? "",
                         "lang_id":language,
                         "guest_type":self.guestType,
                         "total_hrs":"\(hours ?? 1)",
                         "total_days":"1",
                         "start_date": date,
                         "end_date": "",
                         "start_time":sTime,
                         "end_time":""
                ]
                print(param)
                
            } else {
                PrintLog("normal")
                param = [
                         "towerid":userData?.towerid ?? "",
                         "uid":userData?.id ?? "",
                         "unitnumber":userData?.unitnumber ?? "",
                         "lang_id":language,
                         "guest_type":self.guestType,
                         "total_hrs":"",
                         "total_days":"1",
                         "start_date":Date.getCurrentDate(),
                         "end_date": "",
                         "start_time":"",
                         "end_time":""
                ]
                print(param)
            }
            
        }
        else {
            
            PrintLog("frequent guest")
            guard let txtDate = self.txtSDate.text , txtDate.isBlank == false  else {
                ShowAlert(AlertMessage.alSDate, on:"", from: self)
                return
            }
            
            guard let txtEndDate = self.txtEndDate.text , txtEndDate.isBlank == false  else {
                ShowAlert(AlertMessage.alEDate, on:"", from: self)
                return
            }
            
            let sdate = self.convertDateFormater(txtDate)
            let edate = self.convertDateFormater(txtEndDate)
            let totalDays = daysDifferences(from: sdate, to: edate)
            
          
                let now = Date()
                let formatter = DateFormatter()
                formatter.timeZone = TimeZone.current
                formatter.dateFormat = "HH:mm"
                let currentTime = formatter.string(from: now)
           
            
            param = [
                "towerid":userData?.towerid ?? "",
                "uid":userData?.id ?? "",
                "unitnumber":userData?.unitnumber ?? "",
                "lang_id":language,
                "guest_type":self.guestType,
                "total_hrs":"",
                "total_days":"\(totalDays ?? 0)",
                "start_date": sdate,
                "end_date": edate,
                "start_time":currentTime,
                "end_time":""
            ]
            
            print(param)
        }

     
        self.dismiss(animated: true, completion: {
            
            let vc = self.mainStory.instantiateViewController(withIdentifier: "InviteGuestVC") as! InviteGuestVC
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            if indexPath.row == 0 {
                vc.stateID = 1
                vc.params = param
                vc.isOnes = self.isViewOnce
                
            }
            else  {
                vc.stateID = 3
                vc.params = param
                vc.isOnes = self.isViewOnce
            }
            pvc?.present(vc, animated: true, completion: nil)
            
        })
         
    }    
}

extension GuestVC: SambagDatePickerViewControllerDelegate {

    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        
        if isViewOnce {
            
            txtStartDate.text = "\(result)"
        } else {
            
            if sTdate{
                
                txtSDate.text = "\(result)"
            }else{
                
                txtEndDate.text = "\(result)"
            }
            
        }
        
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}

extension GuestVC: SambagTimePickerViewControllerDelegate {
    
    func sambagTimePickerDidSet(_ viewController: SambagTimePickerViewController, result: SambagTimePickerResult) {
        
        if isViewOnce {
            
            txtSTime.text = "\(result)"
        }else {
            /*
            if sTdate{
                selectSatrtTimeTF.text = "\(result)"
            }else{
                selectEndTimeTF.text = "\(result)"
            }
           */
        }
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagTimePickerDidCancel(_ viewController: SambagTimePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func validNextButtonAction(_ sender: Any) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.fieldText = self.txtValid.text ?? ""
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.selectionBtnCallBAck = { [weak self] value in
                self?.txtValid.text = value
            }
            self.present(vc, animated: true, completion: nil)
        }
    }
}




extension Date {

 static func getCurrentDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: Date())
    }
}
