//  SharePopUp.swift
//  Society Management
//  Created by ROOP KISHOR on 16/06/2021.


import UIKit

class SharePopUp: MasterVC {
    
    
    @IBOutlet weak var titleLbl: UILabel!
    var strTitle = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        //self.makeBlureView()
        self.makeBlureView()
        titleLbl.text = "\(strTitle) is not using Almudeer.Tell your friend about Almudeer"
        
    }
    
    
    @IBAction func crossButtunClicked(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func shareButtonClicked(_ sender: UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
   

}
