//  requestContactCell.swift
//  Society Management
//  Created by Jitendra Yadav on 18/05/21.


import UIKit

class requestContactCell: UITableViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var deviceLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
