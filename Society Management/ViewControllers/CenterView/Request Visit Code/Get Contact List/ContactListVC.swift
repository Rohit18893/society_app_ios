//  ContactListVC.swift
//  Society Management
//  Created by Jitendra Yadav on 18/05/21.


import UIKit
import Contacts
import CoreTelephony


class ContactListVC: MasterVC,UISearchBarDelegate {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    fileprivate let arrCells = [requestContactCell.self]
    
    @IBOutlet weak var searchText: UISearchBar!
    
    var isAddfamily = false
    
    var contacts:[CNContact]? = nil {
        didSet {
            setUpCollation()
        }
    }
    
    var searchData = [[CNContact]]()
    var isSearch = false
    
    
    var getContactList:((_ name:String,_ phone:String )->())? = nil
    var contactsWithSections = [[CNContact]]()
    let collation = UILocalizedIndexedCollation.current()
    var sectionTitles = [String]()
    
    
    @objc func setUpCollation(){
        DispatchQueue.main.async {
            if let cn = self.contacts {
                let (arrayContacts, arrayTitles) = self.collation.partitionObjects(array: cn, collationStringSelector: #selector(getter: self.contacts?.first?.givenName))
                self.contactsWithSections = arrayContacts as! [[CNContact]]
                self.sectionTitles = arrayTitles
                self.tblView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        if isAddfamily {
            self.viewNAv.lbltitle.text = AlertMessage.title25
        }else{
            self.viewNAv.lbltitle.text = AlertMessage.title26
        }
        
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnBack.addTarget(self, action: #selector(back), for: .touchUpInside)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        self.tblView.backgroundColor = .white
        // Do any additional setup after loading the view.
        
        self.searchText.delegate = self
        self.searchText.returnKeyType = .done
        self.searchText.enablesReturnKeyAutomatically = false
        self.searchText.searchTextField.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        self.searchText.searchTextField.textColor = UIColor.black
        
        
        self.accessContactList()
        fetchContacts(order: .givenName) { result in
            switch result {
            case .success(let contacts):
                // Do your thing here with [CNContacts] array
                self.contacts = contacts
                break
            case .failure(let error):
                print(error.localizedDescription)
                break
            }
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.tblView.reloadData()
        }
    }
    
    
    
}


extension ContactListVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        //return sectionTitles.count
        if isSearch {
            return 1
        }else{
            return sectionTitles.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  return contactsWithSections[section].count
        if isSearch {
            return searchData[section].count
        }else{
            return contactsWithSections[section].count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "requestContactCell", for: indexPath) as! requestContactCell
        
        var rS:[[CNContact]] {
            if isSearch {
                return searchData
            }else{
                return contactsWithSections
            }
        }
        
        let contact = rS[indexPath.section][indexPath.row]
        
        
        cell.titleLbl?.text = ("\(contact.givenName)" + " \(contact.middleName)" + " \(contact.familyName)").capitalized
        
        if let number = (contact.phoneNumbers.first)?.value , let cn = number.value(forKey: "digits") as? String {
            cell.numberLbl.text = cn
            
            let localizedLabel = CNLabeledValue<NSString>.localizedString(forLabel: contact.phoneNumbers.first?.label ?? "")
            cell.deviceLabel.text = localizedLabel.capitalized
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //   return sectionTitles[section]
        if isSearch {
            return ""
        }else {
            return sectionTitles[section]
        }
    }
    
    //Changing color for the Letters in the section titles
    func tableView(_ tableView: UITableView, willDisplayHeaderView view:UIView, forSection: Int) {
        if let headerTitle = view as? UITableViewHeaderFooterView {
            headerTitle.textLabel?.textColor = UIColor.red
            view.tintColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        
        if isSearch {
            return []
        }else{
            return sectionTitles
        }
        //   return sectionTitles
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        
        var rS:[[CNContact]] {
            if isSearch {
                return searchData
            }else{
                return contactsWithSections
            }
        }
        
        let contact = rS[indexPath.section][indexPath.row]
        
        let name = ("\(contact.givenName)" + " \(contact.middleName)" + " \(contact.familyName)").capitalized
        
        var mobile:String{
            if let number = (contact.phoneNumbers.first)?.value , let cn = number.value(forKey: "digits") as? String {
                return cn
            }else{
                return ""
            }
        }
        
        if isAddfamily {
            if let ind = self.getContactList {
                ind(name,mobile)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            
            self.visitRequest(name: name, mobile: mobile)
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            isSearch = false
            self.view.endEditing(true)
            tblView.reloadData()
        } else {
            
            DispatchQueue.main.async {
                searchContact(SearchString: searchBar.text ?? "a") {[weak self] (result) in
                    switch result{
                    case .success(let contacts):
                        self?.searchData = [contacts]
                        break
                    case .failure(let error):
                        print(error)
                        break
                    }
                }
                self.isSearch = true
                self.tblView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    
    func visitRequest(name:String,mobile:String)  {
        
        let params = ["uid":userData?.id ?? "",
                      "towerid":userData?.towerid ?? "",
                      "lang_id":language,
                      "unitnumber":userData?.unitnumber ?? "",
                      "name":name,
                      "mobile":mobile
                     ] as [String :Any]
        
        APIClient().PostdataRequest(WithApiName: API.verifyMobile.rawValue, Params: params, isNeedLoader: true, objectType: MobileVerifyModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        PrintLog(object)
                        if object.result?.isUser == "0" {
                           
                            let vc = RequestVisitCodeSheduleVC()
                            vc.modalPresentationStyle = .overFullScreen
                            vc.modalTransitionStyle = .crossDissolve
                            vc.selectedName = name
                            vc.id = object.result?.id ?? ""
                            self.present(vc, animated: true, completion: nil)
                            
                        } else if object.result?.isUser == "1" {
                            self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [{[weak self] _ in
                                guard let self = self else {return}
                                NotificationCenter.default.post(name: .communicationRefrash, object: nil)
                                self.dismiss(animated: true, completion: nil)
                              }
                           ])
                        } else{
                            let vc = SharePopUp()
                            vc.modalPresentationStyle = .overFullScreen
                            vc.modalTransitionStyle = .crossDissolve
                            vc.strTitle = name
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                DispatchQueue.main.async  {
                    let vc = SharePopUp()
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    vc.strTitle = name
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    
}



extension UILocalizedIndexedCollation {
    
    //func for partition array in sections
    func partitionObjects(array:[AnyObject], collationStringSelector:Selector) -> ([AnyObject], [String]) {
        var unsortedSections = [[AnyObject]]()
        
        //1. Create a array to hold the data for each section
        for _ in self.sectionTitles {
            unsortedSections.append([]) //appending an empty array
        }
        //2. Put each objects into a section
        for item in array {
            let index:Int = self.section(for: item, collationStringSelector:collationStringSelector)
            unsortedSections[index].append(item)
        }
        //3. sorting the array of each sections
        var sectionTitles = [String]()
        var sections = [AnyObject]()
        for index in 0 ..< unsortedSections.count { if unsortedSections[index].count > 0 {
            sectionTitles.append(self.sectionTitles[index])
            sections.append(self.sortedArray(from: unsortedSections[index], collationStringSelector: collationStringSelector) as AnyObject)
        }
        }
        return (sections, sectionTitles)
    }
}



