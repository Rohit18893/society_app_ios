//
//  RequestVisitCodeSheduleVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 06/07/21.
//

import UIKit

class RequestVisitCodeSheduleVC: MasterVC {

    @IBOutlet weak var vesitorName: UILabel!
    
    var selectedName = String()
    var visitingDate = String()
    var id = String()
    
    @IBOutlet weak var selectDateButton: UIButton!
    var theme: SambagTheme = .light
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.makeBlureView()
        vesitorName.text = selectedName
        selectDateButton.setTitle(dayDifference(), for: .normal)
        visitingDate = Date.getCurrentDate()
        
        // Do any additional setup after loading the view.
        
    }

    
    func dayDifference() -> String {
        let calendar = Calendar.current
        let date =  Date()
        let startOfNow = calendar.startOfDay(for: Date())
        let startOfTimeStamp = calendar.startOfDay(for: date)
        let components = calendar.dateComponents([.day], from: startOfNow, to: startOfTimeStamp)
        let day = components.day!
        if abs(day) < 2 {
            let formatter = DateFormatter()
            formatter.dateStyle = .short
            formatter.timeStyle = .none
            formatter.doesRelativeDateFormatting = true
            return formatter.string(from: date)
        } else if day > 1 {
            return "In \(day) days"
        } else {
            return "\(-day) days ago"
        }
    }
    
    
    @IBAction func onClickSelectDate(_ sender: Any) {
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func onClickRequestButton(_ sender: Any) {
        self.visitRequest()
    }
    
    
    func visitRequest()  {
        let params =  ["visit_code_id":id, "lang_id":language, "visit_date":visitingDate]
        PrintLog(params)
        APIClient().PostdataRequest(WithApiName: API.requestVisit.rawValue, Params: params, isNeedLoader: true, objectType: StatusModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        PrintLog(object)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [{[weak self] _ in
                            guard let self = self else {return}
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: .requestLisRefrash, object: nil)
                                self.dismiss(animated: true, completion: nil)
                            }
                          }
                       ])
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }
        }
    }
}



extension RequestVisitCodeSheduleVC: SambagDatePickerViewControllerDelegate {

    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
        selectDateButton.setTitle("\(result)", for: .normal)
        visitingDate = convertDateFormater("\(result)")
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }

}


//"MMM d, yyyy"
