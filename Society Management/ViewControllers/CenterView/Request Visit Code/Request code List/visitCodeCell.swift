//
//  visitCodeCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 23/06/21.
//

import UIKit

class visitCodeCell: UITableViewCell {

    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var ReSeekButton: UIButton!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var datelbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
