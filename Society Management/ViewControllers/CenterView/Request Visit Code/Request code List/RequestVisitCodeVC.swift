//  RequestVisitCodeVC.swift
//  Society Management
//  Created by Jitendra Yadav on 17/05/21.


import UIKit

class RequestVisitCodeVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var viewNoData: UIView!
    @IBOutlet weak var viewData: UIView!
    @IBOutlet weak var tblData: UITableView!
    
    var requestData:[RequestVisitData]?
    fileprivate let arrCells = [visitCodeCell.self]
    
    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title24
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnBack.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        viewData.isHidden = true
        viewNoData.isHidden = true
        
        self.tblData.register(cellTypes: arrCells)
        //        self.tblData.tableFooterView = UIView()
        self.getRequestVisit(isNeedLoader: true)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(requestLisRefrash(notification:)), name: .requestLisRefrash, object: nil)
    }
    
    
    @objc func requestLisRefrash(notification: NSNotification) {
        self.getRequestVisit(isNeedLoader: false)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .requestLisRefrash, object: nil)
    }
    
    
    @IBAction func RequestButton(_ sender: UIButton) {
        DispatchQueue.main.async {
            let vc = ContactListVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    
    func getRequestVisit(isNeedLoader: Bool) {
        
        let params = ["uid":userData?.id,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.visitcodelist.rawValue, Params: params as [String : Any], isNeedLoader: isNeedLoader, objectType: RequestVisitModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.requestData = (object.data)!
                        
                        if self.requestData?.count ?? 0 > 0 {
                            self.viewData.isHidden = false
                            self.viewNoData.isHidden = true
                            
                        } else {
                            
                            self.viewData.isHidden = true
                            self.viewNoData.isHidden = false
                        }
                        
                        self.tblData.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else
            {
                DispatchQueue.main.async
                {
                    self.viewData.isHidden = true
                    self.viewNoData.isHidden = false
                }
                
                
            }
        }
        
    }
    
    
}

extension RequestVisitCodeVC: UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.requestData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "visitCodeCell", for: indexPath) as! visitCodeCell
        if let rs = self.requestData?[indexPath.row] {
            cell.titleLbl?.text = rs.name
            cell.statusLbl.text = rs.status
            let stamp = rs.created_date ?? 0
            let time = convertTimeStampTo(timeStamp: Double(stamp))
            cell.timeLbl?.text = time
            cell.datelbl.text = TimeStampToDate(timeStamp: rs.visit_date ?? 0)
            cell.ReSeekButton.tag = indexPath.row
            cell.ReSeekButton.addTarget(self, action: #selector(onClickReseek), for: .touchUpInside)
            
            switch rs.statusid {
            case "2":
                cell.ReSeekButton.isHidden = false
                cell.lineView.isHidden = false
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            case "1":
                cell.ReSeekButton.isHidden = true
                cell.lineView.isHidden = true
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0, green: 0.6196078431, blue: 0.05882352941, alpha: 1)
            default:
                cell.ReSeekButton.isHidden = true
                cell.lineView.isHidden = true
                cell.statusLbl.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
            }
        }
        return cell
        
    }
    
    
    @objc func onClickReseek(_ sender:UIButton) {
        if let rs = self.requestData?[sender.tag] {
            let vc = RequestVisitCodeSheduleVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.selectedName = rs.name ?? ""
            vc.id = rs.visit_code_id ?? ""
            self.present(vc, animated: true, completion: nil)
        }
    }
}



