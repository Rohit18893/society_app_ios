//
//  KidsVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 11/05/21.
//

import UIKit
import MaterialComponents

class KidsVC: MasterVC {
    
    @IBOutlet weak var validforNextTF: MDCOutlinedTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.validforNextTF.text  = "1 Hour"
        self.setTextFieldTheme(txtField: validforNextTF, labelText: validforNextTF.placeholder ?? "", allowEditing: true)
        self.className(ClassString: self)
        self.makeBlureView()
        

    }
    
    @IBAction func AdvanceButtonAction(_ sender: UIButton) {
        DispatchQueue.main.async {
            let vc = SelectPopupVC()
            vc.fieldText = self.validforNextTF.text ?? ""
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.selectionBtnCallBAck = { [weak self] value in
                self?.validforNextTF.text = value
            }
            
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func AllowDone(_ sender: UIButton)
    {
        let txtVal: String = self.validforNextTF.text ?? ""
        let hoursStr = txtVal.components(separatedBy: [" "])
        let params = ["uid":userData?.id,"unitnumber":userData?.unitnumber,"allow_hrs":hoursStr[0], "allow_from":self.getCurrentDateWithTime(),"towerid":userData?.towerid,"lang_id":language]
        
        APIClient().PostdataRequest(WithApiName: API.kidsexit.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: .householdReload, object: nil)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                            }
                       ])
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }
        
        
    }

   
}
