//  CenterViewVC.swift
//  Society Management
//  Created by ROOP KISHOR


import UIKit

struct centerViewData
{
    var name:String = ""
    var image = [String]()
    var Data =  [String]()
}

class CenterViewVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var heightView: NSLayoutConstraint!
    @IBOutlet weak var viewCenter: UIView!
    @IBOutlet weak var tblView: UITableView!
    fileprivate let arrCells = [CenterTableViewCell.self]
    var data = [centerViewData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        tblView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblView.frame.size.width, height: 1))
        
        data.append(centerViewData.init(name: "NOTIFY GATE", image:["security","message"], Data: ["Security Alert","Message to Guard"]))
        data.append(centerViewData.init(name: "ALLOW FUTURE ENTRY", image:["cab","delivery_icon","guest","visiting_help"], Data: ["Cab","Delivery","Guest","Visiting Help"]))
        data.append(centerViewData.init(name: "ALLOW EXIT", image: ["kids"], Data: ["Kid"]))
        
//        data.append(centerViewData.init(name: "MY VISIT", image: ["request_code"], Data: ["Request Visit Code"]))
        self.tblView.backgroundColor = UIColor.appColor(.BackgroundColor)

    }
    
}

extension CenterViewVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CenterTableViewCell", for: indexPath) as! CenterTableViewCell
        cell.lblHeader.text = data[indexPath.row].name
        cell.lblHeader.textColor = UIColor.appColor(.textBlack)
        cell.data = data[indexPath.row].Data
        cell.images = data[indexPath.row].image
        cell.selectionBtnCallBAck = { [weak self] selectIndex in
            let position: CGPoint = cell.convert(.zero, to: self?.tblView)
            if let indexpath = self?.tblView.indexPathForRow(at: position) {
                if let ind = self?.data[indexpath.row].Data[selectIndex.item] , let img = self?.data[indexpath.row].image[selectIndex.item] {
                    self?.didselectRow(text: ind.lowercased(), logoName: img)
                }
            }
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
    
    func didselectRow(text:String,logoName:String) {
        if text == "cab" {
            DispatchQueue.main.async {
                let vc = CabVC()
                vc.allowText = "Allow my cab to enter today once in next"
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.logiName = logoName
                vc.categoryID = "1"
                self.present(vc, animated: true, completion: nil)
            }
        }
        else if text == "message to guard"{
            DispatchQueue.main.async {
                let vc = MessageGuardVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
        }
        else if text == "raise"  {
            
            let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpDeskVC")
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else if text == "kid"  {
            DispatchQueue.main.async {
                let vc = KidsVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
        } else if text == "visiting help" {
            DispatchQueue.main.async {
                let vc = VisitingVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.categoryID = "4"
                vc.selectionBtnCallBAck = {[weak self] name , image , visitorCat in
                    guard let self = self else {return}
                    DispatchQueue.main.async {
                        let vc = CabVC()
                        vc.allowText = "Allow my visiting help \(String(describing: name.name ?? "")) to enter today once in next"
                        vc.visitor_cat_name = name.name ?? ""
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.logiName = image
                        vc.categoryID = visitorCat
                        self.present(vc, animated: false, completion: nil)
                    }
                }
                self.present(vc, animated: true, completion: nil)
            }
        } else if text == "delivery" {
            DispatchQueue.main.async {
                let vc = CabVC()
                vc.allowText = "Allow delivery executive to enter today once in next"
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.logiName = logoName
                vc.categoryID = "2"
                self.present(vc, animated: true, completion: nil)
            }
        } else if text == "guest"{
            
            let vc = GuestVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        } else if  text == "security alert" {
            DispatchQueue.main.async {
                let vc = SecurityAlertVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
        } else if  text == "request visit code" {
            DispatchQueue.main.async {
                let vc = RequestVisitCodeVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
    
}
