//  SignUpVC.swift
//  Society Management
//  Created by ROOP KISHOR on 07/04/2021.


import UIKit
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields
import SVProgressHUD
import Alamofire

class SignUpVC: MasterVC,UITextFieldDelegate {
    
    
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtFname: MDCOutlinedTextField!
    @IBOutlet weak var txtLname: MDCOutlinedTextField!
    @IBOutlet weak var txtEmail: MDCOutlinedTextField!
    @IBOutlet weak var txtIsd: MDCOutlinedTextField!
    @IBOutlet weak var txtMobile: MDCOutlinedTextField!
    @IBOutlet weak var txtTower: MDCOutlinedTextField!
    @IBOutlet weak var txtUnit: MDCOutlinedTextField!
    @IBOutlet weak var txtOwnerShipDoc: MDCOutlinedTextField!
    @IBOutlet weak var txtEmiratesID: MDCOutlinedTextField!
    @IBOutlet weak var txtPass: MDCOutlinedTextField!
    @IBOutlet weak var txtConfirmPass: MDCOutlinedTextField!
    @IBOutlet weak var txtCity: MDCOutlinedTextField!
    
    @IBOutlet var btnOwner: UIButton!
    @IBOutlet var btnTenant: UIButton!
    @IBOutlet var btnTermsCondition: UIButton!
    
    var imagePicker: ImagePicker!
    var imageDoc: UIImage!
    var imageEmiratesID: UIImage!
    var isSecurePassword = true
    var isSecureConfirmPassword = true
    
    @IBOutlet weak var imgshowDoc: UIImageView!
    @IBOutlet weak var imgshowID: UIImageView!
    
    @IBOutlet weak var emiratesIDTop: NSLayoutConstraint!
    @IBOutlet weak var passwordTop: NSLayoutConstraint!

    @IBOutlet var btnPassword: UIButton!
    @IBOutlet var btnConfirmPass: UIButton!
    
    let cityModelView = CityModelView ()
    var towerModelView = TowerModalView ()
    var unitModelView = UnitModalView ()
    
    var cityID = String()
    var towerID = String()
    var unitID = String()
    var termsAndCondition = String()
    
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setUpView()
        cityModelView.getAllCities()
        emiratesIDTop.constant = 35
        passwordTop.constant = 35
        self.className(ClassString: self)
        imgshowDoc.isHidden = true
        imgshowID.isHidden = true
        
        self.view.backgroundColor = UIColor.appColor(.BackgroundColor)
        
    }
    
    
    @IBAction func passwordHideShow(sender: AnyObject) {
       
            if(isSecurePassword == true) {
                
                txtPass.isSecureTextEntry = false
                btnPassword.setImage(UIImage(systemName: "eye.slash"), for: .normal)
                
            } else {
                
                txtPass.isSecureTextEntry = true
                btnPassword.setImage(UIImage(systemName: "eye"), for: .normal)
            }

             isSecurePassword = !isSecurePassword
        }
    
    @IBAction func confirmPasswordHideShow(sender: AnyObject) {
       
            if(isSecureConfirmPassword == true) {
                
                txtConfirmPass.isSecureTextEntry = false
                btnConfirmPass.setImage(UIImage(systemName: "eye.slash"), for: .normal)
            } else {
                
                txtConfirmPass.isSecureTextEntry = true
                btnConfirmPass.setImage(UIImage(systemName: "eye"), for: .normal)
            }

             isSecureConfirmPassword = !isSecureConfirmPassword
        }
    
    /*
        UI Set Up
     */
    func setUpView()
    {
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        USER_TYPE = TYPE_OWNER
        termsAndCondition = "1"
        btnTermsCondition.setImage(UIImage(named: "check_box_unactive"), for: UIControl.State.normal)
        btnTermsCondition.isSelected = false
        
        txtMobile.delegate = self
        txtFname.delegate = self
        txtLname.delegate = self
        txtPass.delegate = self
        txtConfirmPass.delegate = self
        txtIsd.setLeftImage(imageName: "flag")
        self.headerViewHeight.constant = HeaderHeight
        self.setTextFieldTheme(txtField:txtFname, labelText: "First Name *", allowEditing: true)
        self.setTextFieldTheme(txtField:txtLname, labelText: "Last Name", allowEditing: true)
        self.setTextFieldTheme(txtField:txtEmail, labelText: "Email", allowEditing: true)
        self.setTextFieldTheme(txtField:txtIsd, labelText: "ISD", allowEditing: false)
        self.setTextFieldTheme(txtField:txtMobile, labelText: "Mobile Number", allowEditing: true)
        self.setTextFieldTheme(txtField:txtTower, labelText: "Building Name", allowEditing: false)
        self.setTextFieldTheme(txtField:txtUnit, labelText: "Unit Number", allowEditing: false)
        self.setTextFieldTheme(txtField:txtOwnerShipDoc, labelText: "Unit Ownership Document Upload", allowEditing: false)
        self.setTextFieldTheme(txtField:txtEmiratesID, labelText: "Emirates ID Upload", allowEditing: false)
        self.setTextFieldTheme(txtField:txtPass, labelText: "Password", allowEditing: true)
        self.setTextFieldTheme(txtField:txtConfirmPass, labelText: "Confirm Password", allowEditing: true)
        self.setTextFieldTheme(txtField:txtCity, labelText: "City", allowEditing: false)
        txtIsd.text = "+971"
        
    }
    
    
    /*
         Action Back Button
     */
    @IBAction func backButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }

    
    /*
         Action sign in Button
     */
    
    @IBAction func signInButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func acceptTermsConditition(sender: AnyObject)
    {
        let urlComponents = URLComponents (string: "https://almudeerit.com/privacy")!
        UIApplication.shared.open (urlComponents.url!)
    }
    
    /*
         Action radio Button owner
     */
    @IBAction func btnOwnerClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        btnOwner.setImage(UIImage(named: "radio_active"), for: .normal)
        btnTenant.setImage(UIImage(named: "radio_unactive"), for: .normal)
        USER_TYPE = TYPE_OWNER
        btnOwner.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnTenant.setTitleColor(UIColor.black, for: .normal)
        self.setTextFieldTheme(txtField:txtOwnerShipDoc, labelText: "Unit Ownership Document Upload", allowEditing: false)
    }
    
    /*
         Action radio Button tenant
     */
    @IBAction func btnTenantClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        btnOwner.setImage(UIImage(named: "radio_unactive"), for: .normal)
        btnTenant.setImage(UIImage(named: "radio_active"), for: .normal)
        USER_TYPE = TYPE_TENANT
        btnTenant.setTitleColor(DEFAULT_COLOR_ORANGE, for: .normal)
        btnOwner.setTitleColor(UIColor.black, for: .normal)
        self.setTextFieldTheme(txtField:txtOwnerShipDoc, labelText: "Tenancy contract Document Upload", allowEditing: false)
    }
    
    /*
         Action signup Button
     */
    @IBAction func signUnButtonClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        guard let fname = txtFname.text , fname.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyFname, on: "", from: self)
            return
            
        }
        guard let Lname = txtLname.text , Lname.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyLname, on: "", from: self)
            return
            
        }
        guard let email = txtEmail.text , email.isBlank == false else {
            ShowAlert(AlertMessage.alMEmailEmpity, on: "", from: self)
            return
            
        }
        guard let emailValid = txtEmail.text , emailValid.isEmail == true  else
        {
            ShowAlert(AlertMessage.alMValidEmail, on: "", from: self)
            return
        }
        
        guard let mobile = txtMobile.text , mobile.isBlank == false else {
            ShowAlert(AlertMessage.alEmptyMobile, on: "", from: self)
            return
            
        }
        if txtMobile.text!.count < 9 {
            
            ShowAlert(AlertMessage.alMValidMobile, on: "", from: self)
            return
        }
        guard let towerName = txtTower.text , towerName.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyTower, on: "", from: self)
            return
            
        }
//        guard let unitNumber = txtUnit.text , unitNumber.isBlank == false else {
//            ShowAlert(AlertMessage.alMEmptyTower, on: "", from: self)
//            return
//
//        }
        guard let unitOwnerShip = txtOwnerShipDoc.text , unitOwnerShip.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyOwnersship, on: "", from: self)
            return

        }

        guard let emiratesID = txtEmiratesID.text , emiratesID.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyEmiratesID, on: "", from: self)
            return

        }
        guard let password = txtPass.text , password.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyPassword, on: "", from: self)
            return
            
        }
        guard let passwordValid = txtPass.text , passwordValid.isValidPass == false else {
            ShowAlert(AlertMessage.alMValidPassword, on: "", from: self)
            return
            
        }
        guard let cPassword = txtConfirmPass.text , cPassword.isBlank == false else {
            ShowAlert(AlertMessage.alMEmptyConfirmPassword, on: "", from: self)
            return
            
        }
        guard let cPasswordValid = txtConfirmPass.text , cPasswordValid.isValidPass == false else {
            ShowAlert(AlertMessage.alMInvalidConfirmPass, on: "", from: self)
            return
            
        }
        
        if txtPass.text != txtConfirmPass.text {
            
            ShowAlert(AlertMessage.alMPasswordNotMatch, on: "", from: self)
            return
            
        }
        if termsAndCondition == "1" {
            ShowAlert(AlertMessage.alMacceptTerms, on: "", from: self)
            return
        }
        
       self.callRegistration()
 
        
    }
    
    
    /*
         Action City button clicked
     */
    
    @IBAction func selectCityClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if  let cityname = cityModelView.cityData?.data?.map({$0.name }) as? [String] ,
          let cityID = cityModelView.cityData?.data?.map({$0.id}) as? [String] {
        CustomPickerView.show(items: cityname , itemIds: cityID ,selectedValue:self.txtCity.text, doneBottonCompletion: { (item, index) in
            
               self.txtCity.text = item
               PrintLog(index)
               self.txtTower.text = ""
               self.txtUnit.text = ""
               self.cityID = index!
               self.towerModelView = TowerModalView()
               self.unitModelView = UnitModalView()
               self.towerModelView.getAllTowers(params: ["city" : index!])
            
            }, didSelectCompletion: { (item, index) in
                self.txtCity.text = item
                
            }) { (item, index) in
            }
       }
    }
    
    /*
          SELCT TOWER
     */
    
    @IBAction func selectTowerClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if  let towerName = towerModelView.towerData?.data?.map({$0.name }) as? [String] ,
            let towerID = towerModelView.towerData?.data?.map({$0.id}) as? [String] {
          CustomPickerView.show(items: towerName , itemIds: towerID ,selectedValue:self.txtTower.text, doneBottonCompletion: { (item, index) in
              
                  self.txtTower.text = item
                  self.towerID = index!
                  self.txtUnit.text = ""
                  self.unitModelView = UnitModalView()
                  self.unitModelView.getAllUnits(params: ["city" : self.cityID,"towerid":self.towerID])
              
              }, didSelectCompletion: { (item, index) in
                  self.txtTower.text = item
                  
                  
              }) { (item, index) in
              }
         }
    }
    
    
    /*
          SELCT TOWER
     */
    
    @IBAction func selectUnitClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if  let unitName = unitModelView.unitData?.data?.map({$0.name }) as? [String] ,
            let unitID = unitModelView.unitData?.data?.map({$0.id}) as? [String] {
          CustomPickerView.show(items: unitName , itemIds: unitID ,selectedValue:self.txtUnit.text, doneBottonCompletion: { (item, index) in
              
                  self.txtUnit.text = item
                  self.unitID = index!
              
              }, didSelectCompletion: { (item, index) in
                  self.txtUnit.text = item
                  
                  
              }) { (item, index) in
              }
         }
    }
    
    /*
          SELCT DOCUMENT CLICKED
     */
    
    @IBAction func selectDocumentClicked(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.imagePicker.tag = sender.tag
        self.imagePicker!.present(from: sender)
    }
    
    /*
          SELCT DOCUMENT CLICKED
     
     */
    
    
    @IBAction func selectEmiratesID(_ sender: UIButton)
    {
        self.view.endEditing(true)
        self.imagePicker.tag = sender.tag
        self.imagePicker!.present(from: sender)
    }
    
    
    /*
        ACCEPT TERMS AND CONDITIONS
     */
    
    @IBAction func termsConditionClicked(_ sender: UIButton)
    {
        if sender .isSelected
        {
            btnTermsCondition.setImage(UIImage(named: "check_box_unactive"), for: UIControl.State.normal)
            btnTermsCondition.isSelected = false
            termsAndCondition = "1"
            
        } else
        {
            btnTermsCondition.setImage(UIImage(named: "check_box_active"), for: UIControl.State.normal)
            btnTermsCondition.isSelected = true
            termsAndCondition = "2"
        }
        
        
    }
    
   
    func callRegistration() {
        
        let mobilenum = String(format: "+971%@", txtMobile.text!)
        let param = [
            "fname":txtFname.text ?? "",
            "lname":txtLname.text ?? "",
            "email":txtEmail.text ?? "",
            "mobile":mobilenum,
            "city":self.cityID,
            "towerid":self.towerID,
            "unitnumber":self.unitID,
            "userrole":USER_TYPE,
            "lang_id":language,
            "password":txtPass.text ?? "",
            "deviceid":deviceId,
            "devicetoken":deviceToken
            
        ]
        
        var imageArr = [imagesUpload]()
        imageArr.append(imagesUpload(key: "document", image: self.imageDoc, fileName: "ownerDoc.png"))
        imageArr.append(imagesUpload(key: "emiratesid", image: self.imageEmiratesID, fileName: "emiratesid.png"))
                
        APIClient().UploadImageWithParametersRequest(WithApiName: "signup", Params: param, Setimages: imageArr, isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.showCustumAlert(message:object.msg ?? "", isBack: true)
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
        
    }
   
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var maxLength = 0
        if textField == txtMobile {
             maxLength = 9
        }
        else if textField == txtFname {
            maxLength = 15
        }
        else if textField == txtLname {
            
            maxLength = 15
        }
        else
        {
             maxLength = 25
        }
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
    }
    
   
   
}


extension UITextField{

    func setLeftImage(imageName:String) {

        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
        imageView.image = UIImage(named: imageName)
        self.leftView = imageView;
        self.leftViewMode = .always
    }
    
   
}

extension SignUpVC :ImagePickerDelegate {
    
    func didSelect(image: UIImage?, tag: Int) {
        
        if tag == 10 {
            
            imageDoc = image
            self.txtOwnerShipDoc.text = "Attached"
            emiratesIDTop.constant = 75
            imgshowDoc.isHidden = false
            imgshowDoc.image = image
            
        } else {
            
            imageEmiratesID = image
            self.txtEmiratesID.text = "Attached"
            passwordTop.constant = 75
            imgshowID.isHidden = false
            imgshowID.image = image
        }
        
    }

}
