//  ImageViewerVC.swift
//  Society Management
//  Created by ROOP KISHOR on 19/08/2021.


import UIKit

class ImageViewerVC: MasterVC {
    
    @IBOutlet weak var fullImage: UIImageView!
    var image = UIImage()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        fullImage.image = image
        self.className(ClassString: self)
        // Do any additional setup after loading the view.
        
    }

    @IBAction func closedButtunClicked(_ sender: UIButton)
    {
        self.dismiss(animated: false, completion: nil)
    }
    

}
