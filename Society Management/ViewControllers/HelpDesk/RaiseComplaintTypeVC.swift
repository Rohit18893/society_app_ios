//  RaiseComplaintTypeVC.swift
//  Society Management
//  Created by Jitendra Yadav on 02/05/21.


import UIKit
import TLPhotoPicker
import Photos

class RaiseComplaintTypeVC: MasterVC {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    var helpDeskType = [ServicePeopleData]()
    var selectedAssets = [TLPHAsset]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title18
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnOption.isHidden = false
        self.viewNAv.completionHandler = {
            PrintLog("Search Click")
        }
        self.className(ClassString: self)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.getHelpdeskType()
        tblView.tableFooterView = UIView()
    }
}

extension RaiseComplaintTypeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return helpDeskType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "typecell", for: indexPath)
        cell.textLabel?.text = helpDeskType[indexPath.row].name?.capitalized
        cell.backgroundColor = .clear
        cell.textLabel?.textColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
       return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "RaiseComplaintVC") as! RaiseComplaintVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getHelpdeskType() {
        
        let params = ["lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.helpdesktype.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ServicePeopleModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.helpDeskType = object.data!
                        self.tblView.reloadData()
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                //ShowAlert("no Data", on: "Opps?", from: self)

            }
        }

    }
    
}
