//
//  HelpRatingVC.swift
//  Society Management
//
//  Created by ROOP KISHOR on 13/06/2021.
//

import UIKit
import Cosmos

class HelpRatingVC: MasterVC {
    
    @IBOutlet weak var ratingView:CosmosView!
    var strComplaintID = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        ratingView.settings.updateOnTouch = true
        ratingView.rating =  0
        self.makeBlureView()
       
    }
    

    @IBAction func addRating(_ sender: UIButton)
    {
        if ratingView.rating == 0
        {
            ShowAlert(AlertMessage.selectRating, on: "", from: self)
            
        } else
        {
            self.addRating()
        }
        
    }
    
    
    func addRating()
    {
        
        let params = ["lang_id":language,"uid":(userData?.id)!,"id":strComplaintID,"help_rating":ratingView.rating] as [String : Any]
        APIClient().PostdataRequest(WithApiName: API.addhelpRating.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        NotificationCenter.default.post(name: .helpComplaintUpdated, object: nil)
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                       ])
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    
                   // self.tblView.reloadData()
                    
                }

            }
            
        }

    }

}
