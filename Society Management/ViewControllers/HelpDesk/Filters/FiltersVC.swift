//  FiltersVC.swift
//  Society Management
//  Created by ROOP KISHOR on 06/05/2021.


import UIKit

protocol filterControllerDelegate
{
    func selecetedFilterData(filterID:String,filterType:Int,filterName:String)
}

class FiltersVC: MasterVC {
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    fileprivate let arrCells = [FilterTableCell.self]
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblType: UILabel!
    
    var filterType = Int()
    var custumDelegate:filterControllerDelegate?
    var catgetory = [Filter_category]()
    var type = [Filter_type]()
    var status = [Filter_status]()
    var filterData = [HelpFilterData]()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        self.tblView.tableFooterView = UIView()
        self.tblView.rowHeight = 50
        self.className(ClassString: self)
        if filterType == 1 {
            lblType.text = "Filter by Type"
        }
        else if filterType == 2 {
              lblType.text = "Filter by Category"
        }
        else {
              lblType.text = "Filter by Status"
        }
        

    }

    @IBAction func dismissButtonCLicked(_ sender: UIButton) {
        self.removeFromParent()
        self.view.removeFromSuperview()
        self.willMove(toParent: nil)
    }
    
    
    func filterByType(type:Int) {
       
        let params = ["lang_id":language,"towerid":userData?.towerid]
        APIClient().PostdataRequest(WithApiName: API.helpfilter.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: HelpFilterDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(self)
                        PrintLog(object.data)
                        self.filterData = object.data!
                        print(self.filterData.count)
                        if let cat = self.filterData.first?.filter_category {
                            self.catgetory = cat
                        }
                        if let typ = self.filterData.first?.filter_type {
                            self.type = typ
                        }
                        if let st = self.filterData.first?.filter_status {
                            self.status = st
                        }
                        self.tblView.reloadData()
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                

            }
        }

    }
    
}

extension FiltersVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if filterType == 1 {
            return self.type.count
        }
        else if filterType == 2 {
            return self.catgetory.count
        }
        else  {
            return self.status.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterTableCell", for: indexPath) as! FilterTableCell
        if filterType == 1 {
            self.viewHeight.constant = CGFloat(type.count*50) + 80
            cell.lblName.text = type[indexPath.row].name
        }
        else if filterType == 2 {
            cell.lblName.text = catgetory[indexPath.row].name
            self.viewHeight.constant = CGFloat(catgetory.count*50) + 80
        }
        else {
            cell.lblName.text = status[indexPath.row].name
            self.viewHeight.constant = CGFloat(status.count*50) + 80
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if filterType == 1 {
            
            self.custumDelegate?.selecetedFilterData(filterID: type[indexPath.row].id!, filterType:filterType, filterName: type[indexPath.row].name!)
        }
        else if filterType == 2  {
            
            self.custumDelegate?.selecetedFilterData(filterID: catgetory[indexPath.row].id!, filterType:filterType, filterName: catgetory[indexPath.row].name!)
        }
        else {
            
            self.custumDelegate?.selecetedFilterData(filterID: status[indexPath.row].id!, filterType:filterType, filterName: status[indexPath.row].name!)
        }
        
        self.removeFromParent()
        self.view.removeFromSuperview()
        self.willMove(toParent: nil)

    }
    
   
}
