//  HelpDeskDetailCell.swift
//  Society Management
//  Created by ROOP KISHOR on 13/06/2021.


import UIKit

class HelpDeskDetailCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblIsUrgent: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblResolvedBy: UILabel!
    
    @IBOutlet weak var imgRate: UIImageView!
    @IBOutlet weak var raisedTopCons: NSLayoutConstraint!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    var vcPrev = UIViewController()
    
    var images = [String]()
    
    @IBOutlet weak var collectionImages: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension HelpDeskDetailCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return images.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionImageCell", for: indexPath as IndexPath) as? CollectionImageCell else { return UICollectionViewCell() }
        
        cell.imgComplaint.sd_setImage(with: URL(string: images[indexPath.row]), placeholderImage:UIImage(named: "No-image"))
        return cell
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Cell = collectionView.cellForItem(at:indexPath) as! CollectionImageCell
        DispatchQueue.main.async {
            let vc = ImageViewerVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.image = Cell.imgComplaint.image!
            self.vcPrev.present(vc, animated: false, completion: nil)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width:(screenWidth/2) - 20 , height: 130)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0)
        return sectionInset
        
    }
    
   
}
