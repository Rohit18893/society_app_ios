//  HelpCommentVC.swift
//  Society Management
//  Created by ROOP KISHOR on 06/06/2021.


import UIKit

class HelpCommentVC: MasterVC ,UITextViewDelegate{
    
    
    @IBOutlet weak var txtMessage: UITextView!
    
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var viewComment: UIView!
   
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    var strComplaintID = String()
    var commentData = [HelpCommentData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.txtMessage.layer.cornerRadius = 6
        self.txtMessage.borderWidth = 0.5
        self.txtMessage.borderColor = UIColor.lightGray
        txtMessage.text = AlertMessage.commentPlaceHolder
        txtMessage.textColor = UIColor.lightGray
        txtMessage.delegate = self
        
        self.makeBlureView()
        
    }
    
    
    
   
    @IBAction func sendComment(_ sender: UIButton)
    {
        guard let comment = txtMessage.text , comment.isBlank == false  else {
            ShowAlert(AlertMessage.enterComment, on: "", from: self)
            return
        }
        
        if txtMessage.text == AlertMessage.commentPlaceHolder {
            
            ShowAlert(AlertMessage.enterComment, on: "", from: self)
            return
        }
        
        self.addComment()
        
        
    }
    
    
    
    func addComment()
    {
        let params = ["lang_id":language,"uid":(userData?.id)!,"towerid":(userData?.towerid)!,"comment":txtMessage.text!,"help_complaint_id":strComplaintID,"unitnumber":(userData?.unitnumber)!]
        APIClient().PostdataRequest(WithApiName: API.addhelpComment.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [
                            {_ in
                                self.dismiss(animated: true, completion: nil)
                                
                            }
                       ])
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    
                    self.tblView.reloadData()
                    
                }

            }
            
        }

    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {

        if textView.text == "" {

            textView.text = AlertMessage.commentPlaceHolder
            textView.textColor = UIColor.lightGray
        }
    }

}
