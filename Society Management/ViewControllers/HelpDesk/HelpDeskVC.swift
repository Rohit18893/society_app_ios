//  HelpDeskVC.swift
//  Society Management
//  Created by Jitendra Yadav on 02/05/21.


import UIKit

class HelpDeskVC: MasterVC ,filterControllerDelegate {
    
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var viewFilter: UIView!
    
    @IBOutlet weak var btnType: UIButton!
    @IBOutlet weak var btnCategory: UIButton!
    @IBOutlet weak var btnStatus: UIButton!
    
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var viewCategory: UIView!
    @IBOutlet weak var viewStatus: UIView!
    
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    
    
    
    fileprivate let arrCells = [helpDeskHeaderCell.self,helpDeskCell.self]
    var complainData = [ComplaintListData]()
    var filter_by_type = String()
    var help_category = String()
    var filter_by_status = String()
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title17
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnOption.isHidden = true
        self.viewNAv.completionHandler = {
            PrintLog("Search Click")
        }
        
        noDataView.isHidden = true
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        
        viewType.isHidden = true
        viewCategory.isHidden = true
        viewStatus.isHidden = true
        
        btnType.isHidden = false
        btnCategory.isHidden = false
        btnStatus.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(helpComplaintUpdated(notification:)), name: .helpComplaintUpdated, object: nil)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
        
    }
    
    
    @objc func refresh(_ refresh: UIRefreshControl) {
       
        self.getCompaintList()
        refresh.endRefreshing()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getCompaintList()
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self, name: .helpComplaintUpdated, object: nil)
        
    }
    
    
    @objc func helpComplaintUpdated(notification: Notification)
    {
        self.getCompaintList()
    }
    
    
    @IBAction func raiseComplaint(_ sender: UIButton){
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "RaiseComplaintVC") as! RaiseComplaintVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func filterbuttunClicked(_ sender: UIButton) {
        
        let headerView = FiltersVC()
        self.addChild(headerView)
        headerView.filterType = sender.tag
        headerView.filterByType(type: sender.tag)
        self.view.addSubview(headerView.view)
        headerView.custumDelegate = self
        headerView.didMove(toParent: self)
        headerView.view.frame = self.view.frame
    }
    
    
    
    
    func selecetedFilterData(filterID: String, filterType: Int, filterName: String)
    {
        print(filterName)
        if filterType == 1 {
            
            filter_by_type = filterID
            btnType.isHidden = true
            viewType.isHidden = false
            lblType.text = filterName
            viewType.layer.cornerRadius = viewType.frame.size.height/2
        }
        else if filterType == 2 {
            help_category = filterID
            btnCategory.isHidden = true
            viewCategory.isHidden = false
            lblCategory.text = filterName
            viewCategory.layer.cornerRadius = viewType.frame.size.height/2
        }
        else {
            
            filter_by_status = filterID
            btnStatus.isHidden = true
            viewStatus.isHidden = false
            lblStatus.text = filterName
            viewStatus.layer.cornerRadius = viewType.frame.size.height/2
            
        }
        
        self.getCompaintList()
    }
    
    
    @IBAction func removeTypeFilter(_ sender: UIButton)
    {
        self.btnType.isHidden = false
        viewType.isHidden = true
        filter_by_type = ""
        self.getCompaintList()
    }
    
    @IBAction func removeCategoryFilter(_ sender: UIButton) {
        
        self.btnCategory.isHidden = false
        viewCategory.isHidden = true
        help_category = ""
        self.getCompaintList()
        
    }
    
    @IBAction func removeStatusFilter(_ sender: UIButton){
        
        self.btnStatus.isHidden = false
        viewStatus.isHidden = true
        filter_by_status = ""
        self.getCompaintList()
        
    }
    
    
    
}

extension HelpDeskVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.complainData.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "helpDeskHeaderCell", for: indexPath) as! helpDeskHeaderCell
        cell.lblTitle.text = complainData[indexPath.row].help_category
        cell.lblDesc.text = complainData[indexPath.row].title
        let stamp = complainData[indexPath.row].created_date
        let date = convertTimeStampTo(timeStamp: stamp!)
        cell.lblDate.text = String(format: "%@ • REQUEST ID %@", date,complainData[indexPath.row].id!)
        cell.lblStatus.text = complainData[indexPath.row].filter_by_status
        
        if complainData[indexPath.row].filter_by_status == "Resolved"
        {
            cell.lblStatus.backgroundColor = UIColor.systemGreen
        }
        else
        {
            cell.lblStatus.backgroundColor = UIColor.systemYellow
        }
        
        if complainData[indexPath.row].is_urgent == "1"
        {
            cell.lblIsUrgent.isHidden = true
            cell.lblIsUrgent.text = ""
            
        } else
        {
            cell.lblIsUrgent.text = "Urgent"
            cell.lblIsUrgent.isHidden = false
        }
       
        cell.lblRating.text = ""
        cell.imgRate.isHidden = true
        cell.raisedTopCons.constant = -18
     
        if complainData[indexPath.row].is_resolved == "1"
        {
            cell.btnResolved.isHidden = true
            cell.btnComment.isHidden = true
            cell.lblResolvedBy.text = "Resolved by \((complainData[indexPath.row].resolved_by ?? ""))"
            if complainData[indexPath.row].help_rating == "0" {
                
                if complainData[indexPath.row].created_by == userData?.id
                {
                    cell.btnRate.isHidden = false
                    cell.btnView.isHidden = false
                    
                } else
                {
                    cell.btnRate.isHidden = true
                    cell.btnView.isHidden = true
                    
                }
                
                
            } else {
                
                cell.btnRate.isHidden = true
                cell.btnView.isHidden = true
                cell.lblRating.text = "Rating - \(complainData[indexPath.row].help_rating ?? "")/5"
                cell.imgRate.isHidden = false
                cell.raisedTopCons.constant = 8
            }
        } else
        {
            
            
            if complainData[indexPath.row].created_by == userData?.id
            {
                cell.btnResolved.isHidden = false
                
            } else
            {
                cell.btnResolved.isHidden = true
                
            }
            cell.lblResolvedBy.text = "Raised by \(complainData[indexPath.row].username ?? "")"
            cell.btnRate.isHidden = true
            cell.btnView.isHidden = false
            cell.btnComment.isHidden = false
            
            
        }
        
        
        
        cell.btnComment.tag = indexPath.row
        cell.btnResolved.tag = indexPath.row
        cell.btnRate.tag = indexPath.row
        
        cell.btnResolved.addTarget(self, action: #selector(self.resolvedClicked), for: .touchUpInside)
        cell.btnComment.addTarget(self, action: #selector(self.commentClicked), for: .touchUpInside)
        cell.btnRate.addTarget(self, action: #selector(self.rateClicked), for: .touchUpInside)
        return cell
        
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpDeskDetailVC") as! HelpDeskDetailVC
        vc.selectedComplaint = complainData[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func resolvedClicked(_ sender: UIButton)
    {
        self.resolvedComplaints(complaintID:complainData[sender.tag].id ?? "")
    }
    
    
    @objc func commentClicked(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpCommentVC") as! HelpCommentVC
        vc.strComplaintID = complainData[sender.tag].id!
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func rateClicked(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpRatingVC") as! HelpRatingVC
        vc.strComplaintID = complainData[sender.tag].id!
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
    }

    @objc func filterBy(_ sender: UIButton) {
        let headerView = FiltersVC()
        self.addChild(headerView)
        headerView.filterType = sender.tag
        headerView.filterByType(type: sender.tag)
        self.view.addSubview(headerView.view)
        headerView.custumDelegate = self
        headerView.didMove(toParent: self)
        headerView.view.frame = self.view.frame
    }
    
    func getCompaintList() {
        
        let params = ["lang_id":language,"uid":userData?.id ?? "","filter_by_type":filter_by_type,"help_category":help_category,"filter_by_status":filter_by_status ] as [String : Any]
        APIClient().PostdataRequest(WithApiName: API.complaintlist.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ComplaintListDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.complainData.removeAll()
                        PrintLog(self)
                        PrintLog(object.data)
                        self.complainData = object.data!
                        self.tblView.isHidden = false
                        self.noDataView.isHidden = true
                        self.tblView.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else {
                
                   DispatchQueue.main.async {
                    self.complainData.removeAll()
                    self.noDataView.isHidden = false
                    self.tblView.isHidden = true
                    self.tblView.reloadData()
                    
                }
                
                
            }
            
        }
        
    }
    
    func resolvedComplaints(complaintID:String) {
        
        let params = ["id":complaintID,"lang_id":language,"uid":userData?.id]
        APIClient().PostdataRequest(WithApiName: API.helpresolved.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [ {_ in
                                self.getCompaintList()
                            }
                        ])
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                
            }
        }
        
    }
    
    
}
