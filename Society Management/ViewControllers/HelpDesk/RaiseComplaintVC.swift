//  RaiseComplaintVC.swift
//  Society Management
//  Created by Jitendra Yadav on 02/05/21.


import UIKit
import TLPhotoPicker
import Photos

class RaiseComplaintVC: MasterVC , UITextViewDelegate,UICollectionViewDelegate, UICollectionViewDataSource{
    
    @IBOutlet weak var raiseTypeLbl: UILabel!
    @IBOutlet weak var personalBtn: UIButton!
    @IBOutlet weak var communityBtn: UIButton!
    @IBOutlet weak var isUrgentBtn: UIButton!
    @IBOutlet weak var noteTxt: UITextView!
    @IBOutlet weak var lblRemainingChara: UILabel!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var txtCat: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var submitTopCons: NSLayoutConstraint!
    
    var helpDeskCategory = [ServicePeopleData]()
    var imageArr = NSMutableArray()
    var imagePicker: ImagePicker!
    var filterType = String()
    var strCateID = String()
    var strIsUrgent = String()
    var selectedAssets = [TLPHAsset]()
    var complaintImages = [UIImage]()
    
    override func viewDidLoad()
    {
        self.className(ClassString: self)
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title18
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        personalBtn.isSelected = true
        communityBtn.isSelected = false
        noteTxt.delegate = self
        self.imagePicker = ImagePicker(presentationController: self, delegate: self)
        self.getCategories()
        filterType = "1"
        strIsUrgent = "1"
        
        collectionView.delegate = self
        collectionView.dataSource = self
        submitTopCons.constant = 20
        collectionView.isHidden  = true
        collectionView.backgroundColor = UIColor.clear
        
    }
    
    @IBAction func personalButton(_ sender: UIButton)
    {
        personalBtn.isSelected = true
        communityBtn.isSelected = false
        filterType = "1"
    }
    
    @IBAction func communityButton(_ sender: UIButton)
    {
        personalBtn.isSelected = false
        communityBtn.isSelected = true
        filterType = "2"
        
    }
    
    
    @IBAction func isUrgentButton(_ sender: UIButton)
    {
        if sender .isSelected
        {
            isUrgentBtn.setImage(UIImage(named: "tick_grey"), for: UIControl.State.normal)
            isUrgentBtn.isSelected = false
            strIsUrgent = "1"
            
        } else
        {
            isUrgentBtn.setImage(UIImage(named: "personal_tick"), for: UIControl.State.normal)
            isUrgentBtn.isSelected = true
            strIsUrgent = "2"
        }
        
    }
    
    @IBAction func attachedButton(_ sender: UIButton)
    {
        let viewController = CustomPhotoPickerViewController()
        viewController.delegate = self
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
            
            self?.showExceededMaximumAlert(vc: picker)
            
        }
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        viewController.logDelegate = self
        viewController.configure.allowedVideo = false
        viewController.configure.maxSelectedAssets = 3
        self.present(viewController, animated: true, completion: nil)
       
    }
    
    
    func showExceededMaximumAlert(vc: UIViewController)
    {
        let alert = UIAlertController(title: "", message: AlertMessage.maxNumber, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(AlertMessage.OK, comment: ""), style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset])
    {
        self.selectedAssets = withTLPHAssets
        complaintImages.removeAll()
        for i in 0..<selectedAssets.count
        {
            let asset : PHAsset = selectedAssets[i].phAsset! as PHAsset
            let img = self.getAssetThumbnail(asset: asset)
            complaintImages.append(img)
        }
       
        
        if complaintImages.count > 0 {
            
            submitTopCons.constant = 100
            collectionView.isHidden  = false
            
        } else {
            
            submitTopCons.constant = 20
            collectionView.isHidden  = true
            
        }
        
        self.collectionView.reloadData()
    }
    
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
            var singleImage = UIImage()
     
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
               // var image = UIImage()
                option.isSynchronous = true
                manager.requestImage(for: asset, targetSize: CGSize(width: 250, height: 250), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                    
                    singleImage = result!
                    
                })
         
           return singleImage
           
        }
    
    @IBAction func submitButton(_ sender: UIButton)
    {
        self.view.endEditing(true)
        guard let cat = txtCat.text , cat.isBlank == false else {
            ShowAlert(AlertMessage.alSelectCat, on: "", from: self)
            return
            
        }
        guard let note = noteTxt.text , note.isBlank == false else {
            ShowAlert(AlertMessage.alNoteBlank, on: "", from: self)
            return
            
        }
        self.submitComplaint()
    }

    @IBAction func typeSelectButton(_ sender: UIButton)
    {
        self.view.endEditing(true)
        if  let unitName = helpDeskCategory.map({$0.name }) as? [String] ,
            let unitID = helpDeskCategory.map({$0.id}) as? [String] {
          CustomPickerView.show(items: unitName , itemIds: unitID ,selectedValue:nil, doneBottonCompletion: { (item, index) in
              
                  self.txtCat.text = item
                  self.strCateID = index!
              
              }, didSelectCompletion: { (item, index) in
                 // self.txtCat.text = item
                  
                  
              }) { (item, index) in
              }
         }
    }
    
    
    func getCategories() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.helpcategory.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ServicePeopleModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.helpDeskCategory = object.data!
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
               // ShowAlert("no Data", on: "Opps?", from: self)
            }
        }

    }
    
    
    func submitComplaint()
    {
        let param = [
            "uid":userData?.id ?? "",
            "towerid":userData?.towerid ?? "",
            "unitnumber":userData?.unitnumber ?? "",
            "help_category":strCateID,
            "filter_by_type":filterType,
            "title":noteTxt.text ?? "",
            "lang_id":language,
            "is_urgent":strIsUrgent
        ]
        
        var imageArr = [imagesUpload]()
        for img in complaintImages {
            imageArr.append(imagesUpload(key: "file", image: img, fileName: "complaints\(randomString(length:15))"))
        }
        
        print(imageArr)
        print(param)
        
        APIClient().UploadMultipleImageWithParametersRequest(WithApiName: API.raisecomplaint.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {response, statescode in
            switch response {
            case .success(let obj):
                PrintLog(obj.msg)
                DispatchQueue.main.async {
                    
                    self.showCustumAlert(message:obj.msg ?? "", isBack: true)
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
        
        
    }
    
}

extension RaiseComplaintVC :ImagePickerDelegate {
    
    func didSelect(image: UIImage?, tag: Int)
    {
        self.imageArr.add(image!)
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        var newText = textView.text!
        newText.removeAll { (character) -> Bool in
            return character == " " || character == "\n"
        }
        
        let remaing = 1000 - (newText.count + text.count)
        self.lblRemainingChara.text = "\(remaing)"
        return (newText.count + text.count) <= 1000
    }

}
extension RaiseComplaintVC: TLPhotosPickerViewControllerDelegate, TLPhotosPickerLogDelegate {

    
    //For Log User Interaction
    func selectedCameraCell(picker: TLPhotosPickerViewController) {
        print("selectedCameraCell")
    }
    
    func selectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        print("selectedPhoto")
    }
    
    func deselectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        print("deselectedPhoto")
    }
    
    func selectedAlbum(picker: TLPhotosPickerViewController, title: String, at: Int) {
        print("selectedAlbum")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return complaintImages.count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReminderPhotoCell", for: indexPath as IndexPath) as? ReminderPhotoCell else { return UICollectionViewCell() }
        cell.imgDoc.image = complaintImages[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 80)
    }
}
