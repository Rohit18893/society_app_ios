//  HelpDeskDetailVC.swift
//  Society Management
//  Created by ROOP KISHOR on 13/06/2021.


import UIKit

class HelpDeskDetailVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    var selectedComplaint : ComplaintListData?
    var commentCount = Int()
    var commentData = [HelpComment]()
    var details : HelpDData?
   
   // HelpDModal

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewNAv.lbltitle.text = AlertMessage.title19
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.tableFooterView = UIView()
        self.className(ClassString: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getDetails()
        
    }
    
    //
    /*
    func getAllComments()
    {
        let params = ["lang_id":language,"uid":(userData?.id)!,"id":selectedComplaint?.id]
        APIClient().PostdataRequest(WithApiName: API.helpComment.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: HelpCommentModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.commentData = object.data!
                        self.commentCount = self.commentData.count
                        self.tblView.reloadData()
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                    
                }
                

            }
            
        }

    }
    */
    
    func getDetails()
    {
        let params = ["lang_id":language,"uid":(userData?.id)!,"id":selectedComplaint?.id]
        APIClient().PostdataRequest(WithApiName: API.helpdetails.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: HelpDModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        
                        self.details = object.data
                        if let comment = object.data?.comment_list
                        {
                            self.commentData = comment
                        }
                       self.commentCount = self.commentData.count
                       self.tblView.reloadData()
                        
                    }
                    
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                    
                }
                

            }
            
        }

    }
    
    
}
extension HelpDeskDetailVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if section == 3
        {
           return self.commentData.count
        }
        else
        {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpDeskDetailCell", for: indexPath) as! HelpDeskDetailCell
            cell.lblTitle.text = selectedComplaint?.help_category
            cell.lblDesc.text = selectedComplaint?.title
            let stamp = selectedComplaint?.created_date
            let date = convertTimeStampTo(timeStamp: stamp!)
            cell.lblDate.text = String(format: "%@ • REQUEST ID %@", date,(selectedComplaint?.id)!)
            cell.lblStatus.text = selectedComplaint?.filter_by_status
            if selectedComplaint?.filter_by_status == "Resolved"
            {
               cell.lblStatus.backgroundColor = UIColor.systemGreen
            }
            else
            {
               cell.lblStatus.backgroundColor = UIColor.systemYellow
            }
           
           if selectedComplaint?.is_urgent == "1"
           {
               cell.lblIsUrgent.isHidden = true
               cell.lblIsUrgent.text = ""
               
           } else
           {
               cell.lblIsUrgent.text = "Urgent"
               cell.lblIsUrgent.isHidden = false
           }
           
           
            cell.lblRating.text = ""
            cell.imgRate.isHidden = true
            cell.raisedTopCons.constant = -18
            
            if selectedComplaint?.is_resolved == "1"
            {
                cell.lblResolvedBy.text = "Resolved by \((selectedComplaint?.resolved_by ?? ""))"
                if selectedComplaint?.help_rating == "0"
                {
                    
                } else
                {
                    cell.lblRating.text = "Rating - \(selectedComplaint?.help_rating ?? "")/5"
                    cell.imgRate.isHidden = false
                    cell.raisedTopCons.constant = 8
                }
            } else
            {
                cell.lblResolvedBy.text = "Raised by \(selectedComplaint?.username ?? "")"
            }
    
            if let allImages = selectedComplaint?.image {
                
                if allImages.count > 0 {
                    
                    cell.images = allImages
                    cell.collectionHeight.constant = 130
                    cell.vcPrev = self
                    cell.collectionImages.reloadData()
                }
                else
                {
                    cell.collectionHeight.constant = 0
                }
                
            }
            
           return cell
            
        } else  if indexPath.section == 1 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceRatingCell", for: indexPath) as! ServiceRatingCell
            cell.ratingView.settings.updateOnTouch = false
            cell.ratingView.rating =  Double(selectedComplaint?.help_rating ?? "0")!
            return cell
            
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCountCell", for: indexPath) as! CommentCountCell
            cell.lblComment.text = "\(self.commentCount) Comments"
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentCell
            
            
            let boldFontAttributes = [NSAttributedString.Key.foregroundColor: UIColor.red, NSAttributedString.Key.font: UIFont(name: "ProximaNova-Regular", size: 15)]
            
                let normalFontAttributes = [NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.font: UIFont(name: "ProximaNova-Regular", size: 15)]
            
            let userName = NSMutableAttributedString(string: "\(commentData[indexPath.row].username ?? "") : ", attributes: boldFontAttributes as [NSAttributedString.Key : Any])
            let comment = NSMutableAttributedString(string: "\(commentData[indexPath.row].comment ?? "")", attributes: normalFontAttributes as [NSAttributedString.Key : Any])

                let combination = NSMutableAttributedString()
                
                combination.append(userName)
                combination.append(comment)
                cell.lblComment.attributedText = combination
            
            
            
            if let dt = commentData[indexPath.row].created_date {
                cell.lblTime.text = timeElapsed(date: Date(timeIntervalSince1970:TimeInterval(dt)))
            }
           return cell
        }

    }
}
