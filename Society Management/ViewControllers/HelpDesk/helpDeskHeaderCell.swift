//  helpDeskHeaderCell.swift
//  Society Management
//  Created by Jitendra Yadav on 02/05/21.


import UIKit

class helpDeskHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblIsUrgent: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblResolvedBy: UILabel!
    
    @IBOutlet weak var btnResolved: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnRate: UIButton!
    @IBOutlet weak var imgRate: UIImageView!
    @IBOutlet weak var raisedTopCons: NSLayoutConstraint!
    
    @IBOutlet weak var btnView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
