//  AcceptCallVC.swift
//  Society Management
//  Created by Jitendra Yadav on 13/07/21.


import UIKit
import AVFoundation

class AcceptCallVC: MasterVC {

    @IBOutlet weak var leaveAtguardLbl: UILabel!
    @IBOutlet weak var leaveAtGuard: ButtonRadius!
    
    @IBOutlet weak var towername: UILabel!
    @IBOutlet weak var entryTypeIcon: Imageradius!
    @IBOutlet weak var visitorPicture: Imageradius!
    @IBOutlet weak var companyImage: UIButton!
    @IBOutlet weak var visitorCompany: UILabel!
    @IBOutlet weak var visitorName: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
        
    var notificationdata:[AnyHashable: Any]?
    
    let recentActivityViewModel = RecentActivityViewModel()
    
    var visitorid :String {
        return notificationdata?["visitorid"] as? String ?? ""
    }
    
    var mobile :String {
        return notificationdata?["visitormobile"] as? String ?? ""
    }
    
    weak var timer: Timer?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        
        let aps = notificationdata?["aps"] as? [String:Any]
        let alert = aps?["alert"] as? [String:Any]
        let body = alert?["body"] as? String
        let title = alert?["title"] as? String
        
        let visitorType = notificationdata?["iconid"] as? String
        
        if visitorType == "2"{
            leaveAtGuard.isHidden = false
            leaveAtguardLbl.isHidden = false
        }else{
            leaveAtGuard.isHidden = true
            leaveAtguardLbl.isHidden = true
        }
        
        self.titleLbl.text = body
        self.towername.text = title
        
        if let visitorname = notificationdata?["visitorname"] as? String {
            self.visitorName.text = visitorname
        }
        if let companyName = notificationdata?["company_name"] as? String {
            self.visitorCompany.text = companyName
        }
        
        if let companyimage = notificationdata?["company_image"] as? String ,companyimage.count > 0 {
            companyImage.sd_setImage(with: URL(string: companyimage), for: .normal, placeholderImage: setImages(visitorType ?? ""))
        }else{
            companyImage.setImage(setImages(visitorType ?? ""), for: .normal)
        }
        
        if let visitorImage = notificationdata?["visitorimage"] as? String ,visitorImage.count > 0 {
            visitorPicture.sd_setImage(with: URL(string: visitorImage),placeholderImage: setImages(visitorType ?? ""))
        }else{
            visitorPicture.image = setImages(visitorType ?? "")
        }
        
        self.entryTypeIcon.image = setImages(visitorType ?? "")
        self.startTimer()
    }
    
    
    func startTimer() {
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 60.0, repeats: true) { [weak self] _ in
            guard let self = self else {return}
            self.back(self)
        }
    }

    func stopTimer() {
        timer?.invalidate()
    }

    // if appropriate, make sure to stop your timer in `deinit`

    deinit {
        stopTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.stopTimer()
    }
    
    @IBAction func onClickCall(_ sender: Any) {
        self.callNumber(phoneNumber: mobile)
    }
    @IBAction func onClickDeny(_ sender: Any) {
        self.AcceptDecline("2")
    }
    @IBAction func onClickLeaveatGaurd(_ sender: Any) {
        self.AcceptDecline("4")
    }
    @IBAction func onClickApproved(_ sender: Any) {
        self.AcceptDecline("1")
    }
    
    func AcceptDecline(_ flag:String){
        let params = ["entry_id":visitorid,
                      "lang_id":language,
                      "accept_decline_time":Date.getCurrentDate(),
                      "accept_decline_flag":flag]
        debugPrint(params)
        APIClient().PostdataRequest(WithApiName: API.acceptDecline.rawValue, Params: params, isNeedLoader: true, objectType: StatusModel.self) { response, statusCode in
            switch response {
            case .success(_):
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .householdReload, object: nil)
                    self.back(self)
                }
            case .failure(let error):
                debugPrint(error)
            }
        }
    }
}
