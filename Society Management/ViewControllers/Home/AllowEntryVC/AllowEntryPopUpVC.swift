//
//  AllowEntryPopUpVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 22/06/21.
//

import UIKit

class AllowEntryPopUpVC: MasterVC {

    var onClickEvent:((String)->())? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeBlureView()
        self.className(ClassString: self)
    }
    
    @IBAction func onClickVisitingHelp(_ sender: Any) {
        if let r = self.onClickEvent{
            r("help")
           
        }
        self.back(self)
    }
    
    @IBAction func onclickDelivery(_ sender: Any) {
        if let r = self.onClickEvent{
            r("delivery")
        }
        self.back(self)
    }
    
    @IBAction func onClickCab(_ sender: Any) {
        if let r = self.onClickEvent{
            r("cab")
        }
        self.back(self)
    }
    
    @IBAction func onClickGuest(_ sender: Any) {
        if let r = self.onClickEvent{
            r("guest")
        }
        self.back(self)
    }
}
