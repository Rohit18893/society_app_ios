//
//  CenterTableViewCell.swift
//  Society Management
//
//  Created by ROOP KISHOR on 29/04/2021.
//

import UIKit

class CenterTableViewCell: UITableViewCell ,UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    var selectionBtnCallBAck:((IndexPath)->())? = nil
    
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    var data = [String]()
    var images = [String]()

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        self.collectionView.register(UINib(nibName: "CenterCollectionCell", bundle: nil), forCellWithReuseIdentifier: "CenterCollectionCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundColor = UIColor.appColor(.BackgroundColor)
        collectionHeight.constant = 120
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // Configure the view for the selected state
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CenterCollectionCell", for: indexPath as IndexPath) as? CenterCollectionCell else { return UICollectionViewCell() }
        cell.lblName.text = data[indexPath.row]
        cell.iconImg.image = UIImage(named:images[indexPath.row])
        cell.backgroundView?.addShadow()
        cell.backgroundColor = setBackgroundColor(textName:data[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       // return CGSize(width: 85 , height: 105)
        return CGSize(width: 110 , height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return sectionInset
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let ind = self.selectionBtnCallBAck {
            ind(indexPath)
        }
        
    }
    
    
    func setBackgroundColor(textName:String)->(UIColor) {
        /*1*/   if textName == "Security Alert" {
           
            return (#colorLiteral(red: 0.1568627451, green: 0.8549019608, blue: 0.4274509804, alpha: 1))
        }
        /*2*/   else if textName == "Message to Guard" {
           
            return (#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1))
        }
        /*3*/   else if textName == "Cab" {
            return (#colorLiteral(red: 0.1178380027, green: 0.2793641686, blue: 0.4150332808, alpha: 1))
        }
        /*4*/   else if textName == "Delivery" {
            
            return (#colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1))
        }
        /*5*/   else if textName == "Guest" {
            return (#colorLiteral(red: 0.2274509804, green: 0.6117647059, blue: 0.9725490196, alpha: 1))
        }
        /*6*/   else if textName == "Visiting Help" {
            return (#colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1))
          
        }
        /*7*/   else if textName == "Kid" {
            return (#colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1))
            
        }
        /*9*/   else{
            return (#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1))
        }
    }
}
