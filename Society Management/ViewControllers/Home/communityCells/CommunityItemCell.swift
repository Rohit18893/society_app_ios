//
//  CommunityItemCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/04/21.
//

import UIKit

class CommunityItemCell: UICollectionViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var textlBl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

}
