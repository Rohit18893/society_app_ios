//  CommunityCells.swift
//  Society Management
//  Created by Jitendra Yadav on 26/04/21.


import UIKit

class CommunityCells: UITableViewCell ,UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var viewAllButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    var vc1: UIViewController?
    var onClickEvent:((String)->())? = nil
    
    var cellType = 1
    
    var data = ["Communications","Helpdesk","Emergency No's","Resident"]
    var data1 = ["Allow\nEnteries","Security\nAlert","Message to\nGuard"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        self.collectionView.register(UINib(nibName: "CommunityItemCell", bundle: nil), forCellWithReuseIdentifier: "CommunityItemCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    // Configure the view for the selected state
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if cellType == 1 {
            return data.count
        }else{
            return data1.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CommunityItemCell", for: indexPath as IndexPath) as? CommunityItemCell else { return UICollectionViewCell() }
        if cellType == 1 {
            let rr = setImageAndBackgroundColor(textName: data[indexPath.item])
            cell.icon.image = rr.0
            cell.textlBl.text = data[indexPath.item]
            cell.view.backgroundColor = rr.1
        }else{
            let rr = setImageAndBackgroundColor(textName: data1[indexPath.item])
            cell.icon.image = rr.0
            cell.textlBl.text = data1[indexPath.item]
            cell.view.backgroundColor = rr.1
        }
        return cell
        
    }
    
    
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if cellType == 1 {
            if let r = self.onClickEvent{
                r(data[indexPath.item])
            }
        } else{
            if let r = self.onClickEvent{
                r(data1[indexPath.item])
            }
        }
    }
}

extension CommunityCells: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 120 , height: 140)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        return sectionInset
    }
}


func setImageAndBackgroundColor(textName:String)->(UIImage,UIColor,String) {
    /*1*/   if textName == "Local Services" {
        return (#imageLiteral(resourceName: "local_icon"),#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1),"Find Daily helps & Service Providers")
    }
    /*2*/   else if textName == "Emergency No's" {
        return (#imageLiteral(resourceName: "emergency"),#colorLiteral(red: 0.9960784314, green: 0.2666666667, blue: 0.3215686275, alpha: 1),"Emergency contacts for your society")
    }
    /*3*/   else if textName == "Resident" {
        return (#imageLiteral(resourceName: "resident"),#colorLiteral(red: 0.9882352941, green: 0.7803921569, blue: 0.2745098039, alpha: 1),"View Society Residents & Management Committee")
    }
    /*4*/   else if textName == "Helpdesk" {
        return (#imageLiteral(resourceName: "helpdesk"),#colorLiteral(red: 0.9058823529, green: 0.2235294118, blue: 0.5529411765, alpha: 1),"Raise complaints & Service Request")
    }
    /*5*/   else if textName == "Allow\nEnteries" {
        return (#imageLiteral(resourceName: "allow_enteries"),#colorLiteral(red: 0.2274509804, green: 0.6117647059, blue: 0.9725490196, alpha: 1),"View Society Announcements")
    }
    /*6*/   else if textName == "Communications" {
        return (#imageLiteral(resourceName: "communications"),#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1),"Host Meeting, Polls and Discussions")
    }
    /*7*/   else if textName == "Security\nAlert" {
        return (#imageLiteral(resourceName: "security_alert"),#colorLiteral(red: 0.1568627451, green: 0.8549019608, blue: 0.4274509804, alpha: 1),"Find and store society & personal documents")
    }
    /*8*/   else if textName == "Message to\nGuard" {
        return (#imageLiteral(resourceName: "message_to_guard"),#colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1),"Pay all due easly \n  ")
    }
    else if textName == "Issue\nGatepass" {
        return (#imageLiteral(resourceName: "issue_gatepass"),#colorLiteral(red: 0.9725490196, green: 0.4784313725, blue: 0, alpha: 1),"Pay all due easly \n  ")
    }
    /*9*/   else{
        return (#imageLiteral(resourceName: "issue_gatepass"),#colorLiteral(red: 0.4235294118, green: 0.2549019608, blue: 0.6823529412, alpha: 1),"")
    }
}


