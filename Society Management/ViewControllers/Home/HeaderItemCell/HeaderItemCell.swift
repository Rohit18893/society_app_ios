//
//  HeaderItemCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 22/06/21.
//

import UIKit

class HeaderItemCell: UITableViewCell {
    @IBOutlet weak var backView: GradientView!
    
    @IBOutlet weak var onClickServiceCharge: UIButton!
    @IBOutlet weak var onClickHealth: UIButton!
    @IBOutlet weak var onClickPreApproved: UIButton!
    @IBOutlet weak var onClickLocalService: UIButton!
    @IBOutlet weak var lblServiceCharge: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        DispatchQueue.main.async {
            self.backView.roundCorners([.bottomLeft, .bottomRight], radius: 20)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
