//  SupportCell.swift
//  Society Management
//  Created by Jitendra Yadav on 26/04/21.


import UIKit

class SupportCell: UITableViewCell {

    @IBOutlet weak var button1: UIButton!
    @IBOutlet weak var button2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        
        DispatchQueue.main.async {
            self.button1.roundCorners([.bottomLeft,.bottomRight], radius: 8)
            //self.button1.roundCorners([.bottomRight], radius: 8)
           // self.button2.roundCorners([.bottomRight], radius: 8)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
