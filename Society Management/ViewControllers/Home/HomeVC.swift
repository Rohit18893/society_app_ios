//  HomeVC.swift
//  Society Management
//  Created by Jitendra Yadav on 26/04/21.


import UIKit
import SideMenu


class HomeVC: MasterVC {
    
    @IBOutlet weak var viewNAv: GradientView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblNotificationCount: PaddingLabel!
    
    var noticeBoardData = [HomeNotice]()
    
    var recentActivity = 0
    
    fileprivate let arrCells = [NotificationBoardCell.self ,recentActivityCell.self ,CommunityCells.self,SupportCell.self,HeaderItemCell.self]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        setupSideMenu()
        updateMenus()
        self.getNoticeBoard()
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.separatorStyle = UITableViewCell.SeparatorStyle.none
        lblNotificationCount.layer.cornerRadius = (lblNotificationCount.frame.size.height + 10)/2
        lblNotificationCount?.layer.masksToBounds = true
        lblNotificationCount.backgroundColor = UIColor.white
        lblNotificationCount.textColor = UIColor.systemBlue
        self.lblNotificationCount.isHidden = true
        
        self.tblView.backgroundColor = UIColor.appColor(.BackgroundColor)
        self.view.backgroundColor = UIColor.appColor(.BackgroundColor)
        if let userflat = userData?.society {
            if let titletxt = self.view.viewWithTag(1111) as? UILabel {
                titletxt.text = userflat
            }
        }
        
    }
    
    private func setupSideMenu()
    {
        // Define the menus
        SideMenuManager.default.leftMenuNavigationController = storyboard?.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? SideMenuNavigationController
        SideMenuManager.default.addPanGestureToPresent(toView: navigationController!.navigationBar)
        SideMenuManager.default.addScreenEdgePanGesturesToPresent(toView: view)
        
    }
    
   
    private func updateMenus() {
        let settings = makeSettings()
        SideMenuManager.default.leftMenuNavigationController?.settings = settings
        SideMenuManager.default.rightMenuNavigationController?.settings = settings
    }

    private func selectedPresentationStyle() -> SideMenuPresentationStyle
    {
        let modes: [SideMenuPresentationStyle] = [.menuSlideIn, .viewSlideOut, .viewSlideOutMenuIn, .menuDissolveIn]
        return modes[0]
    }

    private func makeSettings() -> SideMenuSettings {
        
        let presentationStyle = selectedPresentationStyle()
        presentationStyle.menuStartAlpha = CGFloat(1)
        presentationStyle.menuScaleFactor = CGFloat(1)
        presentationStyle.presentingEndAlpha = CGFloat(0.3)
        var settings = SideMenuSettings()
        settings.presentationStyle = presentationStyle
        settings.menuWidth = self.view.frame.size.width - 65
        return settings
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sideMenuNavigationController = segue.destination as? SideMenuNavigationController else { return }
        sideMenuNavigationController.settings = makeSettings()
    }
    
    @IBAction func serviceChargeClick(_ sender: UIButton)
    {
        if userData?.usertype == "1"
        {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "ServiceChargeVC")
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else
        {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "MyBookingVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func healthProtectionClick(_ sender: UIButton) {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpDeskVC")
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func DeliveryBtnClick(_ sender: UIButton)
    {
        let vc = AllowEntryPopUpVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.onClickEvent = { value in
            self.didSelectItem(type: value)
        }
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func localServiceClick(_ sender: UIButton) {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "LocalServiceVC")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func settingClicked(_ sender: UIButton) {
        
        let vc = self.mainStory.instantiateViewController(withIdentifier: "SettingsVC")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func scannerButtunClicked(_ sender: UIButton)
    {
        let nextViewController = mainStory.instantiateViewController(withIdentifier: "Scan_Reminder") as! Scan_Reminder
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @IBAction func notificationButtunClicked(_ sender: UIButton)
    {
        let nextViewController = mainStory.instantiateViewController(withIdentifier: "NotificationListVC") as! NotificationListVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
}

extension HomeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return recentActivity
        } else if section == 2 {
            return noticeBoardData.count
        } else if section == 3 {
            return 1
        } else if section == 4 {
            return 1
        } else if section == 5 {
            return 1
        } else {
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if indexPath.section == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderItemCell", for: indexPath) as! HeaderItemCell
            cell.onClickServiceCharge.addTarget(self, action: #selector(serviceChargeClick), for: .touchUpInside)
            cell.onClickLocalService.addTarget(self, action: #selector(localServiceClick), for: .touchUpInside)
            cell.onClickPreApproved.addTarget(self, action: #selector(DeliveryBtnClick), for: .touchUpInside)
            cell.onClickHealth.addTarget(self, action: #selector(healthProtectionClick), for: .touchUpInside)
            if userData?.usertype == "1"
            {
                cell.lblServiceCharge.text = "Service\nCharge"
            } else
            {
                cell.lblServiceCharge.text = "My Bookings"
            }
            return cell
            
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "recentActivityCell", for: indexPath) as! recentActivityCell
            return cell
        } else if indexPath.section == 2 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationBoardCell", for: indexPath) as! NotificationBoardCell
            cell.lblTitle.text = self.noticeBoardData[indexPath.row].name
            cell.lblDesc.text = noticeBoardData[indexPath.row].description?.html2String
            return cell
            
        } else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommunityCells", for: indexPath) as! CommunityCells
            cell.cellType = 1
            cell.headerLbl.text = "COMMUNITY"
            cell.onClickEvent = { value in
                self.didSelectItem(type: value)
            }
            cell.viewAllButton.isHidden = false
            cell.viewAllButton.addTarget(self, action: #selector(viewAll), for: .touchUpInside)
            return cell
            
        } else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommunityCells", for: indexPath) as! CommunityCells
            cell.cellType = 2
            cell.vc1 = self
            cell.headerLbl.text = "Notify gate".uppercased()
            cell.onClickEvent = { value in
                self.didSelectItem(type: value)
            }
            cell.viewAllButton.isHidden = true
            return cell
            
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportCell", for: indexPath) as! SupportCell
            cell.button1.addTarget(self, action: #selector(writeToAlmudeer), for: .touchUpInside)
           // cell.button2.addTarget(self, action: #selector(Support), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "NoticeBoardVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
        }
    }
    
    
    @objc func writeToAlmudeer(_ sender:UIButton)
    {
        let email = "IT@almudeer.ae"
        if let url = URL(string: "mailto:\(email)") {
          if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
          } else {
            UIApplication.shared.openURL(url)
          }
        }
    }
    
    @objc func Support(_ sender:UIButton) {
        
        guard let url = URL(string: "https://almudeerit.com/contact") else {
          return //be safe
        }

        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
        
    }
    
    
    @objc func viewAll(_ sender:UIButton) {
        DispatchQueue.main.async {
            self.tabBarController?.selectedIndex = 4
        }
    }
    
    func didSelectItem(type:String) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            
            if type == "Communications" {
                let vc = ComunicationVC()
                vc.modalTransitionStyle = .crossDissolve
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "Helpdesk" {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "HelpDeskVC")
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if type == "Emergency No's" {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "emergenctNoVC")
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if type == "Resident" {
                let vc = self.mainStory.instantiateViewController(withIdentifier: "ResidentVC")
                self.navigationController?.pushViewController(vc, animated: true)
                
            } else if type == "Allow\nEnteries" {
                let vc = AllowEntryPopUpVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.onClickEvent = { value in
                    self.didSelectItem(type: value)
                }
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "Security\nAlert" {
                
                let vc = SecurityAlertVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "Message to\nGuard" {
                
                let vc = MessageGuardVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "help" {
                let vc = VisitingVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.categoryID = "1"
                vc.selectionBtnCallBAck = {[weak self] name , image , visitorCat in
                    guard let self = self else {return}
                    DispatchQueue.main.async {
                        let vc = CabVC()
                        vc.allowText = "Allow my visiting help \(String(describing: name.name!)) to enter today once in next"
                        vc.modalPresentationStyle = .overFullScreen
                        vc.modalTransitionStyle = .crossDissolve
                        vc.logiName = image
                        vc.categoryID = visitorCat
                        self.present(vc, animated: false, completion: nil)
                    }
                }
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "cab" {
                let vc = CabVC()
                vc.allowText = "Allow my cab to enter today once in next"
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.logiName = "cab"
                vc.categoryID = "2"
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "guest" {
                let vc = GuestVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
                
            } else if type == "delivery" {
                let vc = CabVC()
                vc.allowText = "Allow delivery executive to enter today once in next"
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.logiName = "delivery_icon"
                vc.categoryID = "3"
                self.present(vc, animated: true, completion: nil)
            } else {
                let vc = AcceptCallVC()
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                self.present(vc, animated: true, completion: nil)
            }
        }
        
    }
    
    
    func getNoticeBoard() {
        let params = ["lang_id":language,"towerid":userData?.towerid,"uid":userData?.id,"unitnumber":userData?.unitnumber]
        APIClient().PostdataRequest(WithApiName: API.homeNoticeBoard.rawValue, Params: params as [String : Any], isNeedLoader: false, objectType: HomeNoticeModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        self.noticeBoardData.removeAll()
                        self.noticeBoardData = (object.noticeboard)!
                        if object.notification_count == "0"
                        {
                            self.lblNotificationCount.isHidden = true
                        }
                        else
                        {
                            self.lblNotificationCount.isHidden = false
                            self.lblNotificationCount.text = object.notification_count
                        }
                        self.tblView.reloadData()
                        
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async {
                    self.noticeBoardData.removeAll()
                    self.tblView.reloadData()
                }
                
            }
        }
        
    }
}
