//
//  recentActivityItemCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/04/21.
//

import UIKit

class recentActivityItemCell: UICollectionViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        DispatchQueue.main.async {
            self.dateLbl.roundCorners([.bottomLeft, .topLeft], radius: 9)
        }
    }

}
