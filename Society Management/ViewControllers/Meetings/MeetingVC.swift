//  MeetingVC.swift
//  Society Management
//  Created by ROOP KISHOR on 01/07/2021. MeetingCell


import UIKit

class MeetingVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var upcomingMeetings: UIButton!
    @IBOutlet weak var pastMeetings: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title4
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.setUpView(state: 1)
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func upcomingMeetingClicked(_ sender: UIButton)
    {
        setUpView(state: 1)
    }
    
    
    @IBAction func pastMeetingClicked(_ sender: UIButton)
    {
        setUpView(state: 2)
    }
    
    
    func setUpView(state:Int) {
        
        if state == 1 {
            
            
            upcomingMeetings.backgroundColor = UIColor(named: "HeaderColor")
            pastMeetings.backgroundColor =  UIColor.white
            upcomingMeetings.setTitleColor(UIColor.white, for: .normal)
            pastMeetings.setTitleColor(UIColor.black, for: .normal)
        }
        else  {
            
           
            
            pastMeetings.backgroundColor = UIColor(named: "HeaderColor")
            upcomingMeetings.backgroundColor = UIColor.white
            pastMeetings.setTitleColor(UIColor.white, for: .normal)
            upcomingMeetings.setTitleColor(UIColor.black, for: .normal)
        }
    }
    
}

extension MeetingVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "MeetingCell", for: indexPath) as! MeetingCell
        return cell
        
    }
}
