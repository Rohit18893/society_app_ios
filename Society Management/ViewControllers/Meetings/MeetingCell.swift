//  MeetingCell.swift
//  Society Management
//  Created by ROOP KISHOR on 12/09/2021.


import UIKit

class MeetingCell: UITableViewCell {
    
    @IBOutlet weak var tblAgenda: UITableView!
    fileprivate let arrCells = [AgendaCell.self]
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.className(ClassString: self)
        self.tblAgenda.register(cellTypes: arrCells)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
extension MeetingCell: UITableViewDataSource,UITableViewDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AgendaCell", for: indexPath) as! AgendaCell
//        cell.lblHoueseName.text = String(format: "%@ , %@", villaInfo[indexPath.row].towername!,villaInfo[indexPath.row].unitname!)
        return cell
    }
 
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 65.0
//    }
//
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
//        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ActionVillaVC") as! ActionVillaVC
//        nextViewController.villaInfo = villaInfo[indexPath.row]
//        viewVC.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
}
