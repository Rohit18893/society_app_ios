//  AddReminderVC.swift
//  Society Management
//  Created by ROOP KISHOR on 30/06/2021.


import UIKit
import TLPhotoPicker
import Photos
import Alamofire
import MaterialComponents.MaterialTextControls_OutlinedTextAreas
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class AddReminderVC: MasterVC ,TLPhotosPickerViewControllerDelegate, TLPhotosPickerLogDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var txtDocumentType: MDCOutlinedTextField!
    @IBOutlet weak var txtDate: MDCOutlinedTextField!
    @IBOutlet weak var txtDocNumber: MDCOutlinedTextField!
    @IBOutlet weak var txtAttachment: MDCOutlinedTextField!
    var documentDataArr = [DocumentData]()
    var theme: SambagTheme = .light
    var strDocID = String()
    var strExpDate = String()
    var selectedAssets = [TLPHAsset]()
    var documentImages = [UIImage]()
    var strIsEdit = String()
    @IBOutlet weak var btnSubmit: UIButton!
    var reminderDetail : ReminderData?
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var submitTopCons: NSLayoutConstraint!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title6
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        
        self.setTextFieldTheme(txtField:txtDocumentType, labelText: "Document Type Name", allowEditing: false)
        self.setTextFieldTheme(txtField:txtDate, labelText: "Expiry Date", allowEditing: false)
        self.setTextFieldTheme(txtField:txtDocNumber, labelText: "Document Number", allowEditing: true)
        self.setTextFieldTheme(txtField:txtAttachment, labelText: "Attachment", allowEditing: false)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        submitTopCons.constant = 20
        collectionView.isHidden  = true
        self.getDocumentType()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        if self.strIsEdit == "1"
        {
            self.viewNAv.lbltitle.text =  AlertMessage.title7
            self.txtDocumentType.text = reminderDetail?.document_name
            self.txtDate.text = convertFormater(reminderDetail?.end_date ?? "")
            self.strExpDate =  self.txtDate.text ?? ""
            self.txtDocNumber.text = reminderDetail?.document_number
            btnSubmit.setTitle("Save", for: .normal)
            self.strDocID = "1"
            if let allImages = reminderDetail?.images {
                
                txtAttachment.text = " \(allImages.count) Attachments"
                if allImages.count > 0 {
                    for i in 0..<allImages.count
                    {
                        let strUrl = allImages[i]
                        let url = NSURL(string: strUrl)
                        self.downloadImage(from:url! as URL)
                    }
                    
                } else {
                    collectionView.isHidden = true
                    submitTopCons.constant = 20
                }
                
            }
            
        }
        else
        {
            self.viewNAv.lbltitle.text = AlertMessage.title6
            btnSubmit.setTitle("Save", for: .normal)
        }
        
    }
    
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            // always update the UI from the main thread
            DispatchQueue.main.async() { [weak self] in
                
               self?.documentImages.append(UIImage(data: data)!)
                if self?.documentImages.count ?? 0 > 0
                {
                    self?.collectionView.isHidden = false
                    self?.submitTopCons.constant = 100
                    self?.collectionView.isHidden = false
                    self?.collectionView.reloadData()
                }
                
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func getDocumentType() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.documentType.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: DocumentDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object.result)
                        if let rs = object.result
                        {
                            self.documentDataArr = rs
                        }
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
                
            }else {
                
            }
            
        }

    }
   
    
    @IBAction func selectDocumentClicked(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
      if  let documentName = documentDataArr.map({$0.name }) as? [String] ,
          let docID = documentDataArr.map({$0.id}) as? [String] {
        CustomPickerView.show(items: documentName , itemIds: docID ,selectedValue:self.txtDocumentType.text, doneBottonCompletion: { (item, index) in
            
               self.txtDocumentType.text = item
               PrintLog(index)
               self.strDocID = index!
               
            
            }, didSelectCompletion: { (item, index) in
                
                self.txtDocumentType.text = item
                
            }) { (item, index) in
            
            }
        
       }
        
    }
    
    
    @IBAction func selectExpDateClicked(_ sender: UIButton)
    {
        let vc = SambagDatePickerViewController()
        vc.theme = theme
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func selectPhotoClicked(_ sender: UIButton)
    {
        let viewController = CustomPhotoPickerViewController()
        viewController.delegate = self
        viewController.didExceedMaximumNumberOfSelection = { [weak self] (picker) in
            self?.showExceededMaximumAlert(vc: picker)
        }
        
        var configure = TLPhotosPickerConfigure()
        configure.numberOfColumn = 3
        viewController.configure = configure
        viewController.selectedAssets = self.selectedAssets
        viewController.logDelegate = self
        viewController.configure.maxSelectedAssets = 3
        viewController.configure.allowedVideo = false
        self.present(viewController, animated: true, completion: nil)
    }
    
    
    func showExceededMaximumAlert(vc: UIViewController)
    {
        let alert = UIAlertController(title: "", message:AlertMessage.maxNumber, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString(AlertMessage.OK, comment: ""), style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    
    func dismissPhotoPicker(withTLPHAssets: [TLPHAsset])
    {
        self.selectedAssets = withTLPHAssets
        documentImages.removeAll()
        for i in 0..<selectedAssets.count
        {
            let asset : PHAsset = selectedAssets[i].phAsset! as PHAsset
            let img = self.getAssetThumbnail(asset: asset)
            documentImages.append(img)
        }
        
        if documentImages.count > 0 {
            
            submitTopCons.constant = 100
            txtAttachment.text = " \(documentImages.count) Attachments"
            collectionView.isHidden  = false
            
        } else {
            
            submitTopCons.constant = 20
            txtAttachment.text = ""
            collectionView.isHidden  = true
            
        }
        
        
        self.collectionView.reloadData()
       
    }
    
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
            var singleImage = UIImage()
     
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
               // var image = UIImage()
                option.isSynchronous = true
                manager.requestImage(for: asset, targetSize: CGSize(width: 250, height: 250), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                    
                    singleImage = result!
                    
                })
         
           return singleImage
           
        }
    
    @IBAction func submitButtunClicked(_ sender: UIButton)
    {
        guard let docType = txtDocumentType.text , docType.isBlank == false  else {
            ShowAlert(AlertMessage.docType, on: "Error!", from: self)
            return
        }
        
        guard let expDate = txtDate.text , expDate.isBlank == false  else {
            ShowAlert(AlertMessage.expDate, on: "Error!", from: self)
            return
        }
        
        guard let docNum = txtDocNumber.text , docNum.isBlank == false  else {
            ShowAlert(AlertMessage.documentNumber, on: "Error!", from: self)
            return
        }
        
        guard let photo = txtAttachment.text , photo.isBlank == false  else {
            ShowAlert(AlertMessage.documentImage, on: "Error!", from: self)
            return
        }
        
        if self.strIsEdit == "1" {
            
            self.editReminder()
            
        } else {
            
            self.addReminder()
        }
        
      
    }
    
    
    func addReminder() {
       
        let param = [
            "uid":userData?.id ?? "",
            "towerid":userData?.towerid ?? "",
            "unitnumber":userData?.unitnumber ?? "",
            "document_type_id":self.strDocID,
            "end_date":strExpDate,
            "document_number":txtDocNumber.text ?? "",
            "lang_id":language
        ]
        
        var imageArr = [imagesUpload]()
        for img in documentImages {
            imageArr.append(imagesUpload(key: "file", image: img, fileName: "reminder\(randomString(length:15))"))
        }
        
        print(imageArr)
        print(param)
        
        APIClient().UploadMultipleImageWithParametersRequest(WithApiName: API.addreminder.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {response, statescode in
            switch response {
            case .success(let obj):
                PrintLog(obj.msg)
                DispatchQueue.main.async {
                    
                    self.showCustumAlert(message:obj.msg ?? "", isBack: true)
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
        
    }
    
    func editReminder() {
        
        let param = [
            "uid":userData?.id ?? "",
            "towerid":userData?.towerid ?? "",
            "unitnumber":userData?.unitnumber ?? "",
            "document_type_id":"1",
            "end_date":strExpDate,
            "document_number":txtDocNumber.text ?? "",
            "lang_id":language,
            "scan_reminder_id":reminderDetail?.document_id ?? ""
        ]
        
        var imageArr = [imagesUpload]()
        for img in documentImages {
            imageArr.append(imagesUpload(key: "file", image: img, fileName: "reminder\(randomString(length:15))"))
        }
        
        print(imageArr)
        print(param)
        
        APIClient().UploadMultipleImageWithParametersRequest(WithApiName: API.editReminder.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {response, statescode in
            switch response {
            case .success(let obj):
                PrintLog(obj.msg)
                DispatchQueue.main.async {
                    
                    self.showCustumAlert(message:obj.msg ?? "", isBack: true)
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
        
    }
    
    

}
extension AddReminderVC: SambagDatePickerViewControllerDelegate {

    func sambagDatePickerDidSet(_ viewController: SambagDatePickerViewController, result: SambagDatePickerResult) {
       // txtDate.text = "\(result)"
        strExpDate = convertDateFormater("\(result)")
        txtDate.text = strExpDate
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func sambagDatePickerDidCancel(_ viewController: SambagDatePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    //For Log User Interaction
    func selectedCameraCell(picker: TLPhotosPickerViewController) {
        print("selectedCameraCell")
    }
    
    func selectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        print("selectedPhoto")
    }
    
    func deselectedPhoto(picker: TLPhotosPickerViewController, at: Int) {
        print("deselectedPhoto")
    }
    
    func selectedAlbum(picker: TLPhotosPickerViewController, title: String, at: Int) {
        print("selectedAlbum")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return documentImages.count
    }


    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ReminderPhotoCell", for: indexPath as IndexPath) as? ReminderPhotoCell else { return UICollectionViewCell() }
        cell.imgDoc.image = documentImages[indexPath.row]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 80)
    }
    
}



