/*
 * ViewController.swift
 * Created by Michael Michailidis on 01/04/2015.
 * http://blog.karmadust.com/
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


import UIKit
import KDCalendar


class Scan_Reminder: MasterVC {
    
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblView: UITableView!
    
    fileprivate let arrCells = [ReminderCell.self]
    let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let spacingBetweenCells: CGFloat = 5
    var slots = Int()
    var tableViewHeightValue: CGFloat = 0
    var reminderData = [ReminderData]()
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title5
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.viewNAv.btnOption.isHidden = true
        self.viewNAv.btnOption.setImage(UIImage(named:"barcode"), for: .normal)
        self.tblView.register(cellTypes: arrCells)
        let style = CalendarView.Style()
        style.cellShape                = .round
        
        style.cellColorDefault         = UIColor.clear
        
        //style.cellColorToday           = UIColor(red:1.00, green:0.84, blue:0.64, alpha:1.00)
        style.cellColorToday           = UIColor.red
        style.cellSelectedBorderColor  = UIColor(red:1.00, green:0.63, blue:0.24, alpha:1.00)
        style.cellEventColor           = UIColor.red
        style.headerTextColor          = UIColor.black
        
        style.cellTextColorDefault     = UIColor.black
        
        style.cellTextColorToday       = UIColor.white
        style.cellTextColorWeekend     = UIColor(red: 237/255, green: 103/255, blue: 73/255, alpha: 1.0)
        style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)
            
        style.headerBackgroundColor    = UIColor.white
        style.weekdaysBackgroundColor  = UIColor.white
        style.firstWeekday             = .sunday
        
        style.locale                   = Locale(identifier: "en_US")
        
        style.cellFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.headerFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        
        calendarView.style = style
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.direction = .horizontal
        calendarView.multipleSelectionEnable = false
        calendarView.marksWeekends = false
        
      
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
       
        self.getReminders()
        let today = Date()
        self.calendarView.setDisplayDate(today, animated: false)
        

        #if KDCALENDAR_EVENT_MANAGER_ENABLED
        self.calendarView.loadEvents() { error in
            if error != nil {
                let message = "The karmadust calender could not load system events. It is possibly a problem with permissions"
                let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        #endif
      
    }
    
    // MARK : Events
    @IBAction func addReminder(_ sender : UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "AddReminderVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
//    @IBAction func onValueChange(_ picker : UIDatePicker) {
//
//       // self.calendarView.setDisplayDate(picker.date, animated: true)
//    }
    
    @IBAction func goToPreviousMonth(_ sender: Any) {
        self.calendarView.goToPreviousMonth()
    }
    
    @IBAction func goToNextMonth(_ sender: Any) {
        self.calendarView.goToNextMonth()
        
    }
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    
    
    func getReminders() {
        
        self.reminderData.removeAll()
        let params = ["uid":userData?.id,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.myReminder.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ReminderListModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(object.result)
                        
                        if let remineData = object.result
                        {
                            self.reminderData = remineData
                            self.tableViewHeight.constant = CGFloat(150 * self.reminderData.count)
                            self.tblView.reloadData()
                        }
                        self.calendarView.events.removeAll()
                        for remind in self.reminderData
                        {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            if let dates = remind.document_end_date
                            {
                                let dt   = self.dateFromTimestamp(timeStamp: dates)
                                self.calendarView.addEvent("Roop", date: dt)
                               
                            }
                          
                        }
                       
                    }
                case .failure(let error):
                    debugPrint(error)
                }
                
            }else {
                
                DispatchQueue.main.async {
                    
                    self.calendarView.events.removeAll()
                    self.tblView.reloadData()
                    
                }
                
            }
            
        }

    }
    
    
}


extension Scan_Reminder: CalendarViewDataSource {
    func headerString(_ date: Date) -> String? {
        
        return nil
    }
    


      func startDate() -> Date {

          var dateComponents = DateComponents()
          dateComponents.month = 0
          let today = Date()
          let threeMonthsAgo = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
          return threeMonthsAgo
      }

      func endDate() -> Date {

          var dateComponents = DateComponents()
          dateComponents.month = 12
          let today = Date()
          let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
          return twoYearsFromNow

      }

}

extension Scan_Reminder: CalendarViewDelegate {
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date: Date)
    {
        
    }
    
    func calendar(_ calendar: CalendarView, canSelectDate date: Date) -> Bool
    {
        return false
    }
    
    func calendar(_ calendar: CalendarView, didDeselectDate date: Date)
    {
        
        
    }
    
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
           
           print("Did Select: \(date) with \(events.count) events")
           for event in events {
               print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
           }
        
       // self.calendarView.reloadData()
        
       }
       
       func calendar(_ calendar: CalendarView, didLongPressDate date : Date, withEvents events: [CalendarEvent]?) {
          
       }
    
}
extension Scan_Reminder: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        
       return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return reminderData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReminderCell", for: indexPath) as! ReminderCell
        cell.lblTitle.text = reminderData[indexPath.row].document_name
        cell.lblDate.text = reminderData[indexPath.row].end_date
        cell.lblDesc.text = reminderData[indexPath.row].document_number
        
        cell.btnEdit.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        
        cell.btnEdit.addTarget(self, action: #selector(self.editReminderClicked), for: .touchUpInside)
        cell.btnDelete.addTarget(self, action: #selector(self.deleteReminderClicked), for: .touchUpInside)
        
        print(cell.frame.size.height)

        return cell
        
    }
    
    @objc func editReminderClicked(_ sender: UIButton)
    {
        let vc = self.mainStory.instantiateViewController(withIdentifier: "AddReminderVC") as! AddReminderVC
        vc.strIsEdit = "1"
        vc.reminderDetail = reminderData[sender.tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func deleteReminderClicked(_ sender: UIButton)
    {
        self.AlertController(position: sender.tag)
    }
    
    func AlertController(position:Int)
    {
        let optionMenu = UIAlertController(title: "", message:NSLocalizedString(AlertMessage.deleteConfirm, comment: "") , preferredStyle: .actionSheet)
        let OK = UIAlertAction(title:NSLocalizedString(AlertMessage.OK, comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            
            self.deleteReminder(index:position)

            
        })
        let Cancel = UIAlertAction(title:NSLocalizedString(AlertMessage.cancel, comment: ""), style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })

        optionMenu.addAction(OK)
        optionMenu.addAction(Cancel)
        
        
        // 5
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func deleteReminder(index:Int) {
       
        let reminderID = self.reminderData[index].document_id
        
        APIClient().PostdataRequest(WithApiName: API.deleteReminder.rawValue, Params: ["scan_reminder_id":reminderID ?? "" ,"lang_id":language], isNeedLoader: true, objectType: LoginModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.popUpAlert(title: "", message: object.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [{_ in
//                            //self.navigationController?.popViewController(animated: true)
//                            self.reminderData.remove(at: index)
//                            self.tblView.reloadData()
                            self.getReminders()
                            
                            }
                       ])
                        
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                ShowAlert("Invalid Credentials", on: "Opps?", from: self)
            }
        }
        
    }
    
}
