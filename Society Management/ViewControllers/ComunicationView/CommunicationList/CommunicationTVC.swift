//
//  CommunicationTVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 25/06/21.
//

import UIKit

class CommunicationTVC: UITableViewCell {

    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var hideBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var commentBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var statusType: PaddingLabel!
    @IBOutlet weak var createByLbl: UILabel!
    @IBOutlet weak var msgTxtLbl: UILabel!
    @IBOutlet weak var totalCommentLbl: UILabel!
    
    @IBOutlet weak var hashLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func configur(rS:CommunicationList) {
        
        if let title = rS.title {
            self.titleLbl.text = title
        }
        
        if let time = rS.created_date , let name = rS.username {
            let date = TimeStampToDate(timeStamp: time)
            self.createByLbl.text = "\(name) • \(date)".uppercased()
        }
        
        guard let approval_status = rS.approval_status , approval_status == "2" else {
            self.msgTxtLbl.text = AlertMessage.communicationApproved
            self.buttonsView.isHidden = true
            self.totalCommentLbl.text = ""
            self.hashLbl.text = ""
            self.msgTxtLbl.textColor = .red
            return
        }
        
        self.buttonsView.isHidden = false
        self.hashLbl.text = "#"
        
        if let description = rS.description {
            self.msgTxtLbl.text = description
            self.msgTxtLbl.textColor = UIColor(named: AssetsColor.titleColor.rawValue)
        }
        
        if let comment = rS.comment_count {
            if comment == "0" ||  comment == "1" {
                self.totalCommentLbl.text = "\(comment) Comment"
            }else{
                self.totalCommentLbl.text = "\(comment) Comments"
            }
        }
    }
}
