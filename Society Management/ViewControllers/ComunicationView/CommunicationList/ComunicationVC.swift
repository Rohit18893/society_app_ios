//  ComunicationVC.swift
//  Society Management
//  Created by Jitendra Yadav on 25/06/21.


import UIKit

class ComunicationVC: MasterVC {
    
    @IBOutlet weak var noResultFound: UIView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNv: UIView!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var filterTextView: UIView!
    @IBOutlet weak var filterText: UIView!
    var refreshControl = UIRefreshControl()
    
    fileprivate let registerCells = [CommunicationTVC.self]
    let communicationViewModal = CommunicationViewModal()
    
    override func viewDidLoad() {
        self.className(ClassString: self)
        super.viewDidLoad()
        noResultFound.isHidden = true
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: registerCells)
        communicationViewModal.getCommunicationList(isNeedLoader: true, createBy: "0")
        communicationViewModal.vc = self
        NotificationCenter.default.addObserver(self, selector: #selector(houseHoldReload(notification:)), name: .communicationRefrash, object: nil)
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
        
    }
    
    @objc func refresh(_ refresh: UIRefreshControl) {
        communicationViewModal.getCommunicationList(isNeedLoader: false, createBy: "0")
        refresh.endRefreshing()
    }
    
    //MARK: House Hold Reload
    
    @objc func houseHoldReload(notification: NSNotification) {
        DispatchQueue.main.async {
            self.communicationViewModal.getCommunicationList(isNeedLoader: false, createBy: "0")
        }
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .householdReload, object: nil)
    }
    
    @IBAction func OnClickHideutton(_ sender: Any) {
        DispatchQueue.main.async {
            let vc  = HideCommunicationVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func onClickSearchButton(_ sender: Any) {
        
    }
    
    
    @IBAction func onClickFilter(_ sender: Any) {
        
    }
    
    @IBAction func onClickCloseFilter(_ sender: Any) {
        
    }
    
    
    @IBAction func onClickCreateByMe(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let createBy = sender.isSelected ? "1" : "0"
        communicationViewModal.getCommunicationList(isNeedLoader: true, createBy: createBy)
    }
    
    //MARK: Start Discussion Action.......
    
    @IBAction func onClickStartDiscussion(_ sender: Any) {
        DispatchQueue.main.async {
            let vc  = StartCommunicationVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        }
        
    }
    
    //MARK: Add Comment Action.......
    @objc func onClickComment(_ sender:UIButton) {
        let vc = addCommentCommunication()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.communicationList = communicationViewModal.communicationModel?.result?[sender.tag]
        vc.cid = communicationViewModal.communicationModel?.result?[sender.tag].cid ?? ""
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Share Action.......
    @objc func onClickShare(_ sender:UIButton) {
        
        
    }
    
    //MARK: Hide Action.......
    @objc func onClickHide(_ sender:UIButton) {
        AskConfirmation(title: "Do you want to hide this discussion?", message: "") { [weak self](result) in
               if result {
                if let id = self?.communicationViewModal.communicationModel?.result?[sender.tag].cid {
                    self?.communicationViewModal.HideCommunication(isNeedLoader: true, cid: id)
                }
           }
        }
    }
}

extension ComunicationVC: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return communicationViewModal.communicationModel?.result?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(with: CommunicationTVC.self, for: indexPath)
        
        if let rS = communicationViewModal.communicationModel?.result?[indexPath.row] {
            cell.configur(rS: rS)
        }
        cell.commentBtn.tag = indexPath.row
        cell.shareBtn.tag = indexPath.row
        cell.hideBtn.tag = indexPath.row
        cell.hideBtn.isSelected = false
        cell.shareBtn.isHidden = true
        cell.commentBtn.addTarget(self, action: #selector(onClickComment(_:)), for: .touchUpInside)
        cell.shareBtn.addTarget(self, action: #selector(onClickShare(_:)), for: .touchUpInside)
        cell.hideBtn.addTarget(self, action: #selector(onClickHide(_:)), for: .touchUpInside)
        return cell
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let rS = communicationViewModal.communicationModel?.result?[indexPath.row]
        
        guard let approval_status = rS?.approval_status , approval_status == "2" else {
            return ShowAlert(AlertMessage.communicationApproved, on: "", from: self)
        }
        
        let vc = DetailCommunicationVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        if let cid = communicationViewModal.communicationModel?.result?[indexPath.row].cid {
            vc.cid = cid
        }
        self.present(vc, animated: true, completion: nil)
    }
    
}
