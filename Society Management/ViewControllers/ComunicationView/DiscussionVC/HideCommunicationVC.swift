//
//  HideCommunicationVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/06/21.
//

import UIKit

class HideCommunicationVC: MasterVC {
    @IBOutlet weak var noFoundHideCommunication: UIView!
    @IBOutlet weak var tblView: UITableView!
    var showCommunication:(()->())? = nil
    fileprivate let arrCells = [CommunicationTVC.self]
    let communicationViewModal = CommunicationViewModal()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.noFoundHideCommunication.isHidden = true
        self.tblView.register(cellTypes: arrCells)
        communicationViewModal.getHideCommunicationList(isNeedLoader: true, createBy: "")
        communicationViewModal.hideVC = self
        self.className(ClassString: self)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
    }
    
    @objc func refresh(_ refresh: UIRefreshControl) {
        communicationViewModal.getHideCommunicationList(isNeedLoader: true, createBy: "")
        refresh.endRefreshing()
    }

    //MARK: Show Action.......
    @objc func onClickShow(_ sender:UIButton) {
        if let id = communicationViewModal.communicationModel?.result?[sender.tag].cid {
            communicationViewModal.ShowCommunication(isNeedLoader: true, cid: id)
        }
    }
    
}

extension HideCommunicationVC: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return communicationViewModal.communicationModel?.result?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tblView.dequeueReusableCell(with: CommunicationTVC.self, for: indexPath)
        
        if let rS = communicationViewModal.communicationModel?.result?[indexPath.row] {
            cell.configur(rS: rS)
        }
        cell.commentBtn.isHidden = true
        cell.shareBtn.isHidden = true
        cell.hideBtn.isHidden = false
        cell.hideBtn.isSelected = true
        cell.hideBtn.addTarget(self, action: #selector(onClickShow(_:)), for: .touchUpInside)
        
        return cell
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DetailCommunicationVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.isHide = true
        vc.cid = communicationViewModal.communicationModel?.result?[indexPath.row].cid ?? ""
        self.present(vc, animated: true, completion: nil)
    }
    
}
