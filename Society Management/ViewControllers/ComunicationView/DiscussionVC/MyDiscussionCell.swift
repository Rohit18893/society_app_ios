//
//  MyDiscussionCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/06/21.
//

import UIKit

class MyDiscussionCell: UITableViewCell {
    
    @IBOutlet weak var titlrLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imagesName: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var UserDetails: UILabel!
    @IBOutlet weak var groupBy: UILabel!
    @IBOutlet weak var totalComment: UILabel!
    var vcPrev = UIViewController()
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let numberOfItemsPerRow: CGFloat = 1
    let spacingBetweenCells: CGFloat = 10
    
    var selectImages:[String]? = nil {
        didSet {
            self.collectionView.reloadData()
        }
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        self.setCollectionView()
    }
    
    // MARK: set collectionView ............
    func setCollectionView(){
        DispatchQueue.main.async {
            self.collectionView.register(UINib(nibName: "AddImageCVC", bundle: nil), forCellWithReuseIdentifier: "AddImageCVC")
            self.collectionView.delegate = self
            self.collectionView.dataSource = self

        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(rS:CommunicationDeatilsResult) {
        
        self.titlrLbl.text = rS.title
        self.descriptionLbl.text = rS.description
                    
        if let unitnumber = rS.unitnumber,
           let towername =  rS.towername ,
           let username =  rS.username?.uppercased(),
           let time = rS.created_date {
           let date = TimeStampToDate(timeStamp: time)
           self.UserDetails.text = "Create By \(username) ( \(towername) - \(unitnumber) ) • \(date)"
        }
        
        if let comment = rS.comment_count {
            if comment == "0" ||  comment == "1" {
                self.totalComment.text = "\(comment) Comment"
            }else{
                self.totalComment.text = "\(comment) Comments"
            }
        }
        
        if let rs = rS.discussion_images , rs.count > 0 {
            self.selectImages = rs
            self.collectionView.isHidden = false
        }else{
            self.selectImages = []
            self.collectionView.isHidden = true
        }
        
        if let group = rS.group {
            self.groupBy.text = "Groups: \(group)"
        }
    }
}

extension MyDiscussionCell:UICollectionViewDataSource,UICollectionViewDelegate {
    // Configure the view for the selected state
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return selectImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImageCVC", for: indexPath) as! AddImageCVC
        
        if let img = selectImages?[indexPath.item] {
            cell.selectImage.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "No-image"))
        }
        
        cell.DeleteBTn.isHidden = true
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Cell = collectionView.cellForItem(at:indexPath) as! AddImageCVC
        DispatchQueue.main.async {
            let vc = ImageViewerVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.image = Cell.selectImage.image!
            self.vcPrev.present(vc, animated: false, completion: nil)
        }
        
    }
    
}

extension MyDiscussionCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
}
