//
//  DetailCommunicationVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 25/06/21.
//

import UIKit

class DetailCommunicationVC: MasterVC {

    @IBOutlet weak var addCommentView: UIView!
    @IBOutlet weak var addCommentBTN: GradientButton!
    @IBOutlet weak var tblView: UITableView!
    fileprivate let arrCells = [ MyDiscussionCell.self,CommentTableViewCell.self]
    let communicationViewModal = CommunicationViewModal()
    var isHide = false
    var cid  = String()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(cellTypes: arrCells)
        self.addCommentView.isHidden = isHide
        className(ClassString: self)
        self.getDeatils(isNeedLoader: true)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *) {
            tblView.refreshControl = refreshControl
        } else {
            tblView.addSubview(refreshControl)
        }
    }
    
    @objc func refresh(_ refresh: UIRefreshControl) {
        self.getDeatils(isNeedLoader:false)
        refresh.endRefreshing()
    }
    
    func getDeatils(isNeedLoader:Bool) {
        self.communicationViewModal.GetDiscussuionDerails(isNeedLoader: isNeedLoader, cid: cid) {
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        }
    }
    
    //MARK: Add comment Action.......
    
    @IBAction func onClickAddComment(_ sender: Any) {
        let vc = addCommentCommunication()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.communicationDeatilsResult = communicationViewModal.communcationDetailsModel?.result?.first
        vc.sussetFullcreate = {
            DispatchQueue.main.async {
                self.getDeatils(isNeedLoader:false)
            }
        }
        vc.cid = cid
        self.present(vc, animated: true, completion: nil)
    }
    
    //MARK: reply Action.......
    @objc func onClickReply(_ sender:UIButton) {
        let vc = AddReplyCommunicationVC()
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.communicationDeatilsResult = communicationViewModal.communcationDetailsModel?.result?.first
        vc.commentData = communicationViewModal.communcationDetailsModel?.result?.first?.comment_data?[sender.tag]
        vc.cid = cid
        vc.sussetFullcreate = {
            DispatchQueue.main.async {
                self.tblView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
                self.getDeatils(isNeedLoader:false)
            }
        }
        self.present(vc, animated: true, completion: nil)
    }

}

extension DetailCommunicationVC: UITableViewDataSource,UITableViewDelegate {
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return communicationViewModal.communcationDetailsModel?.result?.count ?? 0
        }else{
            return communicationViewModal.communcationDetailsModel?.result?.first?.comment_data?.count ?? 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                
        if indexPath.section == 0 {
            
            let cell = tblView.dequeueReusableCell(with: MyDiscussionCell.self, for: indexPath)
            if let rS = communicationViewModal.communcationDetailsModel?.result?[indexPath.row] {
                cell.vcPrev = self
                cell.config(rS: rS)
            }
            return cell
            
        }else {
            let cell = tblView.dequeueReusableCell(with: CommentTableViewCell.self, for: indexPath)
  
            let rS = communicationViewModal.communcationDetailsModel?.result?.first?.comment_data?[indexPath.row]
            cell.vcPrev = self
            cell.configur(result: rS)
            cell.collectionView.tag = indexPath.row
            cell.replyBtn.isHidden = isHide
            cell.replyBtn.tag = indexPath.row
            cell.replyBtn.addTarget(self, action: #selector(onClickReply(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


    }
    
}
//tableView.scrollToRow(at: indexPath, at: .top, animated: true)

