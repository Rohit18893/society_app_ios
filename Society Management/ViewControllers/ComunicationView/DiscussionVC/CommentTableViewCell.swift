//
//  CommentTableViewCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 26/06/21.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var replyBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var replyCommentlbl: PaddingLabel!
    @IBOutlet weak var titleLbl: UILabel!
    var vcPrev = UIViewController()
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let numberOfItemsPerRow: CGFloat = 1
    let spacingBetweenCells: CGFloat = 10
    
    var selectImages:[String]? = nil {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        self.setCollectionView()
    }
    
    // MARK: set collectionView ............
    func setCollectionView(){
        DispatchQueue.main.async {
            self.collectionView.register(UINib(nibName: "AddImageCVC", bundle: nil), forCellWithReuseIdentifier: "AddImageCVC")
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func configur(result:Comment_data?){
        
        if let unitnumber = result?.unitnumber, let towername =  result?.towername , let username =  result?.username?.uppercased() {
            
            if let iAmUser = result?.is_user , iAmUser == "1" {
                self.titleLbl.text = "You".uppercased()
                self.titleLbl.textColor = UIColor(named: AssetsColor.HeaderColor.rawValue)
            }else{
                self.titleLbl.text = "\(towername) - \(unitnumber) • \(username)"
                self.titleLbl.textColor = UIColor(named: AssetsColor.titleColor.rawValue)
            }
        }
       
        if let dt = result?.created_date {
            self.timeLbl.text = timeElapsed(date: Date(timeIntervalSince1970:TimeInterval(dt)))
        }
        
        if result?.reply?.count ?? 0 > 0 {
            self.replyCommentlbl.isHidden = false
            self.commentLbl.text = result?.reply
            self.replyCommentlbl.text = result?.comment
        }else{
            self.replyCommentlbl.isHidden = true
            self.commentLbl.text = result?.comment
            self.replyCommentlbl.text = ""
        }
        
        if let rs = result?.comment_images , rs.count > 0 {
            self.selectImages = rs
            self.collectionView.isHidden = false
        }else{
            self.selectImages = []
            self.collectionView.isHidden = true
        }
        

    }
}


extension CommentTableViewCell:UICollectionViewDataSource,UICollectionViewDelegate {
    // Configure the view for the selected state
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return selectImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImageCVC", for: indexPath) as! AddImageCVC
        
        if let img = selectImages?[indexPath.item] {
            cell.selectImage.sd_setImage(with: URL(string: img), placeholderImage: UIImage(named: "No-image"))
        }
        
        cell.DeleteBTn.isHidden = true
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let Cell = collectionView.cellForItem(at:indexPath) as! AddImageCVC
        DispatchQueue.main.async {
            let vc = ImageViewerVC()
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            vc.image = Cell.selectImage.image!
            self.vcPrev.present(vc, animated: false, completion: nil)
        }
        
    }
    
}

extension CommentTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
}
