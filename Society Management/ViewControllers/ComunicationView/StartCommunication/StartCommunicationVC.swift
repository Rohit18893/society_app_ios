//
//  StartCommunicationVC.swift
//  Society Management
//
//  Created by Jitendra Yadav on 25/06/21.
//

import UIKit
import MaterialComponents
class StartCommunicationVC: MasterVC,UITextViewDelegate, ImagePickerDelegate {
    
    @IBOutlet weak var tagView: TagListView!
    
    @IBOutlet weak var discriptionLimit: UILabel!
    @IBOutlet weak var titleLimit: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleTV: UITextView!
    @IBOutlet weak var discriptionTV: UITextView!
    @IBOutlet weak var selectGroupTF: MDCOutlinedTextField!
    var sussetFullcreate:(()->())? = nil
    
    var selectTypeID = String()
    
    let communicationViewModal = CommunicationViewModal()
    
    let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let numberOfItemsPerRow: CGFloat = 1
    let spacingBetweenCells: CGFloat = 10
    
    var imagePicker: ImagePicker?
    
    var charCount = 0
    let maxLength = 140
    
    var charCount1 = 0
    let maxLength1 = 1000
    
    var selectImages:[UIImage]? = nil {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setTextFieldTheme(txtField: selectGroupTF, labelText: selectGroupTF.placeholder ?? "", allowEditing: false)
        // Do any additional setup after loading the view.
        self.setTextView(textView: titleTV, placeHolder: AlertMessage.writeTitle)
        self.setTextView(textView: discriptionTV, placeHolder: AlertMessage.writeDetails)
        self.setCollectionView()
        self.communicationViewModal.getFilter()
        self.className(ClassString: self)
        
    }
    
    // MARK: set collectionView ............
    func setCollectionView(){
        DispatchQueue.main.async {
            self.imagePicker = ImagePicker(presentationController: self, delegate: self)
            self.collectionView.register(UINib(nibName: "AddImageCVC", bundle: nil), forCellWithReuseIdentifier: "AddImageCVC")
            self.collectionView.isHidden = true
            self.selectImages = [UIImage]()
        }
    }
    
    func setTextView(textView:UITextView,placeHolder:String){
        textView.text = placeHolder
        textView.textColor = UIColor.lightGray
        textView.delegate = self
    }
    
    
    @IBAction func onClickSelectGroup(_ sender: Any) {
        if  let name = communicationViewModal.communicationFilter?.result?.map({$0.name }) as? [String] ,
            let id = communicationViewModal.communicationFilter?.result?.map({$0.id}) as? [String] {
            CustomPickerView.show(items:name , itemIds: id ,selectedValue:self.selectGroupTF.text, doneBottonCompletion: { (item, index) in
                self.selectGroupTF.text = item
                if let index = index {
                    self.selectTypeID = "\(index)"
                }
            }, didSelectCompletion: { (item, index) in
                self.selectGroupTF.text = item
                if let index = index {
                    self.selectTypeID = "\(index)"
                }
            }) { (item, index) in

            }
        }
    }
    
    @IBAction func onClickAttachFile(_ sender: Any) {
        if let img = self.imagePicker , self.selectImages?.count ?? 0 < 4 {
            img.present(from: self.view)
        }else{
            ShowAlert(AlertMessage.minimumImage, on: "", from: self)
        }
    }
    
    func didSelect(image: UIImage?, tag: Int) {
        if let image = image {
            self.selectImages?.append(image)
            self.collectionView.isHidden = false
        }
    }
    
    @IBAction func onClickSubmit(_ sender: Any) {
                
        guard let title = titleTV.text, title.count > 0  , title.lowercased() != AlertMessage.writeTitle else {
            return ShowAlert(AlertMessage.discussionTitle, on: "", from: self)
        }
        guard let description = discriptionTV.text, description.count > 0 , description.lowercased() != AlertMessage.writeDetails else {
            return ShowAlert(AlertMessage.discussionDetail, on: "", from: self)
        }
        
        guard let selectGroup = selectGroupTF.text, selectGroup.count > 0 else {
            return ShowAlert(AlertMessage.discussionGruop, on:"", from: self)
        }
        
        let param = ["uid":userData?.id ?? "",
                     "towerid":userData?.towerid ?? "",
                     "unitnumber":userData?.unitnumber ?? "",
                     "participant_id":selectTypeID,
                     "title":title,
                     "description":description,
                     "lang_id":language
        ]
        var imageArr = [imagesUpload]()
        
        for img in selectImages ?? [] {
            imageArr.append(imagesUpload(key: "file", image: img, fileName: "Discussion\(randomString(length:15))"))
        }
        
        print(imageArr)
        print(param)
        
        APIClient().UploadMultipleImageWithParametersRequest(WithApiName: API.communicationStart.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {response, statescode in
            switch response {
            case .success(let obj):
                PrintLog(obj.msg)
                DispatchQueue.main.async {
                    self.popUpAlert(title: "", message: obj.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [{[weak self] _ in
                        guard let self = self else {return}
                        NotificationCenter.default.post(name: .communicationRefrash, object: nil)
                        self.dismiss(animated: true, completion: nil)
                      }
                   ])
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    
    
    
    
    @objc func onClickDeleteImage(_ sender:UIButton) {
        self.selectImages?.remove(at: sender.tag)
        if self.selectImages?.count == 0 {
            self.collectionView.isHidden = true
        }
    }
}



// MARK: TextView Delegate ............

extension StartCommunicationVC {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == titleTV {
            if  titleTV.textColor == UIColor.lightGray {
                titleTV.text = ""
                titleTV.textColor = UIColor.black
            }
        }else{
            if  discriptionTV.textColor == UIColor.lightGray {
                discriptionTV.text = ""
                discriptionTV.textColor = UIColor.black
            }
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView == titleTV {
            if  titleTV.text == "" {
                titleTV.text = AlertMessage.writeTitle
                titleTV.textColor = UIColor.lightGray
            }
        }else{
            if discriptionTV.text == "" {
                discriptionTV.text = AlertMessage.writeDetails
                discriptionTV.textColor = UIColor.lightGray
            }
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView == titleTV {
            if text == "" {
                if titleTV.text.count == 0 {
                    charCount = 0
                    titleLimit.text = String(format: "%i", maxLength - charCount)
                    return false
                }
                charCount = (titleTV.text.count - 1)
                titleLimit.text = String(format: "%i", maxLength - charCount)
                return true
            } else {
                charCount = (titleTV.text.count + 1)
                titleLimit.text = String(format: "%i", maxLength - charCount)
                if charCount >= maxLength + 1  {
                    charCount = maxLength
                    titleLimit.text = String(format: "%i", maxLength - charCount)
                    return false;
                }
            }
            return true
        }else{
            if text == "" {
                if discriptionTV.text.count == 0 {
                    charCount1 = 0
                    discriptionLimit.text = String(format: "%i", maxLength1 - charCount1)
                    return false
                }
                charCount1 = (discriptionTV.text.count - 1)
                discriptionLimit.text = String(format: "%i", maxLength1 - charCount1)
                return true
            } else {
                charCount1 = (discriptionTV.text.count + 1)
                discriptionLimit.text = String(format: "%i", maxLength1 - charCount1)
                if charCount1 >= maxLength1 + 1  {
                    charCount1 = maxLength1
                    discriptionLimit.text = String(format: "%i", maxLength1 - charCount1)
                    return false;
                }
            }
            return true
        }
    }
}


extension StartCommunicationVC:UICollectionViewDataSource,UICollectionViewDelegate {
    // Configure the view for the selected state
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return selectImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImageCVC", for: indexPath) as! AddImageCVC
        cell.selectImage.image = selectImages?[indexPath.item]
        cell.DeleteBTn.tag = indexPath.item
        cell.DeleteBTn.addTarget(self, action: #selector(onClickDeleteImage), for: .touchUpInside)
        return cell
        
    }
    
   
    
}

extension StartCommunicationVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
}



