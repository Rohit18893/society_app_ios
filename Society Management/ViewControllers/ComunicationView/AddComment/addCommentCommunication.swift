//
//  addCommentCommunication.swift
//  Society Management
//
//  Created by Jitendra Yadav on 25/06/21.
//

import UIKit

class addCommentCommunication: MasterVC,UITextViewDelegate, ImagePickerDelegate {
    
    @IBOutlet weak var textLimit: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var createBy: UILabel!
    @IBOutlet weak var discriptionTV: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    var sussetFullcreate:(()->())? = nil
    
    var cid  = String()
    let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    let numberOfItemsPerRow: CGFloat = 1
    let spacingBetweenCells: CGFloat = 10
    
    var imagePicker: ImagePicker?
    var communicationDeatilsResult:CommunicationDeatilsResult?
    var communicationList: CommunicationList?
    
    var charCount1 = 0
    let maxLength1 = 1000
    
    var selectImages:[UIImage]? = nil {
        didSet {
            self.collectionView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.className(ClassString: self)
        // Do any additional setup after loading the view.
       
        self.setTextView(textView: discriptionTV, placeHolder: AlertMessage.writeDetails)
        self.setCollectionView()
        
        self.titleLbl.text = communicationDeatilsResult?.title ?? communicationList?.title
        
        if let username =  communicationDeatilsResult?.username?.uppercased() ?? communicationList?.username?.uppercased(),
           let time = communicationDeatilsResult?.created_date  ?? communicationList?.created_date {
           let date = TimeStampToDate(timeStamp: time)
           self.createBy.text = "Create By \(username) • \(date)"
        }

    }
    
    // MARK: set collectionView ............
    func setCollectionView(){
        DispatchQueue.main.async {
            self.imagePicker = ImagePicker(presentationController: self, delegate: self)
            self.collectionView.register(UINib(nibName: "AddImageCVC", bundle: nil), forCellWithReuseIdentifier: "AddImageCVC")
            self.collectionView.isHidden = true
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.selectImages = [UIImage]()
        }
    }
    
    func setTextView(textView:UITextView,placeHolder:String){
        textView.text = placeHolder
        textView.textColor = UIColor.lightGray
        textView.delegate = self
    }

    @IBAction func onClickAttachFile(_ sender: Any) {
        if let img = self.imagePicker , self.selectImages?.count ?? 0 < 4 {
            img.present(from: self.view)
        }else{
            ShowAlert(AlertMessage.minimumImage, on: "", from: self)
        }
    }
    
    func didSelect(image: UIImage?, tag: Int) {
        if let image = image {
            self.selectImages?.append(image)
            self.collectionView.isHidden = false
        }
    }
    
    @IBAction func onClickAddComment(_ sender: Any) {
        
        guard let description = discriptionTV.text, description.count > 0 , description.lowercased() != AlertMessage.writeDetails else {
            return ShowAlert(AlertMessage.discussionDetail, on: "", from: self)
        }

        let param = ["uid":userData?.id ?? "",
                     "towerid":userData?.towerid ?? "",
                     "unitnumber":userData?.unitnumber ?? "",
                     "communication_id":cid,
                     "comment":description,
                     "lang_id":language,
        ]
        
        print(param)
        
        var imageArr = [imagesUpload]()
        for img in selectImages ?? [] {
            imageArr.append(imagesUpload(key: "file", image: img, fileName: "AddComment\(randomString(length:8))"))
        }
                
        APIClient().UploadMultipleImageWithParametersRequest(WithApiName: API.addCommnet_reply.rawValue, Params: param, Setimages: imageArr, isNeedLoader: true, objectType: StatusModel.self) {response, statescode in
            switch response {
            case .success(let obj):
                PrintLog(obj.msg)
                DispatchQueue.main.async {
                    self.popUpAlert(title: "", message: obj.msg ?? "", actiontitle: [AlertMessage.OK], actionstyle: [.default], action: [{[weak self] _ in
                        guard let self = self else {return}
                        if let ind = self.sussetFullcreate {
                            ind()
                            NotificationCenter.default.post(name: .communicationRefrash, object: nil)
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                    ])
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    
    @objc func onClickDeleteImage(_ sender:UIButton) {
        self.selectImages?.remove(at: sender.tag)
        if self.selectImages?.count == 0 {
            self.collectionView.isHidden = true
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

// MARK: TextView Delegate ............

extension addCommentCommunication {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if  textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = AlertMessage.writeDetails.capitalized
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "" {
            if textView.text.count == 0 {
                charCount1 = 0
                textLimit.text = String(format: "%i", maxLength1 - charCount1)
                return false
            }
            charCount1 = (textView.text.count - 1)
            textLimit.text = String(format: "%i", maxLength1 - charCount1)
            return true
        } else {
            charCount1 = (textView.text.count + 1)
            textLimit.text = String(format: "%i", maxLength1 - charCount1)
            if charCount1 >= maxLength1 + 1  {
                charCount1 = maxLength1
                textLimit.text = String(format: "%i", maxLength1 - charCount1)
                return false;
            }
        }
        return true
      }
}

extension addCommentCommunication:UICollectionViewDataSource,UICollectionViewDelegate {
    // Configure the view for the selected state
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return selectImages?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddImageCVC", for: indexPath) as! AddImageCVC
        cell.selectImage.image = selectImages?[indexPath.item]
        cell.DeleteBTn.tag = indexPath.item
        cell.DeleteBTn.addTarget(self, action: #selector(onClickDeleteImage), for: .touchUpInside)
        return cell
       
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.selectImages?.remove(at: indexPath.row)
//    }
        
}

extension addCommentCommunication: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
}
