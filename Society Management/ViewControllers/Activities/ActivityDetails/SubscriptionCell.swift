//  SubscriptionCell.swift
//  Society Management
//  Created by ROOP KISHOR on 23/06/2021.


import UIKit

class SubscriptionCell: UITableViewCell {
    @IBOutlet weak var lblOffer: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
