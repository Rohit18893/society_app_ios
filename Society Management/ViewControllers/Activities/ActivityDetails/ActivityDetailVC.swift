//  ActivityDetailVC.swift
//  Society Management
//  Created by ROOP KISHOR on 21/06/2021.


import UIKit

class ActivityDetailVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var lblStatus: PaddingLabel!
    
    var strActivityId = String()
    var planID = String()
    var activityDetailData = [ActDetailData]()
    fileprivate let arrCells = [WorkoutTblCell.self,ActivityDetailTblCell.self,PhotoCell.self,SubscriptionCell.self]
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title13
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(activityImageChange(notification:)), name: .ActivityImageChanged, object: nil)
        
    }
    
    
    @objc func activityImageChange(notification: NSNotification)
    {
        if let img = notification.object as? String {
            
            let indexPath = IndexPath(row: 0, section: 0)
            let cell = tblView.cellForRow(at: indexPath) as! WorkoutTblCell
            cell.imgHeader.sd_setImage(with: URL(string:img), placeholderImage:UIImage(named: "No-image"))
            
        }
      
    }
    
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .ActivityImageChanged, object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getActivitiesDetails()
        
    }
    
    @IBAction func timeSlotsClicked(_ sender: UIButton)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func bookNowClicked(_ sender: UIButton)
    {
        if let timeslots = activityDetailData[0].time_slot
        {
            let array = timeslots.components(separatedBy: ",")
            let vc = self.mainStory.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            vc.timeSlotArr = array
            vc.activity_id = strActivityId
            vc.planID = self.planID
            vc.isFacality = "2"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else
        {
            let vc = self.mainStory.instantiateViewController(withIdentifier: "CalendarVC") as! CalendarVC
            vc.activity_id = strActivityId
            vc.planID = self.planID
            vc.isFacality = "2"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getActivitiesDetails() {
        
        let params = ["lang_id":language,"activity_id":strActivityId]
        APIClient().PostdataRequest(WithApiName: API.activitiesDetails.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ActDetailModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.activityDetailData = (object.result)!
                        self.tblView.reloadData()
                       
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else {
                
                
            }
        }
        
    }

}
extension ActivityDetailVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        var Count = 0
        if activityDetailData.count > 0
        {
           
            if let images = activityDetailData[0].activities_images {
                
                if images.count > 0 {
                    
                    Count = 3
                    
                } else {
                    
                    Count = 2
                }
                
            }
            
        }
        return Count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if activityDetailData.count > 0
        {
           
            if let images = activityDetailData[0].activities_images {
                
                if images.count > 0 {
                    
                    if indexPath.row == 0 {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutTblCell", for: indexPath) as! WorkoutTblCell
                        cell.viewHeight.constant = screenWidth * 0.45
                        let url = URL.init(string:images[indexPath.row] )
                        cell.imgHeader.sd_setImage(with: url , placeholderImage:UIImage(named: "No-image"))
                        return cell
                        
                    }
                    else if indexPath.row == 1
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
                        if activityDetailData.count > 0 {
                            
                            if let images = activityDetailData[0].activities_images {
                                cell.allImages = images
                                cell.collectionView.reloadData()
                            }
                            
                        }
                        
                        return cell
                    }
                    else
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityDetailTblCell", for: indexPath) as! ActivityDetailTblCell
                        if self.activityDetailData.count > 0 {
                            
                            cell.lblTitle.text = activityDetailData[0].name
                            cell.lblDesc.text = activityDetailData[0].details
                            lblStatus.text = activityDetailData[0].service_status
                            let rating = activityDetailData[0].rating ?? "0"
                            cell.starRating.rating  = NSString(string: rating).doubleValue
                            cell.starRating.text? = ""
                            cell.starRating.isUserInteractionEnabled = false
                            
                            if let plans = activityDetailData[0].plans {
                                
                                self.planID = String(plans[0].plan_id ?? 1)
                                cell.lblPrice.text = "\(plans[0].plan_price ?? "") AED"
                                cell.lblPriceDetail.text = plans[0].plan_description
                                cell.lblDiscount.text = "\(plans[0].plan_discount ?? "") % Discount"
                                if plans[0].plan_discount == "" {
                                    
                                    cell.lblDiscount.isHidden = true
                                }
                                else
                                {
                                    cell.lblDiscount.isHidden = false
                                }
                                
                                
                            }
                            
                            
                            
                            
                        }
                        
                        return cell
                    }
                    
                } else {
                    
                    if indexPath.row == 0 {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutTblCell", for: indexPath) as! WorkoutTblCell
                        cell.viewHeight.constant = screenWidth * 0.45
                        cell.imgHeader.image = UIImage(named: "No-image")
                        return cell
                        
                    }
                    else
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityDetailTblCell", for: indexPath) as! ActivityDetailTblCell
                        
                        if self.activityDetailData.count > 0 {
                            
                            cell.lblTitle.text = activityDetailData[0].name
                            cell.lblDesc.text = activityDetailData[0].details
                            lblStatus.text = activityDetailData[0].service_status
                            let rating = activityDetailData[0].rating ?? "0"
                            cell.starRating.rating  = NSString(string: rating).doubleValue
                            cell.starRating.text? = ""
                            cell.starRating.isUserInteractionEnabled = false
                            if let plans = activityDetailData[0].plans {
                                
                                self.planID = String(plans[0].plan_id ?? 1)
                                cell.lblPrice.text = "\(plans[0].plan_price ?? "") AED"
                                cell.lblPriceDetail.text = plans[0].plan_description
                                cell.lblDiscount.text = "\(plans[0].plan_discount ?? "") % Discount"
                                if plans[0].plan_discount == ""
                                {
                                    cell.lblDiscount.isHidden = true
                                }
                                else
                                {
                                    cell.lblDiscount.isHidden = false
                                }
                                
                                
                                
                            }
                            
                        }
                        
                        return cell
                        
                    }
                }
                
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutTblCell", for: indexPath) as! WorkoutTblCell
                cell.viewHeight.constant = screenWidth * 0.45
                return cell
            }
            
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkoutTblCell", for: indexPath) as! WorkoutTblCell
            cell.viewHeight.constant = screenWidth * 0.45
            return cell
        }
        
        
       
    }
    
    
}

