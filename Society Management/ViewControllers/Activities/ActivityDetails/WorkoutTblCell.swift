//  WorkoutTblCell.swift
//  Society Management
//  Created by ROOP KISHOR on 22/06/2021.


import UIKit

class WorkoutTblCell: UITableViewCell {
    
    @IBOutlet weak var shapeView: UIView!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var imgHeader: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
