//  ActivityDetailTblCell.swift
//  Society Management
//  Created by ROOP KISHOR on 21/06/2021.


import UIKit
import Cosmos

class ActivityDetailTblCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceDetail: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var starRating: CosmosView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
}

