//  ActivitiesCollCell.swift
//  Society Management
//  Created by ROOP KISHOR on 21/06/2021.


import UIKit

class ActivitiesCollCell: UICollectionViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var viewBack: UIView!

    override func awakeFromNib()
    {
        self.className(ClassString: self)
        super.awakeFromNib()
        
    }

}
