//  ActivitiesVC.swift
//  Society Management
//  Created by ROOP KISHOR on 21/06/2021.


import UIKit

class ActivitiesVC: UIViewController ,UICollectionViewDataSource, UICollectionViewDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var viewNAv: Navigationbar!
    var activityData = [ActivityModal]()
    
    let sectionInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
    let spacingBetweenCells: CGFloat = 15
    let numberOfItemsPerRow: CGFloat = 2
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title13
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.collectionView.register(UINib(nibName: "ActivitiesCollCell", bundle: nil), forCellWithReuseIdentifier: "ActivitiesCollCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        lblNoData.isHidden = false
        
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.getActivities()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return activityData.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ActivitiesCollCell", for: indexPath as IndexPath) as? ActivitiesCollCell else { return UICollectionViewCell() }
        
        cell.lblName.text = activityData[indexPath.row].name
        let url = URL.init(string:activityData[indexPath.row].iconimage ?? "")
        cell.img.sd_setImage(with: url , placeholderImage:UIImage(named: "gym"))
        cell.viewBack.layer.shadowColor = UIColor(red: 87/255.0, green: 98/255.0, blue: 212/255.0, alpha: 0.45).cgColor
        cell.viewBack.layer.shadowOffset = CGSize(width:3.0, height: 3.0)
        cell.viewBack.layer.shadowOpacity = 0.3
        cell.viewBack.layer.shadowRadius = 2
        return cell
        
    }
   
    func getActivities() {
        
        let params = ["lang_id":language,"towerid":userData?.towerid]
        APIClient().PostdataRequest(WithApiName: API.activitiesCategory.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ActivityData.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        self.lblNoData.isHidden = true
                        self.activityData = (object.result)!
                        self.collectionView.reloadData()
                       
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                DispatchQueue.main.async {
                    
                    self.lblNoData.isHidden = false
                }
                
            }
        }
        
    }
    
}

extension ActivitiesVC: UICollectionViewDelegateFlowLayout {
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//
//        return CGSize(width: (screenWidth - 20)/2 , height: (screenWidth - 20)/2)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalSpacing = (2 * sectionInsets.left) + ((numberOfItemsPerRow - 1) * spacingBetweenCells)
        //Amount of total spacing in a row
        if let collection = self.collectionView {
            let width = (collection.bounds.width - totalSpacing)/numberOfItemsPerRow
            return CGSize(width: width, height: width)
        } else {
            return CGSize(width: 0, height: 0)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat{
//        return 10
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        
//        let sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right:0)
//        return sectionInset
//        
//    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingBetweenCells
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "ActivityDetailVC") as! ActivityDetailVC
        nextViewController.strActivityId = activityData[indexPath.row].id ?? ""
        self.navigationController?.pushViewController(nextViewController, animated: true)
      
        
    }
}
