//  emergenctNoVC.swift
//  Society Management
//  Created by Jitendra Yadav on 25/04/21.


import UIKit

class emergenctNoVC: MasterVC {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var viewNAv: Navigationbar!
    fileprivate let arrCells = [emergenctNoCell.self]
    var emergencyData = [EmergencyData]()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.className(ClassString: self)
        self.viewNAv.lbltitle.text = AlertMessage.title30
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.tblView.register(cellTypes: arrCells)
        self.tblView.delegate = self
        self.tblView.dataSource = self
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.tintColor = .red
        if #available(iOS 10.0, *)
        {
            tblView.refreshControl = refreshControl
        }
        else
        {
            tblView.addSubview(refreshControl)
        }
        self.getEmergencyNumbers()
        
    }
    
    
    @objc func refresh(_ refresh: UIRefreshControl)
    {
        
        self.getEmergencyNumbers()
        refresh.endRefreshing()
        
    }

}

extension emergenctNoVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.emergencyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "emergenctNoCell", for: indexPath) as! emergenctNoCell
        cell.lblName.text = emergencyData[indexPath.row].name
        cell.lblNumber.text = emergencyData[indexPath.row].number
        cell.lblName.text = emergencyData[indexPath.row].name
        cell.btnCall.tag  = indexPath.row
        cell.btnCall.addTarget(self, action: #selector(self.callEmergencyNumber), for: .touchUpInside)
        cell.img.sd_setImage(with: URL(string: emergencyData[indexPath.row].destination_image ?? ""), placeholderImage:#imageLiteral(resourceName: "hospital"))
        return cell
    }
    
    
    @objc func callEmergencyNumber(_ sender: UIButton)
    {
        if let phone = emergencyData[sender.tag].number
        {
            self.callNumber(phoneNumber: phone)
        }
        
    }
    
    
    func getEmergencyNumbers() {
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.emergencycontacts.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: EmergencyDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        PrintLog(self)
                        PrintLog(object.data)
                        self.emergencyData = (object.data)!
                        self.tblView.reloadData()
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
                DispatchQueue.main.async
                {
                    self.emergencyData.removeAll()
                    self.tblView.reloadData()
                }
                
            }
        }

    }
    
    
}
