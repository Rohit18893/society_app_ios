//
//  emergenctNoCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 25/04/21.
//

import UIKit

class emergenctNoCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblNumber: UILabel!
    
    @IBOutlet weak var btnCall: UIButton!
    
    @IBOutlet weak var img: UIImageView!{
        didSet {
            img.contentMode = .scaleToFill
            img.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            img.borderWidth = 1
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.className(ClassString: self)
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
