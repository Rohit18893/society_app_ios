//  ManagementCommitteeVC.swift
//  Society Management
//  Created by ROOP KISHOR on 04/05/2021.


import UIKit

class ManagementCommitteeVC: MasterVC {
    
    @IBOutlet weak var viewNAv: Navigationbar!
    @IBOutlet weak var tblView: UITableView!
    var managmentData = [ManagmentData]()
    

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.viewNAv.lbltitle.text = AlertMessage.title16
        self.viewNAv.vc = self
        self.viewNAv.btnBack.isHidden = false
        self.getManagmentCommittee()
        self.className(ClassString: self)
    }
    
    
    

}

extension ManagementCommitteeVC: UITableViewDataSource,UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return managmentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommitteeTableCell", for: indexPath) as! CommitteeTableCell
        cell.lblName.text = String(format: "%@ %@", managmentData[indexPath.row].fname!.capitalized,managmentData[indexPath.row].lname!.capitalized)
        cell.lblNumber.text = managmentData[indexPath.row].mobile
        cell.lblDesignation.text = managmentData[indexPath.row].member_type?.capitalized
        cell.btnCall.tag  = indexPath.row
        cell.btnCall.addTarget(self, action: #selector(self.callMember), for: .touchUpInside)
        cell.img.sd_setImage(with: URL(string: managmentData[indexPath.row].image ?? ""), placeholderImage:#imageLiteral(resourceName: "profile_icon"))
        cell.img.layer.cornerRadius = cell.img.frame.height/2
        return cell
        
    }
    
    
    @objc func callMember(_ sender: UIButton)
    {
        if let phone = managmentData[sender.tag].mobile {
            self.callNumber(phoneNumber: phone)
        }

       
    }
    
    func getManagmentCommittee() {
        
        let params = ["towerid":userData?.towerid,"lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.managementcommitee.rawValue, Params: params as [String : Any], isNeedLoader: true, objectType: ManagmentDataModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async { [self] in
                        
                        PrintLog(self)
                        PrintLog(object.data)
                        self.managmentData = object.data!
                        self.tblView.reloadData()
                       
                        
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
                
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }
    
}
