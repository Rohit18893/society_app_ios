//
//  RecentHelperCell.swift
//  Society Management
//
//  Created by Jitendra Yadav on 15/08/21.
//

import UIKit

class RecentHelperCell: UITableViewCell {
    
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var allButtonsView: UIView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var InTime: UILabel!
    @IBOutlet weak var insideIcon: UIImageView!
    @IBOutlet weak var insideTag: PaddingLabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var helperName: UILabel!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var deailHelperImage: Imageradius!
    @IBOutlet weak var star: UIImageView!
    
    var onClickButton:((String,Int)->())? = nil
    
    var recentActivityData:RecentActivityData? = nil {
        didSet{
            self.setData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.statusLbl.adjustsFontSizeToFitWidth = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func secondButton(_ sender: UIButton) {
        if let onClickButton = onClickButton {
            onClickButton(sender.titleLabel?.text ?? "",sender.tag)
        }
    }
    
    @IBAction func CallButton(_ sender: UIButton) {
        if let onClickButton = onClickButton {
            onClickButton("call",sender.tag)
        }
    }
    
    func setData() {
        
        if let icon = recentActivityData?.icon , icon.count > 0 {
            deailHelperImage.sd_setImage(with: URL(string: icon), placeholderImage: #imageLiteral(resourceName: "my_daily"))
        }else{
            deailHelperImage.image = #imageLiteral(resourceName: "my_daily")
        }
        
        self.helperName.text = recentActivityData?.title?.count ?? 0 > 0 ? recentActivityData?.title?.capitalized : ""
        
        if let ratings = recentActivityData?.rating, ratings == "0" || ratings == "0.0" {
            self.rating.text = ""
            self.star.isHidden = true
        }else{
            self.rating.text = recentActivityData?.rating?.count  ?? 0 > 0 ? recentActivityData?.rating:"0"
            self.star.isHidden = false
        }
        
        if let setTime = recentActivityData?.slot_start,let service_name = recentActivityData?.service_name {
            self.InTime.text = "\(getTimeOnly(Int(setTime) ?? 0)) • \(service_name)".uppercased()
        }
        
        self.insideTag.text = recentActivityData?.tag?.uppercased()
        allButtonsView.isHidden = true
        callButton.isHidden = false
        
        if recentActivityData?.tag?.uppercased() == AlertMessage.Inside.uppercased() {
            insideIcon.isHidden = false
            insideIcon.image = #imageLiteral(resourceName: "entry_grey")
            statusIcon.image = UIImage(systemName: "text.bubble")
            self.statusLbl.text  = "\(self.helperName.text?.capitalized ?? "") \(AlertMessage.checkIn)"
          ///  callButton.isHidden = false
            
        } else if recentActivityData?.tag?.uppercased() == AlertMessage.left.uppercased() {
            insideIcon.isHidden = false
            insideIcon.image = #imageLiteral(resourceName: "logout_grey")
            statusIcon.image = UIImage(systemName: "text.bubble")
            self.statusLbl.text  = "\(self.helperName.text?.capitalized ?? "") \(AlertMessage.checkOut)"
           /// allButtonsView.isHidden = true
            
        } else if recentActivityData?.tag?.uppercased() == AlertMessage.cancel.uppercased() {
            insideIcon.isHidden = true
            statusIcon.image = UIImage(systemName: "person")
            self.statusLbl.text  = recentActivityData?.added_by
           /// allButtonsView.isHidden = true
            ///callButton.isHidden = false
        }else{
            insideIcon.isHidden = true
            statusIcon.image = UIImage(systemName: "person")
            self.statusLbl.text  = recentActivityData?.added_by
            ///allButtonsView.isHidden = true
            ///callButton.isHidden = false
        }
        
        self.insideTag.backgroundColor = statusColor(self.insideTag.text?.lowercased() ?? "")
    }
}


