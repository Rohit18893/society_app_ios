//  RecentActivityCell.swift
//  Society Management
//  Created by Cricket Mazza on 11/08/21.


import UIKit

class CellRecentActivity: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var statustag: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var code: UILabel!
    @IBOutlet weak var vailidTime: UILabel!
    @IBOutlet weak var messageType: UILabel!
    @IBOutlet weak var inviteBy: UILabel!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    @IBOutlet weak var thiredButton: UIButton!
    @IBOutlet weak var profileIcon: UIImageView!
    
    @IBOutlet weak var vaildIcon: UIImageView!
    @IBOutlet weak var msgIcon: UIImageView!
    @IBOutlet weak var companyIcon: UIImageView!
    @IBOutlet weak var companyname: UILabel!
    
    @IBOutlet weak var vaildView: UIView!
    @IBOutlet weak var messageTypeView: UIView!
    @IBOutlet weak var inviteByView: UIView!
    @IBOutlet weak var allButtonView: UIStackView!
    
    var onClickButton:((String,Int)->())? = nil
    var recentActivityData:RecentActivityData? = nil {
        didSet{
            
            self.setData()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        time.adjustsFontSizeToFitWidth = true
        statustag.adjustsFontSizeToFitWidth = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData() {
        
        inviteByView.isHidden = false
        companyIcon.isHidden = true
        companyname.text = ""
        code.text =  ""
        self.title.text = recentActivityData?.title?.count ?? 0 > 0 ? recentActivityData?.title : ""
        self.inviteBy.text  = recentActivityData?.added_by
        self.statustag.text = recentActivityData?.tag?.uppercased()
        self.statustag.backgroundColor = statusColor(self.statustag.text?.lowercased() ?? "")
        
        //MARK: Cab
        if recentActivityData?.activity_id == "1" {
            self.Cab_Delivery_visitingHelp_Kid(#imageLiteral(resourceName: "cab"), "1")
            
            //MARK: Delivery
        } else if recentActivityData?.activity_id == "2" {
            self.Cab_Delivery_visitingHelp_Kid(#imageLiteral(resourceName: "delivery_icon"), "2")
            
            //MARK: Guest
        } else if recentActivityData?.activity_id == "3" {
            self.setGuest()
            
            //MARK: Vehcle
        } else if recentActivityData?.activity_id == "5" {
            self.setVehcle()
            
            //MARK: Kid
        } else if recentActivityData?.activity_id == "6" {
            self.Cab_Delivery_visitingHelp_Kid(#imageLiteral(resourceName: "kids"), "6")
            
            //MARK: Visiting Help
        } else if recentActivityData?.activity_id == "7" {
            self.Cab_Delivery_visitingHelp_Kid(#imageLiteral(resourceName: "visiting_help"), "7")
            
            
        } else if recentActivityData?.activity_id == "8" {
            
        } else if recentActivityData?.activity_id == "9" {
            
        } else if recentActivityData?.activity_id == "10" {
            
            
            //MARK: Add Family
        } else if recentActivityData?.activity_id == "11" {
            self.addFamily()
        } else if recentActivityData?.activity_id == "12" {
            self.entryByGate()
        } else if recentActivityData?.activity_id == "13"  {
            self.dailyEntry()
        } else {
            
        }
    }
    
    
    @IBAction func thiredButton(_ sender: UIButton) {
        if let onClickButton = onClickButton {
            onClickButton(sender.titleLabel?.text ?? "",sender.tag)
        }
    }
    
    @IBAction func secondButton(_ sender: UIButton) {
        if let onClickButton = onClickButton {
            onClickButton(sender.titleLabel?.text ?? "",sender.tag)
        }
    }
    
    @IBAction func CallButton(_ sender: UIButton) {
        if let onClickButton = onClickButton {
            onClickButton("call",sender.tag)
        }
        
    }
    
    func Cab_Delivery_visitingHelp_Kid(_ ima:UIImage, _ type:String){
        
        var vehicelNUmber:String {
            return recentActivityData?.vehicle_number?.count ?? 0 > 0 ? "#\(recentActivityData?.vehicle_number ?? "") • " : ""
        }
        var serviceName:String {
            return recentActivityData?.service_name?.count ?? 0 > 0 ? " • \(recentActivityData?.service_name ?? "")" : ""
        }
        
        //code.text = ""
        code.text = recentActivityData?.valid_code?.count ?? 0 > 0 ? "#\(recentActivityData?.valid_code ?? "")" : ""
        
        if let setTime = recentActivityData?.slot_start {
            self.time.text = type == "7" ? "\(getTimeOnly(Int(setTime) ?? 0))\(serviceName)".uppercased() : "\(vehicelNUmber)\(getTimeOnly(Int(setTime) ?? 0))"
        }
        
        if let icon = recentActivityData?.icon , icon.count > 0 {
            profileIcon.sd_setImage(with: URL(string: icon), placeholderImage: ima)
        }else{
            profileIcon.image = ima
        }
        
        if let company_image = recentActivityData?.company_image, let company_name = recentActivityData?.company_name , company_name.count > 0 {
            companyIcon.isHidden = false
            companyname.text = company_name.capitalized
            companyIcon.sd_setImage(with: URL(string: company_image), placeholderImage: ima)
        }else{
            companyIcon.isHidden = true
            companyname.text = ""
        }
        
        if let description = recentActivityData?.description, description.count > 0 {
            messageType.text = description
            messageTypeView.isHidden = false
            msgIcon.image = recentActivityData?.status == "0" ? UIImage(systemName: "person") : UIImage(systemName: "clock")
        }else{
            messageType.text = ""
            messageTypeView.isHidden = true
            msgIcon.image = UIImage(systemName: "clock")
        }
        
        if let timeValid = recentActivityData?.time_valid, timeValid.count > 0 {
            vailidTime.text =  timeValid
            vaildView.isHidden = false
        }else{
            vailidTime.text = ""
            vaildView.isHidden = true
        }
        
        if recentActivityData?.status == "0" {
            firstButton.isHidden = true
            allButtonView.isHidden = true
            vaildView.isHidden = true
        } else if recentActivityData?.status == "2" {
            
            firstButton.isHidden = false
            secondButton.isHidden = false
            thiredButton.isHidden = true
            allButtonView.isHidden = false
            secondButton.setTitle("  Wrong entry", for: .normal)
            secondButton.setImage(UIImage(systemName: "nosign"), for: .normal)
        }else{
            firstButton.isHidden = true
            thiredButton.isHidden = true
            secondButton.isHidden = false
            allButtonView.isHidden = false
            secondButton.setTitle("  cancel", for: .normal)
            secondButton.setImage(UIImage(systemName: "multiply"), for: .normal)
            
        }
    }
    
    
    func addFamily(){
        
        var serviceName:String {
            return recentActivityData?.service_name?.count ?? 0 > 0 ? " • \(recentActivityData?.service_name ?? "")" : ""
        }
        
        code.text =  ""
        vaildView.isHidden = true
        messageTypeView.isHidden = true
        thiredButton.isHidden = true
        if let entry_type = recentActivityData?.entry_type , entry_type == "2" {
            profileIcon.image = #imageLiteral(resourceName: "kids")
        }else{
            if let icon = recentActivityData?.icon , icon.count > 0 {
                profileIcon.sd_setImage(with: URL(string: icon), placeholderImage: #imageLiteral(resourceName: "frequent_guests"))
            }else{
                profileIcon.image = #imageLiteral(resourceName: "frequent_guests")
            }
        }
        if let setTime = recentActivityData?.slot_start  {
            self.time.text = "\(getTimeOnly(Int(setTime) ?? 0))\(serviceName)".uppercased()
        }
        if recentActivityData?.status == "0" {
            firstButton.isHidden = true
            allButtonView.isHidden = true
            messageTypeView.isHidden = true
            vaildView.isHidden = true
        } else if recentActivityData?.status == "2" {
            firstButton.isHidden = true
            allButtonView.isHidden = true
            messageTypeView.isHidden = true
            vaildView.isHidden = true
        }else{
            
            firstButton.isHidden = recentActivityData?.entry_type == "2" ? true:false
            secondButton.isHidden = false
            thiredButton.isHidden = true
            allButtonView.isHidden = false
            secondButton.setTitle("  Invite to ALM Society", for: .normal)
            secondButton.setImage(#imageLiteral(resourceName: "share_sm"), for: .normal)
        }
    }
    
    func setVehcle(){
        code.text =  ""
        vaildView.isHidden = true
        messageTypeView.isHidden = false
        thiredButton.isHidden = true
        msgIcon.image = UIImage(named: "hashtag")
        allButtonView.isHidden = true
        var serviceName:String {
            return recentActivityData?.service_name?.count ?? 0 > 0 ? " • \(recentActivityData?.service_name ?? "")" : ""
        }
        
        if let entry_type = recentActivityData?.entry_type , entry_type == "2" {
            profileIcon.image = #imageLiteral(resourceName: "bike")
        }else{
            profileIcon.image = #imageLiteral(resourceName: "car")
        }
        if let setTime = recentActivityData?.slot_start {
            self.time.text = "\(getTimeOnly(Int(setTime) ?? 0))\(serviceName)".uppercased()
        }
        if let vehicle_number = recentActivityData?.vehicle_number {
            messageType.text = "Vehicle Number: \(vehicle_number)"
        }
    }
    
    
    func setGuest(){
        
        if let setTime = recentActivityData?.slot_start {
            self.time.text = "\(getTimeOnly(Int(setTime) ?? 0))  •"
        }
        code.text = recentActivityData?.valid_code?.count ?? 0 > 0 ? "#\(recentActivityData?.valid_code ?? "")" : ""
        allButtonView.isHidden = false
        thiredButton.isHidden = false
        firstButton.isHidden = true
        secondButton.isHidden = false
        profileIcon.image = #imageLiteral(resourceName: "guest")
        
        if let description = recentActivityData?.description, description.count > 0 {
            messageType.text = description
            messageTypeView.isHidden = false
            msgIcon.image = recentActivityData?.status == "0" ? UIImage(systemName: "person") : UIImage(systemName: "clock")
        }else{
            messageType.text = ""
            messageTypeView.isHidden = true
            msgIcon.image = UIImage(systemName: "clock")
        }
        
        if let timeValid = recentActivityData?.time_valid, timeValid.count > 0 {
            vailidTime.text =  timeValid
            vaildView.isHidden = false
        }else{
            vailidTime.text = ""
            vaildView.isHidden = true
        }
        
        if recentActivityData?.status == "0" {
            firstButton.isHidden = true
            allButtonView.isHidden = true
            vaildView.isHidden = true
        } else if recentActivityData?.status == "2" {
            firstButton.isHidden = true
            allButtonView.isHidden = true
            vaildView.isHidden = true
        }else{
            secondButton.setTitle("  share", for: .normal)
            secondButton.setImage(#imageLiteral(resourceName: "share_sm"), for: .normal)
            thiredButton.setTitle("  cancel", for: .normal)
            thiredButton.setImage(UIImage(systemName: "multiply"), for: .normal)
        }
        
    }
    
    func entryByGate(){
        vaildView.isHidden = true
        
        self.title.text = recentActivityData?.service_name?.count ?? 0 > 0 ? recentActivityData?.service_name?.capitalized : " "
        
        if let setTime = recentActivityData?.slot_start {
            self.time.text = "\(getTimeOnly(Int(setTime) ?? 0))"
        }
        
        if let company_image = recentActivityData?.company_image, let company_name = recentActivityData?.company_name , company_name.count > 0 {
            companyIcon.isHidden = false
            companyname.text = company_name.capitalized
            companyIcon.sd_setImage(with: URL(string: company_image), placeholderImage:#imageLiteral(resourceName: "guest"))
        }else{
            companyIcon.isHidden = true
            companyname.text = ""
        }
        
        if let timeValid = recentActivityData?.description, timeValid.count > 0 {
            vailidTime.text =  timeValid
            vaildView.isHidden = false
        }else{
            vailidTime.text = ""
            vaildView.isHidden = true
        }
        
        if let title = recentActivityData?.title, title.count > 0 {
            messageType.text = title.capitalized
            messageTypeView.isHidden = false
            msgIcon.contentMode = .scaleAspectFill
            msgIcon.cornerRadius = 2
            if let icon = recentActivityData?.icon , icon.count > 0 {
                msgIcon.sd_setImage(with: URL(string: icon), placeholderImage: UIImage(systemName: "person.circle.fill"))
            }else{
                msgIcon.image = UIImage(systemName: "person.circle.fill")
            }
        }else{
            companyIcon.contentMode = .scaleAspectFit
            messageType.text = ""
            messageTypeView.isHidden = true
            msgIcon.image = UIImage(systemName: "clock")
        }
        
        profileIcon.image = setImages(recentActivityData?.entry_type ?? "")
        secondButton.isHidden = false
        firstButton.isHidden = false
        
        if recentActivityData?.accept_flag == "0" {
            allButtonView.isHidden = false
            thiredButton.isHidden = false
            secondButton.setTitle("  Accept", for: .normal)
            secondButton.setImage(UIImage(systemName: "checkmark"), for: .normal)
            thiredButton.setTitle("  Decline", for: .normal)
            thiredButton.setImage(UIImage(systemName: "multiply"), for: .normal)
        } else if recentActivityData?.accept_flag == "1" {
            allButtonView.isHidden = false
            thiredButton.isHidden = true
            secondButton.setTitle("  Wrong entry", for: .normal)
            secondButton.setImage(UIImage(systemName: "nosign"), for: .normal)
        } else if recentActivityData?.accept_flag == "2" {
            allButtonView.isHidden = true
        } else if recentActivityData?.accept_flag == "3" {
            allButtonView.isHidden = true
        } else if recentActivityData?.accept_flag == "4" {
            allButtonView.isHidden = true
        }else{
            allButtonView.isHidden = true
        }
    }
    
    func dailyEntry() {
        inviteByView.isHidden = true
        vaildView.isHidden = true
        code.text = ""
        
//        if let sts = recentActivityData?.tag?.lowercased(), sts == "left" {
//            self.inviteBy.text = "\(recentActivityData?.title ?? "") has checked OUT from your tower."
//        }else{
//            self.inviteBy.text = "\(recentActivityData?.title ?? "") has checked IN from your tower."
//        }
        if let icon = recentActivityData?.icon {
            profileIcon.sd_setImage(with: URL(string: icon), placeholderImage: setImages(recentActivityData?.other_id ?? ""))
        }
        var vehicelNUmber:String {
            return recentActivityData?.vehicle_number?.count ?? 0 > 0 ? "#\(recentActivityData?.vehicle_number ?? "") • " : ""
        }
        var serviceName:String {
            return recentActivityData?.service_name?.count ?? 0 > 0 ? " • \(recentActivityData?.service_name ?? "")" : ""
        }
        if let setTime = recentActivityData?.slot_start {
            self.time.text = recentActivityData?.other_id == "7" ? "\(getTimeOnly(Int(setTime) ?? 0))\(serviceName)".uppercased() : "\(vehicelNUmber)\(getTimeOnly(Int(setTime) ?? 0))"
        }
        if let description = recentActivityData?.description, description.count > 0 {
            messageType.text = description
            messageTypeView.isHidden = false
            msgIcon.image = recentActivityData?.status == "0" ? UIImage(systemName: "person") : UIImage(systemName: "clock")
        }else{
            messageType.text = ""
            messageTypeView.isHidden = true
            msgIcon.image = UIImage(systemName: "clock")
        }
        if let company_image = recentActivityData?.company_image, let company_name = recentActivityData?.company_name , company_name.count > 0 {
            companyIcon.isHidden = false
            companyname.text = company_name.capitalized
            companyIcon.sd_setImage(with: URL(string: company_image), placeholderImage:#imageLiteral(resourceName: "guest"))
        }else{
            companyIcon.isHidden = true
            companyname.text = ""
        }
        if recentActivityData?.mobile?.count ?? 0 > 0 {
            allButtonView.isHidden = false
            firstButton.isHidden = false
            secondButton.isHidden = true
            thiredButton.isHidden = true
        }else{
            allButtonView.isHidden = true
            firstButton.isHidden =  true
            secondButton.isHidden = true
            thiredButton.isHidden = true
        }
    }
}


func setImages(_ entryType:String)-> UIImage {
    if entryType == "1" {
        return #imageLiteral(resourceName: "cab_popup")
    } else if entryType == "2" {
        return #imageLiteral(resourceName: "delivery_icon")
    } else if entryType == "3" {
        return #imageLiteral(resourceName: "frequent_guests")
    } else if entryType == "4" {
        return #imageLiteral(resourceName: "visiting_popup")
    } else {
        return #imageLiteral(resourceName: "issue_gatepass")
    }
    
}

func statusColor(_ text:String)-> UIColor {
    
    if  text.contains("create") ||
                text.contains("pre") ||
                text.contains("edit") ||
                text.contains("invite") ||
                text.contains("allow exit") {
        return #colorLiteral(red: 0.368627451, green: 0.6823529412, blue: 0.9058823529, alpha: 1)
        
    } else if text.contains("added") ||
                text.contains("inside") {
        return #colorLiteral(red: 0, green: 0.4470588235, blue: 0.07058823529, alpha: 1)
        
    } else if text.contains("cancel") ||
                    text.contains("expire") ||
                    text.contains("disable") ||
                    text.contains("exit") ||
                    text.contains("reject") ||
                    text.contains("remove"){
        return #colorLiteral(red: 0.9297258258, green: 0.2434281707, blue: 0.2003417313, alpha: 1)
        
    } else if text.contains("left") {
        return .lightGray
        
    }else{
        return #colorLiteral(red: 0.8549019608, green: 0.6078431373, blue: 0.02352941176, alpha: 1)
    }
}

