//  RoundedButton.swift
//  Society Management
//  Created by ROOP KISHOR on 06/04/2021.


import UIKit

class RoundedButton: UIButton {
    
    override func layoutSubviews() {
        
            super.layoutSubviews()
            gradientLayer.frame = bounds
            self.backgroundColor = UIColor.clear
            self.setTitleColor(UIColor.white, for: .normal)
       
        }

        private lazy var gradientLayer: CAGradientLayer = {
            let l = CAGradientLayer()
            l.frame = self.bounds
            
            let color1 = DEFAULT_COLOR_RED.cgColor
            let color2 = DEFAULT_COLOR_ORANGE.cgColor
            
            l.colors = [color1, color2]
            l.startPoint = CGPoint(x: 0, y: 0.5)
            l.endPoint = CGPoint(x: 1, y: 0.5)
            l.cornerRadius = 4
            layer.insertSublayer(l, at: 0)
            return l
            
        }()
    
    
}
