//
//  CornerView.swift
//  Society Management
//
//  Created by ROOP KISHOR on 25/04/2021.
//

import UIKit

class CornerView: UIView {

    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
    }

}
