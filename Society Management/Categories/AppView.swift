//  AppView.swift
//  Society Management
//  Created by ROOP KISHOR on 06/04/2021.


import UIKit

class AppView: UIView {

    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        self.clipsToBounds = true
        self.layer.cornerRadius = 25
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        self.backgroundColor = UIColor.white
    }
    
    override func layoutSubviews()
    {
        
        super.layoutSubviews()
        let gradient = CAGradientLayer()
        gradient.frame = self.frame
        gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        let color1 = DEFAULT_COLOR_RED.cgColor
        let color2 = DEFAULT_COLOR_ORANGE.cgColor
        gradient.colors = [color1,color2]
        self.layer.insertSublayer(gradient, at: 0)
       // print(self.window!.frame.size.height)
    }

}
