//  AppConstant.swift
//  Created by ROOP KISHOR on 06/04/2021.


import Foundation
import UIKit

// MARK: - Structure

//let appConfig = Config()
//let load = Loader()

typealias  JSON = [String:Any]?

// MARK: - Add Static Key values
let language = "1"

let deviceType: String = "2"


//var deviceToken :String {
//    return UserDefaults.standard.string(forKey: "device_token") ?? "1234567890"
//}


var deviceToken :String {
    return UserDefaults.standard.string(forKey: "FCMtoken") ?? "1234567890"
}
var deviceId:String {
    return getUUID() ?? "0987654321"
}

var userData:LoginData? {
    if let user = UserDefaults.standard.codableData(LoginModel.self, forKey: "userDeatils")?.data {
        return user
    }else{
        return nil
    }
}

var isConnectResidence:String? {
    if let isConnet = UserDefaults.standard.string(forKey: "isConnectResidence") {
        return isConnet
    }else{
        return userData?.connect_residence
    }
}

let userAgent = "!S0ciEty2021@!"

var AuthorizationID: String {
    let userID = "!S0ciEty2021@!"
    let password = "!S0ciEty2021@!"
    let access = userID + ":" + password
    if let auth = access.data(using: .utf8)?.base64EncodedString() {
        return auth
    }else{
        return ""
    }
}




func convertTimeStampToDate(timeStamp:Double)->String
{
    let date = Date(timeIntervalSince1970:timeStamp)
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
    dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
    dateFormatter.timeZone = .current
    let localDate = dateFormatter.string(from: date)
    return localDate
    
}


func convertTimeStampTo(timeStamp:Double)->String {
    let date = Date(timeIntervalSince1970:timeStamp)
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd MMM"
    dateFormatter.timeZone = .current
    let localDate = dateFormatter.string(from: date)
    return localDate.uppercased()
    
}

func getTimeOnly(_ timeStamp:Int)->String {
    let date = Date(timeIntervalSince1970:TimeInterval(timeStamp))
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a"
   // dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
    dateFormatter.locale = NSLocale.current
    let localDate = dateFormatter.string(from: date)
    return localDate.uppercased()
    
}

func getTimedate(_ timeStamp:Int)->String {
    let date = Date(timeIntervalSince1970:TimeInterval(timeStamp))
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "h:mm a, dd MMM"
   // dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
    dateFormatter.locale = NSLocale.current
    let localDate = dateFormatter.string(from: date)
    return localDate.uppercased()
    
}

func getCurrentDateAndTimeOnly()->String {
    let date = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
    dateFormatter.timeZone = .current
    let localDate = dateFormatter.string(from: date)
    return localDate.uppercased()
    
}

func TimeStampToDate(timeStamp:Int)->String
{
    let date = Date(timeIntervalSince1970:TimeInterval(timeStamp))
    let dateFormatter = DateFormatter()
    //dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
    //dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
    dateFormatter.dateFormat = "dd MMM yyyy"
    dateFormatter.timeZone = .current
    let localDate = dateFormatter.string(from: date)
    return localDate.uppercased()
    
}

func selectValidDate(days:Int)-> (String, String, String) {
    
    let today = Date()
    let nextDate = Calendar.current.date(byAdding: .day, value: days, to: today)
    let formate1 = today.getFormattedDate(format: "dd MMM")
    let formate2 = nextDate?.getFormattedDate(format: "dd MMM")
    // return "\(formate1) - \(formate2 ?? "")"
    let sDate = today.getFormattedDate(format: "yyyy-MM-dd")
    let eDate = nextDate?.getFormattedDate(format: "yyyy-MM-dd")
    
    
    return ("\(formate1) - \(formate2 ?? "")",sDate,eDate!)
    
}


extension Date {
    func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}

var TYPE_OWNER        = "1";
var TYPE_TENANT       = "2";
var USER_TYPE         = "1"
var USER_ID           = ""

var DEFAULT_COLOR_RED = UIColor(red: 254.0/255.0, green: 68.0/255.0, blue: 82.0/255.0, alpha: 1.0)
var DEFAULT_COLOR_ORANGE = UIColor(red: 254.0/255.0, green: 88.0/255.0, blue: 50.0/255.0, alpha: 1.0)

let screenRect = UIScreen.main.bounds
let screenWidth = screenRect.size.width
let screenHeight = screenRect.size.height
let HeaderHeight = screenRect.size.height/7



struct Placeholder
{
    static let iMuemail = "Email"
    static let iMupassword = "Password"
    static let iMuConfirmPassword = "Confirm Password"
}


struct APIKeys {
    
    static let iMufullname                = "fullname"
    
    
}

struct ServiceName {
    
    static let iMuSignup            = "driver/driver_signup"
    static let iMuVerifyOtp         = "driver/verify_otp"
    static let iMuResendOtp         = "driver/resend_otp"
    static let iMuLoginIn           = "driver/driver_signin"
    static let iMuResetPassword     = "driver/reset_password"
    static let check_social       = "driver/isSocialActivated"
    static let social_signup      = "driver/social_signup"
    static let driver_profile     = "driver/driver_profile"
    static let vehicle_profile    = "driver/vehicle_profile"
    static let iMudriverDocument    = "driver/driver_document"
    static let iMuaccountDetail     = "driver/driver_accountDetail"
    
}




struct NumberContants {
    
    static let iMuMinPasswordLength = 8
    
}

struct key {
    
    static let iMuModelIdentifier = "Model"
    
}


struct  AlertMessage {
    
    static let alMEmailEmpity                                 = "Please enter email id."
    static let alMValidEmail                                  = "Please enter valid email id."
    static let alEmptyMobile                                  = "Please enter mobile number."
    static let alMValidMobile                                 = "Please enter valid mobile number."
    static let alMEmptyPassword                               = "Please enter password."
    static let alMValidPassword                               = "Password should be atleast 6 character."
    static let alMEmptyFname                                  = "Please enter first name."
    static let alMEmptyLname                                  = "Please enter last name."
    static let alMEmptyCity                                   = "Please select city."
    static let alMEmptyTower                                  = "Please select building name."
    static let alMEmptyUnit                                   = "Please select unit number."
    static let alMEmptyOwnersship                             = "Please Upload unit ownership document."
    static let alMEmptyTenant                                 = "Please Upload tenancy contract document."
    static let alMEmptyEmiratesID                             = "Please upload emirates id."
    static let alMEmptyConfirmPassword                        = "Please enter confirm password."
    static let alMInvalidConfirmPass                          = "Confirm password should be atleast 6 character."
    static let alMPasswordNotMatch                            = "Password & confirm password should be same."
    static let alSelectCat                                    = "Please select complaint category."
    static let alNoteBlank                                    = "Please enter description."
    static let selectRating                                   = "Kindly give rating."
    static let discussionTitle                                = "Please enter title."
    static let discussionDetail                               = "Please enter description."
    static let discussionGruop                                = "Kindly tag the group."
    static let communicationApproved                          = "Please wait till admin approved your communication group."
    static let contactApprove                                 = "Your emergency contact person didn't give approval yet. Kindly contact them."
    static let selectIssue                                    = "Kindly select alert category."
    static let alDateBlank                                    = "Please select date."
    static let alTimeBlank                                    = "Please select time."
    static let alCompanyBlank                                 = "Please choose company name."
    static let alValidity                                     = "Kindly select Validity."
    static let alDays                                         = "Kindly select days."
    static let alStime                                        = "Please choose start time."
    static let alEtime                                        = "Please choose end time."
    static let docType                                        = "Kindly select document type."
    static let expDate                                        = "Kindly select expiry date."
    static let documentNumber                                 = "Kindly enter document number."
    static let documentImage                                  = "Kindly select document image."
    static let currentPassBlank                               = "Please enter current password."
    static let newPassBlank                                   = "Please enter new password."
    static let messageToGurad                                 = "Please enter message."
    static let alMacceptTerms                                 = "Please accept terms and condition."
    static let enterComment                                   = "Please enter comment."
    static let alSDate                                        = "Please select start date."
    static let alEDate                                        = "Please select end date."
    static let selectGuest                                    = "Select atleast one guest."
    static let guestName                                      = "Please enter guest name."
    static let commentPlaceHolder                             = "Write a comment..."
    static let vehicleName                                    = "Please enter your vehicle name."
    static let vehicleNumber                                  = "Please enter your vehicle number."
    static let alMEmptyVerifcation                            = "Please enter verification code."
    static let alMNoInternet                                  = "Please check your internet connection!"
    static let alVehicleBlank                                 = "Please enter vehicle number."
    static let messagePlaceHolder                             = "Write a message to send the guard..."
    static let validOtp                                       = "Please enter valid otp."
    static let selectContact                                  = "Select atleast one contact."
    static let childName                                      = "Please enter your child name."
    static let enterAmount                                    = "Please enter amount."
    static let guestPrivacy                                   = "For the privacy of your guest, we are not taking their mobile number"
    static let minimumImage                                   = "You can add maximum 4 images"
    static let somethingWrong                                 = "Someting is wrong..."
    static let OK                                             = "OK"
    static let cancel                                             = "Cancel"
    static let maxNumber                                      = "Exceed Maximum Number Of Selection"
    static let writeTitle                                     = "write title"
    static let writeDetails                                   = "write details"
    static let deleteConfirm                                  = "Are you sure you want to delete"
    static let selectTimeSlot                                 = "Kindly select time slot."
   
   // Controllers Titles
    static let title1                                          = "Notifications"
    static let title2                                          = "My Bookings"
    static let title3                                          = "Add Flat"
    static let title4                                          = "Meetings"
    static let title5                                          = "Reminder"
    static let title6                                          = "Set Manual Reminder"
    static let title7                                          = "Update Reminder"
    static let title8                                          = "Survey"
    static let title9                                          = "Settings"
    static let title10                                         = "Change Password"
    static let title11                                         = "App Notification Settings"
    static let title12                                         = "Select Date & Time"
    static let title13                                         = "Activities"
    static let title14                                         = "Facilities"
    static let title15                                         = "Entry/Exit Logs"
    static let title16                                         = "Management Committee"
    static let title17                                         = "Help Desk"
    static let title18                                         = "Raise Complaint"
    static let title19                                         = "Complaints Details"
    static let title20                                         = "Notice Board"
    static let title21                                         = "Select Guest Invite"
    static let title22                                         = "Add New"
    static let title23                                         = "Security Alert List"
    static let title24                                         = "My Visits"
    static let title25                                         = "Pic Contacts to add family"
    static let title26                                         = "Pic Contacts to Invite"
    static let title27                                         = "Add New Service"
    static let title28                                         = "Payment Details"
    static let title29                                         = "Payments"
    static let title30                                         = "Emergency Contacts"
    static let title31                                         = "Reviews"
    static let title32                                         = "View Invoice"
    static let title33                                         = "Transaction History"
    static let checkOut                                        = "has checked out from your tower."
    static let checkIn                                        = "has checked in to your tower."
    static let Inside                                         = "inside"
    static let left                                           = "left"

}



struct AlertTitle {
    
    static let iMuOk      = "Okay"
    static let iMuCancel  = "Cancel"
    static let iMuDone    = "Done"
    
}




struct APIUrl
{
    // static let iMuBaseUrl = "http://3.18.213.80:4000/"
    
}


struct OtherConstant {
    
    static let iMuAppDelegate        = UIApplication.shared.delegate as? AppDelegate
    static let iMuBundleID           = Bundle.main.bundleIdentifier!
    static let iMuGenders: [String]  = ["Male", "Female"]
    static let iMuReviewsSortBy: [String] = ["Recent", "Last Month", "Last Year"]
    
}

func Localised(_ aString:String) -> String {
    
    return NSLocalizedString(aString, comment: aString)
    
}


func ShowAlert(_ message: String, on title: String, from sender: UIViewController) -> Void
{
    DispatchQueue.main.async {
        let actionSheetController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction: UIAlertAction = UIAlertAction(title: NSLocalizedString(AlertMessage.OK, comment: ""), style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        sender.present(actionSheetController, animated: true, completion: nil)
    }
    
}


enum PhotoSource2 {
    case library
    case camera
    
}

enum MessageType {
    case photo
    case text
    case video
    case audio
}



/// Creates a new unique user identifier or retrieves the last one created
func getUUID() -> String? {
    
    // create a keychain helper instance
    let keychain = KeychainAccess()
    
    // this is the key we'll use to store the uuid in the keychain
    let uuidKey = "com.myorg.myappid.unique_uuid"
    
    // check if we already have a uuid stored, if so return it
    if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey), uuid.count > 0 {
        return uuid
    }
    
    // generate a new id
    guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
        return nil
    }
    
    // store new identifier in keychain
    try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)
    
    // return new id
    return newId
}

class KeychainAccess {
    
    func addKeychainData(itemKey: String, itemValue: String) throws {
        guard let valueData = itemValue.data(using: .utf8) else {
            print("Keychain: Unable to store data, invalid input - key: \(itemKey), value: \(itemValue)")
            return
        }
        
        //delete old value if stored first
        do {
            try deleteKeychainData(itemKey: itemKey)
        } catch {
            print("Keychain: nothing to delete...")
        }
        
        let queryAdd: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecValueData as String: valueData as AnyObject,
            kSecAttrAccessible as String: kSecAttrAccessibleWhenUnlocked
        ]
        let resultCode: OSStatus = SecItemAdd(queryAdd as CFDictionary, nil)
        
        if resultCode != 0 {
            print("Keychain: value not added - Error: \(resultCode)")
        } else {
            print("Keychain: value added successfully")
        }
    }
    
    func deleteKeychainData(itemKey: String) throws {
        let queryDelete: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject
        ]
        
        let resultCodeDelete = SecItemDelete(queryDelete as CFDictionary)
        
        if resultCodeDelete != 0 {
            print("Keychain: unable to delete from keychain: \(resultCodeDelete)")
        } else {
            print("Keychain: successfully deleted item")
        }
    }
    
    func queryKeychainData (itemKey: String) throws -> String? {
        let queryLoad: [String: AnyObject] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrAccount as String: itemKey as AnyObject,
            kSecReturnData as String: kCFBooleanTrue,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        var result: AnyObject?
        let resultCodeLoad = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(queryLoad as CFDictionary, UnsafeMutablePointer($0))
        }
        
        if resultCodeLoad != 0 {
            print("Keychain: unable to load data - \(resultCodeLoad)")
            return nil
        }
        
        guard let resultVal = result as? NSData, let keyValue = NSString(data: resultVal as Data, encoding: String.Encoding.utf8.rawValue) as String? else {
            print("Keychain: error parsing keychain result - \(resultCodeLoad)")
            return nil
        }
        return keyValue
    }
}


public enum Model : String {
    
    //Simulator
    case simulator     = "simulator/sandbox",
         
         //iPod
         iPod1              = "iPod 1",
         iPod2              = "iPod 2",
         iPod3              = "iPod 3",
         iPod4              = "iPod 4",
         iPod5              = "iPod 5",
         iPod6              = "iPod 6",
         iPod7              = "iPod 7",
         
         //iPad
         iPad2              = "iPad 2",
         iPad3              = "iPad 3",
         iPad4              = "iPad 4",
         iPadAir            = "iPad Air ",
         iPadAir2           = "iPad Air 2",
         iPadAir3           = "iPad Air 3",
         iPadAir4           = "iPad Air 4",
         iPad5              = "iPad 5", //iPad 2017
         iPad6              = "iPad 6", //iPad 2018
         iPad7              = "iPad 7", //iPad 2019
         iPad8              = "iPad 8", //iPad 2020
         
         //iPad Mini
         iPadMini           = "iPad Mini",
         iPadMini2          = "iPad Mini 2",
         iPadMini3          = "iPad Mini 3",
         iPadMini4          = "iPad Mini 4",
         iPadMini5          = "iPad Mini 5",
         
         //iPad Pro
         iPadPro9_7         = "iPad Pro 9.7\"",
         iPadPro10_5        = "iPad Pro 10.5\"",
         iPadPro11          = "iPad Pro 11\"",
         iPadPro2_11        = "iPad Pro 11\" 2nd gen",
         iPadPro3_11        = "iPad Pro 11\" 3nd gen",
         iPadPro12_9        = "iPad Pro 12.9\"",
         iPadPro2_12_9      = "iPad Pro 2 12.9\"",
         iPadPro3_12_9      = "iPad Pro 3 12.9\"",
         iPadPro4_12_9      = "iPad Pro 4 12.9\"",
         iPadPro5_12_9      = "iPad Pro 5 12.9\"",
         
         //iPhone
         iPhone4            = "iPhone 4",
         iPhone4S           = "iPhone 4S",
         iPhone5            = "iPhone 5",
         iPhone5S           = "iPhone 5S",
         iPhone5C           = "iPhone 5C",
         iPhone6            = "iPhone 6",
         iPhone6Plus        = "iPhone 6 Plus",
         iPhone6S           = "iPhone 6S",
         iPhone6SPlus       = "iPhone 6S Plus",
         iPhoneSE           = "iPhone SE",
         iPhone7            = "iPhone 7",
         iPhone7Plus        = "iPhone 7 Plus",
         iPhone8            = "iPhone 8",
         iPhone8Plus        = "iPhone 8 Plus",
         iPhoneX            = "iPhone X",
         iPhoneXS           = "iPhone XS",
         iPhoneXSMax        = "iPhone XS Max",
         iPhoneXR           = "iPhone XR",
         iPhone11           = "iPhone 11",
         iPhone11Pro        = "iPhone 11 Pro",
         iPhone11ProMax     = "iPhone 11 Pro Max",
         iPhoneSE2          = "iPhone SE 2nd gen",
         iPhone12Mini       = "iPhone 12 Mini",
         iPhone12           = "iPhone 12",
         iPhone12Pro        = "iPhone 12 Pro",
         iPhone12ProMax     = "iPhone 12 Pro Max",
         
         // Apple Watch
         AppleWatch1         = "Apple Watch 1gen",
         AppleWatchS1        = "Apple Watch Series 1",
         AppleWatchS2        = "Apple Watch Series 2",
         AppleWatchS3        = "Apple Watch Series 3",
         AppleWatchS4        = "Apple Watch Series 4",
         AppleWatchS5        = "Apple Watch Series 5",
         AppleWatchSE        = "Apple Watch Special Edition",
         AppleWatchS6        = "Apple Watch Series 6",
         
         //Apple TV
         AppleTV1           = "Apple TV 1gen",
         AppleTV2           = "Apple TV 2gen",
         AppleTV3           = "Apple TV 3gen",
         AppleTV4           = "Apple TV 4gen",
         AppleTV_4K         = "Apple TV 4K",
         AppleTV2_4K        = "Apple TV 4K 2gen",
         
         unrecognized       = "?unrecognized?"
}

// #-#-#-#-#-#-#-#-#-#-#-#-#
// MARK: UIDevice extensions
// #-#-#-#-#-#-#-#-#-#-#-#-#

public extension UIDevice {
    
    var type: Model {
        var systemInfo = utsname()
        uname(&systemInfo)
        let modelCode = withUnsafePointer(to: &systemInfo.machine) {
            $0.withMemoryRebound(to: CChar.self, capacity: 1) {
                ptr in String.init(validatingUTF8: ptr)
            }
        }
        
        let modelMap : [String: Model] = [
            
            //Simulator
            "i386"      : .simulator,
            "x86_64"    : .simulator,
            
            //iPod
            "iPod1,1"   : .iPod1,
            "iPod2,1"   : .iPod2,
            "iPod3,1"   : .iPod3,
            "iPod4,1"   : .iPod4,
            "iPod5,1"   : .iPod5,
            "iPod7,1"   : .iPod6,
            "iPod9,1"   : .iPod7,
            
            //iPad
            "iPad2,1"   : .iPad2,
            "iPad2,2"   : .iPad2,
            "iPad2,3"   : .iPad2,
            "iPad2,4"   : .iPad2,
            "iPad3,1"   : .iPad3,
            "iPad3,2"   : .iPad3,
            "iPad3,3"   : .iPad3,
            "iPad3,4"   : .iPad4,
            "iPad3,5"   : .iPad4,
            "iPad3,6"   : .iPad4,
            "iPad6,11"  : .iPad5, //iPad 2017
            "iPad6,12"  : .iPad5,
            "iPad7,5"   : .iPad6, //iPad 2018
            "iPad7,6"   : .iPad6,
            "iPad7,11"  : .iPad7, //iPad 2019
            "iPad7,12"  : .iPad7,
            "iPad11,6"  : .iPad8, //iPad 2020
            "iPad11,7"  : .iPad8,
            
            //iPad Mini
            "iPad2,5"   : .iPadMini,
            "iPad2,6"   : .iPadMini,
            "iPad2,7"   : .iPadMini,
            "iPad4,4"   : .iPadMini2,
            "iPad4,5"   : .iPadMini2,
            "iPad4,6"   : .iPadMini2,
            "iPad4,7"   : .iPadMini3,
            "iPad4,8"   : .iPadMini3,
            "iPad4,9"   : .iPadMini3,
            "iPad5,1"   : .iPadMini4,
            "iPad5,2"   : .iPadMini4,
            "iPad11,1"  : .iPadMini5,
            "iPad11,2"  : .iPadMini5,
            
            //iPad Pro
            "iPad6,3"   : .iPadPro9_7,
            "iPad6,4"   : .iPadPro9_7,
            "iPad7,3"   : .iPadPro10_5,
            "iPad7,4"   : .iPadPro10_5,
            "iPad6,7"   : .iPadPro12_9,
            "iPad6,8"   : .iPadPro12_9,
            "iPad7,1"   : .iPadPro2_12_9,
            "iPad7,2"   : .iPadPro2_12_9,
            "iPad8,1"   : .iPadPro11,
            "iPad8,2"   : .iPadPro11,
            "iPad8,3"   : .iPadPro11,
            "iPad8,4"   : .iPadPro11,
            "iPad8,9"   : .iPadPro2_11,
            "iPad8,10"  : .iPadPro2_11,
            "iPad13,4"  : .iPadPro3_11,
            "iPad13,5"  : .iPadPro3_11,
            "iPad13,6"  : .iPadPro3_11,
            "iPad13,7"  : .iPadPro3_11,
            "iPad8,5"   : .iPadPro3_12_9,
            "iPad8,6"   : .iPadPro3_12_9,
            "iPad8,7"   : .iPadPro3_12_9,
            "iPad8,8"   : .iPadPro3_12_9,
            "iPad8,11"  : .iPadPro4_12_9,
            "iPad8,12"  : .iPadPro4_12_9,
            "iPad13,8"  : .iPadPro5_12_9,
            "iPad13,9"  : .iPadPro5_12_9,
            "iPad13,10" : .iPadPro5_12_9,
            "iPad13,11" : .iPadPro5_12_9,
            
            //iPad Air
            "iPad4,1"   : .iPadAir,
            "iPad4,2"   : .iPadAir,
            "iPad4,3"   : .iPadAir,
            "iPad5,3"   : .iPadAir2,
            "iPad5,4"   : .iPadAir2,
            "iPad11,3"  : .iPadAir3,
            "iPad11,4"  : .iPadAir3,
            "iPad13,1"  : .iPadAir4,
            "iPad13,2"  : .iPadAir4,
            
            
            //iPhone
            "iPhone3,1" : .iPhone4,
            "iPhone3,2" : .iPhone4,
            "iPhone3,3" : .iPhone4,
            "iPhone4,1" : .iPhone4S,
            "iPhone5,1" : .iPhone5,
            "iPhone5,2" : .iPhone5,
            "iPhone5,3" : .iPhone5C,
            "iPhone5,4" : .iPhone5C,
            "iPhone6,1" : .iPhone5S,
            "iPhone6,2" : .iPhone5S,
            "iPhone7,1" : .iPhone6Plus,
            "iPhone7,2" : .iPhone6,
            "iPhone8,1" : .iPhone6S,
            "iPhone8,2" : .iPhone6SPlus,
            "iPhone8,4" : .iPhoneSE,
            "iPhone9,1" : .iPhone7,
            "iPhone9,3" : .iPhone7,
            "iPhone9,2" : .iPhone7Plus,
            "iPhone9,4" : .iPhone7Plus,
            "iPhone10,1" : .iPhone8,
            "iPhone10,4" : .iPhone8,
            "iPhone10,2" : .iPhone8Plus,
            "iPhone10,5" : .iPhone8Plus,
            "iPhone10,3" : .iPhoneX,
            "iPhone10,6" : .iPhoneX,
            "iPhone11,2" : .iPhoneXS,
            "iPhone11,4" : .iPhoneXSMax,
            "iPhone11,6" : .iPhoneXSMax,
            "iPhone11,8" : .iPhoneXR,
            "iPhone12,1" : .iPhone11,
            "iPhone12,3" : .iPhone11Pro,
            "iPhone12,5" : .iPhone11ProMax,
            "iPhone12,8" : .iPhoneSE2,
            "iPhone13,1" : .iPhone12Mini,
            "iPhone13,2" : .iPhone12,
            "iPhone13,3" : .iPhone12Pro,
            "iPhone13,4" : .iPhone12ProMax,
            
            // Apple Watch
            "Watch1,1" : .AppleWatch1,
            "Watch1,2" : .AppleWatch1,
            "Watch2,6" : .AppleWatchS1,
            "Watch2,7" : .AppleWatchS1,
            "Watch2,3" : .AppleWatchS2,
            "Watch2,4" : .AppleWatchS2,
            "Watch3,1" : .AppleWatchS3,
            "Watch3,2" : .AppleWatchS3,
            "Watch3,3" : .AppleWatchS3,
            "Watch3,4" : .AppleWatchS3,
            "Watch4,1" : .AppleWatchS4,
            "Watch4,2" : .AppleWatchS4,
            "Watch4,3" : .AppleWatchS4,
            "Watch4,4" : .AppleWatchS4,
            "Watch5,1" : .AppleWatchS5,
            "Watch5,2" : .AppleWatchS5,
            "Watch5,3" : .AppleWatchS5,
            "Watch5,4" : .AppleWatchS5,
            "Watch5,9" : .AppleWatchSE,
            "Watch5,10" : .AppleWatchSE,
            "Watch5,11" : .AppleWatchSE,
            "Watch5,12" : .AppleWatchSE,
            "Watch6,1" : .AppleWatchS6,
            "Watch6,2" : .AppleWatchS6,
            "Watch6,3" : .AppleWatchS6,
            "Watch6,4" : .AppleWatchS6,
            
            //Apple TV
            "AppleTV1,1" : .AppleTV1,
            "AppleTV2,1" : .AppleTV2,
            "AppleTV3,1" : .AppleTV3,
            "AppleTV3,2" : .AppleTV3,
            "AppleTV5,3" : .AppleTV4,
            "AppleTV6,2" : .AppleTV_4K,
            "AppleTV11,1" : .AppleTV2_4K
        ]
        
        if let model = modelMap[String.init(validatingUTF8: modelCode!)!] {
            if model == .simulator {
                if let simModelCode = ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] {
                    if let simModel = modelMap[String.init(validatingUTF8: simModelCode)!] {
                        return simModel
                    }
                }
            }
            return model
        }
        return Model.unrecognized
    }
}


func timeElapsed(date: Date) -> String {
    
    let date1:Date = date
    let date2: Date = Date() // Same you did before with timeNow variable
    
    let calender:Calendar = Calendar.current
    let components: DateComponents = calender.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date1, to: date2)
    // print(components)
    var returnString:String = ""
    
    print(components.second!)
    
    if components.second! < 60 {
        returnString = "Just Now"
    }
    
    if components.minute! > 1{
        returnString = String(describing: components.minute!) + " minutes ago"
    }
    else if components.minute! == 1 {
        
        returnString = "A minute ago"
    }
    
    if components.hour! > 1{
        returnString = String(describing: components.hour!) + " hours ago"
    }
    else if components.hour == 1 {
        
        returnString = "An hour ago"
    }
    
    if components.day! > 1{
        returnString = String(describing: components.day!) + " days ago"
    }
    else if components.day! == 1 {
        
        returnString = "Yesterday"
    }
    
    if components.month! > 1{
        returnString = String(describing: components.month!)+" months ago"
    }
    else if components.month! == 1 {
        
        returnString = "A month ago"
    }
    
    if components.year! > 1 {
        returnString = String(describing: components.year!)+" years ago"
    }
    else if components.year! == 1 {
        
        returnString = "A year ago"
    }
    
    return returnString
}
