//  Navigationbar.swift
//  Society Gate keeper
//  Created by Jitendra Yadav on 07/04/21.


import UIKit

class Navigationbar : UIView {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var MainView: UIView!
    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var btnOption: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var completionHandler: (() -> Void)?
    
    var vc = UIViewController()
    
    var isAnimated = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async {
            self.MainView.roundCorners([.bottomLeft, .bottomRight], radius: 20)
        }
        
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("Navigationbar", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        containerView.backgroundColor = .clear
        if let userflat = userData?.society {
            self.lbltitle.text = userflat
        }
    }
    
    @IBAction func BackBtn(_ sender: UIButton) {
        vc.navigationController?.popViewController(animated: isAnimated)
    }
    
    @IBAction func SearchBtn(_ sender: UIButton) {
        completionHandler?()
    }
}
