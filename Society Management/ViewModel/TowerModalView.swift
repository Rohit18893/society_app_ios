//  TowerModalView.swift
//  Society Management
//  Created by ROOP KISHOR on 18/04/2021.


import UIKit

class TowerModalView {
    
    var towerData:towerModal?

    func getAllTowers(params:[String:Any]) {
        APIClient().PostdataRequest(WithApiName: API.tower.rawValue, Params: params, isNeedLoader: true, objectType: towerModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.towerData = object
                    }
                case .failure(let error):
                    
                    debugPrint(error)
                }
            }else{
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }
}
