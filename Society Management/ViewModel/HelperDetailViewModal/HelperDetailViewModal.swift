//
//  HelperDetailViewModal.swift
//  Society Management
//
//  Created by Jitendra Yadav on 09/06/21.
//

import Foundation

class HelperDetailViewModal {
    weak var vc:DailyHelperDetailView?
    var helperModel:HelperModal?
    
    func getData(helperID:String,uid:String,lang_id:String,isNeedLoader:Bool){
        let param = ["id":helperID,"uid":uid,"lang_id":lang_id]
        PrintLog(param)
        APIClient().PostdataRequest(WithApiName: API.peopledetails.rawValue, Params: param, isNeedLoader: isNeedLoader, objectType: HelperModal.self) { Response, status in
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        self.helperModel = result
                        self.vc?.sectionData = 3
                        self.vc?.tblView.reloadData()
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
    
}
