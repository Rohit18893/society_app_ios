//
//  RecentActivityViewModel.swift
//  Society Management
//
//  Created by Cricket Mazza on 11/08/21.
//

import Foundation

class RecentActivityViewModel {
    
    var visitorInfoModel:VisitorInfoModel?
    var recentActivityModel:RecentActivityModel?
    
    func featchRecentActivity(_ isNeedLoader:Bool,_ params:[String:String], callback: @escaping (Bool) -> Void) {
        APIClient().PostdataRequest(WithApiName: API.recentActivity.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: RecentActivityModel.self) { [weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        self.recentActivityModel = object
                        if self.recentActivityModel?.recent_activity?.count ?? 0 > 0 {
                            callback(true)
                        }else{
                            callback(false)
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                    callback(false)
                }
            }else{
                callback(false)
            }
        }
    }
    
    func fetchVisitorInfo(_ isNeedLoader:Bool,_ params:[String:String], callback: @escaping (Bool) -> Void) {
        APIClient().PostdataRequest(WithApiName: API.visitorinfo.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: VisitorInfoModel.self) { [weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        self.visitorInfoModel = object
                        if self.recentActivityModel?.recent_activity?.count ?? 0 > 0 {
                            callback(true)
                        }else{
                            callback(false)
                        }
                    }
                case .failure(let error):
                    debugPrint(error)
                    callback(false)
                }
            }else{
                callback(false)
            }
        }
    }
    
}
