//
//  UnitModalView.swift
//  Society Management
//
//  Created by ROOP KISHOR on 18/04/2021.
//

import UIKit

class UnitModalView {
    
    var unitData:UnitModal?

    func getAllUnits(params:[String:Any]) {
        APIClient().PostdataRequest(WithApiName: API.unitnumber.rawValue, Params: params, isNeedLoader: true, objectType: UnitModal.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        
                        self.unitData = object
                    }
                case .failure(let error):
                    
                    debugPrint(error)
                }
            }else{
                
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }

}
