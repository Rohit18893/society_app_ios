//  HouseholdViewModel.swift
//  Society Management
//  Created by Jitendra Yadav on 31/05/21.


import UIKit

class HouseholdViewModel {

    weak var vc: houseHoldVC?
    var householdModel:HouseholdModel?
    
    func getData(uid:String,lang_id:String,isNeedLoader:Bool){
        let param = ["uid":uid,"lang_id":lang_id,"towerid":userData?.towerid ?? "","unitnumber":userData?.unitnumber ?? ""]
        PrintLog(param)
        APIClient().PostdataRequest(WithApiName: API.householdlist.rawValue, Params: param, isNeedLoader: isNeedLoader, objectType: HouseholdModel.self) { Response, status in
            switch Response {
            case .success(let result):
                switch status.statusCode {
                case 200:
                    DispatchQueue.main.async {
                        self.householdModel = result
                        self.vc?.addRaw = 1
                        self.vc?.tblView.reloadData()
                    }
                default:
                    PrintLog(result.msg)
                }
            case .failure(let err):
                PrintLog(err.localizedDescription)
            }
        }
    }
}
