//
//  CommunicationViewModal.swift
//  Society Management
//
//  Created by Jitendra Yadav on 27/06/21.
//

import UIKit

class CommunicationViewModal {
    
    weak var vc:ComunicationVC?
    weak var hideVC:HideCommunicationVC?
    
    var communicationFilter: CommunicaltionFilterModel?
    var communicationModel: CommunicationModel?
    var communcationDetailsModel: CommuncationDetailsModel?
    
    //MARK: List Filter Type API
    func getFilter(){
        let params = ["towerid":userData?.towerid ?? "","lang_id":language]
        APIClient().PostdataRequest(WithApiName: API.communicationtype.rawValue, Params: params, isNeedLoader: false, objectType: CommunicaltionFilterModel.self) {response, statuscode in
            switch response {
            case .success(let result):
                self.communicationFilter = result
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    //MARK: List Communications API
    
//    {
//    "uid":"5",
//    "towerid":"1",
//    "unitnumber":"15",
//    "communication_id":"1",
//    "comment":"test",
//    "lang_id":"1",
//    "file":[],
//    "comment_id":"",
//    "reply":""
//    }
    
    func getCommunicationList(isNeedLoader:Bool,createBy:String){
        let params = ["uid":userData?.id ?? "",
                      "lang_id":language,
                      "towerid":userData?.towerid ?? "",
                      "unitnumber":userData?.unitnumber ?? "",
                      "create_by_me":createBy
        ]
        PrintLog(params)
        APIClient().PostdataRequest(WithApiName: API.discussionList.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: CommunicationModel.self) {response, statuscode in
            switch response {
            case .success(let result):
                switch statuscode.statusCode {
                case 200:
                    
                    DispatchQueue.main.async {
                        if result.result?.count ?? 0  > 0{
                            self.vc?.noResultFound.isHidden = true
                            self.communicationModel = result
                           // self.vc?.tblView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                            self.vc?.tblView.reloadData()
                        }else{
                            self.vc?.noResultFound.isHidden = false
                        }
                    }
                default:
                    DispatchQueue.main.async {
                        self.communicationModel = result
                        self.vc?.tblView.reloadData()
                        self.vc?.noResultFound.isHidden = false
                    }
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    //MARK: Hide Communication API
    func HideCommunication(isNeedLoader:Bool,cid:String){
        let params = ["uid":userData?.id ?? "",
                      "lang_id":language,
                      "communication_id":cid
        ]
        APIClient().PostdataRequest(WithApiName: API.communicationHide.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: StatusModel.self) {response, statuscode in
            switch response {
            case .success(let result):
                DispatchQueue.main.async {
                    self.getCommunicationList(isNeedLoader: false, createBy: "")
                    print(result.msg ?? "")
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    //MARK: Show Communication API
    
    func ShowCommunication(isNeedLoader:Bool,cid:String){
        let params = ["uid":userData?.id ?? "",
                      "lang_id":language,
                      "communication_id":cid
        ]
        APIClient().PostdataRequest(WithApiName: API.communicationShow.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: StatusModel.self) {response, statuscode in
            switch response {
            case .success(let result):
                DispatchQueue.main.async {
                    self.getHideCommunicationList(isNeedLoader: false, createBy: "")
                    print(result.msg ?? "")
                    NotificationCenter.default.post(name: .communicationRefrash, object: nil)
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    //MARK: List Hides Communication API
    
    func getHideCommunicationList(isNeedLoader:Bool,createBy:String){
        let params = ["uid":userData?.id ?? "",
                      "lang_id":language,
                      "towerid":userData?.towerid ?? "",
                      "unitnumber":userData?.unitnumber ?? "",
                      "create_by_me":createBy
        ]
        PrintLog(params)
        APIClient().PostdataRequest(WithApiName: API.communicationHideList.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: CommunicationModel.self) {response, statuscode in
            switch response {
            case .success(let result):
                switch statuscode.statusCode {
                case 209:
                    DispatchQueue.main.async {
                        self.communicationModel = result
                        self.hideVC?.tblView.reloadData()
                        self.hideVC?.noFoundHideCommunication.isHidden = false
                    }
                default:
                    DispatchQueue.main.async {
                        if result.result?.count ?? 0  > 0{
                            self.communicationModel = result
                            self.hideVC?.tblView.reloadData()
                            self.hideVC?.noFoundHideCommunication.isHidden = true
                        }else{
                            self.hideVC?.noFoundHideCommunication.isHidden = false
                        }
                    }
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    //MARK: Show Discussion Detail API
    
    func GetDiscussuionDerails(isNeedLoader:Bool,cid:String, completion: @escaping () -> Void) {
        let params = ["lang_id":language,
                      "communication_id":cid,
                      "uid":userData?.id ?? ""
        ]
        APIClient().PostdataRequest(WithApiName: API.DiscussionDetails.rawValue, Params: params, isNeedLoader: isNeedLoader, objectType: CommuncationDetailsModel.self) {response, statuscode in
            switch response {
            case .success(let result):
                DispatchQueue.main.async {
                    self.communcationDetailsModel = result
                    completion()
                }
            case .failure(let err):
                PrintLog(err)
            }
        }
    }
    
    
}

