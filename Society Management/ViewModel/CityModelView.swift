//  CityModelView.swift
//  Society Management
//  Created by Jitendra Yadav on 18/04/21.


import Foundation

class CityModelView {

    var cityData:cityModel?

    func getAllCities() {
        let params = ["":""]
        APIClient().PostdataRequest(WithApiName: API.city.rawValue, Params: params, isNeedLoader: true, objectType: cityModel.self) {[weak self](response, statuscode) in
            guard let self = self else {return}
            if statuscode.statusCode == 200 {
                switch response {
                case .success(let object):
                    DispatchQueue.main.async {
                        self.cityData = object
                    }
                case .failure(let error):
                    debugPrint(error)
                }
            }else{
//                if let WT = windowT?.rootViewController {
//                    ShowAlert("no Data", on: "Opps?", from: WT)
//                }
            }
        }

    }
}
