//  AppDelegate.swift
//  Society Management
//  Created by ROOP KISHOR on 05/04/2021.


import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseMessaging
import UserNotifications
import Messages

@main

class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        do {
            
            try Network.reachability = Reachability(hostname: "www.google.com")
        }
        catch {
            
            switch error as? Network.Error {
            case let .failedToCreateWith(hostname)?:
                print("Network error:\nFailed to create reachability object With host named:", hostname)
            case let .failedToInitializeWith(address)?:
                print("Network error:\nFailed to initialize reachability object With address:", address)
            case .failedToSetCallout?:
                print("Network error:\nFailed to set callout")
            case .failedToSetDispatchQueue?:
                print("Network error:\nFailed to set DispatchQueue")
            case .none:
                print(error)
                
            }
            
        }
        
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        window = UIWindow(frame: UIScreen.main.bounds)
        RegisterRemoteNotification(application: application)
        Messaging.messaging().delegate = self
        
//        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
//        (granted, error) in
//        guard granted else { return }
//
//           DispatchQueue.main.async
//           {
//              UIApplication.shared.registerForRemoteNotifications()
//           }
//
//        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
        
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>)
    {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    

}


extension AppDelegate {
    
    func RegisterRemoteNotification(application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        UserDefaults.standard.set(token, forKey: "FCMtoken")
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
       // print("Firebase registration token: \(fcmToken ?? "")")
        UserDefaults.standard.set(fcmToken, forKey: "FCMtoken")
        print("FCM token: \(fcmToken ?? "")")
    }
    
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//        if let messageID = userInfo["gcm.message_id"] {
//            print("Message ID: \(messageID)")
//        }
//    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){
        completionHandler(.sound)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID: \(messageID)")
        }
        
        print("user Info : \(userInfo)")
        if let messagetype = userInfo["gcm.notification.messagetype"] as? String, messagetype == "1" {
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            if let topController = keyWindow?.rootViewController {
                let newViewController = AcceptCallVC()
                print(topController)
                newViewController.notificationdata = userInfo
                newViewController.modalPresentationStyle = .overFullScreen
                newViewController.modalTransitionStyle = .crossDissolve
                topController.present(newViewController, animated: true, completion: nil)
            }
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    
    
    
    func extractTokenFromData(deviceToken:Data) -> String {
        
        let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        return token.uppercased();
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if let userInfo = response.notification.request.content.userInfo as? [String:Any] {
          
            print("user Info : \(userInfo)")
           // print("alert: \(alert)")
            
            if let messagetype = userInfo["messagetype"] as? String, messagetype == "1" {
                let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                if let topController = keyWindow?.rootViewController {
                    let newViewController = AcceptCallVC()
                    print(topController)
                    newViewController.notificationdata = userInfo
                    newViewController.modalPresentationStyle = .overFullScreen
                    newViewController.modalTransitionStyle = .crossDissolve
                    topController.present(newViewController, animated: true, completion: nil)
                }
            }
            
            }else{
                
            }
        completionHandler()
    }
    
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        switch application.applicationState {
        case .active:
            print(userInfo)
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            if let topController = keyWindow?.rootViewController {
                let newViewController = AcceptCallVC()
                print(topController)
                newViewController.modalPresentationStyle = .overFullScreen
                newViewController.modalTransitionStyle = .crossDissolve
                topController.present(newViewController, animated: true, completion: nil)
            }
            print("active")
            
            break
        case .inactive:
            print("inactive")
            
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            if let topController = keyWindow?.rootViewController {
                let newViewController = AcceptCallVC()
                print(topController)
                newViewController.modalPresentationStyle = .overFullScreen
                newViewController.modalTransitionStyle = .crossDissolve
                topController.present(newViewController, animated: true, completion: nil)
            }
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            
            break
        case .background:
            print("background")
            let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            if let topController = keyWindow?.rootViewController {
                let newViewController = AcceptCallVC()
                print(topController)
                newViewController.modalPresentationStyle = .overFullScreen
                newViewController.modalTransitionStyle = .crossDissolve
                topController.present(newViewController, animated: true, completion: nil)
            }
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            break
        default:
            break
        }
    }
}
