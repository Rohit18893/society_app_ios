//  NoInterNet.swift
//  Society Management
//  Created by Jitendra Yadav on 19/08/21.


import UIKit

class NoInterNet: UIViewController {

    var done:(()->())? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.className(ClassString: self)
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(blurEffectView)
        self.view.sendSubviewToBack(blurEffectView)

    }

    @IBAction func okButton(_ sender: Any) {
        self.dismiss(animated: true, completion: { [weak self] in
            if let ind = self?.done {
                ind()
            }
        })
    }
   
}
